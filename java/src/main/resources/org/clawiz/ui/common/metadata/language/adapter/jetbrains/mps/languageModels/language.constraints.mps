<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:743c048e-1904-4c81-ac4e-6e787dc50205(org.clawiz.ui.common.language.constraints)">
  <persistence version="9" />
  <languages>
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="xnn4" ref="r:6345ee36-9c34-4137-9c25-34b515eead44(org.clawiz.ui.common.language.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="1147467115080" name="jetbrains.mps.lang.constraints.structure.NodePropertyConstraint" flags="ng" index="EnEH3">
        <reference id="1147467295099" name="applicableProperty" index="EomxK" />
        <child id="1147468630220" name="propertyGetter" index="EtsB7" />
      </concept>
      <concept id="1147467790433" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_PropertyGetter" flags="in" index="Eqf_E" />
      <concept id="1147468365020" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_node" flags="nn" index="EsrRn" />
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213098023997" name="property" index="1MhHOB" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="zPu9ZvpRds">
    <property role="3GE5qa" value="layout.component.field" />
    <ref role="1M2myG" to="xnn4:5HBZIwHrtMV" resolve="Field" />
    <node concept="1N5Pfh" id="IcIQHTvV3J" role="1Mr941">
      <ref role="1N5Vy1" to="xnn4:5HBZIwHrtMW" resolve="formField" />
      <node concept="1dDu$B" id="IcIQHTvV3O" role="1N6uqs">
        <ref role="1dDu$A" to="xnn4:5LB6fQzkxSD" resolve="AbstractFormField" />
      </node>
    </node>
    <node concept="EnEH3" id="PMda9gjTeq" role="1MhHOB">
      <ref role="EomxK" to="tpck:h0TrG11" resolve="name" />
      <node concept="Eqf_E" id="PMda9gjTes" role="EtsB7">
        <node concept="3clFbS" id="PMda9gjTet" role="2VODD2">
          <node concept="3cpWs6" id="PMda9gjXfr" role="3cqZAp">
            <node concept="2OqwBi" id="PMda9glvTp" role="3cqZAk">
              <node concept="2OqwBi" id="PMda9gl1nN" role="2Oq$k0">
                <node concept="EsrRn" id="PMda9gl1iw" role="2Oq$k0" />
                <node concept="3TrEf2" id="PMda9gl1PF" role="2OqNvi">
                  <ref role="3Tt5mk" to="xnn4:5HBZIwHrtMW" resolve="formField" />
                </node>
              </node>
              <node concept="3TrcHB" id="PMda9glwnZ" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="PMda9glwuH">
    <property role="3GE5qa" value="view.form.field" />
    <ref role="1M2myG" to="xnn4:5LB6fQzkxSD" resolve="AbstractFormField" />
  </node>
  <node concept="1M2fIO" id="PMda9glQmP">
    <property role="3GE5qa" value="view.form" />
    <ref role="1M2myG" to="xnn4:5HBZIwHrR$_" resolve="Form" />
  </node>
  <node concept="1M2fIO" id="PMda9gmlCa">
    <property role="3GE5qa" value="view.grid.column" />
    <ref role="1M2myG" to="xnn4:PMda9gmlxm" resolve="LocalGridColumn" />
  </node>
  <node concept="1M2fIO" id="4vQtIF0cY9y">
    <property role="3GE5qa" value="view.grid.order" />
    <ref role="1M2myG" to="xnn4:4vQtIF0cY3v" resolve="GridOrderByColumn" />
    <node concept="1N5Pfh" id="4vQtIF0cYav" role="1Mr941">
      <ref role="1N5Vy1" to="xnn4:4vQtIF0cY3w" resolve="column" />
      <node concept="1dDu$B" id="4vQtIF0cYaw" role="1N6uqs">
        <ref role="1dDu$A" to="xnn4:PMda9gl$Dv" resolve="AbstractGridColumn" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="38vNHSqfSaX">
    <property role="3GE5qa" value="view.grid.column" />
    <ref role="1M2myG" to="xnn4:PMda9gl$Dv" resolve="AbstractGridColumn" />
  </node>
  <node concept="1M2fIO" id="3z2gjk2_1tZ">
    <property role="3GE5qa" value="view.grid.column" />
    <ref role="1M2myG" to="xnn4:3z2gjk2$FqC" resolve="TypeFieldGridColumn" />
    <node concept="1N5Pfh" id="3z2gjk2_1Ay" role="1Mr941">
      <ref role="1N5Vy1" to="xnn4:3z2gjk2$FsC" resolve="typeField" />
      <node concept="1dDu$B" id="3z2gjk2_1A$" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
    <node concept="EnEH3" id="4GIolWHBAfu" role="1MhHOB">
      <ref role="EomxK" to="tpck:h0TrG11" resolve="name" />
      <node concept="Eqf_E" id="4GIolWHBAfw" role="EtsB7">
        <node concept="3clFbS" id="4GIolWHBAfx" role="2VODD2">
          <node concept="3cpWs6" id="4GIolWHBTOi" role="3cqZAp">
            <node concept="2OqwBi" id="4GIolWHC5hY" role="3cqZAk">
              <node concept="2OqwBi" id="4GIolWHBUtv" role="2Oq$k0">
                <node concept="EsrRn" id="4GIolWHBU3p" role="2Oq$k0" />
                <node concept="3TrEf2" id="4GIolWHC4Vs" role="2OqNvi">
                  <ref role="3Tt5mk" to="xnn4:3z2gjk2$FsC" resolve="typeField" />
                </node>
              </node>
              <node concept="3TrcHB" id="4GIolWHC5Kw" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="1MuLfuwPV6C">
    <property role="3GE5qa" value="datasource" />
    <ref role="1M2myG" to="xnn4:1MuLfuwP1id" resolve="DataSource" />
  </node>
  <node concept="1M2fIO" id="5$yeFckRlDv">
    <property role="3GE5qa" value="view.form.field" />
    <ref role="1M2myG" to="xnn4:2MJ_23x7NMs" resolve="TypeFieldFormField" />
    <node concept="1N5Pfh" id="5$yeFckRlDH" role="1Mr941">
      <ref role="1N5Vy1" to="xnn4:2MJ_23x7O1y" resolve="typeField" />
      <node concept="1dDu$B" id="5$yeFckRnUK" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
    <node concept="EnEH3" id="5$yeFckRnV2" role="1MhHOB">
      <ref role="EomxK" to="tpck:h0TrG11" resolve="name" />
      <node concept="Eqf_E" id="5$yeFckRnXB" role="EtsB7">
        <node concept="3clFbS" id="5$yeFckRnXC" role="2VODD2">
          <node concept="3cpWs6" id="5$yeFckRoT2" role="3cqZAp">
            <node concept="2OqwBi" id="5$yeFckR$cw" role="3cqZAk">
              <node concept="2OqwBi" id="5$yeFckRpqH" role="2Oq$k0">
                <node concept="EsrRn" id="5$yeFckRp0B" role="2Oq$k0" />
                <node concept="3TrEf2" id="5$yeFckRzSW" role="2OqNvi">
                  <ref role="3Tt5mk" to="xnn4:2MJ_23x7O1y" resolve="typeField" />
                </node>
              </node>
              <node concept="3TrcHB" id="5$yeFckR$Ff" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

