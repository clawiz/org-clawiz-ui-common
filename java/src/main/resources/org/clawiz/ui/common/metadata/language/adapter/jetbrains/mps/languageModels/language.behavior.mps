<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ffaa2d31-3820-456c-bc0e-695d20453062(org.clawiz.ui.common.language.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="8" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" />
    <import index="xnn4" ref="r:6345ee36-9c34-4137-9c25-34b515eead44(org.clawiz.ui.common.language.structure)" implicit="true" />
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="2644386474301421077" name="jetbrains.mps.lang.smodel.structure.LinkIdRefExpression" flags="nn" index="359W_D">
        <reference id="2644386474301421078" name="conceptDeclaration" index="359W_E" />
        <reference id="2644386474301421079" name="linkDeclaration" index="359W_F" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="5vpT9cKWCx">
    <property role="3GE5qa" value="datasource" />
    <ref role="13h7C2" to="xnn4:1MuLfuwP1id" resolve="DataSource" />
    <node concept="13i0hz" id="5J_TEGDjNm6" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5J_TEGDjNm9" role="3clF47">
        <node concept="3clFbH" id="5J_TEGDjRl4" role="3cqZAp" />
        <node concept="3clFbJ" id="5J_TEGDjNoL" role="3cqZAp">
          <node concept="3clFbS" id="5J_TEGDjNoN" role="3clFbx">
            <node concept="3cpWs6" id="5J_TEGDjNQY" role="3cqZAp">
              <node concept="2YIFZM" id="5J_TEGDjNTL" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="2OqwBi" id="5vpT9cKX2r" role="37wK5m">
                  <node concept="13iPFW" id="5J_TEGDjNVB" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5vpT9cKXph" role="2OqNvi">
                    <ref role="3Tt5mk" to="xnn4:1MuLfuwP7oi" resolve="type" />
                  </node>
                </node>
                <node concept="359W_D" id="5J_TEGDjOtG" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmQ8g" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5J_TEGDjNxE" role="3clFbw">
            <node concept="37vLTw" id="5J_TEGDjNpj" role="2Oq$k0">
              <ref role="3cqZAo" node="5J_TEGDjNmM" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5J_TEGDjNLo" role="2OqNvi">
              <node concept="chp4Y" id="5J_TEGDjNNN" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmPQb" resolve="TypeField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cKX$5" role="3cqZAp" />
        <node concept="3cpWs6" id="5J_TEGDjNnE" role="3cqZAp">
          <node concept="10Nm6u" id="5J_TEGDjNo7" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmM" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5J_TEGDjNmN" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmO" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5J_TEGDjNmP" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5J_TEGDjNmQ" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5J_TEGDjNmR" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="5vpT9cKWCy" role="13h7CW">
      <node concept="3clFbS" id="5vpT9cKWCz" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="5vpT9cKXZZ">
    <property role="3GE5qa" value="view.form" />
    <ref role="13h7C2" to="xnn4:5HBZIwHrR$_" resolve="Form" />
    <node concept="13i0hz" id="5vpT9cKY0a" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5vpT9cKY0b" role="3clF47">
        <node concept="3clFbH" id="5vpT9cKY0c" role="3cqZAp" />
        <node concept="3clFbJ" id="5vpT9cKY0d" role="3cqZAp">
          <node concept="3clFbS" id="5vpT9cKY0e" role="3clFbx">
            <node concept="3cpWs6" id="5vpT9cKY0f" role="3cqZAp">
              <node concept="2YIFZM" id="5vpT9cKY0g" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="2OqwBi" id="5vpT9cKYOJ" role="37wK5m">
                  <node concept="2OqwBi" id="5vpT9cKY0h" role="2Oq$k0">
                    <node concept="13iPFW" id="5vpT9cKY0i" role="2Oq$k0" />
                    <node concept="3TrEf2" id="5vpT9cKYuz" role="2OqNvi">
                      <ref role="3Tt5mk" to="xnn4:2MJ_23x7xgk" resolve="dataSource" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="5vpT9cKZ2Y" role="2OqNvi">
                    <ref role="3Tt5mk" to="xnn4:3z2gjk2xRt4" resolve="type" />
                  </node>
                </node>
                <node concept="359W_D" id="5vpT9cKY0k" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmQ8g" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5vpT9cKY0l" role="3clFbw">
            <node concept="37vLTw" id="5vpT9cKY0m" role="2Oq$k0">
              <ref role="3cqZAo" node="5vpT9cKY0s" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5vpT9cKY0n" role="2OqNvi">
              <node concept="chp4Y" id="5vpT9cKY0o" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmPQb" resolve="TypeField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cKY0p" role="3cqZAp" />
        <node concept="3clFbJ" id="5vpT9cKZb0" role="3cqZAp">
          <node concept="3clFbS" id="5vpT9cKZb1" role="3clFbx">
            <node concept="3cpWs6" id="5vpT9cKZb2" role="3cqZAp">
              <node concept="2YIFZM" id="5vpT9cKZb3" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="13iPFW" id="5vpT9cKZXV" role="37wK5m" />
                <node concept="359W_D" id="5vpT9cKZb9" role="37wK5m">
                  <ref role="359W_E" to="xnn4:5HBZIwHrR$_" resolve="Form" />
                  <ref role="359W_F" to="xnn4:5HBZIwHrqnT" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5vpT9cKZba" role="3clFbw">
            <node concept="37vLTw" id="5vpT9cKZbb" role="2Oq$k0">
              <ref role="3cqZAo" node="5vpT9cKY0s" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5vpT9cKZbc" role="2OqNvi">
              <node concept="chp4Y" id="5vpT9cKZmX" role="2Zo12j">
                <ref role="cht4Q" to="xnn4:5LB6fQzkxSD" resolve="AbstractFormField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cKZaG" role="3cqZAp" />
        <node concept="3cpWs6" id="5vpT9cKY0q" role="3cqZAp">
          <node concept="10Nm6u" id="5vpT9cKY0r" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5vpT9cKY0s" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5vpT9cKY0t" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5vpT9cKY0u" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5vpT9cKY0v" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5vpT9cKY0w" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5vpT9cKY0x" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="5vpT9cKY00" role="13h7CW">
      <node concept="3clFbS" id="5vpT9cKY01" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="5vpT9cL0pC">
    <property role="3GE5qa" value="view.grid" />
    <ref role="13h7C2" to="xnn4:5LB6fQzkjSI" resolve="Grid" />
    <node concept="13i0hz" id="5vpT9cL0pN" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5vpT9cL0pO" role="3clF47">
        <node concept="3clFbH" id="5vpT9cL0pP" role="3cqZAp" />
        <node concept="3clFbJ" id="5vpT9cL0pQ" role="3cqZAp">
          <node concept="3clFbS" id="5vpT9cL0pR" role="3clFbx">
            <node concept="3cpWs6" id="5vpT9cL0pS" role="3cqZAp">
              <node concept="2YIFZM" id="5vpT9cL0pT" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="2OqwBi" id="5vpT9cL0pU" role="37wK5m">
                  <node concept="2OqwBi" id="5vpT9cL0pV" role="2Oq$k0">
                    <node concept="13iPFW" id="5vpT9cL0pW" role="2Oq$k0" />
                    <node concept="3TrEf2" id="5vpT9cL0pX" role="2OqNvi">
                      <ref role="3Tt5mk" to="xnn4:3z2gjk2zYl9" resolve="dataSource" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="5vpT9cL0pY" role="2OqNvi">
                    <ref role="3Tt5mk" to="xnn4:3z2gjk2xRt4" resolve="type" />
                  </node>
                </node>
                <node concept="359W_D" id="5vpT9cL0pZ" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmQ8g" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5vpT9cL0q0" role="3clFbw">
            <node concept="37vLTw" id="5vpT9cL0q1" role="2Oq$k0">
              <ref role="3cqZAo" node="5vpT9cL0qi" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5vpT9cL0q2" role="2OqNvi">
              <node concept="chp4Y" id="5vpT9cL0q3" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmPQb" resolve="TypeField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cL0q4" role="3cqZAp" />
        <node concept="3clFbJ" id="5vpT9cL0q5" role="3cqZAp">
          <node concept="3clFbS" id="5vpT9cL0q6" role="3clFbx">
            <node concept="3cpWs6" id="5vpT9cL0q7" role="3cqZAp">
              <node concept="2YIFZM" id="5vpT9cL0q8" role="3cqZAk">
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <node concept="13iPFW" id="5vpT9cL0q9" role="37wK5m" />
                <node concept="359W_D" id="5vpT9cL0qa" role="37wK5m">
                  <ref role="359W_E" to="xnn4:5LB6fQzkjSI" resolve="Grid" />
                  <ref role="359W_F" to="xnn4:PMda9gl$Tx" resolve="columns" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5vpT9cL0qb" role="3clFbw">
            <node concept="37vLTw" id="5vpT9cL0qc" role="2Oq$k0">
              <ref role="3cqZAo" node="5vpT9cL0qi" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5vpT9cL0qd" role="2OqNvi">
              <node concept="chp4Y" id="5vpT9cL0Cw" role="2Zo12j">
                <ref role="cht4Q" to="xnn4:PMda9gl$Dv" resolve="AbstractGridColumn" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5vpT9cL0qf" role="3cqZAp" />
        <node concept="3cpWs6" id="5vpT9cL0qg" role="3cqZAp">
          <node concept="10Nm6u" id="5vpT9cL0qh" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5vpT9cL0qi" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5vpT9cL0qj" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5vpT9cL0qk" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5vpT9cL0ql" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5vpT9cL0qm" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5vpT9cL0qn" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="5vpT9cL0pD" role="13h7CW">
      <node concept="3clFbS" id="5vpT9cL0pE" role="2VODD2" />
    </node>
  </node>
</model>

