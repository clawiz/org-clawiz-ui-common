package org.clawiz.ui.common.language.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.PropertySupport;
import java.util.Iterator;
import jetbrains.mps.internal.collections.runtime.ListSequence;

public class VerticalAlign_PropertySupport extends PropertySupport {
  public boolean canSetValue(String value) {
    if (value == null) {
      return true;
    }
    Iterator<VerticalAlign> constants = ListSequence.fromList(VerticalAlign.getConstants()).iterator();
    while (constants.hasNext()) {
      VerticalAlign constant = constants.next();
      if (value.equals(constant.getName())) {
        return true;
      }
    }
    return false;
  }
  public String toInternalValue(String value) {
    if (value == null) {
      return null;
    }
    Iterator<VerticalAlign> constants = ListSequence.fromList(VerticalAlign.getConstants()).iterator();
    while (constants.hasNext()) {
      VerticalAlign constant = constants.next();
      if (value.equals(constant.getName())) {
        return constant.getValueAsString();
      }
    }
    return null;
  }
  public String fromInternalValue(String value) {
    VerticalAlign constant = VerticalAlign.parseValue(value);
    if (constant != null) {
      return constant.getName();
    }
    return "";
  }
}
