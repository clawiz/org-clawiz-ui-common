<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6345ee36-9c34-4137-9c25-34b515eead44(org.clawiz.ui.common.language.structure)">
  <persistence version="9" />
  <languages>
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978164219" name="jetbrains.mps.lang.structure.structure.EnumerationDataTypeDeclaration" flags="ng" index="AxPO7">
        <property id="1212080844762" name="hasNoDefaultMember" index="PDuV0" />
        <property id="1197591154882" name="memberIdentifierPolicy" index="3lZH7k" />
        <reference id="1083171729157" name="memberDataType" index="M4eZT" />
        <child id="1083172003582" name="member" index="M5hS2" />
      </concept>
      <concept id="1083171877298" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ig" index="M4N5e">
        <property id="1083923523172" name="externalValue" index="1uS6qo" />
        <property id="1083923523171" name="internalValue" index="1uS6qv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6aLBBl0kNBo">
    <property role="TrG5h" value="AbstractMenu" />
    <property role="3GE5qa" value="menu" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="7111639513022282200" />
    <node concept="1TJgyj" id="5HBZIwHqcKO" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="items" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6586513253200350260" />
      <ref role="20lvS9" node="5HBZIwHpU$3" resolve="AbstractMenuItem" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYZ" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aLBBl0kQEu">
    <property role="3GE5qa" value="menu" />
    <property role="TrG5h" value="Menu" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Menu" />
    <property role="EcuMT" value="7111639513022294686" />
    <ref role="1TJDcQ" node="6aLBBl0kNBo" resolve="AbstractMenu" />
  </node>
  <node concept="1TIwiD" id="5HBZIwHpU$3">
    <property role="3GE5qa" value="menu.item" />
    <property role="TrG5h" value="AbstractMenuItem" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="6586513253200275715" />
    <node concept="1TJgyj" id="5HBZIwHqlGn" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="items" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6586513253200386839" />
      <ref role="20lvS9" node="5HBZIwHpU$3" resolve="AbstractMenuItem" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYW" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="5HBZIwHq6uj" role="1TKVEl">
      <property role="TrG5h" value="title" />
      <property role="IQ2nx" value="6586513253200324499" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5HBZIwHqlG9">
    <property role="TrG5h" value="AbstractLayoutComponent" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="layout" />
    <property role="EcuMT" value="6586513253200386825" />
    <node concept="1TJgyi" id="6_ANtdB8LsO" role="1TKVEl">
      <property role="TrG5h" value="horizontalAlign" />
      <property role="IQ2nx" value="7594984129459066676" />
      <ref role="AX2Wp" node="6_ANtdB8w7a" resolve="HorizontalAlign" />
    </node>
    <node concept="1TJgyi" id="6_ANtdB8LsQ" role="1TKVEl">
      <property role="TrG5h" value="verticalAlign" />
      <property role="IQ2nx" value="7594984129459066678" />
      <ref role="AX2Wp" node="6_ANtdB8Lsx" resolve="VerticalAlign" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiQ4" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873220" />
      <property role="TrG5h" value="text" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiPI" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873198" />
      <property role="TrG5h" value="onClickExpression" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiPM" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873202" />
      <property role="TrG5h" value="visibleExpression" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiPR" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873207" />
      <property role="TrG5h" value="enabledExpression" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiPX" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873213" />
      <property role="TrG5h" value="disabledExpression" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYT" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5HBZIwHrpBw">
    <property role="3GE5qa" value="layout.container.row" />
    <property role="TrG5h" value="Row" />
    <property role="34LRSv" value="row" />
    <property role="EcuMT" value="6586513253200665056" />
    <ref role="1TJDcQ" node="5HBZIwHrpBN" resolve="AbstractContainer" />
  </node>
  <node concept="1TIwiD" id="5HBZIwHrpBN">
    <property role="3GE5qa" value="layout.container" />
    <property role="TrG5h" value="AbstractContainer" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="6586513253200665075" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="5HBZIwHrpCI" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="components" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6586513253200665134" />
      <ref role="20lvS9" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    </node>
  </node>
  <node concept="1TIwiD" id="5HBZIwHrpBQ">
    <property role="3GE5qa" value="layout.container.column" />
    <property role="TrG5h" value="Column" />
    <property role="34LRSv" value="column" />
    <property role="EcuMT" value="6586513253200665078" />
    <ref role="1TJDcQ" node="5HBZIwHrpBN" resolve="AbstractContainer" />
  </node>
  <node concept="1TIwiD" id="5HBZIwHrtMV">
    <property role="3GE5qa" value="layout.component.field" />
    <property role="TrG5h" value="Field" />
    <property role="34LRSv" value="field" />
    <property role="EcuMT" value="6586513253200682171" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="5HBZIwHrtMW" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="formField" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="6586513253200682172" />
      <ref role="20lvS9" node="5LB6fQzkxSD" resolve="AbstractFormField" />
    </node>
  </node>
  <node concept="1TIwiD" id="5HBZIwHrR$_">
    <property role="3GE5qa" value="view.form" />
    <property role="TrG5h" value="Form" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Form" />
    <property role="EcuMT" value="6586513253200787749" />
    <node concept="1TJgyi" id="5$yeFckRBNo" role="1TKVEl">
      <property role="TrG5h" value="title" />
      <property role="IQ2nx" value="6422760559407430872" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByZ5" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="6blNiL0bAU" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="2MJ_23x7xgk" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dataSource" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="3219955127529903124" />
      <ref role="20lvS9" node="3z2gjk2xKjw" resolve="AbstractViewDataSource" />
    </node>
    <node concept="1TJgyj" id="5HBZIwHrqnT" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="fields" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6586513253200668153" />
      <ref role="20lvS9" node="5LB6fQzkxSD" resolve="AbstractFormField" />
    </node>
    <node concept="1TJgyj" id="6aK4n5uDiQl" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234873237" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="topToolbar" />
      <ref role="20lvS9" node="6aK4n5uD8Uc" resolve="Toolbar" />
    </node>
    <node concept="1TJgyj" id="6aK4n5uDiQp" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234873241" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="bottomToolbar" />
      <ref role="20lvS9" node="6aK4n5uD8Uc" resolve="Toolbar" />
    </node>
    <node concept="1TJgyj" id="1rqYE7NxXMH" role="1TKVEi">
      <property role="IQ2ns" value="1646904187239783597" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="layoutComponents" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    </node>
  </node>
  <node concept="AxPO7" id="6_ANtdB8w7a">
    <property role="3GE5qa" value="layout.align" />
    <property role="TrG5h" value="HorizontalAlign" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="6_ANtdB8_qO" role="M5hS2">
      <property role="1uS6qv" value="LEFT" />
      <property role="1uS6qo" value="left" />
    </node>
    <node concept="M4N5e" id="6_ANtdB8_qR" role="M5hS2">
      <property role="1uS6qv" value="RIGHT" />
      <property role="1uS6qo" value="right" />
    </node>
    <node concept="M4N5e" id="6_ANtdB8LsI" role="M5hS2">
      <property role="1uS6qv" value="CENTER" />
      <property role="1uS6qo" value="center" />
    </node>
  </node>
  <node concept="AxPO7" id="6_ANtdB8Lsx">
    <property role="3GE5qa" value="layout.align" />
    <property role="TrG5h" value="VerticalAlign" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="6_ANtdB8Lsy" role="M5hS2">
      <property role="1uS6qv" value="TOP" />
      <property role="1uS6qo" value="top" />
    </node>
    <node concept="M4N5e" id="6_ANtdB8Lsz" role="M5hS2">
      <property role="1uS6qv" value="BOTTOM" />
      <property role="1uS6qo" value="bottom" />
    </node>
    <node concept="M4N5e" id="6_ANtdB8LsE" role="M5hS2">
      <property role="1uS6qv" value="CENTER" />
      <property role="1uS6qo" value="center" />
    </node>
  </node>
  <node concept="1TIwiD" id="6_ANtdBaaa_">
    <property role="3GE5qa" value="layout.component.button" />
    <property role="TrG5h" value="AbstractButton" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="7594984129459430053" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="6TvQk6xXxv_" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="action" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="7953314342872946661" />
      <ref role="20lvS9" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
    </node>
  </node>
  <node concept="1TIwiD" id="3l5chnQM8U2">
    <property role="3GE5qa" value="layout.component.button" />
    <property role="TrG5h" value="Button" />
    <property role="34LRSv" value="button" />
    <property role="EcuMT" value="3838528227807628930" />
    <ref role="1TJDcQ" node="6_ANtdBaaa_" resolve="AbstractButton" />
  </node>
  <node concept="1TIwiD" id="27huHj9NkKs">
    <property role="TrG5h" value="Application" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Application" />
    <property role="3GE5qa" value="application" />
    <property role="EcuMT" value="2436863927721479196" />
    <node concept="1TJgyj" id="6blNiKZRg7" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="forms" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="111278499620156423" />
      <ref role="20lvS9" node="6blNiKZEyA" resolve="FormReference" />
    </node>
    <node concept="1TJgyj" id="6blNiKZRga" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="grids" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="111278499620156426" />
      <ref role="20lvS9" node="6blNiKZRfr" resolve="GridReference" />
    </node>
    <node concept="1TJgyj" id="27huHj9Nw93" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="menus" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="2436863927721525827" />
      <ref role="20lvS9" node="27huHj9NvTU" resolve="MenuReference" />
    </node>
    <node concept="1TJgyj" id="6blNiKZRgm" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="screens" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="111278499620156438" />
      <ref role="20lvS9" node="6blNiKZRfL" resolve="ScreenReference" />
    </node>
    <node concept="1TJgyj" id="1MuLfuwU1xq" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dataSources" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="2062302247217797210" />
      <ref role="20lvS9" node="1MuLfuwS0to" resolve="DataSourceReference" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByYQ" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="27huHj9NvTU">
    <property role="3GE5qa" value="menu" />
    <property role="TrG5h" value="MenuReference" />
    <property role="EcuMT" value="2436863927721524858" />
    <node concept="1TJgyj" id="27huHj9Nw8Z" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="menu" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="2436863927721525823" />
      <ref role="20lvS9" node="6aLBBl0kNBo" resolve="AbstractMenu" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LB6fQzkjSI">
    <property role="3GE5qa" value="view.grid" />
    <property role="TrG5h" value="Grid" />
    <property role="34LRSv" value="Grid" />
    <property role="19KtqR" value="true" />
    <property role="EcuMT" value="6658318051736370734" />
    <node concept="1TJgyj" id="5$yeFckSePo" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="editForm" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="6422760559407590744" />
      <ref role="20lvS9" node="5HBZIwHrR$_" resolve="Form" />
    </node>
    <node concept="1TJgyi" id="5gE51u035op" role="1TKVEl">
      <property role="TrG5h" value="title" />
      <property role="IQ2nx" value="6064681939372889625" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByZc" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="6blNiL0bB4" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="3z2gjk2zYl9" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dataSource" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="4089903107537888585" />
      <ref role="20lvS9" node="3z2gjk2xKjw" resolve="AbstractViewDataSource" />
    </node>
    <node concept="1TJgyj" id="PMda9gl$Tx" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="columns" />
      <property role="20lbJX" value="1..n" />
      <property role="IQ2ns" value="968894741575257697" />
      <ref role="20lvS9" node="PMda9gl$Dv" resolve="AbstractGridColumn" />
    </node>
    <node concept="1TJgyj" id="OjKtCFUpsL" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="orderBy" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="942309889834325809" />
      <ref role="20lvS9" node="4vQtIF0cY3v" resolve="GridOrderByColumn" />
    </node>
    <node concept="1TJgyj" id="5gE51u05dAM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6064681939373447602" />
      <ref role="20lvS9" node="5gE51u05dAL" resolve="AbstractGridProperty" />
    </node>
    <node concept="1TJgyj" id="6aK4n5uDiQ_" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234873253" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="topToolbar" />
      <ref role="20lvS9" node="6aK4n5uD8Uc" resolve="Toolbar" />
    </node>
    <node concept="1TJgyj" id="6aK4n5uDiQA" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234873254" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="bottomToolbar" />
      <ref role="20lvS9" node="6aK4n5uD8Uc" resolve="Toolbar" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LB6fQzkxSD">
    <property role="3GE5qa" value="view.form.field" />
    <property role="TrG5h" value="AbstractFormField" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="6658318051736428073" />
    <node concept="PrWs8" id="7YEFpv2ByZ2" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="5LB6fQzkGfD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="6658318051736470505" />
      <ref role="20lvS9" node="5LB6fQzkGaR" resolve="AbstractFormFieldProperty" />
    </node>
    <node concept="1TJgyi" id="6Y5bOk1_yAN" role="1TKVEl">
      <property role="TrG5h" value="label" />
      <property role="IQ2nx" value="8035881083538385331" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiQc" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873228" />
      <property role="TrG5h" value="hint" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LB6fQzkG5M">
    <property role="3GE5qa" value="view.form.field" />
    <property role="TrG5h" value="LocalFormField" />
    <property role="34LRSv" value="local" />
    <property role="EcuMT" value="6658318051736469874" />
    <ref role="1TJDcQ" node="5LB6fQzkxSD" resolve="AbstractFormField" />
    <node concept="1TJgyj" id="5LB6fQzkGaD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="6658318051736470185" />
      <ref role="20lvS9" to="lehn:6porqNrmPx8" resolve="AbstractValueType" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LB6fQzkGaR">
    <property role="3GE5qa" value="view.form.field.property" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractFormFieldProperty" />
    <property role="EcuMT" value="6658318051736470199" />
  </node>
  <node concept="1TIwiD" id="5LB6fQzkGb4">
    <property role="3GE5qa" value="view.form.field.property" />
    <property role="TrG5h" value="HintFormFieldProperty" />
    <property role="R5$K7" value="false" />
    <property role="34LRSv" value="hint" />
    <property role="EcuMT" value="6658318051736470212" />
    <ref role="1TJDcQ" node="5LB6fQzkGaR" resolve="AbstractFormFieldProperty" />
    <node concept="1TJgyi" id="5LB6fQzkGb5" role="1TKVEl">
      <property role="TrG5h" value="hint" />
      <property role="IQ2nx" value="6658318051736470213" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="PMda9gl$Dv">
    <property role="3GE5qa" value="view.grid.column" />
    <property role="TrG5h" value="AbstractGridColumn" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="968894741575256671" />
    <node concept="1TJgyi" id="6Y5bOk1_yM8" role="1TKVEl">
      <property role="TrG5h" value="caption" />
      <property role="IQ2nx" value="8035881083538386056" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="PMda9gmlDT" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="968894741575457401" />
      <ref role="20lvS9" node="PMda9gmlD$" resolve="AbstractGridColumnProperty" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByZ9" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="PMda9gmlxm">
    <property role="3GE5qa" value="view.grid.column" />
    <property role="TrG5h" value="LocalGridColumn" />
    <property role="34LRSv" value="local" />
    <property role="EcuMT" value="968894741575456854" />
    <ref role="1TJDcQ" node="PMda9gl$Dv" resolve="AbstractGridColumn" />
    <node concept="1TJgyj" id="PMda9gmlxn" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="968894741575456855" />
      <ref role="20lvS9" to="lehn:6porqNrmPx8" resolve="AbstractValueType" />
    </node>
  </node>
  <node concept="1TIwiD" id="PMda9gmlD$">
    <property role="3GE5qa" value="view.grid.column.property" />
    <property role="TrG5h" value="AbstractGridColumnProperty" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="968894741575457380" />
  </node>
  <node concept="AxPO7" id="4vQtIF0cXv_">
    <property role="TrG5h" value="GridOrderByDirection" />
    <property role="3GE5qa" value="view.grid.order" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="4vQtIF0cXvA" role="M5hS2">
      <property role="1uS6qv" value="ASC" />
      <property role="1uS6qo" value="asc" />
    </node>
    <node concept="M4N5e" id="4vQtIF0cXvB" role="M5hS2">
      <property role="1uS6qv" value="DESC" />
      <property role="1uS6qo" value="desc" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vQtIF0cY3v">
    <property role="3GE5qa" value="view.grid.order" />
    <property role="TrG5h" value="GridOrderByColumn" />
    <property role="EcuMT" value="5185462771582755039" />
    <node concept="1TJgyj" id="4vQtIF0cY3w" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="column" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="5185462771582755040" />
      <ref role="20lvS9" node="PMda9gl$Dv" resolve="AbstractGridColumn" />
    </node>
    <node concept="1TJgyi" id="4vQtIF0d0D0" role="1TKVEl">
      <property role="TrG5h" value="direction" />
      <property role="IQ2nx" value="5185462771582765632" />
      <ref role="AX2Wp" node="4vQtIF0cXv_" resolve="GridOrderByDirection" />
    </node>
  </node>
  <node concept="1TIwiD" id="2D7LzB77ET6">
    <property role="3GE5qa" value="view.form.field.property" />
    <property role="TrG5h" value="ReadOnlyFormFieldProperty" />
    <property role="R5$K7" value="false" />
    <property role="34LRSv" value="read only" />
    <property role="EcuMT" value="3046621624395542086" />
    <ref role="1TJDcQ" node="5LB6fQzkGaR" resolve="AbstractFormFieldProperty" />
  </node>
  <node concept="1TIwiD" id="6blNiKXqT6">
    <property role="3GE5qa" value="view.screen" />
    <property role="TrG5h" value="Screen" />
    <property role="34LRSv" value="Screen" />
    <property role="19KtqR" value="true" />
    <property role="EcuMT" value="111278499619515974" />
    <node concept="1TJgyj" id="7JHZZtIawm1" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="extendsScreen" />
      <property role="IQ2ns" value="8930075074315814273" />
      <ref role="20lvS9" node="6blNiKXqT6" resolve="Screen" />
    </node>
    <node concept="PrWs8" id="7YEFpv2ByZg" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="6blNiL0bBe" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="6blNiKXECt" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="components" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="111278499619580445" />
      <ref role="20lvS9" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    </node>
  </node>
  <node concept="1TIwiD" id="6blNiKXFiq">
    <property role="3GE5qa" value="layout.component" />
    <property role="TrG5h" value="FormLayoutComponent" />
    <property role="34LRSv" value="form" />
    <property role="EcuMT" value="111278499619583130" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="6blNiKXFir" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="form" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="111278499619583131" />
      <ref role="20lvS9" node="5HBZIwHrR$_" resolve="Form" />
    </node>
  </node>
  <node concept="1TIwiD" id="6blNiKXFy6">
    <property role="3GE5qa" value="layout.component" />
    <property role="TrG5h" value="GridLayoutComponent" />
    <property role="34LRSv" value="grid" />
    <property role="EcuMT" value="111278499619584134" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="6blNiKXFLc" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="grid" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="111278499619585100" />
      <ref role="20lvS9" node="5LB6fQzkjSI" resolve="Grid" />
    </node>
  </node>
  <node concept="1TIwiD" id="6blNiKZEyA">
    <property role="3GE5qa" value="view.form" />
    <property role="TrG5h" value="FormReference" />
    <property role="EcuMT" value="111278499620104358" />
    <node concept="1TJgyj" id="6blNiKZRf1" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="form" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="111278499620156353" />
      <ref role="20lvS9" node="5HBZIwHrR$_" resolve="Form" />
    </node>
  </node>
  <node concept="1TIwiD" id="6blNiKZRfr">
    <property role="3GE5qa" value="view.grid" />
    <property role="TrG5h" value="GridReference" />
    <property role="EcuMT" value="111278499620156379" />
    <node concept="1TJgyj" id="6blNiKZRfs" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="grid" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="111278499620156380" />
      <ref role="20lvS9" node="5LB6fQzkjSI" resolve="Grid" />
    </node>
  </node>
  <node concept="1TIwiD" id="6blNiKZRfL">
    <property role="3GE5qa" value="view.screen" />
    <property role="TrG5h" value="ScreenReference" />
    <property role="EcuMT" value="111278499620156401" />
    <node concept="1TJgyj" id="6blNiKZRfM" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="screen" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="111278499620156402" />
      <ref role="20lvS9" node="6blNiKXqT6" resolve="Screen" />
    </node>
  </node>
  <node concept="1TIwiD" id="3z2gjk2xKjw">
    <property role="TrG5h" value="AbstractViewDataSource" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="view.datasource" />
    <property role="EcuMT" value="4089903107537306848" />
    <node concept="1TJgyj" id="3z2gjk2xRt4" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="4089903107537336132" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
    <node concept="PrWs8" id="3z2gjk2xRt1" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="3z2gjk2xRt3">
    <property role="3GE5qa" value="view.datasource" />
    <property role="TrG5h" value="TypeViewDataSource" />
    <property role="34LRSv" value="type" />
    <property role="EcuMT" value="4089903107537336131" />
    <ref role="1TJDcQ" node="3z2gjk2xKjw" resolve="AbstractViewDataSource" />
  </node>
  <node concept="1TIwiD" id="3z2gjk2zSPc">
    <property role="3GE5qa" value="view.datasource" />
    <property role="TrG5h" value="EmptyViewDataSource" />
    <property role="34LRSv" value="no data source" />
    <property role="EcuMT" value="4089903107537866060" />
    <ref role="1TJDcQ" node="3z2gjk2xKjw" resolve="AbstractViewDataSource" />
  </node>
  <node concept="1TIwiD" id="3z2gjk2$FqC">
    <property role="3GE5qa" value="view.grid.column" />
    <property role="TrG5h" value="TypeFieldGridColumn" />
    <property role="34LRSv" value="field" />
    <property role="EcuMT" value="4089903107538073256" />
    <ref role="1TJDcQ" node="PMda9gl$Dv" resolve="AbstractGridColumn" />
    <node concept="1TJgyj" id="3z2gjk2$FsC" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="typeField" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="4089903107538073384" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
  <node concept="1TIwiD" id="4GIolWHBdZ8">
    <property role="3GE5qa" value="view.grid.column.property" />
    <property role="TrG5h" value="HiddenColumnProperty" />
    <property role="34LRSv" value="hidden" />
    <property role="EcuMT" value="5417374463188328392" />
    <ref role="1TJDcQ" node="PMda9gmlD$" resolve="AbstractGridColumnProperty" />
  </node>
  <node concept="1TIwiD" id="2MJ_23x7NMs">
    <property role="3GE5qa" value="view.form.field" />
    <property role="TrG5h" value="TypeFieldFormField" />
    <property role="34LRSv" value="field" />
    <property role="EcuMT" value="3219955127529979036" />
    <ref role="1TJDcQ" node="5LB6fQzkxSD" resolve="AbstractFormField" />
    <node concept="1TJgyj" id="2MJ_23x7O1y" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="typeField" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="3219955127529980002" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
  <node concept="1TIwiD" id="1MuLfuwP1id">
    <property role="TrG5h" value="DataSource" />
    <property role="34LRSv" value="Data source" />
    <property role="3GE5qa" value="datasource" />
    <property role="19KtqR" value="true" />
    <property role="EcuMT" value="2062302247216485517" />
    <node concept="1TJgyj" id="1MuLfuwSGZb" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="filters" />
      <property role="20lbJX" value="0..n" />
      <property role="IQ2ns" value="2062302247217450955" />
      <ref role="20lvS9" node="1MuLfuwSBXx" resolve="AbstractDataSourceFilter" />
    </node>
    <node concept="1TJgyj" id="1MuLfuwP7oi" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="2062302247216510482" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
    <node concept="PrWs8" id="7YEFpv2B$d3" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="1MuLfuwPV4F" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="1MuLfuwS0to">
    <property role="3GE5qa" value="datasource" />
    <property role="TrG5h" value="DataSourceReference" />
    <property role="EcuMT" value="2062302247217268568" />
    <node concept="1TJgyj" id="1MuLfuwS0Gw" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="dataSource" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="2062302247217269536" />
      <ref role="20lvS9" node="1MuLfuwP1id" resolve="DataSource" />
    </node>
  </node>
  <node concept="1TIwiD" id="1MuLfuwSBXx">
    <property role="3GE5qa" value="datasource.filter" />
    <property role="TrG5h" value="AbstractDataSourceFilter" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="2062302247217430369" />
  </node>
  <node concept="1TIwiD" id="5gE51u05dAL">
    <property role="3GE5qa" value="view.grid.property" />
    <property role="TrG5h" value="AbstractGridProperty" />
    <property role="R5$K7" value="true" />
    <property role="EcuMT" value="6064681939373447601" />
  </node>
  <node concept="1TIwiD" id="5$yeFckSR9U">
    <property role="3GE5qa" value="view.grid.column.property" />
    <property role="TrG5h" value="HorizontalAlignGridColumnProperty" />
    <property role="34LRSv" value="horizontal align" />
    <property role="EcuMT" value="6422760559407755898" />
    <ref role="1TJDcQ" node="PMda9gmlD$" resolve="AbstractGridColumnProperty" />
    <node concept="1TJgyi" id="5$yeFckSR9V" role="1TKVEl">
      <property role="TrG5h" value="align" />
      <property role="IQ2nx" value="6422760559407755899" />
      <ref role="AX2Wp" node="6_ANtdB8w7a" resolve="HorizontalAlign" />
    </node>
  </node>
  <node concept="1TIwiD" id="5$yeFckTehF">
    <property role="3GE5qa" value="view.grid.column.property" />
    <property role="TrG5h" value="VerticalAlignGridColumnProperty" />
    <property role="34LRSv" value="vertical align" />
    <property role="EcuMT" value="6422760559407850603" />
    <ref role="1TJDcQ" node="PMda9gmlD$" resolve="AbstractGridColumnProperty" />
    <node concept="1TJgyi" id="5$yeFckTehG" role="1TKVEl">
      <property role="TrG5h" value="align" />
      <property role="IQ2nx" value="6422760559407850604" />
      <ref role="AX2Wp" node="6_ANtdB8Lsx" resolve="VerticalAlign" />
    </node>
  </node>
  <node concept="1TIwiD" id="3afENqY5Mzw">
    <property role="3GE5qa" value="layout.component.button.action" />
    <property role="TrG5h" value="NoActionButtonAction" />
    <property role="34LRSv" value="no action" />
    <property role="EcuMT" value="3643318875171793120" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="34Qn6O$1_nZ">
    <property role="3GE5qa" value="layout.component.button.action" />
    <property role="TrG5h" value="SubmitButtonAction" />
    <property role="34LRSv" value="submit" />
    <property role="EcuMT" value="3546123380426429951" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="3l5chnQMfPM">
    <property role="3GE5qa" value="layout.component.button.action" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractButtonAction" />
    <property role="EcuMT" value="3838528227807657330" />
  </node>
  <node concept="1TIwiD" id="7CwOiypWVf1">
    <property role="EcuMT" value="8800263644193731521" />
    <property role="3GE5qa" value="view.grid.property" />
    <property role="TrG5h" value="RowsPerPageGridProperty" />
    <property role="34LRSv" value="rows per page" />
    <ref role="1TJDcQ" node="5gE51u05dAL" resolve="AbstractGridProperty" />
    <node concept="1TJgyi" id="7CwOiypWVf2" role="1TKVEl">
      <property role="IQ2nx" value="8800263644193731522" />
      <property role="TrG5h" value="count" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="7CwOiypWVfn">
    <property role="EcuMT" value="8800263644193731543" />
    <property role="3GE5qa" value="view.grid.property" />
    <property role="TrG5h" value="InlineEditGridProperty" />
    <property role="34LRSv" value="inline edit" />
    <ref role="1TJDcQ" node="5gE51u05dAL" resolve="AbstractGridProperty" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8Tt">
    <property role="3GE5qa" value="layout.component.button.action" />
    <property role="TrG5h" value="CancelButtonAction" />
    <property role="34LRSv" value="cancel" />
    <property role="EcuMT" value="7111202990234832477" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8TE">
    <property role="EcuMT" value="7111202990234832490" />
    <property role="3GE5qa" value="layout.container.collection" />
    <property role="TrG5h" value="AbstractCollection" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="5HBZIwHrpBN" resolve="AbstractContainer" />
    <node concept="1TJgyi" id="6aK4n5uDiPy" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873186" />
      <property role="TrG5h" value="direction" />
      <ref role="AX2Wp" node="6aK4n5uDiPj" resolve="CollectionDirection" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8TF">
    <property role="3GE5qa" value="layout.container.panel" />
    <property role="TrG5h" value="Panel" />
    <property role="34LRSv" value="panel" />
    <property role="EcuMT" value="7111202990234832491" />
    <ref role="1TJDcQ" node="5HBZIwHrpBN" resolve="AbstractContainer" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8U3">
    <property role="EcuMT" value="7111202990234832515" />
    <property role="3GE5qa" value="layout.container.collection" />
    <property role="TrG5h" value="IteratorCollection" />
    <property role="34LRSv" value="iterator" />
    <ref role="1TJDcQ" node="6aK4n5uD8TE" resolve="AbstractCollection" />
    <node concept="1TJgyi" id="6aK4n5uDiP$" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873188" />
      <property role="TrG5h" value="iteratorName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6aK4n5uDiPA" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234873190" />
      <property role="TrG5h" value="variableName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8U6">
    <property role="EcuMT" value="7111202990234832518" />
    <property role="3GE5qa" value="layout.component" />
    <property role="TrG5h" value="Text" />
    <property role="34LRSv" value="text" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyi" id="6aK4n5uD8U9" role="1TKVEl">
      <property role="IQ2nx" value="7111202990234832521" />
      <property role="TrG5h" value="expression" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8Uc">
    <property role="EcuMT" value="7111202990234832524" />
    <property role="3GE5qa" value="layout.component.toolbar" />
    <property role="TrG5h" value="Toolbar" />
    <property role="34LRSv" value="toolbar" />
    <ref role="1TJDcQ" node="5HBZIwHqlG9" resolve="AbstractLayoutComponent" />
    <node concept="1TJgyj" id="6aK4n5uD8Ud" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234832525" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="left" />
      <ref role="20lvS9" node="5HBZIwHrpBN" resolve="AbstractContainer" />
    </node>
    <node concept="1TJgyj" id="6aK4n5uD8Uj" role="1TKVEi">
      <property role="IQ2ns" value="7111202990234832531" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="right" />
      <ref role="20lvS9" node="5HBZIwHrpBN" resolve="AbstractContainer" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8Ut">
    <property role="EcuMT" value="7111202990234832541" />
    <property role="3GE5qa" value="view.grid.button.action" />
    <property role="TrG5h" value="CreateRowButtonAction" />
    <property role="34LRSv" value="create grid row" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8Uu">
    <property role="EcuMT" value="7111202990234832542" />
    <property role="3GE5qa" value="view.grid.button.action" />
    <property role="TrG5h" value="DeleteRowButtonAction" />
    <property role="34LRSv" value="delete grid row" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uD8Uv">
    <property role="EcuMT" value="7111202990234832543" />
    <property role="3GE5qa" value="view.grid.button.action" />
    <property role="TrG5h" value="EditRowButtonAction" />
    <property role="34LRSv" value="edit grid row" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="AxPO7" id="6aK4n5uDiPj">
    <property role="3GE5qa" value="layout.container.collection.direction" />
    <property role="TrG5h" value="CollectionDirection" />
    <property role="PDuV0" value="true" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="6aK4n5uDiPk" role="M5hS2">
      <property role="1uS6qv" value="HORIZONTAL" />
      <property role="1uS6qo" value="horizontal" />
    </node>
    <node concept="M4N5e" id="6aK4n5uDiPv" role="M5hS2">
      <property role="1uS6qv" value="VERTICAL" />
      <property role="1uS6qo" value="vertical" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uDiSj">
    <property role="EcuMT" value="7111202990234873363" />
    <property role="3GE5qa" value="view.form.button.action" />
    <property role="TrG5h" value="SaveButtonAction" />
    <property role="34LRSv" value="save" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="6aK4n5uDiSk">
    <property role="EcuMT" value="7111202990234873364" />
    <property role="3GE5qa" value="view.form.button.action" />
    <property role="TrG5h" value="DiscardButtonAction" />
    <property role="34LRSv" value="discard" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
  </node>
  <node concept="1TIwiD" id="7ZBQMKvBeB9">
    <property role="EcuMT" value="9216576144992430537" />
    <property role="3GE5qa" value="layout.component.button.action" />
    <property role="TrG5h" value="ServiceMethodCallButtonAction" />
    <property role="34LRSv" value="service method" />
    <ref role="1TJDcQ" node="3l5chnQMfPM" resolve="AbstractButtonAction" />
    <node concept="1TJgyj" id="7ZBQMKvBeBR" role="1TKVEi">
      <property role="IQ2ns" value="9216576144992430583" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="serviceMethod" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    </node>
  </node>
</model>

