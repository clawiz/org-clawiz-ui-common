<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4bdfaa88-3597-40c6-ae9b-cb56bcbf4803(org.clawiz.ui.common.language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
  </languages>
  <imports>
    <import index="xnn4" ref="r:6345ee36-9c34-4137-9c25-34b515eead44(org.clawiz.ui.common.language.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6aLBBl0kQE$">
    <property role="3GE5qa" value="menu" />
    <ref role="1XX52x" to="xnn4:6aLBBl0kQEu" resolve="Menu" />
    <node concept="3EZMnI" id="6aLBBl0kSu8" role="2wV5jI">
      <node concept="3F0ifn" id="6aLBBl0kSuf" role="3EZMnx">
        <property role="3F0ifm" value="Menu" />
      </node>
      <node concept="3F0A7n" id="6aLBBl0kSul" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="6aLBBl0kSub" role="2iSdaV" />
      <node concept="3F0ifn" id="5HBZIwHqeBk" role="3EZMnx">
        <node concept="pVoyu" id="5HBZIwHqlvI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5HBZIwHqlxr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5HBZIwHqlxA" role="3EZMnx">
        <property role="3F0ifm" value="items" />
        <node concept="pVoyu" id="5HBZIwHqlCi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5HBZIwHqlDZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5HBZIwHqlEU" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHqcKO" resolve="items" />
        <node concept="2EHx9g" id="5HBZIwHqlFo" role="2czzBx" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5HBZIwHrpK8">
    <property role="3GE5qa" value="layout.container.column" />
    <ref role="1XX52x" to="xnn4:5HBZIwHrpBQ" resolve="Column" />
    <node concept="3EZMnI" id="5HBZIwHrpKa" role="2wV5jI">
      <node concept="3F0ifn" id="5HBZIwHrpKh" role="3EZMnx">
        <property role="3F0ifm" value="column" />
      </node>
      <node concept="3F2HdR" id="5HBZIwHrpKn" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHrpCI" resolve="components" />
        <node concept="2EHx9g" id="5HBZIwHrpKy" role="2czzBx" />
        <node concept="VPM3Z" id="5HBZIwHrpKr" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvB7_X" role="3EZMnx">
        <property role="3F0ifm" value=" " />
      </node>
      <node concept="l2Vlx" id="5HBZIwHrpKd" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5HBZIwHrpKJ">
    <property role="3GE5qa" value="layout.container.row" />
    <ref role="1XX52x" to="xnn4:5HBZIwHrpBw" resolve="Row" />
    <node concept="3EZMnI" id="5HBZIwHrpKL" role="2wV5jI">
      <node concept="3F0ifn" id="5HBZIwHrpKS" role="3EZMnx">
        <property role="3F0ifm" value="row" />
      </node>
      <node concept="3F2HdR" id="5HBZIwHrpKY" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHrpCI" resolve="components" />
        <node concept="2iRfu4" id="5HBZIwHrpL1" role="2czzBx" />
        <node concept="VPM3Z" id="5HBZIwHrpL2" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="l2Vlx" id="5HBZIwHrpKO" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5HBZIwHrtN4">
    <property role="3GE5qa" value="layout.component.field" />
    <ref role="1XX52x" to="xnn4:5HBZIwHrtMV" resolve="Field" />
    <node concept="3EZMnI" id="5HBZIwHrtNp" role="2wV5jI">
      <node concept="3F0ifn" id="5HBZIwHrtNz" role="3EZMnx">
        <property role="3F0ifm" value="field" />
      </node>
      <node concept="1iCGBv" id="5HBZIwHrtNG" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHrtMW" resolve="formField" />
        <node concept="1sVBvm" id="5HBZIwHrtNI" role="1sWHZn">
          <node concept="3F0A7n" id="5HBZIwHrtNU" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="5HBZIwHrtNs" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6_ANtdBaaaL">
    <property role="3GE5qa" value="layout.component.button" />
    <ref role="1XX52x" to="xnn4:6_ANtdBaaa_" resolve="AbstractButton" />
    <node concept="3F0A7n" id="3l5chnQM37S" role="2wV5jI">
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
    </node>
  </node>
  <node concept="24kQdi" id="3l5chnQM8U8">
    <property role="3GE5qa" value="layout.component.button" />
    <ref role="1XX52x" to="xnn4:3l5chnQM8U2" resolve="Button" />
    <node concept="3EZMnI" id="3l5chnQM8Ua" role="2wV5jI">
      <node concept="3F0ifn" id="3l5chnQM8Uh" role="3EZMnx">
        <property role="3F0ifm" value="button" />
      </node>
      <node concept="3F0A7n" id="3l5chnQM8Ux" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3EZMnI" id="3l5chnQM8VU" role="3EZMnx">
        <node concept="VPM3Z" id="3l5chnQM8VW" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3F0ifn" id="3l5chnQM8W8" role="3EZMnx">
          <property role="3F0ifm" value="text  " />
        </node>
        <node concept="3F0A7n" id="3l5chnQM8Wg" role="3EZMnx">
          <ref role="1NtTu8" to="xnn4:6aK4n5uDiQ4" resolve="text" />
        </node>
        <node concept="l2Vlx" id="3l5chnQM8VZ" role="2iSdaV" />
        <node concept="3F0ifn" id="3l5chnQM8Wq" role="3EZMnx">
          <property role="3F0ifm" value="action" />
          <node concept="pVoyu" id="3l5chnQMfPJ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="6TvQk6xXxvB" role="3EZMnx">
          <ref role="1NtTu8" to="xnn4:6TvQk6xXxv_" resolve="action" />
        </node>
      </node>
      <node concept="2iRfu4" id="3l5chnQM8Ud" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="27huHj9OY0M">
    <property role="3GE5qa" value="menu" />
    <ref role="1XX52x" to="xnn4:27huHj9NvTU" resolve="MenuReference" />
    <node concept="1iCGBv" id="27huHj9OY0O" role="2wV5jI">
      <ref role="1NtTu8" to="xnn4:27huHj9Nw8Z" resolve="menu" />
      <node concept="1sVBvm" id="27huHj9OY0Q" role="1sWHZn">
        <node concept="3F0A7n" id="27huHj9OY10" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5LB6fQzkyGM">
    <property role="3GE5qa" value="view.form" />
    <ref role="1XX52x" to="xnn4:5HBZIwHrR$_" resolve="Form" />
    <node concept="3EZMnI" id="5LB6fQzkyGO" role="2wV5jI">
      <node concept="3F0ifn" id="5HBZIwHrR_Y" role="3EZMnx">
        <property role="3F0ifm" value="Form" />
      </node>
      <node concept="3F0A7n" id="5HBZIwHrR_Z" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5HBZIwHrRA1" role="3EZMnx">
        <node concept="pVoyu" id="5HBZIwHrRA2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzk9mx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5LB6fQzkyID" role="3EZMnx">
        <property role="3F0ifm" value="title      " />
        <node concept="pVoyu" id="5LB6fQzkyIE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzkyIF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5$yeFckRDSx" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:5$yeFckRBNo" resolve="title" />
      </node>
      <node concept="3F0ifn" id="5$yeFckRC33" role="3EZMnx">
        <property role="3F0ifm" value="dataSource " />
        <node concept="pVoyu" id="5$yeFckRC34" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5$yeFckRC35" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="2MJ_23x7NLK" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:2MJ_23x7xgk" resolve="dataSource" />
      </node>
      <node concept="3F0ifn" id="2MJ_23x7N8Q" role="3EZMnx">
        <node concept="pVoyu" id="2MJ_23x7N8R" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="2MJ_23x7N8S" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5HBZIwHrRA3" role="3EZMnx">
        <property role="3F0ifm" value="fields     " />
        <node concept="pVoyu" id="5HBZIwHrRA4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5HBZIwHrRA5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5HBZIwHrRA6" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHrqnT" resolve="fields" />
        <node concept="2EHx9g" id="5HBZIwHrRA7" role="2czzBx" />
        <node concept="VPM3Z" id="5HBZIwHrRA8" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="5HBZIwHrRA9" role="3EZMnx">
        <node concept="pVoyu" id="5HBZIwHrRAa" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5HBZIwHrRAb" role="3EZMnx">
        <property role="3F0ifm" value="layout     " />
        <node concept="pVoyu" id="5HBZIwHrRAc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5HBZIwHrRAd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5HBZIwHrRAe" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:1rqYE7NxXMH" resolve="layoutComponents" />
        <node concept="2EHx9g" id="5HBZIwHrRAf" role="2czzBx" />
        <node concept="VPM3Z" id="5HBZIwHrRAg" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="l2Vlx" id="5LB6fQzkyGR" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5LB6fQzkGac">
    <property role="3GE5qa" value="view.form.field" />
    <ref role="1XX52x" to="xnn4:5LB6fQzkG5M" resolve="LocalFormField" />
    <node concept="3EZMnI" id="5LB6fQzkGae" role="2wV5jI">
      <node concept="3F0ifn" id="5LB6fQzkGal" role="3EZMnx">
        <property role="3F0ifm" value="local" />
      </node>
      <node concept="3F0A7n" id="5LB6fQzkGat" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F1sOY" id="5LB6fQzkGaK" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5LB6fQzkGaD" resolve="valueType" />
      </node>
      <node concept="3F0ifn" id="6Y5bOk1_yNW" role="3EZMnx">
        <property role="3F0ifm" value=" &quot;" />
      </node>
      <node concept="3F0A7n" id="6Y5bOk1__AA" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:6Y5bOk1_yAN" resolve="label" />
      </node>
      <node concept="3F0ifn" id="6Y5bOk1__Aq" role="3EZMnx">
        <property role="3F0ifm" value="&quot;" />
      </node>
      <node concept="3F2HdR" id="5LB6fQzkGhc" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5LB6fQzkGfD" resolve="properties" />
        <node concept="2EHx9g" id="5LB6fQzkGho" role="2czzBx" />
        <node concept="VPM3Z" id="5LB6fQzkGhg" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="5LB6fQzkGah" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5LB6fQzkGbj">
    <property role="3GE5qa" value="view.form.field.property" />
    <ref role="1XX52x" to="xnn4:5LB6fQzkGb4" resolve="HintFormFieldProperty" />
    <node concept="3EZMnI" id="5LB6fQzkGbx" role="2wV5jI">
      <node concept="3F0ifn" id="5LB6fQzkGby" role="3EZMnx">
        <property role="3F0ifm" value="hint" />
      </node>
      <node concept="3F0A7n" id="5LB6fQzkGbz" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5LB6fQzkGb5" resolve="hint" />
      </node>
      <node concept="2iRfu4" id="5LB6fQzkGb$" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5LB6fQzljis">
    <property role="3GE5qa" value="view.grid" />
    <ref role="1XX52x" to="xnn4:5LB6fQzkjSI" resolve="Grid" />
    <node concept="3EZMnI" id="5LB6fQzljiU" role="2wV5jI">
      <node concept="3F0ifn" id="5LB6fQzljkl" role="3EZMnx">
        <property role="3F0ifm" value="Grid" />
      </node>
      <node concept="3F0A7n" id="5LB6fQzljkm" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5LB6fQzljkn" role="3EZMnx">
        <node concept="pVoyu" id="5LB6fQzljko" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzljkp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5LB6fQzljkq" role="3EZMnx">
        <property role="3F0ifm" value="title       " />
        <node concept="pVoyu" id="5LB6fQzljkr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzljks" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="5gE51u03ibY" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:5gE51u035op" resolve="title" />
      </node>
      <node concept="3F0ifn" id="5gE51u05dBH" role="3EZMnx">
        <property role="3F0ifm" value="data source " />
        <node concept="pVoyu" id="5gE51u05dBI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5gE51u05dBJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="3z2gjk2zYld" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:3z2gjk2zYl9" resolve="dataSource" />
      </node>
      <node concept="3F0ifn" id="5LB6fQzljkw" role="3EZMnx">
        <property role="3F0ifm" value="edit form   " />
        <node concept="pVoyu" id="5LB6fQzljkx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzljky" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="5$yeFckSeTb" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5$yeFckSePo" resolve="editForm" />
        <node concept="1sVBvm" id="5$yeFckSeTd" role="1sWHZn">
          <node concept="3F0A7n" id="5$yeFckSeU4" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <property role="39s7Ar" value="true" />
            <property role="1O74Pk" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5$yeFckSeRo" role="3EZMnx">
        <node concept="pVoyu" id="5$yeFckSeRp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5$yeFckSeRq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5LB6fQzljkz" role="3EZMnx">
        <property role="3F0ifm" value="columns    " />
        <node concept="pVoyu" id="5LB6fQzljk$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5LB6fQzljk_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="PMda9gmabP" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:PMda9gl$Tx" resolve="columns" />
        <node concept="2EHx9g" id="PMda9gmacs" role="2czzBx" />
        <node concept="VPM3Z" id="PMda9gmabT" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="PMda9glBB3" role="3EZMnx">
        <node concept="pVoyu" id="PMda9glBB4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="PMda9glBB5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="PMda9glBAo" role="3EZMnx">
        <property role="3F0ifm" value="order by   " />
        <node concept="pVoyu" id="PMda9glBAp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="PMda9glBAq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="OjKtCFUptG" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:OjKtCFUpsL" resolve="orderBy" />
        <node concept="2iRkQZ" id="OjKtCFUptJ" role="2czzBx" />
        <node concept="VPM3Z" id="OjKtCFUptK" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="PMda9glB_K" role="3EZMnx">
        <node concept="pVoyu" id="PMda9glB_L" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="PMda9glB_M" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="PMda9glB_b" role="3EZMnx">
        <property role="3F0ifm" value="properties " />
        <node concept="pVoyu" id="PMda9glB_c" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="PMda9glB_d" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5gE51u05j_O" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5gE51u05dAM" resolve="properties" />
        <node concept="2EHx9g" id="5gE51u05jPf" role="2czzBx" />
        <node concept="VPM3Z" id="5gE51u05j_S" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="5gE51u05dHc" role="3EZMnx">
        <node concept="pVoyu" id="5gE51u05dHd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5gE51u05dHe" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5LB6fQzljkD" role="3EZMnx">
        <node concept="pVoyu" id="5LB6fQzljkE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="5LB6fQzljiX" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="PMda9gmlCg">
    <property role="3GE5qa" value="view.grid.column" />
    <ref role="1XX52x" to="xnn4:PMda9gmlxm" resolve="LocalGridColumn" />
    <node concept="3EZMnI" id="PMda9gmlCH" role="2wV5jI">
      <node concept="3F0ifn" id="PMda9gmlCI" role="3EZMnx">
        <property role="3F0ifm" value="local" />
      </node>
      <node concept="3F0A7n" id="PMda9gmlCJ" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F1sOY" id="PMda9gmlCK" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:PMda9gmlxn" resolve="valueType" />
      </node>
      <node concept="3F0ifn" id="6Y5bOk1_IOA" role="3EZMnx">
        <property role="3F0ifm" value=" &quot;" />
      </node>
      <node concept="3F0A7n" id="6Y5bOk1_IOB" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:6Y5bOk1_yM8" resolve="caption" />
      </node>
      <node concept="3F0ifn" id="6Y5bOk1_IOC" role="3EZMnx">
        <property role="3F0ifm" value="&quot; " />
      </node>
      <node concept="3F2HdR" id="PMda9gmlCM" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:PMda9gmlDT" resolve="properties" />
        <node concept="2EHx9g" id="PMda9gmlCN" role="2czzBx" />
        <node concept="VPM3Z" id="PMda9gmlCO" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="PMda9gmlCP" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4vQtIF0cYqy">
    <property role="3GE5qa" value="view.grid.order" />
    <ref role="1XX52x" to="xnn4:4vQtIF0cY3v" resolve="GridOrderByColumn" />
    <node concept="3EZMnI" id="4vQtIF0d0p$" role="2wV5jI">
      <node concept="1iCGBv" id="4vQtIF0d0pF" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:4vQtIF0cY3w" resolve="column" />
        <node concept="1sVBvm" id="4vQtIF0d0pH" role="1sWHZn">
          <node concept="3F0A7n" id="4vQtIF0d0pO" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0A7n" id="4vQtIF0d0D8" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:4vQtIF0d0D0" resolve="direction" />
      </node>
      <node concept="2iRfu4" id="4vQtIF0d0pB" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2D7LzB77KzP">
    <property role="3GE5qa" value="view.form.field.property" />
    <ref role="1XX52x" to="xnn4:2D7LzB77ET6" resolve="ReadOnlyFormFieldProperty" />
    <node concept="3EZMnI" id="2D7LzB77PZB" role="2wV5jI">
      <node concept="3F0ifn" id="2D7LzB77PZC" role="3EZMnx">
        <property role="3F0ifm" value="read only" />
      </node>
      <node concept="2iRfu4" id="2D7LzB77PZE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKXxjW">
    <property role="3GE5qa" value="view.screen" />
    <ref role="1XX52x" to="xnn4:6blNiKXqT6" resolve="Screen" />
    <node concept="3EZMnI" id="6blNiKXADJ" role="2wV5jI">
      <node concept="3F0ifn" id="6blNiKXAE3" role="3EZMnx">
        <property role="3F0ifm" value="Screen" />
      </node>
      <node concept="3F0A7n" id="6blNiKXAE9" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="7JHZZtIaAnl" role="3EZMnx">
        <property role="3F0ifm" value="extends" />
      </node>
      <node concept="1iCGBv" id="7JHZZtIaAof" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:7JHZZtIawm1" resolve="extendsScreen" />
        <node concept="1sVBvm" id="7JHZZtIaAoh" role="1sWHZn">
          <node concept="3F0A7n" id="7JHZZtIaAoN" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6blNiKXF84" role="3EZMnx">
        <node concept="lj46D" id="6blNiKXF85" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="6blNiKXF86" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6blNiKXEjM" role="3EZMnx">
        <property role="3F0ifm" value="layout    " />
        <node concept="lj46D" id="6blNiKXEjN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="6blNiKXEjO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6blNiKXEEw" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:6blNiKXECt" resolve="components" />
        <node concept="2EHx9g" id="6blNiKXEEx" role="2czzBx" />
        <node concept="VPM3Z" id="6blNiKXEEy" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="6blNiKXFaN" role="3EZMnx">
        <node concept="lj46D" id="6blNiKXFaO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="6blNiKXFaP" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="6blNiKXADM" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKXFrD">
    <property role="3GE5qa" value="layout.component" />
    <ref role="1XX52x" to="xnn4:6blNiKXFiq" resolve="FormLayoutComponent" />
    <node concept="3EZMnI" id="6blNiKXFrF" role="2wV5jI">
      <node concept="3F0ifn" id="6blNiKXFrM" role="3EZMnx">
        <property role="3F0ifm" value="form" />
      </node>
      <node concept="1iCGBv" id="6blNiKXFrS" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:6blNiKXFir" resolve="form" />
        <node concept="1sVBvm" id="6blNiKXFrU" role="1sWHZn">
          <node concept="3F0A7n" id="6blNiKXFs5" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="6blNiKXFrI" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKXFLj">
    <property role="3GE5qa" value="layout.component" />
    <ref role="1XX52x" to="xnn4:6blNiKXFy6" resolve="GridLayoutComponent" />
    <node concept="3EZMnI" id="6blNiKXFLl" role="2wV5jI">
      <node concept="3F0ifn" id="6blNiKXFLs" role="3EZMnx">
        <property role="3F0ifm" value="grid" />
      </node>
      <node concept="1iCGBv" id="6blNiKXFLy" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:6blNiKXFLc" resolve="grid" />
        <node concept="1sVBvm" id="6blNiKXFL$" role="1sWHZn">
          <node concept="3F0A7n" id="6blNiKXFLG" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="6blNiKXFLo" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKZRfd">
    <property role="3GE5qa" value="view.form" />
    <ref role="1XX52x" to="xnn4:6blNiKZEyA" resolve="FormReference" />
    <node concept="1iCGBv" id="6blNiKZRff" role="2wV5jI">
      <ref role="1NtTu8" to="xnn4:6blNiKZRf1" resolve="form" />
      <node concept="1sVBvm" id="6blNiKZRfh" role="1sWHZn">
        <node concept="3F0A7n" id="6blNiKZRfo" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKZRfz">
    <property role="3GE5qa" value="view.grid" />
    <ref role="1XX52x" to="xnn4:6blNiKZRfr" resolve="GridReference" />
    <node concept="1iCGBv" id="6blNiKZRf_" role="2wV5jI">
      <ref role="1NtTu8" to="xnn4:6blNiKZRfs" resolve="grid" />
      <node concept="1sVBvm" id="6blNiKZRfB" role="1sWHZn">
        <node concept="3F0A7n" id="6blNiKZRfI" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6blNiKZRfT">
    <property role="3GE5qa" value="view.screen" />
    <ref role="1XX52x" to="xnn4:6blNiKZRfL" resolve="ScreenReference" />
    <node concept="1iCGBv" id="6blNiKZRfV" role="2wV5jI">
      <ref role="1NtTu8" to="xnn4:6blNiKZRfM" resolve="screen" />
      <node concept="1sVBvm" id="6blNiKZRfX" role="1sWHZn">
        <node concept="3F0A7n" id="6blNiKZRg4" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3z2gjk2xRtb">
    <property role="3GE5qa" value="view.datasource" />
    <ref role="1XX52x" to="xnn4:3z2gjk2xRt3" resolve="TypeViewDataSource" />
    <node concept="3EZMnI" id="3z2gjk2xTNB" role="2wV5jI">
      <node concept="3F0ifn" id="3z2gjk2xTNI" role="3EZMnx">
        <property role="3F0ifm" value="type" />
      </node>
      <node concept="1iCGBv" id="3z2gjk2xTNO" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:3z2gjk2xRt4" resolve="type" />
        <node concept="1sVBvm" id="3z2gjk2xTNQ" role="1sWHZn">
          <node concept="3F0A7n" id="3z2gjk2xTNY" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="l2Vlx" id="3z2gjk2xTNE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3z2gjk2zSPi">
    <property role="3GE5qa" value="view.datasource" />
    <ref role="1XX52x" to="xnn4:3z2gjk2zSPc" resolve="EmptyViewDataSource" />
    <node concept="3EZMnI" id="3z2gjk2zSPk" role="2wV5jI">
      <node concept="3F0ifn" id="3z2gjk2zSPr" role="3EZMnx">
        <property role="3F0ifm" value="no data source" />
      </node>
      <node concept="l2Vlx" id="3z2gjk2zSPn" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3z2gjk2$FwH">
    <property role="3GE5qa" value="view.grid.column" />
    <ref role="1XX52x" to="xnn4:3z2gjk2$FqC" resolve="TypeFieldGridColumn" />
    <node concept="3EZMnI" id="3z2gjk2$FwJ" role="2wV5jI">
      <node concept="3F0ifn" id="3z2gjk2$FwQ" role="3EZMnx">
        <property role="3F0ifm" value="field" />
      </node>
      <node concept="1iCGBv" id="3z2gjk2$FwW" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:3z2gjk2$FsC" resolve="typeField" />
        <node concept="1sVBvm" id="3z2gjk2$FwY" role="1sWHZn">
          <node concept="3F0A7n" id="3z2gjk2$Fx6" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4GIolWHAHTZ" role="3EZMnx" />
      <node concept="3F0ifn" id="3z2gjk2$Fxh" role="3EZMnx">
        <property role="3F0ifm" value=" &quot;" />
      </node>
      <node concept="3F0A7n" id="3z2gjk2$FxB" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:6Y5bOk1_yM8" resolve="caption" />
      </node>
      <node concept="3F0ifn" id="3z2gjk2$FxR" role="3EZMnx">
        <property role="3F0ifm" value="&quot;" />
      </node>
      <node concept="3F2HdR" id="3z2gjk2$Fyt" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:PMda9gmlDT" resolve="properties" />
        <node concept="2EHx9g" id="5$yeFckTzTv" role="2czzBx" />
        <node concept="VPM3Z" id="3z2gjk2$Fyx" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="3z2gjk2$FwM" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4GIolWHBeel">
    <property role="3GE5qa" value="view.grid.column.property" />
    <ref role="1XX52x" to="xnn4:4GIolWHBdZ8" resolve="HiddenColumnProperty" />
    <node concept="3EZMnI" id="4GIolWHBeeq" role="2wV5jI">
      <node concept="3F0ifn" id="4GIolWHBeex" role="3EZMnx">
        <property role="3F0ifm" value="hidden" />
      </node>
      <node concept="2iRfu4" id="4GIolWHBeet" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2MJ_23x7O21">
    <property role="3GE5qa" value="view.form.field" />
    <ref role="1XX52x" to="xnn4:2MJ_23x7NMs" resolve="TypeFieldFormField" />
    <node concept="3EZMnI" id="2MJ_23x7O23" role="2wV5jI">
      <node concept="3F0ifn" id="2MJ_23x7O2F" role="3EZMnx">
        <property role="3F0ifm" value="field" />
      </node>
      <node concept="1iCGBv" id="2MJ_23x7O2G" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:2MJ_23x7O1y" resolve="typeField" />
        <node concept="1sVBvm" id="2MJ_23x7O2H" role="1sWHZn">
          <node concept="3F0A7n" id="2MJ_23x7O2I" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2MJ_23x7O2J" role="3EZMnx" />
      <node concept="3F0ifn" id="2MJ_23x7O2K" role="3EZMnx">
        <property role="3F0ifm" value=" &quot;" />
      </node>
      <node concept="3F0A7n" id="2MJ_23x7O2L" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="xnn4:6Y5bOk1_yAN" resolve="label" />
      </node>
      <node concept="3F0ifn" id="2MJ_23x7O2M" role="3EZMnx">
        <property role="3F0ifm" value="&quot;" />
      </node>
      <node concept="3F2HdR" id="2MJ_23x7O2N" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5LB6fQzkGfD" resolve="properties" />
        <node concept="2iRkQZ" id="2MJ_23x7O2O" role="2czzBx" />
        <node concept="VPM3Z" id="2MJ_23x7O2P" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="2MJ_23x7O26" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1MuLfuwPK2C">
    <property role="3GE5qa" value="datasource" />
    <ref role="1XX52x" to="xnn4:1MuLfuwP1id" resolve="DataSource" />
    <node concept="3EZMnI" id="1MuLfuwPOaq" role="2wV5jI">
      <node concept="3F0ifn" id="1MuLfuwPOi1" role="3EZMnx">
        <property role="3F0ifm" value="Data source" />
      </node>
      <node concept="3F0A7n" id="1MuLfuwPV4J" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1MuLfuwPOi7" role="3EZMnx">
        <node concept="lj46D" id="1MuLfuwPSjz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwPSlj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwPSlp" role="3EZMnx">
        <property role="3F0ifm" value="type         " />
        <node concept="lj46D" id="1MuLfuwPSlq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwPSlr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="1MuLfuwPV5l" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:1MuLfuwP7oi" resolve="type" />
        <node concept="1sVBvm" id="1MuLfuwPV5n" role="1sWHZn">
          <node concept="3F0A7n" id="1MuLfuwPV5J" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwPSl_" role="3EZMnx">
        <node concept="lj46D" id="1MuLfuwPSlA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwPSlB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwPSm2" role="3EZMnx">
        <property role="3F0ifm" value="filters (or) " />
        <node concept="lj46D" id="1MuLfuwPSm3" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwPSm4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="1MuLfuwSGZE" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:1MuLfuwSGZb" resolve="filters" />
        <node concept="2iRkQZ" id="1MuLfuwSGZH" role="2czzBx" />
        <node concept="VPM3Z" id="1MuLfuwSGZI" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwSBOa" role="3EZMnx">
        <node concept="lj46D" id="1MuLfuwSBOb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwSBOc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwSBOx" role="3EZMnx">
        <node concept="lj46D" id="1MuLfuwSBOy" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwSBOz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1MuLfuwSBOV" role="3EZMnx">
        <node concept="lj46D" id="1MuLfuwSBOW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="1MuLfuwSBOX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="1MuLfuwPOat" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1MuLfuwS0GB">
    <property role="3GE5qa" value="datasource" />
    <ref role="1XX52x" to="xnn4:1MuLfuwS0to" resolve="DataSourceReference" />
    <node concept="3EZMnI" id="1MuLfuwS0GD" role="2wV5jI">
      <node concept="1iCGBv" id="1MuLfuwS0GK" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:1MuLfuwS0Gw" resolve="dataSource" />
        <node concept="1sVBvm" id="1MuLfuwS0GM" role="1sWHZn">
          <node concept="3F0A7n" id="1MuLfuwS0GT" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="1MuLfuwS0GG" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5$yeFckSRa2">
    <property role="3GE5qa" value="view.grid.column.property" />
    <ref role="1XX52x" to="xnn4:5$yeFckSR9U" resolve="HorizontalAlignGridColumnProperty" />
    <node concept="3EZMnI" id="5$yeFckSRa4" role="2wV5jI">
      <node concept="3F0ifn" id="5$yeFckSRab" role="3EZMnx">
        <property role="3F0ifm" value="horizontal align" />
      </node>
      <node concept="3F0A7n" id="5$yeFckSRah" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5$yeFckSR9V" resolve="align" />
      </node>
      <node concept="2iRfu4" id="5$yeFckSRa7" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5$yeFckTehM">
    <property role="3GE5qa" value="view.grid.column.property" />
    <ref role="1XX52x" to="xnn4:5$yeFckTehF" resolve="VerticalAlignGridColumnProperty" />
    <node concept="3EZMnI" id="5$yeFckTehO" role="2wV5jI">
      <node concept="3F0ifn" id="5$yeFckTehV" role="3EZMnx">
        <property role="3F0ifm" value="vertical align" />
      </node>
      <node concept="3F0A7n" id="5$yeFckTei1" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5$yeFckTehG" resolve="align" />
      </node>
      <node concept="2iRfu4" id="5$yeFckTehR" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3afENqY5MzA">
    <property role="3GE5qa" value="layout.component.button.action" />
    <ref role="1XX52x" to="xnn4:3afENqY5Mzw" resolve="NoActionButtonAction" />
    <node concept="3EZMnI" id="3afENqY5Nbs" role="2wV5jI">
      <node concept="3F0ifn" id="3afENqY5Nbz" role="3EZMnx">
        <property role="3F0ifm" value="no action" />
      </node>
      <node concept="l2Vlx" id="3afENqY5Nbv" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="34Qn6O$1EH3">
    <property role="3GE5qa" value="layout.component.button.action" />
    <ref role="1XX52x" to="xnn4:34Qn6O$1_nZ" resolve="SubmitButtonAction" />
    <node concept="3EZMnI" id="34Qn6O$1IA3" role="2wV5jI">
      <node concept="3F0ifn" id="34Qn6O$1IAa" role="3EZMnx">
        <property role="3F0ifm" value="submit" />
      </node>
      <node concept="l2Vlx" id="34Qn6O$1IA6" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7CwOiypWVf4">
    <property role="3GE5qa" value="view.grid.property" />
    <ref role="1XX52x" to="xnn4:7CwOiypWVf1" resolve="RowsPerPageGridProperty" />
    <node concept="3EZMnI" id="7CwOiypWVf6" role="2wV5jI">
      <node concept="3F0ifn" id="7CwOiypWVfd" role="3EZMnx">
        <property role="3F0ifm" value="rows per page" />
      </node>
      <node concept="3F0A7n" id="7CwOiypWVfj" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:7CwOiypWVf2" resolve="count" />
      </node>
      <node concept="2iRfu4" id="7CwOiypWVf9" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7CwOiypWVfo">
    <property role="3GE5qa" value="view.grid.property" />
    <ref role="1XX52x" to="xnn4:7CwOiypWVfn" resolve="InlineEditGridProperty" />
    <node concept="3EZMnI" id="7CwOiypWVfq" role="2wV5jI">
      <node concept="3F0ifn" id="7CwOiypWVfx" role="3EZMnx">
        <property role="3F0ifm" value="inline edit" />
      </node>
      <node concept="2iRfu4" id="7CwOiypWVft" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6aK4n5uD8Tu">
    <property role="3GE5qa" value="layout.component.button.action" />
    <ref role="1XX52x" to="xnn4:6aK4n5uD8Tt" resolve="CancelButtonAction" />
    <node concept="3EZMnI" id="6aK4n5uD8Tw" role="2wV5jI">
      <node concept="3F0ifn" id="6aK4n5uD8TB" role="3EZMnx">
        <property role="3F0ifm" value="cancel" />
      </node>
      <node concept="2iRfu4" id="6aK4n5uD8Tz" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6aK4n5uD8TG">
    <property role="3GE5qa" value="layout.container.panel" />
    <ref role="1XX52x" to="xnn4:6aK4n5uD8TF" resolve="Panel" />
    <node concept="3EZMnI" id="6aK4n5uD8TI" role="2wV5jI">
      <node concept="3F0ifn" id="6aK4n5uD8TP" role="3EZMnx">
        <property role="3F0ifm" value="panel" />
      </node>
      <node concept="3F2HdR" id="6aK4n5uD8TV" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:5HBZIwHrpCI" resolve="components" />
        <node concept="2iRfu4" id="6aK4n5uD8TW" role="2czzBx" />
        <node concept="VPM3Z" id="6aK4n5uD8TX" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="l2Vlx" id="6aK4n5uD8TL" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7ZBQMKvATbj">
    <property role="3GE5qa" value="application" />
    <ref role="1XX52x" to="xnn4:27huHj9NkKs" resolve="Application" />
    <node concept="3EZMnI" id="7ZBQMKvATbl" role="2wV5jI">
      <node concept="3F0ifn" id="7ZBQMKvATbs" role="3EZMnx">
        <property role="3F0ifm" value="Application" />
      </node>
      <node concept="3F0A7n" id="7ZBQMKvATby" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZT6" role="3EZMnx">
        <node concept="lj46D" id="7ZBQMKvAZTb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZTg" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZTk" role="3EZMnx">
        <property role="3F0ifm" value="grids" />
        <node concept="lj46D" id="7ZBQMKvAZTl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZTm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="7ZBQMKvAZVW" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:6blNiKZRga" resolve="grids" />
        <node concept="2iRkQZ" id="7ZBQMKvAZVZ" role="2czzBx" />
        <node concept="VPM3Z" id="7ZBQMKvAZW0" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZTv" role="3EZMnx">
        <node concept="lj46D" id="7ZBQMKvAZTw" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZTx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZTH" role="3EZMnx">
        <property role="3F0ifm" value="forms" />
        <node concept="lj46D" id="7ZBQMKvAZTI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZTJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="7ZBQMKvAZWX" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:6blNiKZRg7" resolve="forms" />
        <node concept="2iRkQZ" id="7ZBQMKvAZX0" role="2czzBx" />
        <node concept="VPM3Z" id="7ZBQMKvAZX1" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZUD" role="3EZMnx">
        <node concept="lj46D" id="7ZBQMKvAZUE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZUF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7ZBQMKvAZV3" role="3EZMnx">
        <node concept="lj46D" id="7ZBQMKvAZV4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="7ZBQMKvAZV5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="7ZBQMKvATbo" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7ZBQMKvBeBk">
    <property role="3GE5qa" value="layout.component.button.action" />
    <ref role="1XX52x" to="xnn4:7ZBQMKvBeB9" resolve="ServiceMethodCallButtonAction" />
    <node concept="3EZMnI" id="7ZBQMKvBeBm" role="2wV5jI">
      <node concept="3F0ifn" id="7ZBQMKvBeBt" role="3EZMnx">
        <property role="3F0ifm" value="service method" />
      </node>
      <node concept="3F1sOY" id="7ZBQMKvBeBX" role="3EZMnx">
        <ref role="1NtTu8" to="xnn4:7ZBQMKvBeBR" resolve="serviceMethod" />
      </node>
      <node concept="l2Vlx" id="7ZBQMKvBeBp" role="2iSdaV" />
    </node>
  </node>
</model>

