package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsRowsPerPageGridPropertyPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsAbstractGridProperty {
    
    public BigDecimal count;
    
    public BigDecimal getCount() {
        return this.count;
    }
    
    public void setCount(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.count = null;
        }
        this.count = StringUtils.toBigDecimal(value);
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8800263644193731521";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.RowsPerPageGridProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "8800263644193731521", "org.clawiz.ui.common.language.structure.RowsPerPageGridProperty", ConceptPropertyType.PROPERTY, "8800263644193731522", "count"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("8800263644193731521", "count", getCount() != null ? getCount().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty) node;
        
        structure.setCount(getCount());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty) node;
        
        setCount(structure.getCount() != null ? structure.getCount().toString() : null);
        
    }
}
