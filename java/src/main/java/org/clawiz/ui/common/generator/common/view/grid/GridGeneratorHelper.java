/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.view.grid;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.CreateRowButtonAction;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction;
import org.clawiz.ui.common.metadata.data.view.grid.button.action.EditRowButtonAction;

public class GridGeneratorHelper extends Service {


    protected Button addCreateRowButton(Grid grid) {
        Button button = grid.getTopToolbar().getLeft().add(Button.class);

        button.setDisabledExpression("! canCreateRow");
        button.setAction(CreateRowButtonAction.class);

        return button;
    }

    protected Button addEditRowButton(Grid grid) {
        Button button = grid.getTopToolbar().getLeft().add(Button.class);

        button.setDisabledExpression("! canEditRow");
        button.setAction(EditRowButtonAction.class);

        return button;
    }

    protected Button addDeleteRowButton(Grid grid) {
        Button button = grid.getTopToolbar().getLeft().add(Button.class);

        button.setDisabledExpression("! canDeleteRow");
        button.setAction(DeleteRowButtonAction.class);

        return button;
    }

    public void prepareTopToolbar(Grid grid) {

        addCreateRowButton(grid);
        addEditRowButton(grid);
        addDeleteRowButton(grid);

    }

    public void prepareBottomToolbar(Grid grid) {


    }

    public void prepareToolbars(Grid grid) {

        prepareTopToolbar(grid);
        prepareBottomToolbar(grid);

        grid.getTopToolbar().prepareNode(getSession());
        grid.getBottomToolbar().prepareNode(getSession());

    }

    public void prepare(Grid grid) {

        prepareToolbars(grid);

    }
    
}
