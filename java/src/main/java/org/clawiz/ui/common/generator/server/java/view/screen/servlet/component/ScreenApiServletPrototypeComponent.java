/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.screen.servlet.component;


import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.FormApiServletDoPostMethodElement;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.element.GridApiProcessLoadServletMethodElement;
import org.clawiz.ui.common.generator.server.java.view.screen.AbstractUiScreenJavaClassComponent;

public class ScreenApiServletPrototypeComponent extends AbstractUiScreenJavaClassComponent {

    FormApiServletDoPostMethodElement doPost;

    public String getServletPath() {
        return "/api/" + StringUtils.toLowerFirstChar(getScreen().getName());
    }

    GridApiProcessLoadServletMethodElement executeQuery;

    public FormApiServletDoPostMethodElement getDoPost() {
        return doPost;
    }


    @Override
    public void process() {
        super.process();

        setName(ScreenApiServletComponent.screenToApiServletClassName(getScreen()) + "Prototype");

        if ( getScreen().getExtendsScreen() != null ) {
            setExtends(getScreenGenerator().getScreenPackageName(getScreen().getExtendsScreen()) + "." + ScreenApiServletComponent.screenToApiServletClassName(getScreen().getExtendsScreen()));
        } else {
            setExtends(getScreenGenerator().getContext().getBaseServletClassName());
        }

        JavaMethodElement pathMethod = addMethod("String", "getPath");
        pathMethod.addText("return \"" + getServletPath() + "\";");


    }

}
