package org.clawiz.ui.common.metadata.data.view.grid.column.property;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractGridColumnPropertyPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractGridColumnProperty withName(String value) {
        setName(value);
        return (AbstractGridColumnProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
