/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.storage;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.client.ts.application.AbstractApplicationTypeScriptClassComponent;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;

import java.math.BigDecimal;
import java.util.HashMap;

public class CommonDataSourceInterfacesComponent extends AbstractApplicationTypeScriptClassComponent {

    HashMap<String, DataSource> typeApiCache = new HashMap<>();

    @Override
    public void process() {
        super.process();

        setName("Interfaces");

        setDestinationPath(getGenerator().getApplicationDestinationPath() + ApplicationDataSourceComponent.STORAGE_DATASOURCE_PATH);

        addImport(getGenerator().getObjectComponent().getName(), null, "./" + nameToDottedFileName(getGenerator().getObjectComponent().getName()));

        for( DataSourceGeneratorContext dataSourceContext : getGenerator().getContext().getDataSourceContexts() ) {

            DataSource dataSource = dataSourceContext.getDataSource();
            typeApiCache.put(dataSource.getType().getFullName(), dataSource);

            for ( TypeField field : dataSource.getType().getFields() ) {

                if ( field.getValueType() instanceof ValueTypeEnumeration ) {

                    addImport(
                            field.getValueTypeJavaClassName()
                            , dataSource.getJavaClassName() + field.getValueTypeJavaClassName()
                            , getGenerator().getDataSourceImportPath(dataSource));
                }
            }
        }

    }

    @Override
    public void write() {

        writeImports();

        pln("");

        for( DataSourceGeneratorContext dataSourceContext : getGenerator().getContext().getDataSourceContexts() ) {

            DataSource dataSource = dataSourceContext.getDataSource();

            pln("export interface " + dataSource.getJavaClassName() + " extends " + getGenerator().getObjectComponent().getName() + " {");

            int maxLen = 0;
            for ( TypeField field : dataSource.getType().getFields() ) {
                if ( field.getJavaVariableName().length() > maxLen) {
                    maxLen = field.getJavaVariableName().length();
                }
            }

            for ( TypeField field : dataSource.getType().getFields() ) {

                if ( field.getValueType() instanceof ValueTypeList) {  continue;  }

                pln("");

                pln("  " + field.getJavaVariableName()
                        + StringUtils.space(maxLen - field.getJavaVariableName().length())
                        + " : "
                        + ( field.getValueType() instanceof ValueTypeEnumeration ? dataSource.getJavaClassName() : "" )
                        + toTypeScriptType(field) + ";"
                );

                if ( field.getValueType() instanceof ValueTypeObject ) {
                    ValueTypeObject vto = (ValueTypeObject) field.getValueType();
                    if ( vto.getReferencedType() != null && typeApiCache.containsKey(vto.getReferencedType().getFullName()) ) {
                        String variableName = getFieldDereferenceVariableName(field);
                        DataSource referenceApi = typeApiCache.get(vto.getReferencedType().getFullName());
                        pln("  " + variableName + StringUtils.space(maxLen - variableName.length()) + " : " + referenceApi.getJavaClassName() + ";");
                    }
                }

            }

            pln("");

            pln("}");


        }

    }
}
