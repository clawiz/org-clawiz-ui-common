/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletCreateRequestMethodElement extends AbstractFormServletMethodElement {

    public static final String ACTION_PARAMETER_NAME               = "action";
    public static final String AFTER_CREATE_REQUEST_EXTENSION_NAME = "afterPrepareRequest";

    @Override
    public void process() {
        super.process();

        setName("createRequest");

        setType(getRequestContextClassName());
        addParameter("AbstractHttpRequestContext", "httpRequest");

        addText("");
        addText(getRequestContextClassName() + " request = new " + getRequestContextClassName() + "();");
        addText("");
        addText("request.setHttpRequest(httpRequest);");
        addText("request.setAction(httpRequest.getParameterValue(\"" + ACTION_PARAMETER_NAME + "\"));");
        addText("");
        addText("request.setId(nodeGuidToObjectId(httpRequest.getParameterValue(\"id\")));");


        addText("");
        addText("String modelStr = httpRequest.getParameterValue(\"model\");");
        addText("if ( !StringUtils.isEmpty(modelStr) ) {");
        addText("");
        addText("    JsonObject modelJson = fromJson(modelStr);");
        addText("");
        addText("    " + getModelClassName() + " model = new " + getModelClassName() + "();");
        addText("");

        for(AbstractFormField field : getUnitedWithRootTypeFormFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }

//            String getValueStr = "getJsonElementAsString(modelJson, \"" + field.getJavaVariableName() + "\")";
//            String valueStr    = getValueStr + ".getAsString()";
            String valueStr    = "getJsonElementAsString(modelJson, \"" + field.getJavaVariableName() + "\")";


            if ( field.getValueType() instanceof ValueTypeString) {
            } else if ( field.getValueType() instanceof ValueTypeDate) {
                valueStr = "stringToDate(" + valueStr + ")";
            } else if ( field.getValueType() instanceof ValueTypeDateTime) {
                valueStr = "stringToDateTime(" + valueStr + ")";
            } else if ( field.getValueType() instanceof ValueTypeNumber) {
                valueStr = "stringToBigDecimal(" + valueStr + ")";
            } else if ( field.getValueType() instanceof ValueTypeObject ) {
                valueStr = "nodeGuidToObjectId(" + valueStr + ")";
            } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
                if ( field.getTypeField() != null ) {
                    valueStr = getComponent().getValueTypeJavaClassName(field)
                               + ".to" + field.getTypeField().getValueTypeJavaClassName() + "("
                               + valueStr + ")";
                } else {
                    throwException("Value type ENUM supported only for type fields");
                }
            } else if ( field.getValueType() instanceof ValueTypeBoolean) {
                valueStr = "stringToBoolean(" + valueStr + ")";
            } else {
                throwException("Wrong field '?' value type '?'", new Object[]{field.getJavaClassName(), field.getValueType().toString()});
            }

            addText("    model.set" + StringUtils.toUpperFirstChar(field.getJavaVariableName()) + "(" + valueStr + ");");
        }

        addText("    request.setModel(model);");
        addText("}");

        processExtensions(AFTER_CREATE_REQUEST_EXTENSION_NAME);

        addText("");
        addText("return request;");
    }
}
