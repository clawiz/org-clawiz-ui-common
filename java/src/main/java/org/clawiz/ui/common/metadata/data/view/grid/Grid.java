/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.view.grid;

import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.install.EmptyMetadataNodeInstaller;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.view.datasource.TypeViewDataSource;
import org.clawiz.ui.common.metadata.data.view.grid.property.InlineEditGridProperty;
import org.clawiz.ui.common.metadata.data.view.grid.property.RowsPerPageGridProperty;

public class Grid extends GridPrototype {

    public String getTitle() {
        return super.getTitle() != null ? super.getTitle() : getName();
    }

    public Type getRootType() {
        return getDataSource() instanceof TypeViewDataSource
                ? ((TypeViewDataSource) getDataSource()).getType() : null;
    }


    public int getRowsPerPage() {
        RowsPerPageGridProperty property = getProperties().get(RowsPerPageGridProperty.class);
        return property != null && property.getCount() != null ? property.getCount().intValue() : 10;
    }

    public boolean isInlineEdit() {
        return getProperties().get(InlineEditGridProperty.class) != null;
    }

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) EmptyMetadataNodeInstaller.class;
    }

    private boolean generatorHelperProcessed = false;

    public boolean isGeneratorHelperProcessed() {
        return generatorHelperProcessed;
    }

    public void setGeneratorHelperProcessed(boolean generatorHelperProcessed) {
        this.generatorHelperProcessed = generatorHelperProcessed;
    }

}
