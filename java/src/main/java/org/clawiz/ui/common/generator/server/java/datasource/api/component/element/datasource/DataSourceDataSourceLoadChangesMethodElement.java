/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component.element.datasource;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.ddl.TypeDatabaseObjectsInstallService;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;

public class DataSourceDataSourceLoadChangesMethodElement extends AbstractDataSourceDataSourceJavaClassMethodElement {

    public static String getNewFieldColumnName(TypeField field) {
        return TypeDatabaseObjectsInstallService.getNewFieldColumnName(field);
    }

    @Override
    public void process() {
        super.process();

        setName("loadChanges");

        addParameter(getExecuteContextClassName(), "context");

        addText("");

        addText("StringBuilder sql            = new StringBuilder();");
        addText("ArrayList<Object> parameters = new ArrayList<>();");
        addText("");
        StringBuilder sb = new StringBuilder();
        sb.append("a.scn, a.id, a.action_type");


        for (TypeField field : getDataSource().getType().getFields() ) {
            sb.append(", a." + getNewFieldColumnName(field));
        }
        addText("sql.append(\"select " + sb.toString() + "\\n\");");

        addText("");
        addText("sql.append(\" from " + TypeDatabaseObjectsInstallService.getTypeAuditTableName(getDataSource().getType()) +  " a\\n\");");
        addText("sql.append(\" where a.scn >= ? and a.scn <= ?\\n\");");
        addText("parameters.add(context.getFirstScn());");
        addText("parameters.add(context.getLastScn());");
        addText("");
        addText("if ( context.getObjectId() != null ) {");
        addText("    context.getFilters().clear();");
        addText("    context.addRowIdFilter(context.getObjectId());");
        addText("}");
        addText("");
        addText("addContextFilters(context.getFilters(), sql, parameters);");
        addText("");
        addText("sql.append(\" order by a.id, a.scn desc\");");
        addText("");
        addText("BigDecimal currentObjectId          = null;");
        addText("Statement statement                 = executeQuery(sql.toString(), parameters.toArray());");
        addText("while ( statement.next() ) {");
        addText("");
        addText("    if ( currentObjectId != null && currentObjectId.equals(statement.getBigDecimal(2))) {");
        addText("        continue;");
        addText("    }");
        addText("    currentObjectId        = statement.getBigDecimal(2);");
        addText("");
        addText("    " + getRowModelClassName() + " row = new " + getRowModelClassName() + "();");
        addText("    row.setScn(statement.getBigDecimal(1));");
        addText("    row.setId(currentObjectId);");
        addText("    row.setAction(statement.getString(3));");
        addText("    row.setTableName(\"" + getDataSource().getType().getTableName() +  "\");");
        addText("");
        addText("    if ( ! \"DELETE\".equals(row.getAction()) ) {");

        int index = 4;
        for ( TypeField field : getDataSource().getType().getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {  continue;  }

            String getter;
            if ( field.getValueType() instanceof ValueTypeBoolean) {
                getter = "\"T\".equals(" + "statement.getString" + "(" + (index++) + "))";
            } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
                getter = field.getValueTypeJavaClassName() + ".to" + field.getValueTypeJavaClassName() + "(" + "statement.getString" + "(" + (index++) + "))";
            } else {
                getter = "statement.get" + field.getValueType().getStatementValueTypeName() + "(" + (index++) + ")";
            }
            addText("        row." + field.getSetMethodName() + "(" + getter + ");");
        }

        boolean objectsFound = false;
        for ( TypeField field : getDataSource().getType().getFields() ) {
            if ( ! ( field.getValueType() instanceof ValueTypeObject) ) {
                continue;
            }
            if ( ! objectsFound ) {
                addText("");
                objectsFound = true;
            }
            addText("        context.addRererence(row." + field.getGetMethodName() + "());");
        }
        if ( objectsFound ) {
            addText("");
        }

        addText("    }");
        addText("");
        addText("    context.addRow(row);");
        addText("}");
        addText("statement.close();");

        addText("");

    }
}
