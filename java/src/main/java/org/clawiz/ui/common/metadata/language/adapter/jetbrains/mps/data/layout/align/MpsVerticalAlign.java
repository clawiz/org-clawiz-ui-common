package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align;

import org.clawiz.core.common.CoreException;

public enum MpsVerticalAlign {

    TOP, BOTTOM, CENTER;

    
    public static MpsVerticalAlign toMpsVerticalAlign(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsVerticalAlign.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsVerticalAlign value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsVerticalAlign value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case TOP : return "TOP";
            case BOTTOM : return "BOTTOM";
            case CENTER : return "CENTER";
        }
         
        return null;
    }
}
