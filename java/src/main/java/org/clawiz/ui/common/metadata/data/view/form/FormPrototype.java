package org.clawiz.ui.common.metadata.data.view.form;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class FormPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String title;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource dataSource;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList fields = new org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList();
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar topToolbar;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar bottomToolbar;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.LayoutComponentList layoutComponents = new org.clawiz.ui.common.metadata.data.layout.LayoutComponentList();
    
    public Form withName(String value) {
        setName(value);
        return (Form) this;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String value) {
        this.title = value;
    }
    
    public Form withTitle(String value) {
        setTitle(value);
        return (Form) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource getDataSource() {
        return this.dataSource;
    }
    
    public void setDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value) {
        this.dataSource = value;
    }
    
    public Form withDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value) {
        setDataSource(value);
        return (Form) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource> T createDataSource(Class<T> nodeClass) {
        if ( getDataSource() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "dataSource", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value = createChildNode(nodeClass, "dataSource");
        setDataSource(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource createDataSource() {
        return createDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource.class);
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList getFields() {
        return this.fields;
    }
    
    public Form withField(org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField value) {
        getFields().add(value);
        return (Form) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField> T createField(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField value = createChildNode(nodeClass, "fields");
        getFields().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField createField() {
        return createField(org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar getTopToolbar() {
        return this.topToolbar;
    }
    
    public void setTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        this.topToolbar = value;
    }
    
    public Form withTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        setTopToolbar(value);
        return (Form) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar> T createTopToolbar(Class<T> nodeClass) {
        if ( getTopToolbar() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "topToolbar", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value = createChildNode(nodeClass, "topToolbar");
        setTopToolbar(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar createTopToolbar() {
        return createTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar getBottomToolbar() {
        return this.bottomToolbar;
    }
    
    public void setBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        this.bottomToolbar = value;
    }
    
    public Form withBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        setBottomToolbar(value);
        return (Form) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar> T createBottomToolbar(Class<T> nodeClass) {
        if ( getBottomToolbar() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "bottomToolbar", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value = createChildNode(nodeClass, "bottomToolbar");
        setBottomToolbar(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar createBottomToolbar() {
        return createBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.LayoutComponentList getLayoutComponents() {
        return this.layoutComponents;
    }
    
    public Form withLayoutComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value) {
        getLayoutComponents().add(value);
        return (Form) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent> T createLayoutComponent(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value = createChildNode(nodeClass, "layoutComponents");
        getLayoutComponents().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent createLayoutComponent() {
        return createLayoutComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getDataSource() != null ) { 
            getDataSource().prepare(session);
        }
        for (MetadataNode node : getFields()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getTopToolbar() != null ) { 
            getTopToolbar().prepare(session);
        }
        if ( getBottomToolbar() != null ) { 
            getBottomToolbar().prepare(session);
        }
        for (MetadataNode node : getLayoutComponents()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getDataSource());
        
        for (MetadataNode node : getFields()) {
            references.add(node);
        }
        
        references.add(getTopToolbar());
        
        references.add(getBottomToolbar());
        
        for (MetadataNode node : getLayoutComponents()) {
            references.add(node);
        }
        
    }
}
