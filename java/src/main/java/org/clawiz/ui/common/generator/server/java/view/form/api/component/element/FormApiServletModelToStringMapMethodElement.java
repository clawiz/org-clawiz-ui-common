/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.system.generator.java.component.element.JavaBlockElement;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.server.java.ServerJavaGeneratorUtils;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletModelToStringMapMethodElement extends AbstractFormServletMethodElement {

    public static final String ADD_STRINGMAP_VALUES_EXTENSION_NAME = "addFormApiServletModelToStringMapValues";

    JavaBlockElement actionSwitch;

    public JavaBlockElement getActionSwitch() {
        return actionSwitch;
    }

    @Override
    public void process() {
        super.process();
        setName("modelToStringMap");
        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");
        setType("StringMap<JsonElement>");
        addParameter(getModelClassName(), "model");

        addText("StringMap<JsonElement> map = new StringMap<>();");
        addText("");

        addText("if ( model.getId() != null ) {");
        addText("    map.put(\"id\", new JsonPrimitive(objectIdToNodeGuid(model.getId())));");
        addText("}");
        for (AbstractFormField formField : getUnitedWithRootTypeFormFields() ) {
            ServerJavaGeneratorUtils.addStringMapValue(this, formField.getJavaVariableName(), formField.getValueType(), formField.getTypeField());
        }

        processExtensions(ADD_STRINGMAP_VALUES_EXTENSION_NAME);

        addText("");
        addText("return map;");


    }



}
