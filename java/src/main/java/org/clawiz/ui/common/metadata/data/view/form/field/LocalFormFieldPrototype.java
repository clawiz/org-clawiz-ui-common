package org.clawiz.ui.common.metadata.data.view.form.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class LocalFormFieldPrototype extends org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType valueType;
    
    public LocalFormField withName(String value) {
        setName(value);
        return (LocalFormField) this;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value) {
        this.valueType = value;
    }
    
    public LocalFormField withValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value) {
        setValueType(value);
        return (LocalFormField) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType> T createValueType(Class<T> nodeClass) {
        if ( getValueType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "valueType", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value = createChildNode(nodeClass, "valueType");
        setValueType(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType createValueType() {
        return createValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValueType() != null ) { 
            getValueType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValueType());
        
    }
}
