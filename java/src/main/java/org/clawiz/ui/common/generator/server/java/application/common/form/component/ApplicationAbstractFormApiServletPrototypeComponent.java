/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.form.component;

import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.query.QueryBuilder;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValueList;
import org.clawiz.ui.common.generator.server.java.application.component.ApplicationAbstractServletComponent;
import org.clawiz.ui.common.portal.application.form.FormQueryCacheMode;

import java.util.concurrent.ConcurrentHashMap;

public class ApplicationAbstractFormApiServletPrototypeComponent extends AbstractApplicationFormJavaClassComponent {


    public void addImports() {
        addImport(ConcurrentHashMap.class);
        addImport(Query.class);
        addImport(QueryBuilder.class);
        addImport(FormQueryCacheMode.class);
        addImport(SelectValueList.class);

    }

    public void addVariables() {
        addVariable("ConcurrentHashMap<String, Query>", "staticQueryCache")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE).withStatic(true)
                .setInitStatement("new ConcurrentHashMap<>()");
        addVariable("ConcurrentHashMap<String, Query>", "sessionQueryCache")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                .setInitStatement("new ConcurrentHashMap<>()");
        addVariable("QueryBuilder", "queryBuilder").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
    }

    public void addPrepareQuery() {
        JavaMethodElement method = addMethod(null, "prepareQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addParameter("Query", "query");
    }

    @SuppressWarnings("Duplicates")
    public void addCreateQuery() {
        JavaMethodElement method = addMethod("Query", "createQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addText("Query query = queryBuilder.create();");
        method.addText("prepareQuery(request, query);");
        method.addText("return query;");
    }


    public void addGetQuery() {
        JavaMethodElement method = addMethod("Query", "getQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");

        method.addText("Query query = null;");
        method.addText("");
        method.addText("String hashString = getClass().getName();");
        method.addText("switch (getQueryCacheMode()) {");
        method.addText("    case STATIC:  query = staticQueryCache.get(hashString);  break;");
        method.addText("    case SESSION: query = sessionQueryCache.get(hashString); break;");
        method.addText("}");
        method.addText("");
        method.addText("if ( query != null ) {");
        method.addText("    return query;");
        method.addText("}");
        method.addText("");
        method.addText("query = createQuery(request);");
        method.addText("switch (getQueryCacheMode()) {");
        method.addText("    case STATIC:  staticQueryCache.put(hashString,query); break;");
        method.addText("    case SESSION: sessionQueryCache.put(hashString, query); break;");
        method.addText("}");
        method.addText("");
        method.addText("return query;");

    }

    public void addGetQueryCacheMode() {
        JavaMethodElement method = addMethod("FormQueryCacheMode", "getQueryCacheMode").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addText("return FormQueryCacheMode.STATIC;");
    }


    @Override
    public void process() {
        super.process();

        setName(getComponentByClass(ApplicationAbstractFormApiServletComponent.class).getName() + "Prototype");
        setExtends(getComponentByClass(ApplicationAbstractServletComponent.class).getFullName());

        setTypeParameters("R extends " + getComponentByClass(ApplicationAbstractFormApiRequestContextComponent.class).getName());

        addImports();

        addVariables();

        addGetQueryCacheMode();
        addPrepareQuery();
        addCreateQuery();
        addGetQuery();

    }

}
