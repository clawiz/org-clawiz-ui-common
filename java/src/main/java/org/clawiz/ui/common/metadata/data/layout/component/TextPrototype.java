package org.clawiz.ui.common.metadata.data.layout.component;

import java.lang.Boolean;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TextPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeAttribute
    private java.lang.Boolean expression;
    
    public Text withName(String value) {
        setName(value);
        return (Text) this;
    }
    
    public java.lang.Boolean isExpression() {
        return this.expression;
    }
    
    public void setExpression(java.lang.Boolean value) {
        this.expression = value;
    }
    
    public Text withExpression(java.lang.Boolean value) {
        setExpression(value);
        return (Text) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
