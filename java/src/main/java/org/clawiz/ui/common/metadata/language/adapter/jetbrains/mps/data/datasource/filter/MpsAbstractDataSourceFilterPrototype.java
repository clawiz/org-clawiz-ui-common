package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.filter;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractDataSourceFilterPrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2062302247217430369";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractDataSourceFilter";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter structure = (org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter structure = (org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter) node;
        
    }
}
