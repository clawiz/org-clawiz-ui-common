package org.clawiz.ui.common.metadata.data.layout.container.collection;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractCollectionPrototype extends org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer {
    
    @ExchangeAttribute
    private org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection direction;
    
    public AbstractCollection withName(String value) {
        setName(value);
        return (AbstractCollection) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection getDirection() {
        return this.direction;
    }
    
    public void setDirection(org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection value) {
        this.direction = value;
    }
    
    public AbstractCollection withDirection(org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection value) {
        setDirection(value);
        return (AbstractCollection) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
