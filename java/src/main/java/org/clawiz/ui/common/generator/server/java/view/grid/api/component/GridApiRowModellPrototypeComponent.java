/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid.api.component;


import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.common.generator.server.java.application.common.grid.component.ApplicationAbstractGridApiRowModelComponent;
import org.clawiz.ui.common.generator.server.java.view.grid.component.AbstractGridJavaClassComponent;

import java.math.BigDecimal;
import java.util.Date;

public class GridApiRowModellPrototypeComponent extends AbstractGridJavaClassComponent {

    protected void addImports() {
        addImport(BigDecimal.class);
        addImport(Date.class);
    }

    protected void addFieldVariable(AbstractGridColumn column) {

        if ( column.getValueType() instanceof ValueTypeList) {
            return;
        }

        addVariable(getValueTypeJavaClassName(column), column.getJavaVariableName(), null, true, true);
        if ( column.getValueType() instanceof ValueTypeEnumeration) {
            addVariable("String",     column.getToStringJavaVariableName(), null, true, true);
        }

    }

    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getGenerator().getRowModelClassName() + "Prototype");

        addImports();

        AbstractJavaClassComponent abstractServlet =  getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractGridApiRowModelComponent.class);
        addImport(abstractServlet.getFullName());
        setExtends(abstractServlet.getName());



        for (AbstractGridColumn column : getGrid().getColumns()) {
            addFieldVariable(column);
        }

    }


}
