package org.clawiz.ui.common.portal.application.grid.request;

import org.clawiz.core.common.system.database.Statement;

public class GridApiLoadContext {

    String    token;
    Statement statement;
    int       lastRowIndex;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public int getLastRowIndex() {
        return lastRowIndex;
    }

    public void setLastRowIndex(int lastRowIndex) {
        this.lastRowIndex = lastRowIndex;
    }

}
