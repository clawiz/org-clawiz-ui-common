/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.view.form;


import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList;
import org.clawiz.ui.common.metadata.data.view.form.field.TypeFieldFormField;
import org.clawiz.ui.common.generator.AbstractUiGenerator;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.AbstractApplicationGenerator;

import java.util.HashMap;

public class FormGenerator extends AbstractUiGenerator {

    FormGeneratorContext formContext;
    FormGeneratorHelper formHelper;

    public FormGeneratorContext getContext() {
        return formContext;
    }

    public AbstractApplicationGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        formContext = (FormGeneratorContext) context;
    }

    private HashMap<MetadataNode, String> nodeToIdCache           = new HashMap<>();
    private HashMap<MetadataNode, String> nodeToVariableNameCache = new HashMap<>();
    private HashMap<String, MetadataNode> idToNodeCache           = new HashMap<>();
    public String getNodeId(MetadataNode node) {

        String id       = nodeToIdCache.get(node);
        if ( id != null ) {
            return id;
        }

        String nodeClassName = node.getJavaClassName();
        if ( nodeClassName == null ) {
            nodeClassName = StringUtils.splitByLastTokenOccurrence(node.getClass().getName(), "\\.")[1];
        }
        String idPrefix   = getForm().getJavaVariableName() + nodeClassName;
        String namePrefix = StringUtils.toLowerFirstChar(nodeClassName);
        id              = idPrefix;
        String name     = namePrefix;
        int index = 1;
        while (idToNodeCache.containsKey(id)) {
            id   = idPrefix   + index;
            name = namePrefix + index++;
        }
        nodeToIdCache.put(node, id);
        idToNodeCache.put(id, node);

        nodeToVariableNameCache.put(node, StringUtils.toLowerFirstChar(name));

        return id;
    }

    public String getNodeVariableName(MetadataNode node) {
        String name = nodeToVariableNameCache.get(node);
        if ( name != null ) {
            return name;
        }
        getNodeId(node);
        return nodeToVariableNameCache.get(node);
    }

    public Form getForm() {
        return getContext().getForm();
    }

    FormFieldList unitedWithRootTypeFormFields;
    public FormFieldList getUnitedWithRootTypeFormFields() {
        if (unitedWithRootTypeFormFields != null ) {
            return unitedWithRootTypeFormFields;
        }
        FormFieldList _unitedWithRootTypeFormFields       = new FormFieldList();
        HashMap<String, AbstractFormField> existsCache = new HashMap<>();

        for (AbstractFormField formField : getForm().getFields() ) {
            _unitedWithRootTypeFormFields.add(formField);
            if ( formField instanceof TypeFieldFormField ) {
                existsCache.put(formField.getTypeField().getFullName(), formField);
            }
        }
        if ( getForm().getRootType() != null ) {
            for (TypeField typeField : getForm().getRootType().getFields() ) {
                if ( existsCache.containsKey(typeField.getFullName()) || typeField.getValueType() instanceof ValueTypeList) {
                    continue;
                }
                TypeFieldFormField formField = new TypeFieldFormField();
                formField.setTypeField(typeField);
                _unitedWithRootTypeFormFields.add(formField);

            }
        }
        unitedWithRootTypeFormFields = _unitedWithRootTypeFormFields;
        return unitedWithRootTypeFormFields;
    }


    public FormFieldList getFields() {
        return getForm().getFields();
    }

    private void prepareComponentsId(AbstractLayoutComponent component) {
        getNodeId(component);
        if ( component instanceof AbstractContainer) {
            for(AbstractLayoutComponent child : ((AbstractContainer) component).getComponents() ) {
                prepareComponentsId(child);
            }
        }
    }

        protected void prepareForm() {

            if ( getForm().getTopToolbar() == null ) {
                Toolbar toolbar = getForm().createChildNode(Toolbar.class, "topToolbar");
                getForm().setTopToolbar(toolbar);
                toolbar.setLeft(toolbar.createChildNode(AbstractContainer.class, "left"));
                toolbar.setRight(toolbar.createChildNode(AbstractContainer.class, "right"));
            }

            if ( getForm().getBottomToolbar() == null ) {
                Toolbar toolbar = getForm().createChildNode(Toolbar.class, "bottomToolbar");
                getForm().setBottomToolbar(toolbar);
                toolbar.setLeft(toolbar.createChildNode(AbstractContainer.class, "left"));
                toolbar.setRight(toolbar.createChildNode(AbstractContainer.class, "right"));
            }

        if ( ! getForm().isGeneratorHelperProcessed() ) {
            formHelper.prepare(getForm());
            getForm().setGeneratorHelperProcessed(true);
        }

    }


    @Override
    protected void process() {
        super.process();

        prepareForm();

        for (AbstractLayoutComponent component : getForm().getLayoutComponents() ) {
            prepareComponentsId(component);
        }

    }
}
