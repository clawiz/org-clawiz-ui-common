package org.clawiz.ui.common.metadata.data.layout.component.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class FieldPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField formField;
    
    public Field withName(String value) {
        setName(value);
        return (Field) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField getFormField() {
        return this.formField;
    }
    
    public void setFormField(org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField value) {
        this.formField = value;
    }
    
    public Field withFormField(org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField value) {
        setFormField(value);
        return (Field) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getFormField() != null ) { 
            getFormField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getFormField());
        
    }
}
