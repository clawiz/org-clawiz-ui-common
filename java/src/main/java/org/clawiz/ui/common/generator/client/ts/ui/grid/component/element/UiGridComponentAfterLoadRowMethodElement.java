/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.grid.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.TypeScriptGridRowModelComponent;

public class UiGridComponentAfterLoadRowMethodElement extends AbstractUiGridComponentMethodElement {

    @Override
    public void process() {
        super.process();

        setName("afterLoadRow");
        setType("Promise<any>");

        AbstractComponent component = getGenerator().getComponentByClass(TypeScriptGridRowModelComponent.class);
        addParameter("loadedRow", component.getName());

        addText("");
        addText("return super.afterLoadRow(loadedRow)");
        addText("  .then(() => {");

        for (AbstractGridColumn column : getGrid().getColumns() ) {
            if ( column.getValueType() instanceof ValueTypeDate) {
                addText("    loadedRow." + column.getJavaVariableName() + " = this.dateUtils.stringToDate((<any>loadedRow)['" + column.getJavaVariableName() + "'], this.dateUtils.ISO8601_DATE_FORMAT);");
            } else if ( column.getValueType() instanceof ValueTypeDateTime) {
                addText("    loadedRow." + column.getJavaVariableName() + " = this.dateUtils.stringToDateTime((<any>loadedRow)['" + column.getJavaVariableName() + "'], this.dateUtils.ISO8601_DATE_TIME_FORMAT);");
            }
        }

        addText("    return Promise.resolve();");
        addText("  });");
        addText("");
    }
}
