package org.clawiz.ui.common.metadata.data.layout.component;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class GridLayoutComponentPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.grid.Grid grid;
    
    public GridLayoutComponent withName(String value) {
        setName(value);
        return (GridLayoutComponent) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.Grid getGrid() {
        return this.grid;
    }
    
    public void setGrid(org.clawiz.ui.common.metadata.data.view.grid.Grid value) {
        this.grid = value;
    }
    
    public GridLayoutComponent withGrid(org.clawiz.ui.common.metadata.data.view.grid.Grid value) {
        setGrid(value);
        return (GridLayoutComponent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getGrid() != null ) { 
            getGrid().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getGrid());
        
    }
}
