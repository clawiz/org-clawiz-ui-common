/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element;


import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationDataSourceDataSourcePrototypeComponent;

public class ApplicationDataSourceDataSourceSaveChangeMethodElement extends AbstractApplicationDataSourceServletMethodElement {

    @Override
    public void process() {
        super.process();

        setName("saveChange");

        addParameter("R", "row");

        addText("");
        addText("if ( row.getTableName() == null ) {");
        addText("    throwException(\"Table name of synchronized object (?) must be not null\", row.getId());");
        addText("}");
        addText("");
        addText("switch (row.getTableName().toUpperCase()) {");

        for (ApplicationDataSourceDataSourcePrototypeComponent.DataSourceContext ac : ((ApplicationDataSourceDataSourcePrototypeComponent) getComponent()).getDataSourceContexts() ) {
            addText("    case \"" + ac.tableName.toUpperCase() + "\" :");
            addText("        " + ac.dataSourceVariableName + ".saveChange(("
                    + ac.rowModelClassName
                    + ") row);");
            addText("        break;");
        }
        addText("    default : throwException(\"Wrong synchronized object (?) table name ?\", row.getTableName(), row.getId());");
        addText("}");
        addText("");

    }
}
