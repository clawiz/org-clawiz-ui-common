/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form;

import org.clawiz.ui.common.generator.AbstractApplicationGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;

public class TypeScriptUiFormGeneratorContext extends FormGeneratorContext {

    TypeScriptClientCommonGeneratorContext typeScriptClientCommonGeneratorContext;
    TypeScriptClientCommonGenerator typeScriptClientCommonGenerator;

    @Override
    public TypeScriptClientCommonGeneratorContext getApplicationContext() {
        return typeScriptClientCommonGeneratorContext;
    }

    @Override
    public void setApplicationContext(AbstractApplicationGeneratorContext applicationContext) {
        super.setApplicationContext(applicationContext);
        typeScriptClientCommonGeneratorContext = (TypeScriptClientCommonGeneratorContext) applicationContext;
    }

    @Override
    public TypeScriptClientCommonGenerator getApplicationGenerator() {
        return typeScriptClientCommonGenerator;
    }

    @Override
    public void setApplicationGenerator(AbstractApplicationGenerator applicationGenerator) {
        super.setApplicationGenerator(applicationGenerator);
        typeScriptClientCommonGenerator = (TypeScriptClientCommonGenerator) applicationGenerator;
    }


}
