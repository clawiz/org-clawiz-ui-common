package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractFormFieldPrototype extends AbstractMpsNode {
    
    public String label;
    
    public String hint;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsAbstractFormFieldProperty> properties = new ArrayList<>();
    
    public String getLabel() {
        return this.label;
    }
    
    public void setLabel(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.label = null;
        }
        this.label = value;
    }
    
    public String getHint() {
        return this.hint;
    }
    
    public void setHint(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.hint = null;
        }
        this.hint = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsAbstractFormFieldProperty> getProperties() {
        return this.properties;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6658318051736428073";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractFormField";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736428073", "org.clawiz.ui.common.language.structure.AbstractFormField", ConceptPropertyType.PROPERTY, "8035881083538385331", "label"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736428073", "org.clawiz.ui.common.language.structure.AbstractFormField", ConceptPropertyType.PROPERTY, "7111202990234873228", "hint"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736428073", "org.clawiz.ui.common.language.structure.AbstractFormField", ConceptPropertyType.CHILD, "6658318051736470505", "properties"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("6658318051736428073", "label", getLabel());
        addConceptNodeProperty("6658318051736428073", "hint", getHint());
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("6658318051736428073", "properties", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField structure = (org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField) node;
        
        structure.setLabel(getLabel());
        
        structure.setHint(getHint());
        
        structure.getProperties().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsAbstractFormFieldProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField structure = (org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField) node;
        
        setLabel(structure.getLabel());
        
        setHint(structure.getHint());
        
        getProperties().clear();
        for (org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
    }
}
