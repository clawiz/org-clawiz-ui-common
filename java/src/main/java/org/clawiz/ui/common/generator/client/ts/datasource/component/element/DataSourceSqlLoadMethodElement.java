/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;

public class DataSourceSqlLoadMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("sqlLoad");
        addParameter("id", "number");
        setType("Promise<" + getObjectInterfaceName() + ">");

        addText("");
        addText("return this.executeSql('select "
                + getGenerator().getObjectComponent().getSelectTableColumnsString()
                + " from " + getDataSource().getType().getTableName() + " where id = ?', id)");
        addText("  .then((data) => {");
        addText("");
        addText("    if ( data.rows.length == 0 ) {");
        addText("     return this.clawiz.getPromiseReject('Wrong record id ? in ?', id, '" + getDataSource().getType().getName() + "');");
        addText("    }");
        addText("");
        addText("    let row   = data.rows.item(0);");

        addText("    let object = new " + getObjectImplementationClassName() + "();");
        addText("    object.id  = row.id;");
        addText("");

        int maxLength = getMaxFieldJavaClassVariableNameLength();
        for ( TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            addText("    object." + field.getJavaVariableName()
                    + StringUtils.space(maxLength - field.getJavaVariableName().length())
                    + " = " + prepareFieldToObjectGetter(field,"row." + field.getColumnName()) + ";");
        }
        addText("");

        addText("    return Promise.resolve(object);");
        addText("");
        addText("  })");
        addText("  .catch((reason) => {");
        addText("    return this.clawiz.getPromiseReject('Exception on load table ? record by id ? : ?', this.table.name, id, reason.message, reason);");
        addText("  });");
        addText("");


    }
}
