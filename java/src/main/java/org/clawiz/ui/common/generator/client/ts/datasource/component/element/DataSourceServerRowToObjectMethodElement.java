/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;

public class DataSourceServerRowToObjectMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("serverRowToObject");
        addParameter("row", null);

        addText("let object = new " + getObjectImplementationClassName() + "();");

        int     maxLength       = 0;
        boolean objectsExists   = false;
        boolean promiseDeclared = false;
        //noinspection Duplicates
        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            if ( field.getJavaVariableName().length() > maxLength ) {
                maxLength = field.getJavaVariableName().length();
            }
            if ( field.getValueType() instanceof ValueTypeObject) {
                objectsExists = true;
            }
        }

        addText("");
        addText("let me     = this;");
        addText("");

        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }
            if ( ! ( field.getValueType() instanceof ValueTypeObject ) ) {
                String getter = "row." + field.getJavaVariableName();
                if ( ! (field.getValueType() instanceof ValueTypeBoolean) ) {
                    getter = prepareFieldToObjectGetter(field, getter);
                }
                if ( field.getValueType() instanceof ValueTypeNumber) {
                    getter = getter + " != null ? +" + getter + " : null";
                }
                addText("object." + field.getJavaVariableName()
                        + StringUtils.space(maxLength - field.getJavaVariableName().length())
                        + " = " + getter + ";");
            }
        }
        addText("");

        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            if ( field.getValueType() instanceof ValueTypeObject ) {
                if ( promiseDeclared ) {
                    addText("    return me.guidToId(row." + field.getJavaVariableName() + ");");
                    addText("  })");
                } else {
                    promiseDeclared = true;
                    addText("return me.guidToId(row." + field.getJavaVariableName() + ")");
                }
                addText("  .then((id) => {");
                addText("    object." + field.getJavaVariableName() + " = id;");
            }
        }

        if ( promiseDeclared ) {

            addText("    return me.guidToId(row.id, true, true)");
            addText("             .then((id) => {");
            addText("                             object.id = id;");
            addText("                             return Promise.resolve(object);");
            addText("                           })");
            addText("             .catch((reason) => { ");
            addText("                              return Promise.reject(reason);");
            addText("                           });");
            addText("  })");
            addText("  .catch((reason) => {");
            addText("    return me.clawiz.getPromiseReject('Exception on prepare object from action ? row of table ? and id ? : ?', row.action, row.table_name, row.id, reason.message, reason);");
            addText("  });");
            addText("");

        } else {
            addText("return me.guidToId(row.id, true, true)");
            addText("    .then((id) => {");
            addText("      object.id = id;");
            addText("      return Promise.resolve(object);");
            addText("    })");
            addText("  .catch((reason) => {");
            addText("    return me.clawiz.getPromiseReject('Exception on prepare object from action ? row of table ? and id ? : ?', row.action, row.table_name, row.id, reason.message, reason);");
            addText("  });");
            addText("");
        }




    }
}
