/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.builder.clawiz;

import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.AbstractClientApplicationTypeScriptBuilderComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;

public class ApplicationClawizConfigImplementationPrototypeComponent extends AbstractClientApplicationTypeScriptBuilderComponent {

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(ApplicationClawizConfigImplementationComponent.class).getName() + "Prototype");

        AbstractTypeScriptClassComponent component = getGenerator().getComponentByClass(ApplicationClawizConfigComponent.class);
        addImport(component);
        addImplements(component.getName());

        setExtends(TypeScriptClientCommonGeneratorContext.CLASS_NAME_CLAWIZ_CONFIG_IMPLEMENTATION);

    }
}
