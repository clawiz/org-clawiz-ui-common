package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align;

import org.clawiz.core.common.CoreException;

public enum MpsVericalAlign {

    TOP, BOTTOM, CENTER;

    
    public static MpsVericalAlign toMpsVericalAlign(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsVericalAlign.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsVericalAlign value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsVericalAlign value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case TOP : return "TOP";
            case BOTTOM : return "BOTTOM";
            case CENTER : return "CENTER";
        }
         
        return null;
    }
}
