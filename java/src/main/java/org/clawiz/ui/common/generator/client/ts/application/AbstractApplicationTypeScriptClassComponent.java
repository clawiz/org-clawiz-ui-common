/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptFieldElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.builder.ClawizBuilderComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizComponent;
import org.clawiz.ui.common.generator.client.ts.application.storage.ApplicationDataSourceComponent;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;

public class AbstractApplicationTypeScriptClassComponent extends AbstractTypeScriptClassComponent {

    private TypeScriptClientCommonGenerator applicationGenerator;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        this.applicationGenerator = (TypeScriptClientCommonGenerator) generator;
    }

    @Override
    public TypeScriptClientCommonGenerator getGenerator() {
        return applicationGenerator;
    }

    public String getFieldDereferenceVariableName(TypeField field) {
        return field.getValueType() instanceof ValueTypeObject ? field.getJavaVariableName().substring(0, field.getJavaVariableName().length() - 2) : field.getJavaVariableName();
    }

    @Override
    public void addImport(String name, String path) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getGenerator().getContext().getImportMap(name);
        if (mapObject == null) {
            super.addImport(name, path);
        } else {
            super.addImport(mapObject.name, null, mapObject.path, true);
        }
    }

    public void addImport(String name) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getGenerator().getContext().getImportMap(name);
        if (mapObject == null) {
            throwException("Import map for '?' not defined", name);
        }
        super.addImport(mapObject.name, null, mapObject.path, true);
    }

    public String getImportMapName(String sourceName) {
        return getGenerator().getContext().getImportMapName(sourceName);
    }

    @Override
    public void setExtends(String extendsName) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getGenerator().getContext().getImportMap(extendsName);
        if ( mapObject == null ) {
            super.setExtends(extendsName);
        } else {
            addImport(extendsName);
            super.setExtends(mapObject.name);
        }
    }

    protected void addClawizField() {

        ApplicationClawizComponent clawizComponent = getGenerator().getComponentByClass(ApplicationClawizComponent.class);
        addImport(clawizComponent);

        String fieldName = StringUtils.toLowerFirstChar(clawizComponent.getName());

        TypeScriptFieldElement field = addField(fieldName, clawizComponent.getName());

        TypeScriptMethodElement getter = addMethod("clawiz");
        getter.setMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
        getter.setType(clawizComponent.getName());
        getter.addText("return this." + fieldName + ";");

        TypeScriptMethodElement setter = addMethod("clawiz");
        setter.setMethodModifier(TypeScriptMethodElement.MethodModifier.SET);
        setter.addParameter("clawiz", clawizComponent.getName());
        setter.addText("this." + fieldName + " = clawiz;");

    }


    public void addPrepareDataSourceFieldsMethod() {
        TypeScriptMethodElement method = addMethod("prepareDataSourceFields");

        method.addText("");

        ClawizBuilderComponent builderComponent = getGenerator().getComponentByClass(ClawizBuilderComponent.class);
        addImport(builderComponent);

        method.addText("this.clawiz = " + builderComponent.getName() + ".clawiz;");
        method.addText("");

        int maxLength = 0;
        for (DataSourceGeneratorContext dataSourceGeneratorContext : getGenerator().getContext().getDataSourceContexts() ) {
            DataSource dataSource = dataSourceGeneratorContext.getDataSource();
            if ( dataSource.getJavaClassName().length() > maxLength ) {
                maxLength = dataSource.getJavaClassName().length();
            }
        }

        for (DataSourceGeneratorContext dataSourceGeneratorContext : getGenerator().getContext().getDataSourceContexts() ) {
            DataSource dataSource = dataSourceGeneratorContext.getDataSource();
            String dataSourceClassName = ApplicationDataSourceComponent.getDataSourceClassName(dataSource);
            String fieldName           = StringUtils.toLowerFirstChar(dataSourceClassName);

            addField(fieldName, dataSourceClassName);

            addImport(dataSourceClassName, getGenerator().getContext().getApplicationCommonImportPath());

            method.addText("this."
                    + fieldName + StringUtils.space(maxLength - dataSource.getJavaClassName().length())
                    + " = this.clawiz.storage." + fieldName + ";");
        }


        method.addText("");
    }

    public TypeScriptClientCommonGeneratorContext getApplicationContext() {
        return getGenerator().getContext();
    }

    public String getApplicationJavaClassName() {
        return getGenerator().getContext().getApplicationJavaClassName();
    }


}