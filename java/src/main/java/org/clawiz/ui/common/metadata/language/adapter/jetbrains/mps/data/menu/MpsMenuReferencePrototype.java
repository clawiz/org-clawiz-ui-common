package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsMenuReferencePrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsAbstractMenu menu;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsAbstractMenu getMenu() {
        return this.menu;
    }
    
    public void setMenu(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsAbstractMenu value) {
        this.menu = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2436863927721524858";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.MenuReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721524858", "org.clawiz.ui.common.language.structure.MenuReference", ConceptPropertyType.REFERENCE, "2436863927721525823", "menu"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("2436863927721524858", "menu", getMenu());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
