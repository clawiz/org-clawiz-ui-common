/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.view.form;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.button.action.CancelButtonAction;
import org.clawiz.ui.common.metadata.data.layout.component.button.action.SubmitButtonAction;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.button.action.DiscardButtonAction;
import org.clawiz.ui.common.metadata.data.view.form.button.action.SaveButtonAction;

public class FormGeneratorHelper extends Service {

    public static final String AFTER_PREPARE_FORM_EXTENSION_NAME = "afterPrepareForm";


    public void prepareTopToolbar(Form form) {
    }

    public void prepareBottomToolbar(Form form) {

        form.getBottomToolbar().getRight().add(Button.class).setAction(SaveButtonAction.class);
        form.getBottomToolbar().getRight().add(Button.class).setAction(DiscardButtonAction.class);

    }

    public void prepareToolbars(Form form) {

        prepareTopToolbar(form);
        prepareBottomToolbar(form);

    }

    public void prepare(Form form) {

        prepareToolbars(form);

        processExtensions(AFTER_PREPARE_FORM_EXTENSION_NAME, form);

        form.getTopToolbar().prepareNode(getSession());
        form.getBottomToolbar().prepareNode(getSession());

    }

}
