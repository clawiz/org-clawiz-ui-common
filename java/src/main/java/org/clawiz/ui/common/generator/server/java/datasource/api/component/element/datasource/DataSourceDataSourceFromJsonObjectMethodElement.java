/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component.element.datasource;

import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.metadata.data.type.field.TypeField;

public class DataSourceDataSourceFromJsonObjectMethodElement extends AbstractDataSourceDataSourceJavaClassMethodElement {

    @Override
    public void process() {
        super.process();

        setName("fromJsonObject");

        setType(getRowModelClassName());

        addParameter("JsonObject" , "object");


        addText("");
        addText(getRowModelClassName() + " row = new " + getRowModelClassName() + "();");
        addText("");
        addText("row.setAction(getAsString(object, \"action\"));");
        addText("if ( row.getAction() == null ) {");
        addText("    throwException(\"Row (?) action not defined\", row.getId());");
        addText("}");
        addText("row.setRemoteNodeGuid(getAsString(object, \"id\"));");
        addText("row.setScn(getAsBigDecimal(object, \"scn\"));  ");
        addText("row.setTableName(getAsString(object, \"table_name\"));");
        addText("if ( ! row.getAction().equals(\"NEW\") ) {");
        addText("    row.setId(nodeGuidToObjectId(row.getRemoteNodeGuid()));");
        addText("}");

        for (TypeField field : getDataSource().getType().getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {  continue;  }

            String getter = "(object, \"" + field.getJavaVariableName() + "\")";

            if ( field.getValueType() instanceof ValueTypeDate) {
                getter = "getAsDate" + getter;
            } else if ( field.getValueType() instanceof ValueTypeDateTime) {
                getter = "getAsDateTime" + getter;
            } else if ( field.getValueType() instanceof ValueTypeString) {
                getter = "getAsString" + getter;
            } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
                getter = field.getValueTypeJavaClassName() + ".to" + field.getValueTypeJavaClassName() + "(getAsString" + getter + ")";
            } else if ( field.getValueType() instanceof ValueTypeNumber) {
                getter = "getAsBigDecimal" + getter;
            } else if ( field.getValueType() instanceof ValueTypeObject ) {
                getter = "nodeGuidToObjectId(getAsString" + getter + ")";
            } else if ( field.getValueType() instanceof ValueTypeBoolean) {
                getter = "getAsBoolean" + getter;
            } else {
                throwException("Field ? value type ? not supported  in fromJson deserialization", field.getFullName(), field.getValueType().toString());
            }

            addText("");
            addText("row." + field.getSetMethodName() + "(" + getter + ");");
            if ( field.getValueType() instanceof ValueTypeObject ) {
                addText("if ( row." + field.getGetMethodName() + "() == null && getAsString(object, \"" + field.getJavaVariableName() + "\") != null ) {");
                addText("    throwException(\"Cannot resolve node guid '?' for field '?'\", getAsString(object, \"" + field.getJavaVariableName() + "\"), \"" + field.getFullName() + "\");");
                addText("}");
            }

        }
        addText("");

        addText("return row;");

    }
}
