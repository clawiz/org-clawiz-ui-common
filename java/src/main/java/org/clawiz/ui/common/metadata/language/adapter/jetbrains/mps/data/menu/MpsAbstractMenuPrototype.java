package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractMenuPrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem> items = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem> getItems() {
        return this.items;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111639513022282200";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractMenu";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111639513022282200", "org.clawiz.ui.common.language.structure.AbstractMenu", ConceptPropertyType.CHILD, "6586513253200350260", "items"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getItems() ) {
            addConceptNodeChild("7111639513022282200", "items", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.menu.AbstractMenu.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.menu.AbstractMenu structure = (org.clawiz.ui.common.metadata.data.menu.AbstractMenu) node;
        
        structure.getItems().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem mpsNode : getItems() ) {
            if ( mpsNode != null ) {
                structure.getItems().add((org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem) mpsNode.toMetadataNode(structure, "items"));
            } else {
                structure.getItems().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.menu.AbstractMenu structure = (org.clawiz.ui.common.metadata.data.menu.AbstractMenu) node;
        
        getItems().clear();
        for (org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem metadataNode : structure.getItems() ) {
            if ( metadataNode != null ) {
                getItems().add(loadChildMetadataNode(metadataNode));
            } else {
                getItems().add(null);
            }
        }
        
    }
}
