package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTextPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public Boolean expression;
    
    public Boolean isExpression() {
        return this.expression;
    }
    
    public void setExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.expression = null;
        }
        this.expression = StringUtils.toBoolean(value);
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234832518";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Text";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832518", "org.clawiz.ui.common.language.structure.Text", ConceptPropertyType.PROPERTY, "7111202990234832521", "expression"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("7111202990234832518", "expression", isExpression() != null ? isExpression().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.Text.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.Text structure = (org.clawiz.ui.common.metadata.data.layout.component.Text) node;
        
        structure.setExpression(isExpression());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.Text structure = (org.clawiz.ui.common.metadata.data.layout.component.Text) node;
        
        setExpression(structure.isExpression() ? "true" : "false");
        
    }
}
