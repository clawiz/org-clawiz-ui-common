/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.menu;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.ui.common.metadata.data.menu.Menu;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;

public class MenuGeneratorContext extends AbstractUiGeneratorContext {

    Menu menu;
    String menuName;

    @Override
    public MetadataNode getMetadataNode() {
        return menu;
    }

    @Override
    public void setMetadataNode(MetadataNode metadataNode) {
        setMenu((Menu) metadataNode);
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public void prepare(Session session) {
        if ( menu == null ) {
            menu = session.getService(MetadataBase.class).getNode(Menu.class, getPackageName(), getMenuName(), true);
        }
        if ( menu == null ) {
            throw new CoreException("Unknown UI menu '?'", new Object[]{getPackageName() + "." + getMenuName()});
        }
        menuName = menu.getName();
    }
}
