package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsLocalFormFieldPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType valueType;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.common.valuetype.MpsAbstractValueType value) {
        this.valueType = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6658318051736469874";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.LocalFormField";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736469874", "org.clawiz.ui.common.language.structure.LocalFormField", ConceptPropertyType.CHILD, "6658318051736470185", "valueType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("6658318051736469874", "valueType", getValueType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.form.field.LocalFormField.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.form.field.LocalFormField structure = (org.clawiz.ui.common.metadata.data.view.form.field.LocalFormField) node;
        
        if ( getValueType() != null ) {
            structure.setValueType((org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType) getValueType().toMetadataNode(structure, "valueType"));
        } else {
            structure.setValueType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.form.field.LocalFormField structure = (org.clawiz.ui.common.metadata.data.view.form.field.LocalFormField) node;
        
        if ( structure.getValueType() != null ) {
            setValueType(loadChildMetadataNode(structure.getValueType()));
        } else {
            setValueType(null);
        }
        
    }
}
