/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletProcessSaveMethodElement extends AbstractFormServletMethodElement {

    @Override
    public void process() {
        super.process();

        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");

        setName("processSave");
        addParameter(getRequestContextClassName(), "request");

        if ( getRootType() == null ) {
            return;
        }

        addText("");
        addText("if ( request.getModel() == null) {");
        addText("    throwException(\"Parameter 'model' must be defined\");");
        addText("}");

        String objectClassName = getRootType().getJavaClassName() + "Object";
        String serviceName     = getRootType().getJavaVariableName() + "Service";
        setType(objectClassName);

        addText("");
        addText(objectClassName + " object = request.getId() != null ? " + serviceName + ".load(request.getId()) : " +  serviceName + ".create();");
        addText("");
        for(AbstractFormField field : getUnitedWithRootTypeFormFields() ) {

            if ( field.getTypeField() == null ) {
                continue;
            }

            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }
            if ( field.isReadOnly() ) {
                continue;
            }

            TypeField typeField = field.getTypeField();
            addText("object." + typeField.getSetMethodName() + "(request.getModel()." + field.getGetMethodName() + "());");
        }

        addText("");
        addText("object.save();");
        addText("");
        addText("if ( request.getId() == null ) {");
        addText("    request.setId(object.getId());");
        addText("}");
        addText("");

        addText("return object;");
    }
}
