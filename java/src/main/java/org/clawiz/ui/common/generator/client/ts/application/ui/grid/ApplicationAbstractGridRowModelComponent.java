/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.ui.grid;

import org.clawiz.ui.common.generator.client.ts.application.AbstractApplicationTypeScriptClassComponent;

import static org.clawiz.ui.common.generator.client.ts.application.ui.ApplicationAbstractUiComponentComponent.UI_COMPONENT_PATH;

public class ApplicationAbstractGridRowModelComponent extends AbstractApplicationTypeScriptClassComponent {

    public static final String UI_GRID_COMPONENT_PATH = UI_COMPONENT_PATH + "/grid";

    @Override
    public void process() {
        super.process();
        setDestinationPath(getGenerator().getApplicationDestinationPath() + UI_GRID_COMPONENT_PATH);

        setOverwriteMode(OverwriteMode.SKIP);

        setName("Abstract" + getApplicationJavaClassName() + getGenerator().getBuilderClassNamePrefix() + "GridRowModel");

        setExtendsOfPrototype();

    }
}
