package org.clawiz.ui.common.metadata.data.view.grid.column.property;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class HiddenColumnPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty {
    
    public HiddenColumnProperty withName(String value) {
        setName(value);
        return (HiddenColumnProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
