/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.builder.element;

import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.client.ts.application.AbstractApplicationTypeScriptMethodElement;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigImplementationComponent;

public class ClawizBuilderInitMethodElement extends AbstractApplicationTypeScriptMethodElement {

    ApplicationClawizConfigComponent clawizConfigComponent;
    ApplicationClawizConfigImplementationComponent clawizConfigImplementationComponent;
    ApplicationClawizComponent clawizComponent;
    ApplicationClawizImplementationComponent clawizImplementationComponent;

    @Override
    public void process() {
        super.process();

        setName("init");
        setStatic(true);

        clawizComponent                     = getGenerator().getComponentByClass(ApplicationClawizComponent.class);
        clawizImplementationComponent       = getGenerator().getComponentByClass(ApplicationClawizImplementationComponent.class);
        clawizConfigComponent               = getGenerator().getComponentByClass(ApplicationClawizConfigComponent.class);
        clawizConfigImplementationComponent = getGenerator().getComponentByClass(ApplicationClawizConfigImplementationComponent.class);

        addParameter("config", clawizConfigComponent.getName());

        setType("Promise<" + clawizComponent.getName() + ">");

        addText("");
        addText("if ( config.application == undefined ) { config.application = {} }");
        addText("if ( config.application.name == undefined ) {");
        addText("  config.application.name               = '" + getGenerator().getContext().getApplicationJavaClassName() + "';");
        addText("}");
        addText("");
        addText("if ( config.storage == undefined ) { config.storage = {}; }");
        addText("if ( config.storage.dataSourceApiPath == undefined ) {");
        addText("  config.storage.dataSourceApiPath = '/api/datasource/" + StringUtils.toLowerFirstChar(getGenerator().getContext().getApplicationJavaClassName())  + "';");
        addText("}");
        addText("");

        addText("return " + getComponent().getImportMapName(TypeScriptClientCommonGeneratorContext.CLASS_NAME_CLAWIZ_BUILDER)
                + ".createClawizInstance("
                + clawizImplementationComponent.getName() + ", " + clawizConfigImplementationComponent.getName() + ", config)");
        addText("  .then((clawiz : " + clawizImplementationComponent.getName() + ") => {");
        addText("    " + getComponent().getName() + "." + StringUtils.toLowerFirstChar(clawizComponent.getName()) + " = clawiz;");
        addText("    return Promise.resolve(clawiz);");
        addText("  });");
        addText("");



    }
}
