package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsHintFormFieldPropertyPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsAbstractFormFieldProperty {
    
    public String hint;
    
    public String getHint() {
        return this.hint;
    }
    
    public void setHint(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.hint = null;
        }
        this.hint = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6658318051736470212";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.HintFormFieldProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736470212", "org.clawiz.ui.common.language.structure.HintFormFieldProperty", ConceptPropertyType.PROPERTY, "6658318051736470213", "hint"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("6658318051736470212", "hint", getHint());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.form.field.property.HintFormFieldProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.form.field.property.HintFormFieldProperty structure = (org.clawiz.ui.common.metadata.data.view.form.field.property.HintFormFieldProperty) node;
        
        structure.setHint(getHint());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.form.field.property.HintFormFieldProperty structure = (org.clawiz.ui.common.metadata.data.view.form.field.property.HintFormFieldProperty) node;
        
        setHint(structure.getHint());
        
    }
}
