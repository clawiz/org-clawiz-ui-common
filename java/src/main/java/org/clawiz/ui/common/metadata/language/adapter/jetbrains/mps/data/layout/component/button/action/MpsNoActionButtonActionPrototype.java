package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsNoActionButtonActionPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction {
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "3643318875171793120";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.NoActionButtonAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction structure = (org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction structure = (org.clawiz.ui.common.metadata.data.layout.component.button.action.NoActionButtonAction) node;
        
    }
}
