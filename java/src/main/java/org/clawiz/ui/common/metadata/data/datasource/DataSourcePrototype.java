package org.clawiz.ui.common.metadata.data.datasource;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class DataSourcePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.datasource.filter.DataSourceFilterList filters = new org.clawiz.ui.common.metadata.data.datasource.filter.DataSourceFilterList();
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type type;
    
    public DataSource withName(String value) {
        setName(value);
        return (DataSource) this;
    }
    
    public org.clawiz.ui.common.metadata.data.datasource.filter.DataSourceFilterList getFilters() {
        return this.filters;
    }
    
    public DataSource withFilter(org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter value) {
        getFilters().add(value);
        return (DataSource) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter> T createFilter(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter value = createChildNode(nodeClass, "filters");
        getFilters().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter createFilter() {
        return createFilter(org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.type = value;
    }
    
    public DataSource withType(org.clawiz.core.common.metadata.data.type.Type value) {
        setType(value);
        return (DataSource) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getFilters()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getType() != null ) { 
            getType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getFilters()) {
            references.add(node);
        }
        
        references.add(getType());
        
    }
}
