/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.datasource;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeService;
import org.clawiz.core.common.storage.remotenodeguid.RemoteNodeGuidObject;
import org.clawiz.core.common.storage.remotenodeguid.RemoteNodeGuidService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import org.clawiz.ui.common.portal.application.datasource.execute.DataSourceExecuteContext;
import org.clawiz.ui.common.portal.application.datasource.execute.DataSourceExecuteFilter;
import org.clawiz.ui.common.portal.application.datasource.model.DataSourceRowModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public abstract class AbstractDataSource<R extends DataSourceRowModel> extends Service {

    ObjectService         objectService;
    RemoteNodeGuidService remoteNodeGuidService;
    RemoteNodeService     remoteNodeService;


    public String getDateFormat() {
        return DateUtils.ISO8601_DATE_FORMAT;
    }

    public String getDateTimeFormat() {
        return DateUtils.ISO8601_DATE_TIME_FORMAT;
    }

    public BigDecimal getTypeId() {
        return null;
    }

    public String getAsString(JsonObject object, String name) {
        JsonElement element = object.get(name);
        return element != null && ! (element instanceof JsonNull) ? element.getAsString() : null;
    }

    public BigDecimal getAsBigDecimal(JsonObject object, String name) {
        JsonElement element = object.get(name);
        return element != null && ! (element instanceof JsonNull) ? StringUtils.toBigDecimal(element.getAsString()) : null;
    }

    public Date getAsDate(JsonObject object, String name) {
        JsonElement element = object.get(name);
        if ( element == null || (element instanceof JsonNull) ) {
            return null;
        }
        String str = element.getAsString();
        if ( StringUtils.isEmpty( str )) {
            return null;
        }
        return DateUtils.toDate(str, getDateFormat());
    }

    public Date getAsDateTime(JsonObject object, String name) {
        JsonElement element = object.get(name);
        if ( element == null || (element instanceof JsonNull) ) {
            return null;
        }
        String str = element.getAsString();
        if ( StringUtils.isEmpty( str )) {
            return null;
        }
        return DateUtils.toDate(str, getDateTimeFormat());
    }

    public boolean getAsBoolean(JsonObject object, String name) {
        JsonElement element = object.get(name);
        return element != null && ! (element instanceof JsonNull) ? element.getAsBoolean() : false;
    }


    public void set(JsonObject object, String name, String value) {
        if ( value != null ) {
            object.addProperty(name, value);
        }
    }

    public void set(JsonObject object, String name, Date value) {
        if ( value != null ) {
            object.addProperty(name,  DateUtils.toString(value, getDateFormat()));
        }
    }

    public void setDateTime(JsonObject object, String name, Date value) {
        if ( value != null ) {
            object.addProperty(name,  DateUtils.toString(value, getDateTimeFormat()));
        }
    }

    public void set(JsonObject object, String name, BigDecimal value) {
        if ( value != null ) {
            object.addProperty(name, value.toString());
        }
    }

    public void set(JsonObject object, String name, boolean value) {
        object.addProperty(name, value);
    }

    public BigDecimal nodeGuidToObjectId(String nodeGuid) {

        if ( nodeGuid == null ) {
            return null;
        }

        BigDecimal objectId;
        if ( getSession().getRemoteNodeId() != null ) {
            objectId = executeQueryBigDecimal(
                    "select object_id from cw_core_remote_node_guids where remote_node_id = ? and upper_node_guid = ?"
                    , getSession().getRemoteNodeId()
                    , nodeGuid.toUpperCase()
            );
            if ( objectId != null ) {
                return objectId;
            }
        }



        objectId = objectService.guidToId(nodeGuid);
        if ( objectId == null ) {
            throwException("Unknown node guid ?", nodeGuid);
        }

        return objectId;

    }

    public String objectIdToNodeGuid(DataSourceExecuteContext context, BigDecimal objectId) {
        return context.objectIdToRemoteNodeGuid(objectId);
    }


    public String objectIdToNodeGuid(BigDecimal objectId) {

        if ( objectId == null ) {
            return null;
        }

        String guid;
        if ( getSession().getRemoteNodeId() != null ) {
            guid = executeQueryString("select node_guid from cw_core_remote_node_guids where remote_node_id = ? and object_id = ?", getSession().getRemoteNodeId(), objectId);
            if ( guid != null ) {
                return guid;
            }
        }


        guid = objectService.idToGuid(objectId);
        if ( guid == null ) {
            throwException("Unknown object id ?", objectId);
        }

        return guid;

    }

    public void saveNodeGuid(BigDecimal objectId, String nodeGuid) {

        BigDecimal remoteNodeId = getSession().getRemoteNodeId();
        if ( remoteNodeId == null ) {
            throwException("Remote node not logged in");
        }

        if ( nodeGuid == null || objectId == null) {
            throwException("Node guid and object id cannot be null");
        }

        String guid = objectService.idToGuid(objectId);
        if ( guid == null ) {
            throwException("Unknown object id ?", objectId);
        }

        if ( guid.equals(nodeGuid) ) {
            return;
        }

        BigDecimal guidId = remoteNodeGuidService.remoteNodeGuidToId(remoteNodeId, nodeGuid);
        if ( guidId != null ) {
            return;
        }

        RemoteNodeGuidObject object = remoteNodeGuidService.create();
        object.setRemoteNodeId(remoteNodeId);
        object.setObjectId(objectId);
        object.setNodeGuid(nodeGuid);

        object.save();

    }

    public void deleteNodeGuid(String nodeGuid) {
        BigDecimal remoteNodeId = getSession().getRemoteNodeId();
        if ( remoteNodeId == null ) {
            throwException("Remote node not logged in");
        }

        if ( nodeGuid == null ) {
            throwException("Node guid cannot be null");
        }

        BigDecimal id = remoteNodeGuidService.remoteNodeGuidToId(remoteNodeId, nodeGuid);

        if ( id != null ) {
            remoteNodeGuidService.delete(id);
        } else {
            // nothing to delete if remote guid is server guid
//            if ( objectService.guidToId(nodeGuid) == null ) {
                throwException("Unknown remote node @ guid ?", remoteNodeId, nodeGuid);
//            }
        }

    }


    public JsonObject toJsonObject(R row) {
        return null;
    }

    public R fromJsonObject(JsonObject object) {
        return null;
    }

    public BigDecimal getLastReceivedScn() {
        if ( getSession().getRemoteNodeId() == null ) {
            throwException("Remote node not logged in");
        }
        return remoteNodeService.load(getSession().getRemoteNodeId()).getLastReceivedScn();
    }

    public void saveLastReceivedScn(BigDecimal scn) {
        if ( getSession().getRemoteNodeId() == null ) {
            throwException("Remote node not logged in");
        }
        RemoteNodeObject object = remoteNodeService.load(getSession().getRemoteNodeId());
        object.setLastReceivedScn(scn);
        object.save();
    }

    protected void addContextFilters(ArrayList<DataSourceExecuteFilter> filters, StringBuilder sql, ArrayList<Object> parameters) {
        String prefix = " and ( ";
        boolean found = false;
        for (DataSourceExecuteFilter filter : filters ) {
            sql.append(prefix).append("( ").append(filter.getWhereCondition()).append(" )").append("\n");
            for ( Object obj : filter.getParameters() ) {
                parameters.add(obj);
            }
            prefix = "      or ";
            found = true;
        }
        if ( found ) {
            sql.append(")\n");
        }

    }

    public void loadChanges(DataSourceExecuteContext context) {

    }

    public void loadChanges(BigDecimal objectId, String tableName, DataSourceExecuteContext context) {
    }

    public void saveChange(R row) {

    }

    public DataSourceExecuteContext createExecuteContext(DataSourceExecuteContext parentContext) {
        return null;
    }
}
