/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.builder.clawiz;

import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMemberElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.client.ts.application.builder.AbstractClientApplicationTypeScriptBuilderComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.ui.router.ApplicationRouterServiceComponent;

public class ApplicationClawizImplementationPrototypeComponent extends AbstractClientApplicationTypeScriptBuilderComponent {

    ApplicationClawizConfigComponent   configComponent;
    String                             storageServiceClassName;
    
    protected void addGetStorageServiceMethod() {

        TypeScriptMethodElement method = addMethod("getStorageService");
        method.setAccessLevel(TypeScriptMemberElement.AccessLevel.PROTECTED);
        method.setType("Promise<" + storageServiceClassName + ">");

        method.addText("return this.getService(" + storageServiceClassName + ");");

    }

    protected void addGetRouterServiceMethod() {

        ApplicationRouterServiceComponent routerComponent = getGenerator().getComponentByClass(ApplicationRouterServiceComponent.class);


        addImport(routerComponent);


        TypeScriptMethodElement method = addMethod("getRouterService");
        method.setAccessLevel(TypeScriptMemberElement.AccessLevel.PROTECTED);
        method.setType("Promise<" + routerComponent.getName() + ">");

        method.addText("return this.getService(" + routerComponent.getName() + ");");

    }

    protected void addStorageField() {

        String fieldName = StringUtils.toLowerFirstChar(getApplicationJavaClassName()) + "Storage";
        addField(fieldName, storageServiceClassName);

        TypeScriptMethodElement getter = addMethod("storage");
        getter.setMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
        getter.addText("return this." + fieldName + ";");

        TypeScriptMethodElement setter = addMethod("storage");
        setter.setMethodModifier(TypeScriptMethodElement.MethodModifier.SET);
        setter.addParameter("storage", storageServiceClassName);
        setter.addText("this." + fieldName + " = storage;");

    }


    protected void addConfigField() {

        String fieldName = StringUtils.toLowerFirstChar(getApplicationJavaClassName()) + "Config";
        addField(fieldName, configComponent.getName());

        TypeScriptMethodElement getter = addMethod("config");
        getter.setMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
        getter.addText("return this." + fieldName + ";");

        TypeScriptMethodElement setter = addMethod("config");
        setter.setMethodModifier(TypeScriptMethodElement.MethodModifier.SET);
        setter.addParameter("config", configComponent.getName());
        setter.addText("this." + fieldName + " = config;");

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(ApplicationClawizImplementationComponent.class).getName() + "Prototype");


        AbstractTypeScriptClassComponent component = getGenerator().getComponentByClass(ApplicationClawizComponent.class);
        addImport(component);
        addImplements(component.getName());

        setExtends(TypeScriptClientCommonGeneratorContext.CLASS_NAME_CLAWIZ_IMPLEMENTATION);

        addImport(TypeScriptClientCommonGeneratorContext.CLASS_NAME_APPLICATION_STORAGE_SERVICE);
        storageServiceClassName = getImportMapName(TypeScriptClientCommonGeneratorContext.CLASS_NAME_APPLICATION_STORAGE_SERVICE);
        addStorageField();

        configComponent = getGenerator().getComponentByClass(ApplicationClawizConfigComponent.class);
        addImport(configComponent);
        addConfigField();

        addGetStorageServiceMethod();
        addGetRouterServiceMethod();

    }
}
