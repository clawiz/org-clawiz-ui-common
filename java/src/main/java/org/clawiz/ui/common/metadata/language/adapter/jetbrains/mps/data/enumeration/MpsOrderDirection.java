package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.enumeration;

import org.clawiz.core.common.CoreException;

public enum MpsOrderDirection {

    ASC, DESC;

    
    public static MpsOrderDirection toMpsOrderDirection(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsOrderDirection.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsOrderDirection value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsOrderDirection value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case ASC : return "ASC";
            case DESC : return "DESC";
        }
         
        return null;
    }
}
