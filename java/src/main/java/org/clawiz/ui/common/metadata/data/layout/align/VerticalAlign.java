package org.clawiz.ui.common.metadata.data.layout.align;

import org.clawiz.core.common.CoreException;

public enum VerticalAlign {

    TOP, BOTTOM, CENTER;

    
    public static VerticalAlign toVerticalAlign(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return VerticalAlign.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong VerticalAlign value '?'", value);
        }
        
    }
    
    public static String toDescription(VerticalAlign value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case TOP   : return "top";
            case BOTTOM: return "bottom";
            case CENTER: return "center";
        }
        
        return value.toString();
    }
}
