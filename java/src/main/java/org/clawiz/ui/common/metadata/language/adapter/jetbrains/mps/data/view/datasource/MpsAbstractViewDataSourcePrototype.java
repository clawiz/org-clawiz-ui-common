package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractViewDataSourcePrototype extends AbstractMpsNode {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType type;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.type = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4089903107537306848";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractViewDataSource";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "4089903107537306848", "org.clawiz.ui.common.language.structure.AbstractViewDataSource", ConceptPropertyType.REFERENCE, "4089903107537336132", "type"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("4089903107537306848", "type", getType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource structure = (org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource) node;
        
        if ( getType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "type", false, getType());
        } else {
            structure.setType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource structure = (org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource) node;
        
        if ( structure.getType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "type", false, structure.getType());
        } else {
            setType(null);
        }
        
    }
}
