package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractGridColumnPrototype extends AbstractMpsNode {
    
    public String caption;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsAbstractGridColumnProperty> properties = new ArrayList<>();
    
    public String getCaption() {
        return this.caption;
    }
    
    public void setCaption(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.caption = null;
        }
        this.caption = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsAbstractGridColumnProperty> getProperties() {
        return this.properties;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "968894741575256671";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractGridColumn";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "968894741575256671", "org.clawiz.ui.common.language.structure.AbstractGridColumn", ConceptPropertyType.PROPERTY, "8035881083538386056", "caption"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "968894741575256671", "org.clawiz.ui.common.language.structure.AbstractGridColumn", ConceptPropertyType.CHILD, "968894741575457401", "properties"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("968894741575256671", "caption", getCaption());
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("968894741575256671", "properties", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn) node;
        
        structure.setCaption(getCaption());
        
        structure.getProperties().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsAbstractGridColumnProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn) node;
        
        setCaption(structure.getCaption());
        
        getProperties().clear();
        for (org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
    }
}
