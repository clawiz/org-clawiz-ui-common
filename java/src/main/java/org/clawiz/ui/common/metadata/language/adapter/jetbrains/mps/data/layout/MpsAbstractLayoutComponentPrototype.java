package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractLayoutComponentPrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign horizontalAlign;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsVerticalAlign verticalAlign;
    
    public String text;
    
    public String onClickExpression;
    
    public String visibleExpression;
    
    public String enabledExpression;
    
    public String disabledExpression;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign getHorizontalAlign() {
        return this.horizontalAlign;
    }
    
    public void setHorizontalAlign(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.horizontalAlign = null;
        }
        this.horizontalAlign = org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign.toMpsHorizontalAlign(value);
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsVerticalAlign getVerticalAlign() {
        return this.verticalAlign;
    }
    
    public void setVerticalAlign(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.verticalAlign = null;
        }
        this.verticalAlign = org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsVerticalAlign.toMpsVerticalAlign(value);
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.text = null;
        }
        this.text = value;
    }
    
    public String getOnClickExpression() {
        return this.onClickExpression;
    }
    
    public void setOnClickExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.onClickExpression = null;
        }
        this.onClickExpression = value;
    }
    
    public String getVisibleExpression() {
        return this.visibleExpression;
    }
    
    public void setVisibleExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.visibleExpression = null;
        }
        this.visibleExpression = value;
    }
    
    public String getEnabledExpression() {
        return this.enabledExpression;
    }
    
    public void setEnabledExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.enabledExpression = null;
        }
        this.enabledExpression = value;
    }
    
    public String getDisabledExpression() {
        return this.disabledExpression;
    }
    
    public void setDisabledExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.disabledExpression = null;
        }
        this.disabledExpression = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200386825";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractLayoutComponent";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7594984129459066676", "horizontalAlign"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7594984129459066678", "verticalAlign"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7111202990234873220", "text"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7111202990234873198", "onClickExpression"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7111202990234873202", "visibleExpression"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7111202990234873207", "enabledExpression"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200386825", "org.clawiz.ui.common.language.structure.AbstractLayoutComponent", ConceptPropertyType.PROPERTY, "7111202990234873213", "disabledExpression"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("6586513253200386825", "horizontalAlign", org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign.toConceptNodePropertyString(getHorizontalAlign()));
        addConceptNodeProperty("6586513253200386825", "verticalAlign", org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsVerticalAlign.toConceptNodePropertyString(getVerticalAlign()));
        addConceptNodeProperty("6586513253200386825", "text", getText());
        addConceptNodeProperty("6586513253200386825", "onClickExpression", getOnClickExpression());
        addConceptNodeProperty("6586513253200386825", "visibleExpression", getVisibleExpression());
        addConceptNodeProperty("6586513253200386825", "enabledExpression", getEnabledExpression());
        addConceptNodeProperty("6586513253200386825", "disabledExpression", getDisabledExpression());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent) node;
        
        structure.setHorizontalAlign(( getHorizontalAlign() != null ? org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign.valueOf(getHorizontalAlign().toString()) : null ));
        
        structure.setVerticalAlign(( getVerticalAlign() != null ? org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign.valueOf(getVerticalAlign().toString()) : null ));
        
        structure.setText(getText());
        
        structure.setOnClickExpression(getOnClickExpression());
        
        structure.setVisibleExpression(getVisibleExpression());
        
        structure.setEnabledExpression(getEnabledExpression());
        
        structure.setDisabledExpression(getDisabledExpression());
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent) node;
        
        setHorizontalAlign(structure.getHorizontalAlign() != null ? structure.getHorizontalAlign().toString() : null);
        
        setVerticalAlign(structure.getVerticalAlign() != null ? structure.getVerticalAlign().toString() : null);
        
        setText(structure.getText());
        
        setOnClickExpression(structure.getOnClickExpression());
        
        setVisibleExpression(structure.getVisibleExpression());
        
        setEnabledExpression(structure.getEnabledExpression());
        
        setDisabledExpression(structure.getDisabledExpression());
        
    }
}
