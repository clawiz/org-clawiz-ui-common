package org.clawiz.ui.common.metadata.data.view.form.button.action;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class SaveButtonActionPrototype extends org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction {
    
    public SaveButtonAction withName(String value) {
        setName(value);
        return (SaveButtonAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
