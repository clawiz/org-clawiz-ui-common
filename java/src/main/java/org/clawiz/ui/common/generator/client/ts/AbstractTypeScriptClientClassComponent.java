/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts;

import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;

public abstract class AbstractTypeScriptClientClassComponent extends AbstractTypeScriptClassComponent {


    abstract protected TypeScriptClientCommonGenerator getApplicationGenerator();

    public String getFieldDereferenceVariableName(TypeField field) {
        return field.getValueType() instanceof ValueTypeObject ? field.getJavaVariableName().substring(0, field.getJavaVariableName().length()-2) : field.getJavaVariableName();
    }

    @Override
    public void addImport(String name, String path) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getApplicationGenerator().getContext().getImportMap(name);
        if (mapObject == null) {
            super.addImport(name, path);
        } else {
            super.addImport(mapObject.name, null, mapObject.path, true);
        }
    }

    public void addImport(String name) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getApplicationGenerator().getContext().getImportMap(name);
        if (mapObject == null) {
            throwException("Import map for '?' not defined", name);
        }
        super.addImport(mapObject.name, null, mapObject.path, true);
    }

    public String getImportMapName(String sourceName) {
        return getApplicationGenerator().getContext().getImportMapName(sourceName);
    }

    @Override
    public void setExtends(String extendsName) {
        TypeScriptClientCommonGeneratorContext.ImportMapObject mapObject = getApplicationGenerator().getContext().getImportMap(extendsName);
        if ( mapObject == null ) {
            super.setExtends(extendsName);
        } else {
            addImport(extendsName);
            super.setExtends(mapObject.name);
        }
    }



}
