package org.clawiz.ui.common.metadata.data.datasource.filter;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractDataSourceFilterPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractDataSourceFilter withName(String value) {
        setName(value);
        return (AbstractDataSourceFilter) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
