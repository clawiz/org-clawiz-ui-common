package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsDataSourcePrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.filter.MpsAbstractDataSourceFilter> filters = new ArrayList<>();
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType type;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.filter.MpsAbstractDataSourceFilter> getFilters() {
        return this.filters;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.type = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2062302247216485517";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.DataSource";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2062302247216485517", "org.clawiz.ui.common.language.structure.DataSource", ConceptPropertyType.CHILD, "2062302247217450955", "filters"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2062302247216485517", "org.clawiz.ui.common.language.structure.DataSource", ConceptPropertyType.REFERENCE, "2062302247216510482", "type"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getFilters() ) {
            addConceptNodeChild("2062302247216485517", "filters", value);
        }
        addConceptNodeRef("2062302247216485517", "type", getType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.datasource.DataSource.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.datasource.DataSource structure = (org.clawiz.ui.common.metadata.data.datasource.DataSource) node;
        
        structure.getFilters().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.filter.MpsAbstractDataSourceFilter mpsNode : getFilters() ) {
            if ( mpsNode != null ) {
                structure.getFilters().add((org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter) mpsNode.toMetadataNode(structure, "filters"));
            } else {
                structure.getFilters().add(null);
            }
        }
        
        if ( getType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "type", false, getType());
        } else {
            structure.setType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.datasource.DataSource structure = (org.clawiz.ui.common.metadata.data.datasource.DataSource) node;
        
        getFilters().clear();
        for (org.clawiz.ui.common.metadata.data.datasource.filter.AbstractDataSourceFilter metadataNode : structure.getFilters() ) {
            if ( metadataNode != null ) {
                getFilters().add(loadChildMetadataNode(metadataNode));
            } else {
                getFilters().add(null);
            }
        }
        
        if ( structure.getType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "type", false, structure.getType());
        } else {
            setType(null);
        }
        
    }
}
