package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractMenuItemPrototype extends AbstractMpsNode {
    
    public String title;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem> items = new ArrayList<>();
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.title = null;
        }
        this.title = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem> getItems() {
        return this.items;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200275715";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractMenuItem";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200275715", "org.clawiz.ui.common.language.structure.AbstractMenuItem", ConceptPropertyType.PROPERTY, "6586513253200324499", "title"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200275715", "org.clawiz.ui.common.language.structure.AbstractMenuItem", ConceptPropertyType.CHILD, "6586513253200386839", "items"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("6586513253200275715", "title", getTitle());
        for (AbstractMpsNode value : getItems() ) {
            addConceptNodeChild("6586513253200275715", "items", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem structure = (org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem) node;
        
        structure.setTitle(getTitle());
        
        structure.getItems().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem mpsNode : getItems() ) {
            if ( mpsNode != null ) {
                structure.getItems().add((org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem) mpsNode.toMetadataNode(structure, "items"));
            } else {
                structure.getItems().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem structure = (org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem) node;
        
        setTitle(structure.getTitle());
        
        getItems().clear();
        for (org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem metadataNode : structure.getItems() ) {
            if ( metadataNode != null ) {
                getItems().add(loadChildMetadataNode(metadataNode));
            } else {
                getItems().add(null);
            }
        }
        
    }
}
