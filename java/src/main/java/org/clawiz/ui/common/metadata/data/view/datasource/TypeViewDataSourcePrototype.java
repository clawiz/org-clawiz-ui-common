package org.clawiz.ui.common.metadata.data.view.datasource;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeViewDataSourcePrototype extends org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource {
    
    public TypeViewDataSource withName(String value) {
        setName(value);
        return (TypeViewDataSource) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
