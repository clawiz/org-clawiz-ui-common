/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptComponent;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.datasource.component.*;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGenerator;

public class ClientTypeScriptDataSourceGenerator extends DataSourceGenerator {

    ObjectImplementationComponent objectComponent;

    ClientTypeScriptDataSourceGeneratorContext webTypeScriptDataSourceGeneratorContext;


    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        webTypeScriptDataSourceGeneratorContext = (ClientTypeScriptDataSourceGeneratorContext) context;
    }

    @Override
    public ClientTypeScriptDataSourceGeneratorContext getContext() {
        return webTypeScriptDataSourceGeneratorContext;
    }

    public String getApplicationDestinationPath() {
        return getApplicationGenerator().getApplicationDestinationPath();
    }

    protected <T extends AbstractTypeScriptComponent> T addDataSourceComponent(Class<T> clazz) {
        T component =  super.addComponent(clazz);
        component.setDestinationPath(getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), getPackageName())
                + "/" + getDataSource().getJavaClassName().toLowerCase());
        return component;
    }


    public TypeScriptClientCommonGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }

    public ObjectImplementationComponent getObjectComponent() {
        return objectComponent;
    }

    @Override
    protected void process() {
        super.process();

       for (TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeEnumeration) {
                addDataSourceComponent(FieldValueTypeEnumComponent.class).setField(field);
            }
        }
        addDataSourceComponent(DataModelComponent.class);
        addDataSourceComponent(DataModelPrototypeComponent.class);

        objectComponent = addDataSourceComponent(ObjectImplementationComponent.class);
        addDataSourceComponent(ObjectImplementationPrototypeComponent.class);

        addDataSourceComponent(DataSourceComponent.class);
        addDataSourceComponent(DataSourcePrototypeComponent.class);

        addDataSourceComponent(DataSourceIndexComponent.class);

    }
}
