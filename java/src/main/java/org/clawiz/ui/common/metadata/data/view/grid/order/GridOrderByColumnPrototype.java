package org.clawiz.ui.common.metadata.data.view.grid.order;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class GridOrderByColumnPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByDirection direction;
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn column;
    
    public GridOrderByColumn withName(String value) {
        setName(value);
        return (GridOrderByColumn) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByDirection getDirection() {
        return this.direction;
    }
    
    public void setDirection(org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByDirection value) {
        this.direction = value;
    }
    
    public GridOrderByColumn withDirection(org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByDirection value) {
        setDirection(value);
        return (GridOrderByColumn) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn getColumn() {
        return this.column;
    }
    
    public void setColumn(org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn value) {
        this.column = value;
    }
    
    public GridOrderByColumn withColumn(org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn value) {
        setColumn(value);
        return (GridOrderByColumn) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getColumn() != null ) { 
            getColumn().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getColumn());
        
    }
}
