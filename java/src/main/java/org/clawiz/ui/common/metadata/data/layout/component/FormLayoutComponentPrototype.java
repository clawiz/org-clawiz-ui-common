package org.clawiz.ui.common.metadata.data.layout.component;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class FormLayoutComponentPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.form.Form form;
    
    public FormLayoutComponent withName(String value) {
        setName(value);
        return (FormLayoutComponent) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.Form getForm() {
        return this.form;
    }
    
    public void setForm(org.clawiz.ui.common.metadata.data.view.form.Form value) {
        this.form = value;
    }
    
    public FormLayoutComponent withForm(org.clawiz.ui.common.metadata.data.view.form.Form value) {
        setForm(value);
        return (FormLayoutComponent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getForm() != null ) { 
            getForm().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getForm());
        
    }
}
