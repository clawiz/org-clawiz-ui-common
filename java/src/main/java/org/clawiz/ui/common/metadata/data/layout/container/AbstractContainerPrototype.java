package org.clawiz.ui.common.metadata.data.layout.container;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractContainerPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.LayoutComponentList components = new org.clawiz.ui.common.metadata.data.layout.LayoutComponentList();
    
    public AbstractContainer withName(String value) {
        setName(value);
        return (AbstractContainer) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.LayoutComponentList getComponents() {
        return this.components;
    }
    
    public AbstractContainer withComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value) {
        getComponents().add(value);
        return (AbstractContainer) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent> T createComponent(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value = createChildNode(nodeClass, "components");
        getComponents().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent createComponent() {
        return createComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getComponents()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getComponents()) {
            references.add(node);
        }
        
    }
}
