/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element.choosevaluerequest;


import org.clawiz.core.common.system.generator.java.component.element.JavaBlockElement;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.jdbc.JdbcSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletPrototypeComponent;

public class FormApiServletProcessJdbcSelectChooseValueRequestMethodElement extends AbstractFormApiServletQuerySelectValuesMethodElement {


    @Override
    public void process() {
        super.process();

        JdbcSelectValue vcc = (JdbcSelectValue) getFormField().getSelectValueContext();

        if ( vcc.getDataSource() != null ) {
            addText("DataSource dataSource = Core.createDataSource(");
            addText("    \"" +      encodeJavaQuotes(vcc.getDataSource().getConfig().getUrl()) + "\"");
            addText("    ,\"" + encodeJavaQuotes(vcc.getDataSource().getConfig().getUser()) + "\"");
            addText("    ,\"" + encodeJavaQuotes(vcc.getDataSource().getConfig().getPassword()) + "\");");
            addText("Session requestSession = Core.newSession(dataSource, null);");
        } else {
            addText("Session requestSession = getSession();");
        }

        addText("Statement statement = requestSession.executeQuery(\""  + encodeJavaQuotes(vcc.getSelectStatement()) + " \", new Object[]{requestContext.getInputValue()});");

        addText("while ( statement.next() ) {");
        addText("");
        addText("   requestContext.getValues().addValue(");
        addText("            statement.getObject(1)");
        addText("            , (statement.getColumns().size() > 1 ? statement.getString(2) : statement.getObject(1).toString())");
        addText("    );");
        addText("");
        addText("}");
        addText("statement.close();");
        if ( vcc.getDataSource() != null ) {
            addText("requestSession.destroy();");
        }

        JavaBlockElement block = getGenerator().getComponentByClass(FormApiServletPrototypeComponent.class).getProcessDoPost().getActionSwitch();
        block.addText("//noinspection Duplicates");
        block.addText("if ( \"" + getQuerySelectValuesMethodName(getFormField()) + "\".equals(request.getAction()) ) {");

        block.addText("    ChooseValueRequestContext requestContext = new DefaultChooseValueRequestContext();");
        block.addText("    requestContext.setInputValue(request.getHttpRequest().getParameterValue(\"inputValue\"));");
        block.addText("    " + getName() + "(request.getHttpRequest(), requestContext);");
        block.addText("    ");
        block.addText("    " + getResponseContextClassName() + " response = new " + getResponseContextClassName() + "();");
        block.addText("    response.setChooseValues(requestContext);");
        block.addText("    ");
        block.addText("    return response;");
        block.addText("}");

    }
}
