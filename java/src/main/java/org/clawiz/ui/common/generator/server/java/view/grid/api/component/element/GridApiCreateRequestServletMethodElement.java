/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid.api.component.element;


import org.clawiz.ui.common.generator.server.java.view.grid.component.element.AbstractGridServletMethodElement;

public class GridApiCreateRequestServletMethodElement extends AbstractGridServletMethodElement {

    public static final String ACTION_PARAMETER_NAME = "action";
    public static final String PAGE_PARAMETER_NAME   = "page";

    @Override
    public void process() {
        super.process();
        setName("createRequest");
        addParameter("AbstractHttpRequestContext", "httpRequest");

        setType(getGenerator().getRequestContextClassName());

        addText(getGenerator().getRequestContextClassName() + " request   = new " + getGenerator().getRequestContextClassName() + "();");
        addText("");
        addText("request.setHttpRequest(httpRequest);");
        addText("request.setAction(httpRequest.getParameterValue(\"" + ACTION_PARAMETER_NAME + "\"));");
        addText("");

        addText("String startRowStr = (httpRequest.getParameterValue(\"startRow\"));");
        addText("if ( startRowStr != null ) {");
        addText("    request.setStartRow(StringUtils.toBigDecimal(startRowStr).intValue());");
        addText("}");
        addText("");
        addText("String endRowStr = (httpRequest.getParameterValue(\"endRow\"));");
        addText("if ( startRowStr != null ) {");
        addText("    request.setEndRow(StringUtils.toBigDecimal(endRowStr).intValue());");
        addText("}");
        addText("");

        addText(getGenerator().getParametersClassName() + " parameters    = new " + getGenerator().getParametersClassName() + "();");
        addText("request.setParameters(parameters);");
        addText("");
        addText("return request;");
    }
}
