package org.clawiz.ui.common.metadata.data.view.form.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractFormFieldPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String label;
    
    @ExchangeAttribute
    private String hint;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.form.field.property.FormFieldPropertyList properties = new org.clawiz.ui.common.metadata.data.view.form.field.property.FormFieldPropertyList();
    
    public AbstractFormField withName(String value) {
        setName(value);
        return (AbstractFormField) this;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public void setLabel(String value) {
        this.label = value;
    }
    
    public AbstractFormField withLabel(String value) {
        setLabel(value);
        return (AbstractFormField) this;
    }
    
    public String getHint() {
        return this.hint;
    }
    
    public void setHint(String value) {
        this.hint = value;
    }
    
    public AbstractFormField withHint(String value) {
        setHint(value);
        return (AbstractFormField) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.field.property.FormFieldPropertyList getProperties() {
        return this.properties;
    }
    
    public AbstractFormField withPropertie(org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty value) {
        getProperties().add(value);
        return (AbstractFormField) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty createPropertie() {
        return createPropertie(org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
    }
}
