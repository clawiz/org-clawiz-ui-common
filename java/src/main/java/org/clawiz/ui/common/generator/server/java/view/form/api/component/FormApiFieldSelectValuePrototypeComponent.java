/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component;

import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.component.AbstractFormJavaClassComponent;

public class FormApiFieldSelectValuePrototypeComponent extends AbstractFormJavaClassComponent {


    AbstractFormField formField;
    JavaMethodElement prepareMethod;


    public AbstractFormField getFormField() {
        return formField;
    }

    public void setFormField(AbstractFormField formField) {
        this.formField = formField;
    }

    public AbstractSelectValue getSelectValueContext(){
        return getFormField().getSelectValueContext();
    }

    protected void addImports() {

        addImport(SelectValue.class);
        addImport(Session.class);

    }

    protected void processTypeContext(TypeSelectValue context) {

        StringBuilder textExpression = new StringBuilder();
        String        textPrefix     = "";

        for (TypeKeyField keyField : context.getToStringKey().getFields() ) {
            TypeField field = keyField.getField();

            textExpression.append(textPrefix);
            textPrefix = " + \" \" + ";

            String getter = "getData()."
                    + (field.getValueType() instanceof ValueTypeBoolean ? "is" : "get")
                    + StringUtils.toUpperFirstChar(field.getJavaVariableName()) + "()";
            if ( field.getValueType() instanceof ValueTypeNumber
                    || field.getValueType() instanceof ValueTypeBoolean
                    || field.getValueType() instanceof ValueTypeEnumeration
                    ) {
                getter = "( " + getter + " != null ? " + getter + ".toString() : null )";
            }

            textExpression.append(getter);

        }

        prepareMethod.addText("text = " + textExpression.toString() + ";");
    }


    @Override
    public void process() {
        super.process();



        boolean found = false;
        for (FormApiFieldSelectValueComponent selectValueComponent : getGenerator().getComponentListByClass(FormApiFieldSelectValueComponent.class) ) {
            if ( selectValueComponent.getFormField() == getFormField() ) {
                setName(selectValueComponent.getName() + "Prototype");
                found = true;
                break;
            }
        }

        if ( ! found ) {
            throwException("Internal error. Generator does not contain component FormApiFieldSelectValueComponent for field ?", getFormField().getName());
        }

        addImports();

        found = false;
        for (FormApiFieldSelectValueModelComponent selectValueModelComponent : getGenerator().getComponentListByClass(FormApiFieldSelectValueModelComponent.class) ) {
            if ( selectValueModelComponent.getFormField() == getFormField() ) {
                setExtends("SelectValue<" + selectValueModelComponent.getName() + ">");
                found = true;
                break;
            }
        }

        if ( ! found ) {
            throwException("Internal error. Generator does not contain component FormApiFieldSelectValueModelComponent for field ?", getFormField().getName());
        }

        prepareMethod = addMethod("prepare");
        prepareMethod.addParameter("Session", "session");

        if ( getSelectValueContext() instanceof TypeSelectValue) {
            processTypeContext((TypeSelectValue) getSelectValueContext());
        } else {
            throwException("Wrong select value context class ?", getFormField().getClass().getName());
        }
    }
}
