package org.clawiz.ui.common.metadata.data.layout.component.button;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractButtonPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction action;
    
    public AbstractButton withName(String value) {
        setName(value);
        return (AbstractButton) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction getAction() {
        return this.action;
    }
    
    public void setAction(org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction value) {
        this.action = value;
    }
    
    public AbstractButton withAction(org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction value) {
        setAction(value);
        return (AbstractButton) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction> T createAction(Class<T> nodeClass) {
        if ( getAction() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "action", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction value = createChildNode(nodeClass, "action");
        setAction(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction createAction() {
        return createAction(org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getAction() != null ) { 
            getAction().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getAction());
        
    }
}
