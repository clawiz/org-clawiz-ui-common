/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.install;

import org.clawiz.ui.common.generator.AbstractApplicationGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;

public abstract class AbstractXMLScriptGenerateApplicationInstaller extends AbstractXMLScriptApplicationInstaller {

    public <T extends AbstractApplicationGenerator> Class<T> getApplicationGeneratorClass() {
        return (Class<T>) AbstractApplicationGenerator.class;
    }

    protected void fillApplicationGeneratorContext(AbstractApplicationGeneratorContext applicationGeneratorContext) {

    }

    @Override
    public void process() {

        super.process();

        AbstractApplicationGenerator        applicationGenerator = getService(getApplicationGeneratorClass(), true);

        AbstractApplicationGeneratorContext applicationGeneratorContext = null;

        try {
            applicationGeneratorContext = applicationGenerator.getApplicationGenerateContextClass().newInstance();
        } catch (Exception e) {
            throwException("Exception on create generate application context class '?' instance : ?", applicationGenerator.getApplicationGenerateContextClass().getName(), e.getMessage(), e);
        }

        applicationGeneratorContext.setPackageName(application.getPackageName());
        applicationGeneratorContext.setDestinationPath(getDestinationPath());
        applicationGeneratorContext.setApplication(application);

        fillApplicationGeneratorContext(applicationGeneratorContext);

        applicationGeneratorContext.prepare(getSession());


        applicationGenerator.setContext(applicationGeneratorContext);
        applicationGenerator.setIncrementalMode(isIncrementalMode());
        applicationGenerator.run();

    }


}
