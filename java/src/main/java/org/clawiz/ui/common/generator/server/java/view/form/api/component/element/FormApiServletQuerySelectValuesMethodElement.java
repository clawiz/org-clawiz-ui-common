/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.choosevaluerequest.AbstractFormApiServletQuerySelectValuesMethodElement;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletQuerySelectValuesMethodElement extends AbstractFormServletMethodElement {

    @Override
    public void process() {
        super.process();

        setName("querySelectValues");
        setType(getResponseContextClassName());
        addParameter(getRequestContextClassName(), "request");


        addText("");
        addText("String name = request.getHttpRequest().getParameterValue(\"name\");");
        addText("if ( name == null ) {");
        addText("    throwException(\"Query select values parameter 'name' not defined\");");
        addText("}");
        addText("String pattern = request.getHttpRequest().getParameterValue(\"pattern\");");
        addText("");
        addText(getResponseContextClassName() + " response = new " + getResponseContextClassName() + "();");
        addText("");
        addText("SelectValueList values = null;");

        addText("");

        for (AbstractFormField formField : getForm().getFields() ) {
            if ( formField.getSelectValueContext() == null || formField.getSelectValueContext() instanceof StaticListSelectValue) {
                continue;
            }

            if ( formField.getSelectValueContext() instanceof TypeSelectValue) {

                addText("if ( name.equals(\"" + formField.getJavaVariableName() + "\") ) { values = "
                        + AbstractFormApiServletQuerySelectValuesMethodElement.getQuerySelectValuesMethodName(formField)
                        + "(request, pattern); }");

            } else {
                throwException("Wrong select value context class ?", formField.getSelectValueContext().getClass().getName());
            }
        }

        addText("");
        addText("if ( values == null ) {");
        addText("    throwException(\"Wrong query select values name '?'\", name);");
        addText("}");
        addText("");
        addText("response.setSelectValues(values);");
        addText("return response;");

    }
}
