/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;


import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;

public class DataSourcePrepareObjectMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("prepareObject");

        setType("Promise<any>");

        addParameter("object", getObjectInterfaceName());


        boolean promiseDeclared = false;

        for ( TypeField field : getDataSource().getType().getFields() ) {

            if ( ! (field.getValueType() instanceof ValueTypeObject) ) {
                continue;
            }

            ValueTypeObject vto = (ValueTypeObject) field.getValueType();

            if ( ! promiseDeclared ) {
                addText("");
                addText("let promises      : Promise<any>[] = [];");
                promiseDeclared = true;
            }




            DataSourceGeneratorContext referenceDataSourceContext = getGenerator().getApplicationGenerator().getContext().getDataSourceContext(vto.getReferencedType());
            if ( referenceDataSourceContext == null ) {
                continue;
            }

            DataSource referenceDataSource = referenceDataSourceContext.getDataSource();
            String referencedClassName = referenceDataSource.getJavaClassName();
            String variableName        = getComponent().getFieldDereferenceVariableName(field);


            addText("");
            addText("if ( object." + field.getJavaVariableName() + " != null ) {");
            addText("  let ds = this.clawiz.storage.getDataSource('" + referenceDataSource.getType().getTableName() + "');");
            addText("  if ( ds == null ) { ");
            addText("    return this.clawiz.getPromiseReject('Unknown data source name ?', '" + referenceDataSource.getType().getTableName() + "');");
            addText("  }");
            addText("  promises.push( ds.load(object." + field.getJavaVariableName() + ")");
            addText("    .then((data : " + referencedClassName + ") => {");
            addText("      object." + variableName + " = data;");
            addText("    })");
            addText("   );");
            addText("}");

        }

        if ( ! promiseDeclared ) {
            addText("return Promise.resolve();");
        } else {
            addText("");
            addText("return Promise.all(promises)");
            addText("  .catch((reason) => {");
            addText("    return this.clawiz.getPromiseReject('Exception on prepare data object ? for id ? : ?', '" + getDataSource().getType().getTableName() +  "', object.id, reason.message, reason);");
            addText("  });");
            addText("");
        }


    }
}
