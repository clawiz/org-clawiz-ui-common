/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;

public class DataSourceSqlLoadOrderedListMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("sqlLoadOrderedList");
        addParameter("whereClause", "string");
        addParameter("orderByClause", "string");
        addParameter("...parameters", null);

        setType("Promise<" + getObjectInterfaceName() + "[]>");


        addText("");
        addText("return this.executeSql('select "
                + getGenerator().getObjectComponent().getSelectTableColumnsString()
                + " from " + getDataSource().getType().getTableName() + "'");
        addText("  + ( whereClause   != null ? ' where '    + whereClause   : '')");
        addText("  + ( orderByClause != null ? ' order by ' + orderByClause : '')");
        addText("  , parameters)");
        addText("  .then((data) => {");
        addText("");
        addText("    let result : " + getObjectInterfaceName() + "[] = []; ");
        addText("");
        addText("    for (let i=0; i < data.rows.length; i++) {");
        addText("");
        addText("      let object = new " + getObjectImplementationClassName() + "();");
        addText("      let row   = data.rows.item(i);");
        addText("");

        int maxLength = getMaxFieldJavaClassVariableNameLength();

        for ( TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }
            addText("      object." + field.getJavaVariableName()
                    + StringUtils.space(maxLength - field.getJavaVariableName().length())
                    + " = " + prepareFieldToObjectGetter(field, "row." + field.getColumnName()) + ";");
        }
        addText("");

        addText("      result.push(object);");
        addText("");
        addText("    }");
        addText("");
        addText("    return Promise.resolve(result);");
        addText("");
        addText("  })");
        addText("  .catch((reason) => {");
        addText("     return this.clawiz.getPromiseReject('Exception on load table ? records with where \"?\" and orderBy \"?\" : ?', this.table.name, whereClause, orderByClause, reason.message, reason);");
        addText("  });");
        addText("");

    }
}
