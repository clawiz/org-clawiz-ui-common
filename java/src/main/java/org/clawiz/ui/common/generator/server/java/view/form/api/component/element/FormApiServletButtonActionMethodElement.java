/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletButtonActionMethodElement extends AbstractFormServletMethodElement {

    Button button;

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

/*
    protected void addMethodBody() {
        if ( button.getAction() != null ) {
            if ( button.getAction() instanceof ServiceMethodCallButtonAction ) {
                ServiceMethodCallButtonAction sc = (ServiceMethodCallButtonAction) button.getAction();
                addText("return getService(" +
                        getGenerator().getPackageName() + "." + sc.getServiceMethod().getService().getName()
                        + ".class)."
                        + sc.getServiceMethod().getName() + "(model);");
            } else if ( button.getAction() instanceof SubmitButtonAction) {
                addText("return new " + getResponseContextClassName() + "();");
            } else {
                throwException("Wrong form '?' button '?' action class ?", new Object[]{getForm().getJavaClassName(), button.getName(), button.getAction().getClass().getName()});
            }
        }
    }
*/

    @Override
    public void process() {
        super.process();

/*
        JavaBlockElement be = getGenerator().getComponentByClass(FormApiServletPrototypeComponent.class).getProcessDoPost().getActionSwitch();

        setName("handleOnClick" + StringUtils.toUpperFirstChar(button.getName()));
        setType(getResponseContextClassName());
        addParameter(getRequestContextClassName(), "request");

        be.addText("if ( \"" + button.getName() + "\".equals(request.getAction()) ) { ");
        be.addText("    return " + getName() + "(request);");
        be.addText("}");

        addMethodBody();
*/
    }
}
