package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsFormReferencePrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm form;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm getForm() {
        return this.form;
    }
    
    public void setForm(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm value) {
        this.form = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499620104358";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.FormReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499620104358", "org.clawiz.ui.common.language.structure.FormReference", ConceptPropertyType.REFERENCE, "111278499620156353", "form"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("111278499620104358", "form", getForm());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getForm());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
