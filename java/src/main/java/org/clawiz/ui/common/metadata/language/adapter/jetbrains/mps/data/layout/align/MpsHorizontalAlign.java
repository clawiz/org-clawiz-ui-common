package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align;

import org.clawiz.core.common.CoreException;

public enum MpsHorizontalAlign {

    LEFT, RIGHT, CENTER;

    
    public static MpsHorizontalAlign toMpsHorizontalAlign(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsHorizontalAlign.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsHorizontalAlign value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsHorizontalAlign value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case LEFT : return "LEFT";
            case RIGHT : return "RIGHT";
            case CENTER : return "CENTER";
        }
         
        return null;
    }
}
