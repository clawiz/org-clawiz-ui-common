package org.clawiz.ui.common.metadata.data.view.grid.property;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class InlineEditGridPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty {
    
    public InlineEditGridProperty withName(String value) {
        setName(value);
        return (InlineEditGridProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
