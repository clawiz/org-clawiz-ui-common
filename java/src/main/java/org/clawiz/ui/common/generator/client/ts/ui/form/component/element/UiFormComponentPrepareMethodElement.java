/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form.component.element;


import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.jdbc.JdbcSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletPrototypeComponent;

public class UiFormComponentPrepareMethodElement extends AbstractUiFormComponentMethodElement {


    protected void addPrepareDateField(AbstractFormField field) {
        addText("    promises.push(this.prepareDateInput({ id  : '" + getGenerator().getNodeId(field) + "', name : '" + getGenerator().getNodeVariableName(field) + "' }));");
    }


    protected void addPrepareStaticSelectInput(AbstractFormField field) {
        addText("    promises.push(this.prepareStaticSelectInput({ id  : '" + getGenerator().getNodeId(field)
                + "', name : '" + getGenerator().getNodeVariableName(field)
                + "', options : this.data." + getGenerator().getNodeVariableName(field) + "SelectOptions }));");
    }

    protected void addPrepareDynamicSelectInput(AbstractFormField field) {
        addText("    promises.push(this.prepareDynamicStaticSelectInput({ id  : '" + getGenerator().getNodeId(field)
                + "', name : '" + getGenerator().getNodeVariableName(field)
                + "', options : this.data." + getGenerator().getNodeVariableName(field) + "SelectOptions }));");
    }

    protected void addPrepareField(AbstractFormField field) {

        if (field.getValueType() instanceof ValueTypeDate ) {
            addPrepareDateField(field);
        }

        if ( field.getSelectValueContext() == null ) {

        } else if ( field.getSelectValueContext() instanceof TypeSelectValue || field.getSelectValueContext() instanceof JdbcSelectValue) {
            addPrepareDynamicSelectInput(field);
        } else if ( field.getSelectValueContext() instanceof StaticListSelectValue) {
            addPrepareStaticSelectInput(field);
        } else {
            throwException("Wrong select value context class ?", field.getSelectValueContext().getClass().getName());
        }
    }

    @Override
    public void process() {
        super.process();

        setName("prepare");
        setType("Promise<any>");


        addText("");

        addText("this.apiPath     = '" + FormApiServletPrototypeComponent.getServletPath(getForm()) + "';");

        addText("");
        addText("return super.prepare()");
        addText("  .then(() => {");
        addText("");
        addText("    let promises : Promise<any>[] = [];");
        addText("");
        for (AbstractFormField field : getForm().getFields() ) {
            if ( form.isFormFieldExistsInLayout(field)) {
                addPrepareField(field);
            }
        }
        addText("");
        addText("  return Promise.all(promises);");
        addText("});");
    }
}
