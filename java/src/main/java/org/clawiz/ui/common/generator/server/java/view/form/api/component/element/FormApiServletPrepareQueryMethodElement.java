/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.TypeFieldFormField;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletPrepareQueryMethodElement extends AbstractFormServletMethodElement {

    public static final String AFTER_PREPARE_QUERY_EXTENSION_NAME = "afterFormApiServletPrepareQuery";

    String modelServiceName;

    public String getModelServiceName() {
        return modelServiceName;
    }

    @Override
    public void process() {
        super.process();

        setName("prepareQuery");
        setAccessLevel(AccessLevel.PROTECTED);


        addParameter(getRequestContextClassName(), "request");
        addParameter("Query", "query");

        Type rootType = getRootType();
        if ( rootType == null ) {
            throwException("Cannot create query without root type");
        }

        modelServiceName = StringUtils.toLowerFirstChar(rootType.getJavaClassName()) + "Model";


        addText("query.select()");
        addText("        .column(" + modelServiceName + ".id())");

        for(AbstractFormField field : getUnitedWithRootTypeFormFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            if ( field instanceof TypeFieldFormField) {
                TypeFieldFormField typeFormField = (TypeFieldFormField) field;
                addText("        .column(" + modelServiceName + "." + typeFormField.getTypeField().getJavaVariableName() + "())");
            }
        }


        processExtensions(AFTER_PREPARE_QUERY_EXTENSION_NAME);

        addText("        ;");

    }
}
