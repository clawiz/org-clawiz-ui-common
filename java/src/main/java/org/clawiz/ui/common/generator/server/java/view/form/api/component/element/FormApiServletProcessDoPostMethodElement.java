/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.system.generator.java.component.element.JavaBlockElement;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletProcessDoPostMethodElement extends AbstractFormServletMethodElement {

    public static final String PROCESS_DO_POST_EXTENSION_NAME = "formApiServletProcessDoPost";

    JavaBlockElement actionSwitch;

    public JavaBlockElement getActionSwitch() {
        return actionSwitch;
    }

    @Override
    public void process() {
        super.process();

        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");

        setName("processDoPost");
        setType(getResponseContextClassName());
        addParameter(getRequestContextClassName(), "request");

        actionSwitch = addElement(JavaBlockElement.class);

        actionSwitch.addText("if ( \"load\".equals(request.getAction()) ) {");
        actionSwitch.addText("    return load(request);");
        actionSwitch.addText("} else if ( \"create\".equals(request.getAction()) ) {");
        actionSwitch.addText("    return create(request);");
        actionSwitch.addText("} else if ( \"querySelectValues\".equals(request.getAction()) ) {");
        actionSwitch.addText("    return querySelectValues(request);");
        actionSwitch.addText("} else if ( \"save\".equals(request.getAction()) ) {");
        actionSwitch.addText("    return save(request);");
        actionSwitch.addText("}");

        addText("");
        processExtensions(PROCESS_DO_POST_EXTENSION_NAME);

        addText("throwException(\"Wrong POST call action '?'\", new Object[]{request.getAction()});");
        addText("return null;");

    }



}
