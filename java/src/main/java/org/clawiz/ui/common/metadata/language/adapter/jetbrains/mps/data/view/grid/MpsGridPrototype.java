package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsGridPrototype extends AbstractMpsNode {
    
    public String title;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm editForm;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource dataSource;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn> columns = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByColumn> orderBy = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsAbstractGridProperty> properties = new ArrayList<>();
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar topToolbar;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar bottomToolbar;
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.title = null;
        }
        this.title = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm getEditForm() {
        return this.editForm;
    }
    
    public void setEditForm(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm value) {
        this.editForm = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource getDataSource() {
        return this.dataSource;
    }
    
    public void setDataSource(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource value) {
        this.dataSource = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn> getColumns() {
        return this.columns;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByColumn> getOrderBy() {
        return this.orderBy;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsAbstractGridProperty> getProperties() {
        return this.properties;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar getTopToolbar() {
        return this.topToolbar;
    }
    
    public void setTopToolbar(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar value) {
        this.topToolbar = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar getBottomToolbar() {
        return this.bottomToolbar;
    }
    
    public void setBottomToolbar(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar value) {
        this.bottomToolbar = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6658318051736370734";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Grid";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.PROPERTY, "6064681939372889625", "title"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.REFERENCE, "6422760559407590744", "editForm"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "4089903107537888585", "dataSource"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "968894741575257697", "columns"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "942309889834325809", "orderBy"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "6064681939373447602", "properties"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "7111202990234873253", "topToolbar"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6658318051736370734", "org.clawiz.ui.common.language.structure.Grid", ConceptPropertyType.CHILD, "7111202990234873254", "bottomToolbar"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("6658318051736370734", "title", getTitle());
        addConceptNodeChild("6658318051736370734", "dataSource", getDataSource());
        for (AbstractMpsNode value : getColumns() ) {
            addConceptNodeChild("6658318051736370734", "columns", value);
        }
        for (AbstractMpsNode value : getOrderBy() ) {
            addConceptNodeChild("6658318051736370734", "orderBy", value);
        }
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("6658318051736370734", "properties", value);
        }
        addConceptNodeChild("6658318051736370734", "topToolbar", getTopToolbar());
        addConceptNodeChild("6658318051736370734", "bottomToolbar", getBottomToolbar());
        addConceptNodeRef("6658318051736370734", "editForm", getEditForm());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.Grid.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.Grid structure = (org.clawiz.ui.common.metadata.data.view.grid.Grid) node;
        
        structure.setTitle(getTitle());
        
        if ( getEditForm() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "editForm", false, getEditForm());
        } else {
            structure.setEditForm(null);
        }
        
        if ( getDataSource() != null ) {
            structure.setDataSource((org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource) getDataSource().toMetadataNode(structure, "dataSource"));
        } else {
            structure.setDataSource(null);
        }
        
        structure.getColumns().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn mpsNode : getColumns() ) {
            if ( mpsNode != null ) {
                structure.getColumns().add((org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn) mpsNode.toMetadataNode(structure, "columns"));
            } else {
                structure.getColumns().add(null);
            }
        }
        
        structure.getOrderBy().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByColumn mpsNode : getOrderBy() ) {
            if ( mpsNode != null ) {
                structure.getOrderBy().add((org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn) mpsNode.toMetadataNode(structure, "orderBy"));
            } else {
                structure.getOrderBy().add(null);
            }
        }
        
        structure.getProperties().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsAbstractGridProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
        if ( getTopToolbar() != null ) {
            structure.setTopToolbar((org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) getTopToolbar().toMetadataNode(structure, "topToolbar"));
        } else {
            structure.setTopToolbar(null);
        }
        
        if ( getBottomToolbar() != null ) {
            structure.setBottomToolbar((org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) getBottomToolbar().toMetadataNode(structure, "bottomToolbar"));
        } else {
            structure.setBottomToolbar(null);
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getEditForm());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.Grid structure = (org.clawiz.ui.common.metadata.data.view.grid.Grid) node;
        
        setTitle(structure.getTitle());
        
        if ( structure.getEditForm() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "editForm", false, structure.getEditForm());
        } else {
            setEditForm(null);
        }
        
        if ( structure.getDataSource() != null ) {
            setDataSource(loadChildMetadataNode(structure.getDataSource()));
        } else {
            setDataSource(null);
        }
        
        getColumns().clear();
        for (org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn metadataNode : structure.getColumns() ) {
            if ( metadataNode != null ) {
                getColumns().add(loadChildMetadataNode(metadataNode));
            } else {
                getColumns().add(null);
            }
        }
        
        getOrderBy().clear();
        for (org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn metadataNode : structure.getOrderBy() ) {
            if ( metadataNode != null ) {
                getOrderBy().add(loadChildMetadataNode(metadataNode));
            } else {
                getOrderBy().add(null);
            }
        }
        
        getProperties().clear();
        for (org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
        if ( structure.getTopToolbar() != null ) {
            setTopToolbar(loadChildMetadataNode(structure.getTopToolbar()));
        } else {
            setTopToolbar(null);
        }
        
        if ( structure.getBottomToolbar() != null ) {
            setBottomToolbar(loadChildMetadataNode(structure.getBottomToolbar()));
        } else {
            setBottomToolbar(null);
        }
        
    }
}
