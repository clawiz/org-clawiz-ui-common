/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid;


import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.common.view.grid.GridGenerator;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGenerator;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.*;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.GridApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.GridApiServletPrototypeComponent;

import java.util.ArrayList;

public class ServerGridGenerator extends GridGenerator {

    ServerGridGeneratorContext gridContext;

    GridApiRowModelComponent rowModelComponent;
    GridApiModelComponent modelComponent;

    GridApiParametersComponent parametersComponent;

    GridApiRequestContextComponent requestContextComponent;
    GridApiResponseContextComponent responseContextComponent;

    public ServerGridGeneratorContext getContext() {
        return gridContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        this.gridContext = (ServerGridGeneratorContext) context;
    }

    public ServerJavaApplicationGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }

    public Grid getGrid() {
        return getContext().getGrid();
    }


    public String getRowModelClassName() {
        return rowModelComponent.getName();
    }

    public String getModelClassName() {
        return modelComponent.getName();
    }

    public String getParametersClassName() {
        return parametersComponent.getName();
    }

    public String getRequestContextClassName() {
        return requestContextComponent.getName();
    }

    public String getResponseContextClassName() {
        return responseContextComponent.getName();
    }

    private ArrayList<Type> gridTypes;

    public Type getRootType() {
        return getGrid().getRootType();
    }

    @Override
    protected void process() {
        super.process();

        logDebug("Start generate grid '" + getPackageName() + "." + getGrid().getName() + "'");

        rowModelComponent = addComponent(GridApiRowModelComponent.class);
        addComponent(GridApiRowModellPrototypeComponent.class);

        modelComponent = addComponent(GridApiModelComponent.class);
        addComponent(GridApiModelPrototypeComponent.class);

        parametersComponent = addComponent(GridApiParametersComponent.class);
        addComponent(GridApiParametersPrototypeComponent.class);

        responseContextComponent = addComponent(GridApiResponseContextComponent.class);
        requestContextComponent  = addComponent(GridApiRequestContextComponent.class);

        addComponent(GridApiResponseContextPrototypeComponent.class);
        addComponent(GridApiRequestContextPrototypeComponent.class);

        addComponent(GridApiServletComponent.class);
        addComponent(GridApiServletPrototypeComponent.class);

    }


    @Override
    protected void done() {
        logDebug("Done generate grid '" + getPackageName() + "." + getGrid().getName() + "'");
        super.done();
    }


}

