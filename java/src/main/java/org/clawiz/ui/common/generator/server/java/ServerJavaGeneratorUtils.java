/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.system.generator.java.component.element.AbstractJavaClassElement;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.utils.StringUtils;

public class ServerJavaGeneratorUtils {

    public static String getValueTypeJavaClassName(AbstractValueType valueType, TypeField typeField) {

        if ( typeField == null ) {
            return valueType.getJavaClass().toString();
        }

        if ( ! ( valueType instanceof ValueTypeEnumeration) ) {
            return valueType.getJavaClass().getName();
        }

        return  typeField.getType().getServicePackageName() + "." +
                typeField.getValueTypeJavaClassName();

    }

    public static void addStringMapValue(AbstractJavaClassElement element, String fieldName, AbstractValueType valueType, TypeField typeField) {

        String capitalized = StringUtils.toUpperFirstChar(fieldName);
        String get         = "model."
                + (valueType instanceof ValueTypeBoolean ? "is" : "get")
                + capitalized + "()";
        String preparedGet = get;


        if ( valueType instanceof ValueTypeNumber ) {
            preparedGet = "bigDecimalToString(" + get + ")";
        } else if ( valueType instanceof ValueTypeBoolean ) {
//            preparedGet = "booleanToString(" + get + ")";
            preparedGet = get;
        } else if ( valueType instanceof ValueTypeDate) {
            preparedGet = "dateToString(" + get + ")";
        } else if ( valueType instanceof ValueTypeDateTime) {
            preparedGet = "dateTimeToString(" + get + ")";
        }

        if ( valueType instanceof ValueTypeNumber
                || valueType instanceof ValueTypeString
                || valueType instanceof ValueTypeDate
                || valueType instanceof ValueTypeDateTime
                || valueType instanceof ValueTypeBoolean
                ) {
            element.addText("if ( " + get + " != null ) { map.put(\"" + fieldName + "\", new JsonPrimitive(" + preparedGet + ")); }" );
            return;
        }

        String toStringFieldName = fieldName + "ToString";

        if ( valueType instanceof ValueTypeObject ) {
            element.addText("if ( " + get + " != null ) { " );
            element.addText("    map.put(\"" + fieldName + "\", new JsonPrimitive(objectIdToNodeGuid(" + preparedGet + ")));" );
            element.addText("    map.put(\"" + toStringFieldName + "\", new JsonPrimitive(notNull(objectIdToString(" + get + "), \"\")));" );
            element.addText("}" );

            return;
        }

        if ( valueType instanceof ValueTypeEnumeration ) {
            String toStringGetter = "model.get" + capitalized + "ToString()";
            element.addText("if ( " + get + " != null ) { " );
            element.addText("    map.put(\"" + fieldName + "\", new JsonPrimitive(" + preparedGet + ".toString().toUpperCase()));" );
            element.addText("    if ( " + toStringGetter + " == null ) {");
            element.addText("        switch (" + get + ") {");
            for (ValueTypeEnumerationValue value : ((ValueTypeEnumeration) valueType).getValues()) {
                element.addText("            case " +  value.getName().toUpperCase()
                        + " :"
                        + " map.put(\"" + toStringFieldName + "\", new JsonPrimitive(\"" + value.getDescription() + "\")); break;");
            }
            element.addText("            default : " + " map.put(\"" + toStringFieldName + "\", new JsonPrimitive(" + get + ".toString()));");
            element.addText("        }" );
            element.addText("    } else {" );
            element.addText("        map.put(\"" + toStringFieldName + "\", new JsonPrimitive(" +toStringGetter + "));");
            element.addText("    }" );
            element.addText("}" );
            return;
        }


        if ( valueType instanceof ValueTypeList ) {
            return;
        }


        throw new CoreException("Cannot set json value for field '?' of value type '?'", fieldName, valueType.toString());

    }


}
