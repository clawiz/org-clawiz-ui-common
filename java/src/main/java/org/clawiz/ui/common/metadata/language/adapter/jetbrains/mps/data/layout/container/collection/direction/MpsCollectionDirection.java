package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.direction;

import org.clawiz.core.common.CoreException;

public enum MpsCollectionDirection {

    HORIZONTAL, VERTICAL;

    
    public static MpsCollectionDirection toMpsCollectionDirection(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsCollectionDirection.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsCollectionDirection value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsCollectionDirection value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case HORIZONTAL : return "HORIZONTAL";
            case VERTICAL : return "VERTICAL";
        }
         
        return null;
    }
}
