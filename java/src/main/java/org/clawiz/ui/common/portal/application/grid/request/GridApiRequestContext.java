/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.grid.request;


import com.google.gson.Gson;
import org.clawiz.portal.common.servlet.http.api.request.ApiRequestContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

public class GridApiRequestContext<P extends GridApiRequestParameters> extends ApiRequestContext
{
    private String                     action;
    private String                     loadToken;
    private GridApiLoadContext         loadContext;
    private int                        page;
    private int                        rowsPerPage;
    private P                          parameters;
    private int                        startRow;
    private int                        endRow;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLoadToken() {
        return loadToken;
    }

    public void setLoadToken(String loadToken) {
        this.loadToken = loadToken;
    }

    public GridApiLoadContext getLoadContext() {
        return loadContext;
    }

    public void setLoadContext(GridApiLoadContext loadContext) {
        this.loadContext = loadContext;
    }

    public P getParameters() {
        return parameters;
    }

    public void setParameters(P parameters) {
        this.parameters = parameters;
    }

    public void write(String str) {
        getHttpRequest().write(str);
    }

    public void writeln(String str) {
        getHttpRequest().writeln(str);
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public String toHashString() {
        return action + ":" + (parameters != null ? new Gson().toJson(parameters) : "");
    }

}
