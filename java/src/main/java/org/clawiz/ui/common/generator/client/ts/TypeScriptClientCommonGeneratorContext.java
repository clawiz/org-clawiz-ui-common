/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.file.FileUtils;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.client.ts.datasource.ClientTypeScriptDataSourceGeneratorContext;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGeneratorContext;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;

import java.util.HashMap;

public class TypeScriptClientCommonGeneratorContext extends AbstractApplicationGeneratorContext {


    public static final String CLASS_NAME_CLAWIZ_BUILDER                      = "ClawizBuilder";
    public static final String CLASS_NAME_CLAWIZ_CONFIG                       = "ClawizConfig";
    public static final String CLASS_NAME_CLAWIZ_CONFIG_IMPLEMENTATION        = "ClawizConfigImplementation";
    public static final String CLASS_NAME_CLAWIZ                              = "ClawizImplementation";
    public static final String CLASS_NAME_CLAWIZ_IMPLEMENTATION               = "ClawizImplementation";

    public static final String CLASS_NAME_ABSTRACT_SERVICE                    = "AbstractService";

    public static final String CLASS_NAME_ABSTRACT_DATA_MODEL_IMPLEMENTATION  = "AbstractDataModelImplementation";
    public static final String CLASS_NAME_DATA_MODEL_FIELD                          = "DataModelField";
    public static final String CLASS_NAME_ABSTRACT_DATA_MODEL_FIELD_IMPLEMENTATION  = "AbstractDataModelFieldImplementation";
    public static final String CLASS_NAME_STORAGE_SERVICE                           = "StorageService";
    public static final String CLASS_NAME_STORAGE_SERVICE_IMPLEMENTATION            = "StorageServiceImplementation";
    public static final String CLASS_NAME_ABSTRACT_ROUTER_SERVICE                   = "AbstractRouterService";
    public static final String CLASS_NAME_ABSTRACT_DATA_SOURCE_IMPLEMENTATION       = "AbstractDataSourceImplementation";
    public static final String CLASS_NAME_ABSTRACT_DATA_OBJECT_IMPLEMENTATION       = "AbstractDataObjectImplementation";
    public static final String CLASS_NAME_QUERY_CONTEXT                             = "QueryContext";

    public static final String CLASS_NAME_ABSTRACT_UI_COMPONENT               = "AbstractUiComponent";

    public static final String CLASS_NAME_ABSTRACT_FORM_COMPONENT             = "AbstractFormComponent";
    public static final String CLASS_NAME_FORM_MODEL                          = "FormModel";
    public static final String CLASS_NAME_VALIDATE_FORM_DATA_CONTEXT          = "ValidateFormDataContext";
    public static final String CLASS_NAME_SELECT_VALUE                        = "SelectValue";

    public static final String CLASS_NAME_ABSTRACT_GRID_COMPONENT             = "AbstractGridComponent";
    public static final String CLASS_NAME_GRID_ROW_MODEL                      = "GridRowModel";

    public static final String CLASS_NAME_APPLICATION_STORAGE_SERVICE         = "ApplicationStorageService";
    public static final String CLASS_NAME_APPLICATION_ROUTER_SERVICE          = "ApplicationRouterService";

    private String clawizCoreImportPath = "clawiz-core";

    public String getClawizCoreImportPath() {
        return clawizCoreImportPath;
    }

    public void setClawizCoreImportPath(String clawizCoreImportPath) {
        this.clawizCoreImportPath = clawizCoreImportPath;
    }

    public class ImportMapObject {
        public String name;
        public String path;

        public ImportMapObject(String name, String path) {
            this.name = name;
            this.path = path;
        }
    }

    protected HashMap<String, ImportMapObject> importMaps = new HashMap<>();
    public void addImportMap(String sourceName, String mapName, String mapPath) {
        importMaps.put(sourceName, new ImportMapObject(mapName, mapPath));
    }

    public void addImportMap(String sourceName, String mapPath) {
        importMaps.put(sourceName, new ImportMapObject(sourceName, mapPath));
    }

    public ImportMapObject getImportMap(String sourceName) {
        return importMaps.get(sourceName);
    }

    public String getImportMapName(String sourceName) {
        ImportMapObject object = importMaps.get(sourceName);
        return object != null ? object.name : sourceName;
    }


    private void addImportMap(String sourceName) {
        addImportMap(sourceName, getClawizCoreImportPath());
    }

    protected String nameToDottedFileName(String name) {
        return FileUtils.nameToDottedFileName(name);
    }

    protected void prepareImportMaps() {
        addImportMap(CLASS_NAME_CLAWIZ_BUILDER);
        addImportMap(CLASS_NAME_CLAWIZ_CONFIG);
        addImportMap(CLASS_NAME_CLAWIZ_CONFIG_IMPLEMENTATION);
        addImportMap(CLASS_NAME_CLAWIZ);
        addImportMap(CLASS_NAME_CLAWIZ_IMPLEMENTATION);

        addImportMap(CLASS_NAME_ABSTRACT_SERVICE);

        addImportMap(CLASS_NAME_DATA_MODEL_FIELD);
        addImportMap(CLASS_NAME_ABSTRACT_DATA_MODEL_FIELD_IMPLEMENTATION);
        addImportMap(CLASS_NAME_ABSTRACT_DATA_MODEL_IMPLEMENTATION);
        addImportMap(CLASS_NAME_STORAGE_SERVICE);
        addImportMap(CLASS_NAME_STORAGE_SERVICE_IMPLEMENTATION);

        addImportMap(CLASS_NAME_ABSTRACT_ROUTER_SERVICE);

        addImportMap(CLASS_NAME_ABSTRACT_DATA_SOURCE_IMPLEMENTATION);
        addImportMap(CLASS_NAME_ABSTRACT_DATA_OBJECT_IMPLEMENTATION);
        addImportMap(CLASS_NAME_QUERY_CONTEXT);

        addImportMap(CLASS_NAME_ABSTRACT_UI_COMPONENT);
        addImportMap(CLASS_NAME_ABSTRACT_FORM_COMPONENT);
        addImportMap(CLASS_NAME_FORM_MODEL);
        addImportMap(CLASS_NAME_VALIDATE_FORM_DATA_CONTEXT);
        addImportMap(CLASS_NAME_SELECT_VALUE);
        addImportMap(CLASS_NAME_ABSTRACT_GRID_COMPONENT);
        addImportMap(CLASS_NAME_GRID_ROW_MODEL);

        String storageServiceClassName = getApplicationJavaClassName() + "StorageService";
        addImportMap(CLASS_NAME_APPLICATION_STORAGE_SERVICE, storageServiceClassName
                ,getApplicationDestinationPath() + "/storage/" + nameToDottedFileName(storageServiceClassName)
        );

        String routerServiceClassName =  getApplicationJavaClassName() + "RouterService";
        addImportMap(CLASS_NAME_APPLICATION_ROUTER_SERVICE, routerServiceClassName
                ,getApplicationDestinationPath() + "/ui/router/" + nameToDottedFileName(routerServiceClassName)
        );

    }

    public String getApplicationDestinationPath() {
        return super.getDestinationPath() + "/" + getApplicationJavaClassName().toLowerCase() + "/common";
    }

    public String getApplicationCommonImportPath() {
        return super.getDestinationPath() + "/" +  getApplicationJavaClassName().toLowerCase() + "/common";
    }

    public String getClientCommonImportPath() {
        return getDestinationPath() + "/" + getApplicationJavaClassName().toLowerCase() + "/common";
    }

    public DataSource getTypeDataSource(Type type) {
        for (DataSourceGeneratorContext ds : getDataSourceContexts() ) {
            if ( ds.getDataSource().getType().getFullName().equals(type.getFullName())) {
                return ds.getDataSource();
            }
        }
        throw new CoreException("DataSource for type '?' not exists in application", type.getFullName());
    }

    protected void prepareNodeToContextMap() {

//        addNodeToContextMap(Menu.class, MenuGeneratorContext.class);

        addNodeToContextMap(Form.class, TypeScriptUiFormGeneratorContext.class);

        addNodeToContextMap(Grid.class, TypeScriptUiGridGeneratorContext.class);

//        addNodeToContextMap(Screen.class, ScreenGeneratorContext.class);

        addNodeToContextMap(DataSource.class, ClientTypeScriptDataSourceGeneratorContext.class);
    }

    @Override
    public void prepare(Session session) {
        super.prepare(session);
        prepareImportMaps();
    }


}
