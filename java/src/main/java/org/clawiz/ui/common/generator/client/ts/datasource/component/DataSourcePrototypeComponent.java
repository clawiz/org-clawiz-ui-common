/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component;

import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptFieldElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.client.ts.application.storage.CommonDataSourceInterfacesComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.component.element.*;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;
import org.clawiz.ui.common.generator.server.java.datasource.api.component.DataSourceApiServletPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.storage.ApplicationDataSourceComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.AbstractTypeScriptClientDataSourceClassComponent;

public class DataSourcePrototypeComponent extends AbstractTypeScriptClientDataSourceClassComponent {


    TypeScriptFieldElement              modelField;
    CommonDataSourceInterfacesComponent interfacesComponent;

    protected void addModel() {
        String fieldName = StringUtils.toLowerFirstChar(getDataSource().getJavaClassName()) + "DataModel";

        DataModelComponent modelComponent = getGenerator().getComponentByClass(DataModelComponent.class);
        addImport(modelComponent);

        modelField = addField(fieldName, modelComponent.getName());

        TypeScriptMethodElement getter = addMethod("model");
        getter.setMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
        getter.setType(modelComponent.getName());
        getter.addText("return this." + fieldName + ";");


    }


    TypeScriptMethodElement initMethod;
    protected void addInit() {
        initMethod = addMethod("init");
        initMethod.setType("Promise<any>");

        initMethod.addText("return super.init()");
        initMethod.addText("  .then(() => {");
        initMethod.addText("");
        initMethod.addText("    this.name                   = '" + getDataSource().getType().getTableName() + "';");
        initMethod.addText("    this.dataSourceApiPath = '"
                + DataSourceApiServletPrototypeComponent.getServletPath(getDataSource())
                + "';");

        initMethod.addText("");

        int maxLength = 0;
        for (TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getColumnName().length() > maxLength ) {
                maxLength = field.getColumnName().length();
            }
        }

        initMethod.addText("    this.table.addColumn('id', "
                + StringUtils.space(maxLength - 2)
                + " 'integer');");
        for (TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList) {  continue;  }
            initMethod.addText("    this.table.addColumn('" + field.getColumnName() + "', "
                    + StringUtils.space(maxLength - field.getColumnName().length())
                    + " '" + toSQLiteType(field) + "');");
        }

        initMethod.addText("");
        initMethod.addText("    this." + modelField.getName() + " = new " + modelField.getType() + "();");
        initMethod.addText("    this." + modelField.getName() + ".init();");
        initMethod.addText("");
        initMethod.addText("    return Promise.resolve();");
        initMethod.addText("  });");


    }


    protected void addFieldValueImports() {

        for (TypeField field : getDataSource().getType().getFields() ) {

            if ( field.getValueType() instanceof ValueTypeEnumeration) {
                addImport(field.getValueTypeJavaClassName(), "./" + nameToDottedFileName(field.getValueTypeJavaClassName()));
            }



            if ( field.getValueType() instanceof ValueTypeObject) {
                ValueTypeObject vto = (ValueTypeObject) field.getValueType();
                DataSourceGeneratorContext referenceDataSource = getGenerator().getContext().getApplicationGenerator().getContext().getDataSourceContext(vto.getReferencedType());
                if ( referenceDataSource != null ) {
                    String referencedClassName = referenceDataSource.getDataSource().getJavaClassName();
                    addImport(referencedClassName, interfacesComponent);
                }

            }

        }
    }



    @Override
    public void process() {
        super.process();

        interfacesComponent = getApplicationGenerator().getComponentByClass(CommonDataSourceInterfacesComponent.class);

        setName(getGenerator().getComponentByClass(DataSourceComponent.class).getName() + "Prototype");

        AbstractTypeScriptClassComponent applicationDataSource = getGenerator().getContext().getApplicationGenerator().getComponentByClass(ApplicationDataSourceComponent.class);

        addImport(applicationDataSource);
        addImport(getObjectInterfaceName(), interfacesComponent);
        addImport(getObjectInterfaceName() + "Implementation", "./" + nameToDottedFileName(getObjectInterfaceName() + "Implementation"));

        addImport(TypeScriptClientCommonGeneratorContext.CLASS_NAME_QUERY_CONTEXT);


        setExtends(applicationDataSource.getName() + "<" + getObjectInterfaceName() + ">");

        addFieldValueImports();

        addModel();

        addInit();

        addElement(DataSourcePrepareObjectMethodElement.class);

        addElement(DataSourceSqlLoadMethodElement.class);
        addElement(DataSourceSqlLoadListMethodElement.class);
        addElement(DataSourceSqlLoadOrderedListMethodElement.class);

        addElement(DataSourceSqlSaveMethodElement.class);

        addElement(DataSourceServerRowToObjectMethodElement.class);
        addElement(DataSourceObjectToServerRowMethodElement.class);


    }

}
