/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.form.query;

import java.util.ArrayList;

public class QuerySelectValuesContext {

    String             from;

    StringBuilder      columns;
    StringBuilder      where;
    ArrayList<String>  patternsWhereParts;
    StringBuilder      orderBy;

    String             pattern;
    String[]           patterns;

    String             sql;
    ArrayList<Object>  parameters = new ArrayList<>();

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String[] getPatterns() {
        return patterns;
    }

    public void setPatterns(String[] patterns) {
        this.patterns = patterns;
    }

    public void addColumn(String columnName) {
        if ( columns != null ) { 
            columns.append(", "); 
        } else {
            columns = new StringBuilder();
        }
        columns.append(columnName);
    }

    public void addWhere(String where) {
        if ( this.where != null ) {
            this.where.append(" and ");
        } else {
            this.where = new StringBuilder();
        }
        this.where.append(where);
    }

    public void addPatternWhere(String where) {
        if ( patternsWhereParts == null ) {
            patternsWhereParts = new ArrayList<>();
        }
        patternsWhereParts.add(where);
    }

    public void addOrderBy(String orderBy) {
        if ( this.orderBy != null ) {
            this.orderBy.append(", ");
        } else {
            this.orderBy = new StringBuilder();
        }
        this.orderBy.append(orderBy);
    }
    

    private boolean prepared = false;
    public void prepare() {
        if ( patterns.length > 0 ) {
            for ( int ip=0; ip < patterns.length; ip++) {
                for (String part : patternsWhereParts) {
                    addWhere(part);
                    parameters.add(patterns[ip]);
                }
            }
        }

        sql = "select "
                + (columns != null ? columns.toString() : "*")
                + " from " + from
                + (where != null ? " where " + where.toString() : "")
                + (orderBy != null ? " order by " + orderBy.toString() : "");
        prepared = true;
    }

    public String getSql() {
        if ( ! prepared ) { prepare(); }
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Object[] getParametersArray() {
        if ( ! prepared ) { prepare(); }
        return parameters.toArray();
    }


}
