package org.clawiz.ui.common.metadata.data.menu;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractMenuPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.menu.item.MenuItemList items = new org.clawiz.ui.common.metadata.data.menu.item.MenuItemList();
    
    public AbstractMenu withName(String value) {
        setName(value);
        return (AbstractMenu) this;
    }
    
    public org.clawiz.ui.common.metadata.data.menu.item.MenuItemList getItems() {
        return this.items;
    }
    
    public AbstractMenu withItem(org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem value) {
        getItems().add(value);
        return (AbstractMenu) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem> T createItem(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem value = createChildNode(nodeClass, "items");
        getItems().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem createItem() {
        return createItem(org.clawiz.ui.common.metadata.data.menu.item.AbstractMenuItem.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getItems()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getItems()) {
            references.add(node);
        }
        
    }
}
