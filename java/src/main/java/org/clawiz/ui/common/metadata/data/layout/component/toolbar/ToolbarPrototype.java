package org.clawiz.ui.common.metadata.data.layout.component.toolbar;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ToolbarPrototype extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent {
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer left;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer right;
    
    public Toolbar withName(String value) {
        setName(value);
        return (Toolbar) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer getLeft() {
        return this.left;
    }
    
    public void setLeft(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value) {
        this.left = value;
    }
    
    public Toolbar withLeft(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value) {
        setLeft(value);
        return (Toolbar) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer> T createLeft(Class<T> nodeClass) {
        if ( getLeft() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "left", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value = createChildNode(nodeClass, "left");
        setLeft(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer createLeft() {
        return createLeft(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer getRight() {
        return this.right;
    }
    
    public void setRight(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value) {
        this.right = value;
    }
    
    public Toolbar withRight(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value) {
        setRight(value);
        return (Toolbar) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer> T createRight(Class<T> nodeClass) {
        if ( getRight() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "right", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer value = createChildNode(nodeClass, "right");
        setRight(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer createRight() {
        return createRight(org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getLeft() != null ) { 
            getLeft().prepare(session);
        }
        if ( getRight() != null ) { 
            getRight().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getLeft());
        
        references.add(getRight());
        
    }
}
