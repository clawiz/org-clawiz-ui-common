package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsGridReferencePrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid grid;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid getGrid() {
        return this.grid;
    }
    
    public void setGrid(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid value) {
        this.grid = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499620156379";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.GridReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499620156379", "org.clawiz.ui.common.language.structure.GridReference", ConceptPropertyType.REFERENCE, "111278499620156380", "grid"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("111278499620156379", "grid", getGrid());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getGrid());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
