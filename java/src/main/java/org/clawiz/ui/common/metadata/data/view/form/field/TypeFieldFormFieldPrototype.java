package org.clawiz.ui.common.metadata.data.view.form.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeFieldFormFieldPrototype extends org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.field.TypeField typeField;
    
    public TypeFieldFormField withName(String value) {
        setName(value);
        return (TypeFieldFormField) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField getTypeField() {
        return this.typeField;
    }
    
    public void setTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        this.typeField = value;
    }
    
    public TypeFieldFormField withTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        setTypeField(value);
        return (TypeFieldFormField) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getTypeField() != null ) { 
            getTypeField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getTypeField());
        
    }
}
