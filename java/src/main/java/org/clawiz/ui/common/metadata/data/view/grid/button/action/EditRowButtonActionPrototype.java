package org.clawiz.ui.common.metadata.data.view.grid.button.action;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class EditRowButtonActionPrototype extends org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction {
    
    public EditRowButtonAction withName(String value) {
        setName(value);
        return (EditRowButtonAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
