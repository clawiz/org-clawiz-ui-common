/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component.element.api;


import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.server.java.datasource.ServerDataSourceGenerator;
import org.clawiz.ui.common.generator.server.java.datasource.api.component.DataSourceApiServletPrototypeComponent;

public class AbstractDataSourceApiJavaClassMethodElement extends JavaMethodElement {


    private DataSourceApiServletPrototypeComponent apiComponent;
    private ServerDataSourceGenerator apiGenerator;
    private DataSource api;

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        apiComponent = (DataSourceApiServletPrototypeComponent) component;
        apiGenerator = apiComponent.getGenerator();
        api          = apiGenerator.getDataSource();
    }

    public DataSourceApiServletPrototypeComponent getComponent() {
        return apiComponent;
    }

    public ServerDataSourceGenerator getGenerator() {
        return apiGenerator;
    }

    public DataSource getDataSource() {
        return api;
    }

    public String getRequestContextClassName() {
        return getGenerator().getRequestContextComponent().getName();
    }

    public String getResponseContextClassName() {
        return getGenerator().getResponseContextComponent().getName();
    }

    public String getExecuteContextClassName() {
        return getGenerator().getExecuteContextComponentt().getName();
    }

    public String getRowModelClassName() {
        return getGenerator().getRowModelComponent().getName();
    }

    public String getServiceVariableName() {
        return getComponent().getServiceVariableName();
    }

}