/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component;

import com.google.gson.JsonObject;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationAbstractDataSourceDataSourceComponent;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationAbstractDataSourceExecuteContextComponent;
import org.clawiz.ui.common.generator.server.java.datasource.api.component.element.datasource.*;
import org.clawiz.ui.common.portal.application.datasource.DataSourceActionType;
import org.clawiz.ui.common.portal.application.datasource.execute.DataSourceExecuteContext;

import java.math.BigDecimal;
import java.util.ArrayList;

public class DataSourceDataSourcePrototypeComponent extends AbstractDataSourceApiJavaClassComponent {

    private JavaVariableElement serviceVariable;

    protected void addImports() {

        addImport(ArrayList.class);
        addImport(Statement.class);
        addImport(DataSourceActionType.class);
        addImport(DataSourceExecuteContext.class);
        addImport(JsonObject.class);
        addImport(BigDecimal.class);


        addImport(getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractDataSourceExecuteContextComponent.class).getPackageName() + "." +
                getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractDataSourceExecuteContextComponent.class).getName()
        );


        String typePackageName = getDataSource().getType().getPackageName() + "." + getDataSource().getType().getJavaClassName().toLowerCase();
        addImport(typePackageName + "." + getDataSource().getType().getJavaClassName() + "Service");
        addImport(typePackageName + "." + getDataSource().getType().getJavaClassName() + "Object");

    }

    protected void addGetTypeIdMethod() {
        JavaMethodElement method = addMethod("getTypeId");
        method.setType("BigDecimal");
        method.addText("return " + serviceVariable.getName() + ".getTypeId();");
    }

    public String getServiceVariableName() {
        return serviceVariable.getName();
    }


    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getGenerator().getComponentByClass(DataSourceDataSourceComponent.class).getName() + "Prototype");

        AbstractJavaClassComponent abstractServlet =  getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractDataSourceDataSourceComponent.class);
        addImport(abstractServlet.getFullName());
        setExtends(abstractServlet.getName() + "<" + getGenerator().getComponentByClass(DataSourceRowModelComponent.class).getName() + ">");

        addImports();
        addFieldValueTypeEnumerationImports();

        serviceVariable = addVariable(getDataSource().getType().getJavaClassName() + "Service",
                StringUtils.toLowerFirstChar(getDataSource().getType().getJavaClassName()) + "Service")
                .withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);

        addGetTypeIdMethod();

        addElement(DataSourceDataSourceToJsonObjectMethodElement.class);
        addElement(DataSourceDataSourceFromJsonObjectMethodElement.class);

        addElement(DataSourceDataSourceCreateExecuteContextMethodElement.class);

        addElement(DataSourceDataSourceLoadChangesMethodElement.class);
        addElement(DataSourceDataSourceSaveChangeMethodElement.class);
    }


}
