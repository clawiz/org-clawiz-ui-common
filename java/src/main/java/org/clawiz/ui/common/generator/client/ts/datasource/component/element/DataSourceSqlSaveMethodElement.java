/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;

public class DataSourceSqlSaveMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("sqlSave");
        addParameter("object", getObjectInterfaceName());
        addParameter("asNew", "boolean");

        StringBuilder insertColumns    = new StringBuilder();
        StringBuilder insertValues     = new StringBuilder();
        StringBuilder insertParameters = new StringBuilder();
        StringBuilder updateColumns    = new StringBuilder();
        StringBuilder updateParameters = new StringBuilder();

        insertColumns.append("id");
        insertValues.append("?");
        insertParameters.append("object.id");


        boolean isNotFirst = false;

        for (int i =0; i < getDataSource().getType().getFields().size(); i++ ) {
            TypeField field = getDataSource().getType().getFields().get(i);
            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            String cn     = field.getColumnName();
            String getter = prepareFieldFromObjectGetter(field, "object." + field.getJavaVariableName());

            insertColumns.append(", " + cn);
            insertValues.append(", ?");
            insertParameters.append(", " + getter);

            updateColumns.append((isNotFirst ? ", " : "") + cn + " = ?");
            updateParameters.append((isNotFirst ? ", " : "") + getter);

            isNotFirst = true;
        }

        updateParameters.append(", object.id");


        addText("");
        addText("let me = this;");
        addText("");
        addText("if ( asNew) {");
        addText("  return me.executeSql('insert into "
                + getDataSource().getType().getTableName()
                +  "( " + insertColumns.toString() + " ) values (" + insertValues.toString() + ")', " + insertParameters.toString() +  ");");
        addText("} else {");
        addText("  return me.executeSql('update "
                + getDataSource().getType().getTableName()
                + " set " + updateColumns.toString() + " where id = ?', " + updateParameters.toString() + ");");
        addText("}");
        addText("");

    }
}
