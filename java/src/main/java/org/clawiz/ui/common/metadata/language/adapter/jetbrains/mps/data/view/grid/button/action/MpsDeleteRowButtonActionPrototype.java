package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.button.action;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsDeleteRowButtonActionPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction {
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234832542";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.DeleteRowButtonAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction structure = (org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction structure = (org.clawiz.ui.common.metadata.data.view.grid.button.action.DeleteRowButtonAction) node;
        
    }
}
