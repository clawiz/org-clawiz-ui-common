package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsFormPrototype extends AbstractMpsNode {
    
    public String title;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource dataSource;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField> fields = new ArrayList<>();
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar topToolbar;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar bottomToolbar;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> layoutComponents = new ArrayList<>();
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.title = null;
        }
        this.title = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource getDataSource() {
        return this.dataSource;
    }
    
    public void setDataSource(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource value) {
        this.dataSource = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField> getFields() {
        return this.fields;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar getTopToolbar() {
        return this.topToolbar;
    }
    
    public void setTopToolbar(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar value) {
        this.topToolbar = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar getBottomToolbar() {
        return this.bottomToolbar;
    }
    
    public void setBottomToolbar(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar value) {
        this.bottomToolbar = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> getLayoutComponents() {
        return this.layoutComponents;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200787749";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Form";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.PROPERTY, "6422760559407430872", "title"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.CHILD, "3219955127529903124", "dataSource"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.CHILD, "6586513253200668153", "fields"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.CHILD, "7111202990234873237", "topToolbar"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.CHILD, "7111202990234873241", "bottomToolbar"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200787749", "org.clawiz.ui.common.language.structure.Form", ConceptPropertyType.CHILD, "1646904187239783597", "layoutComponents"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("6586513253200787749", "title", getTitle());
        addConceptNodeChild("6586513253200787749", "dataSource", getDataSource());
        for (AbstractMpsNode value : getFields() ) {
            addConceptNodeChild("6586513253200787749", "fields", value);
        }
        addConceptNodeChild("6586513253200787749", "topToolbar", getTopToolbar());
        addConceptNodeChild("6586513253200787749", "bottomToolbar", getBottomToolbar());
        for (AbstractMpsNode value : getLayoutComponents() ) {
            addConceptNodeChild("6586513253200787749", "layoutComponents", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.form.Form.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.form.Form structure = (org.clawiz.ui.common.metadata.data.view.form.Form) node;
        
        structure.setTitle(getTitle());
        
        if ( getDataSource() != null ) {
            structure.setDataSource((org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource) getDataSource().toMetadataNode(structure, "dataSource"));
        } else {
            structure.setDataSource(null);
        }
        
        structure.getFields().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField mpsNode : getFields() ) {
            if ( mpsNode != null ) {
                structure.getFields().add((org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField) mpsNode.toMetadataNode(structure, "fields"));
            } else {
                structure.getFields().add(null);
            }
        }
        
        if ( getTopToolbar() != null ) {
            structure.setTopToolbar((org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) getTopToolbar().toMetadataNode(structure, "topToolbar"));
        } else {
            structure.setTopToolbar(null);
        }
        
        if ( getBottomToolbar() != null ) {
            structure.setBottomToolbar((org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) getBottomToolbar().toMetadataNode(structure, "bottomToolbar"));
        } else {
            structure.setBottomToolbar(null);
        }
        
        structure.getLayoutComponents().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent mpsNode : getLayoutComponents() ) {
            if ( mpsNode != null ) {
                structure.getLayoutComponents().add((org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent) mpsNode.toMetadataNode(structure, "layoutComponents"));
            } else {
                structure.getLayoutComponents().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.form.Form structure = (org.clawiz.ui.common.metadata.data.view.form.Form) node;
        
        setTitle(structure.getTitle());
        
        if ( structure.getDataSource() != null ) {
            setDataSource(loadChildMetadataNode(structure.getDataSource()));
        } else {
            setDataSource(null);
        }
        
        getFields().clear();
        for (org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField metadataNode : structure.getFields() ) {
            if ( metadataNode != null ) {
                getFields().add(loadChildMetadataNode(metadataNode));
            } else {
                getFields().add(null);
            }
        }
        
        if ( structure.getTopToolbar() != null ) {
            setTopToolbar(loadChildMetadataNode(structure.getTopToolbar()));
        } else {
            setTopToolbar(null);
        }
        
        if ( structure.getBottomToolbar() != null ) {
            setBottomToolbar(loadChildMetadataNode(structure.getBottomToolbar()));
        } else {
            setBottomToolbar(null);
        }
        
        getLayoutComponents().clear();
        for (org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent metadataNode : structure.getLayoutComponents() ) {
            if ( metadataNode != null ) {
                getLayoutComponents().add(loadChildMetadataNode(metadataNode));
            } else {
                getLayoutComponents().add(null);
            }
        }
        
    }
}
