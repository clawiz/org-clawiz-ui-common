package org.clawiz.ui.common.metadata.data.view.grid.order;

import org.clawiz.core.common.CoreException;

public enum GridOrderByDirection {

    ASC, DESC;

    
    public static GridOrderByDirection toGridOrderByDirection(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return GridOrderByDirection.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong GridOrderByDirection value '?'", value);
        }
        
    }
    
    public static String toDescription(GridOrderByDirection value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case ASC : return "asc";
            case DESC: return "desc";
        }
        
        return value.toString();
    }
}
