package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractButtonPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction action;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction getAction() {
        return this.action;
    }
    
    public void setAction(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction value) {
        this.action = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7594984129459430053";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractButton";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7594984129459430053", "org.clawiz.ui.common.language.structure.AbstractButton", ConceptPropertyType.CHILD, "7953314342872946661", "action"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("7594984129459430053", "action", getAction());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton structure = (org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton) node;
        
        if ( getAction() != null ) {
            structure.setAction((org.clawiz.ui.common.metadata.data.layout.component.button.action.AbstractButtonAction) getAction().toMetadataNode(structure, "action"));
        } else {
            structure.setAction(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton structure = (org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton) node;
        
        if ( structure.getAction() != null ) {
            setAction(loadChildMetadataNode(structure.getAction()));
        } else {
            setAction(null);
        }
        
    }
}
