package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsFormLayoutComponentPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm form;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm getForm() {
        return this.form;
    }
    
    public void setForm(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm value) {
        this.form = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499619583130";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.FormLayoutComponent";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499619583130", "org.clawiz.ui.common.language.structure.FormLayoutComponent", ConceptPropertyType.REFERENCE, "111278499619583131", "form"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("111278499619583130", "form", getForm());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent) node;
        
        if ( getForm() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "form", false, getForm());
        } else {
            structure.setForm(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getForm());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent) node;
        
        if ( structure.getForm() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "form", false, structure.getForm());
        } else {
            setForm(null);
        }
        
    }
}
