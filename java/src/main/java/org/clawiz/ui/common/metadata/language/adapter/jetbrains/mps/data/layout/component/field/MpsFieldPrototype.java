package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.field;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsFieldPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField formField;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField getFormField() {
        return this.formField;
    }
    
    public void setFormField(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField value) {
        this.formField = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200682171";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Field";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200682171", "org.clawiz.ui.common.language.structure.Field", ConceptPropertyType.REFERENCE, "6586513253200682172", "formField"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("6586513253200682171", "formField", getFormField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.field.Field.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.field.Field structure = (org.clawiz.ui.common.metadata.data.layout.component.field.Field) node;
        
        if ( getFormField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "formField", false, getFormField());
        } else {
            structure.setFormField(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.field.Field structure = (org.clawiz.ui.common.metadata.data.layout.component.field.Field) node;
        
        if ( structure.getFormField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "formField", false, structure.getFormField());
        } else {
            setFormField(null);
        }
        
    }
}
