package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractCollectionPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.direction.MpsCollectionDirection direction;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.direction.MpsCollectionDirection getDirection() {
        return this.direction;
    }
    
    public void setDirection(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.direction = null;
        }
        this.direction = org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.direction.MpsCollectionDirection.toMpsCollectionDirection(value);
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234832490";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractCollection";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832490", "org.clawiz.ui.common.language.structure.AbstractCollection", ConceptPropertyType.PROPERTY, "7111202990234873186", "direction"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("7111202990234832490", "direction", org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.direction.MpsCollectionDirection.toConceptNodePropertyString(getDirection()));
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection structure = (org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection) node;
        
        structure.setDirection(( getDirection() != null ? org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection.valueOf(getDirection().toString()) : null ));
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection structure = (org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection) node;
        
        setDirection(structure.getDirection() != null ? structure.getDirection().toString() : null);
        
    }
}
