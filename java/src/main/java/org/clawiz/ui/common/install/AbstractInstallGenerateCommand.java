/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.install;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.shell.command.AbstractCommand;

import java.util.Optional;

public class AbstractInstallGenerateCommand extends AbstractCommand {

    public static final String INCREMENTAL                = "incremental";
    public static final String ATTRIBUTE_NAME             = "name";
    public static final String PARAMETER_DESTINATION_PATH = "destination-path";

    protected String getDestinationPathParameterName() {
        return PARAMETER_DESTINATION_PATH;
    }

    Optional<String> destinationPath = null;
    public String getDestinationPath() {
/*
        if ( destinationPath != null ) {
            return destinationPath.get();
        }
        String path = getStringParameter(getDestinationPathParameterName());

        if ( StringUtils.isEmpty(path)) {
            path = getElement().getAttribute(getDestinationPathParameterName());
        }

        if ( StringUtils.isEmpty(path)) {
            path = getElement().getAttribute(PARAMETER_DESTINATION_PATH);
        }

        path = path.replaceAll("\\\\", "/");

        if ( path.length() > 3 && path.substring(0, 3).equals("../")) {
            File parentFolder = new File(System.getProperty("user.dir").replaceAll("\\\\", "/"));
            File b = new File(parentFolder, path);
            try {
                path = b.getCanonicalPath();
            } catch (IOException e) {
                throwException("Exception on get destination path  for (? , ?) : ? ", parentFolder, path, e.getMessage(), e);
            }
        }


        if ( StringUtils.isEmpty(path) ) {
            throwException("Destination path parameter '" + getDestinationPathParameterName() + "' not defined");
        }

        logInfo("Generate sources destination path = " + path);

        destinationPath = Optional.of(path);

        return path;
*/
        throw new CoreException("Method " + this.getClass().getName() + ".getDestinationPath not implemented");

    }

    Optional<Boolean> incrementalMode;
    protected boolean isIncrementalMode() {
/*
        if ( incrementalMode != null ) {
            return incrementalMode.get();
        }
        String incremental = getStringParameter(PARAMETER_INCREMENTAL);

        if ( StringUtils.isEmpty(incremental) ) {
            incrementalMode = Optional.of(false);
        } else {
            incrementalMode = Optional.of(StringUtils.toBoolean(incremental));
        }

        return incrementalMode.get();
*/
        throw new CoreException("Method " + this.getClass().getName() + ".isIncrementalMode not implemented");
    }

    public String getCurrentPackage() {
        throw new CoreException("Method " + this.getClass().getName() + ".getCurrentPackage not implemented");
    }

    public String getAttribute(String name, boolean raiseException) {
        throw new CoreException("Method " + this.getClass().getName() + ".getAttribute not implemented");
    }

    @Override
    public void process() {
        throwException("Method " + this.getClass().getName() + ".process not implemented");
    }
}
