/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.client.ts.application.storage.ApplicationStorageServicePrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.AbstractTypeScriptClientDataSourceClassComponent;

public class DataSourceComponent extends AbstractTypeScriptClientDataSourceClassComponent {

    protected void addToApplicationStorage() {

        ApplicationStorageServicePrototypeComponent storageService = getGenerator().getApplicationGenerator().getComponentByClass(ApplicationStorageServicePrototypeComponent.class);

        storageService.addImport(this);

        String variableName = StringUtils.toLowerFirstChar(getName());
        storageService.addField(variableName, getName());

        storageService.getPrepareSourcesMethod().addText("");
        storageService.getPrepareSourcesMethod().addText("promises.push(");
        storageService.getPrepareSourcesMethod().addText("  this.addDataSource(" + getName() + ")");
        storageService.getPrepareSourcesMethod().addText("    .then((dataSource : " + getName() + ") => {");
        storageService.getPrepareSourcesMethod().addText("      this." + variableName + " = dataSource;");
        storageService.getPrepareSourcesMethod().addText("      return Promise.resolve();");
        storageService.getPrepareSourcesMethod().addText("    })");
        storageService.getPrepareSourcesMethod().addText(");");

    }


    @Override
    public void process() {
        super.process();

        setOverwriteMode(AbstractComponent.OverwriteMode.SKIP);

        setName(getDataSource().getJavaClassName() + "DataSource");

        setExtendsOfPrototype();

        addToApplicationStorage();

    }
}
