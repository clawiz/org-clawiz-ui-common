/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.client.ts.datasource.AbstractTypeScriptClientDataSourceClassComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.component.ObjectImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.ClientTypeScriptDataSourceGenerator;

public class AbstractClientDataSourceTypeScriptMethodElemenet extends TypeScriptMethodElement {

    AbstractTypeScriptClientDataSourceClassComponent dataSourceComponent;

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        dataSourceComponent = (AbstractTypeScriptClientDataSourceClassComponent) component;
    }

    @Override
    public AbstractTypeScriptClientDataSourceClassComponent getComponent() {
        return dataSourceComponent;
    }

    public ClientTypeScriptDataSourceGenerator getGenerator() {
        return dataSourceComponent.getGenerator();
    }

    public DataSource getDataSource() {
        return getGenerator().getDataSource();
    }

    public String getObjectInterfaceName() {
        return getComponent().getObjectInterfaceName();
    }

    public String getObjectImplementationClassName() {
        return getComponent().getObjectInterfaceName() + "Implementation";
    }

    public ObjectImplementationComponent getObjectComponent() {
        return getGenerator().getObjectComponent();
    }

    protected String prepareFieldToObjectGetter(TypeField field, String getter) {

        if ( field.getValueType() instanceof ValueTypeBoolean ) {
            return "( 'true' === " + getter + " )";
        } else if ( field.getValueType() instanceof ValueTypeDate ) {
            return "this.dateUtils.stringToDate(" + getter + ", this.dateUtils.ISO8601_DATE_FORMAT)";
        } else if ( field.getValueType() instanceof ValueTypeDateTime) {
            return "this.dateUtils.stringToDateTime(" + getter + ", this.dateUtils.ISO8601_DATE_TIME_FORMAT)";
        } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
            return field.getValueTypeJavaClassName() + "[ " + getter + " + '' ]";
        }

        return getter;
    }

    protected String prepareFieldFromObjectGetter(TypeField field, String getter) {

        if ( field.getValueType() instanceof ValueTypeBoolean ) {
            return "( " + getter + " ? 'true' : 'false' )";
        } else if ( field.getValueType() instanceof ValueTypeDate ) {
            return "this.dateUtils.dateToString(" + getter + ", this.dateUtils.ISO8601_DATE_FORMAT)";
        } else if ( field.getValueType() instanceof ValueTypeDateTime) {
            return "this.dateUtils.dateTimeToString(" + getter + ", this.dateUtils.ISO8601_DATE_TIME_FORMAT)";
        } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
            return field.getValueTypeJavaClassName() + "[ " + getter + " ]";
        }

        return getter;
    }
    private int maxFieldJavaClassVariableNameLength;
    public int getMaxFieldJavaClassVariableNameLength() {
        if ( maxFieldJavaClassVariableNameLength == 0 ) {
            int maxLength = 0;
            for ( TypeField field : getDataSource().getType().getFields() ) {
                if ( field.getJavaVariableName().length() > maxLength ) {
                    maxLength = field.getJavaVariableName().length();
                }
            }
            maxFieldJavaClassVariableNameLength = maxLength;
        }
        return maxFieldJavaClassVariableNameLength;
    }

}
