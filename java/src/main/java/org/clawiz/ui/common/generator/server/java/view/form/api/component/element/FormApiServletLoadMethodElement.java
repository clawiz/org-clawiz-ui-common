/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.TypeFieldFormField;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletLoadMethodElement extends AbstractFormServletMethodElement {

    public static final String ADD_MODEL_SETTERS_EXTENSION_NAME = "addFormApiServletLoadModelSetters";

    public int statementColumnIndex;

    @Override
    public void process() {
        super.process();

        setName("load");
        setType(getResponseContextClassName());
        addParameter(getRequestContextClassName(), "request");

        addText(getResponseContextClassName() + " response = new " + getResponseContextClassName() + "();");
        addText(getModelClassName() + "           model    = new " + getModelClassName() + "();");
        addText("response.setModel(model);");

        addText("");
        addText("if ( request.getId() == null ) {");
        addText("    throwException(\"Parameter 'Id' not defined\");");
        addText("}");
        addText("Statement statement = executeQuery(getQuery(request).getSQL() + \" where t1.id = ?\", request.getId());");
        addText("if ( ! statement.next() ) {");
        addText("    throwException(\"Wrong id ?\", request.getHttpRequest().getParameterValue(\"id\"));");
        addText("}");

        addText("");
        addText("model.setId(statement.getBigDecimal(1));");

        statementColumnIndex = 2;
        for (AbstractFormField field : getUnitedWithRootTypeFormFields()) {
            if ( field instanceof TypeFieldFormField) {

                if ( field.getValueType() instanceof ValueTypeList ) {
                    continue;
                }

                if ( field.getValueType() instanceof ValueTypeEnumeration) {
                    if ( field.getTypeField() != null ) {
                        addText("model." + field.getSetMethodName() + "("
                                + getComponent().getValueTypeJavaClassName(field)
                                + ".to" + field.getTypeField().getValueTypeJavaClassName() + "("
                                + "(statement.getString(" + (statementColumnIndex++) + "))));");
                    } else {
                        throwException("Value type ENUM of not type field '?' not supported", field.getName());
                    }
                } else {
                    String getter = "statement.get" + field.getValueType().getStatementValueTypeName() +"(" + (statementColumnIndex++) + ")";
                    if ( field.getValueType() instanceof ValueTypeBoolean) {
                        getter = "StringUtils.toBoolean(" + getter + ")";
                    }
                    addText("model." + field.getSetMethodName() + "(" + getter + ");");
                }

            }

        }
        processExtensions(ADD_MODEL_SETTERS_EXTENSION_NAME);

        addText("");
        addText("prepareModel(request, model);");

        addText("");
        addText("statement.close();");
        addText("");


        addText("return response;");

    }
}
