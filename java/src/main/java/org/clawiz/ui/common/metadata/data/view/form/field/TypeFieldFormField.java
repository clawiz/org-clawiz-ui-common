/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.view.form.field;


import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;

import java.math.BigDecimal;
import java.util.Optional;

public class TypeFieldFormField extends TypeFieldFormFieldPrototype {


    @Override
    public String getLabel() {
        return super.getLabel() != null ? super.getLabel() : getTypeField().getDescription();
    }

    @Override
    public BigDecimal getPrecision() {
        return getTypeField().getValueType().getPrecision();
    }

    @Override
    public BigDecimal getScale() {
        return getTypeField().getValueType().getScale();
    }

    @Override
    public boolean isRequired() {
        return getTypeField().isRequired();
    }

    @Override
    public AbstractValueType getValueType() {
        return getTypeField().getValueType();
    }

    Optional<AbstractSelectValue> _selectValueContext;
    @Override
    public AbstractSelectValue getSelectValueContext() {

        AbstractSelectValue svc = super.getSelectValueContext();
        if ( svc != null ) {
            return null;
        }
        if ( _selectValueContext != null ) {
            return _selectValueContext.isPresent() ? _selectValueContext.get() : null;
        }

        if ( getTypeField().getValueType() instanceof ValueTypeObject  )  {
            ValueTypeObject vto = (ValueTypeObject) getTypeField().getValueType();
            if ( vto.getReferencedType() != null ) {
                TypeSelectValue tc = new TypeSelectValue();
                tc.setType(vto.getReferencedType());
                svc = tc;
            }



        } else if ( getTypeField().getValueType() instanceof ValueTypeEnumeration) {

            StaticListSelectValue sc = new StaticListSelectValue();
            svc = sc;

            for (ValueTypeEnumerationValue value : ((ValueTypeEnumeration) getTypeField().getValueType()).getValues() ) {
                sc.addValue(value.getName(), value.getDescription());
            }


        }

        _selectValueContext = svc != null ? Optional.of(svc) : Optional.empty();

        return svc;
    }

    @Override
    public String getName() {
        return super.getName() != null ? super.getName() : getTypeField().getName();
    }
}
