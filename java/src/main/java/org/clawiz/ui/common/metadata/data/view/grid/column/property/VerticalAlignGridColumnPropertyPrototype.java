package org.clawiz.ui.common.metadata.data.view.grid.column.property;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class VerticalAlignGridColumnPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty {
    
    @ExchangeAttribute
    private org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign align;
    
    public VerticalAlignGridColumnProperty withName(String value) {
        setName(value);
        return (VerticalAlignGridColumnProperty) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign getAlign() {
        return this.align;
    }
    
    public void setAlign(org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign value) {
        this.align = value;
    }
    
    public VerticalAlignGridColumnProperty withAlign(org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign value) {
        setAlign(value);
        return (VerticalAlignGridColumnProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
