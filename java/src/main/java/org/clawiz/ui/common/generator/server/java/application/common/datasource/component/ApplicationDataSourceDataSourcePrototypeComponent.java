/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.datasource.component;

import com.google.gson.JsonObject;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element.ApplicationDataSourceFromJsonObjectMethodElement;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element.ApplicationDataSourceDataSourceLoadChangesMethodElement;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element.ApplicationDataSourceDataSourceSaveChangeMethodElement;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element.ApplicationDataSourceDataSourceToJsonObjectMethodElement;
import org.clawiz.ui.common.portal.application.datasource.execute.DataSourceExecuteContext;
import org.clawiz.ui.common.portal.application.datasource.model.DataSourceRowModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class ApplicationDataSourceDataSourcePrototypeComponent extends AbstractApplicationDataSourceJavaClassComponent {


    protected void addImports() {

        addImport(BigDecimal.class);
        addImport(Collection.class);
        addImport(ArrayList.class);
        addImport(CoreException.class);
        addImport(JsonObject.class);
        addImport(Comparator.class);
        addImport(DataSourceRowModel.class);
        addImport(DataSourceExecuteContext.class);
    }

    public class DataSourceContext {
        public String             tableName;
        public String             dataSourceVariableName;
        public String             rowModelClassName;
        public DataSource         api;
    }
    ArrayList<DataSourceContext> dataSourceContexts;

    public ArrayList<DataSourceContext> getDataSourceContexts() {
        return dataSourceContexts;
    }

    protected void prepareApiContexts() {

        dataSourceContexts = new ArrayList<>();

        for (org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext appluicationDataSourceContext : getApplicationContext().getDataSourceContexts() ) {

            DataSource dataSource = appluicationDataSourceContext.getDataSource();

            DataSourceContext ac = new DataSourceContext();
            dataSourceContexts.add(ac);
            ac.tableName = dataSource.getType().getTableName();
            ac.api       = dataSource;

            String dataSourceClassName   = dataSource.getJavaClassName() + "DataSource";
            String dataSourcePackageName = dataSource.getPackageName() + "." + dataSource.getJavaClassName().toLowerCase();

            ac.rowModelClassName = dataSource.getJavaClassName() + "RowModel";

            addImport(dataSourcePackageName + "." + dataSourceClassName);
            addImport(dataSourcePackageName + "." + ac.rowModelClassName);

            ac.dataSourceVariableName = StringUtils.toLowerFirstChar(dataSourceClassName);
            addVariable(dataSourceClassName, ac.dataSourceVariableName).withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);

        }

    }

    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getComponentNameByClass(ApplicationDataSourceDataSourceComponent.class) + "Prototype");

        setTypeParameters("R extends " + getGenerator().getComponentByClass(ApplicationAbstractDataSourceRowModelComponent.class).getName() );
        setExtends(getGenerator().getComponentByClass(ApplicationAbstractDataSourceDataSourceComponent.class).getName() + "<R>");

        addImports();

        prepareApiContexts();

        addElement(ApplicationDataSourceDataSourceToJsonObjectMethodElement.class);
        addElement(ApplicationDataSourceFromJsonObjectMethodElement.class);

        addElement(ApplicationDataSourceDataSourceLoadChangesMethodElement.class);
        addElement(ApplicationDataSourceDataSourceSaveChangeMethodElement.class);


    }
}
