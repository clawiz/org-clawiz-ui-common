/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.component;


import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValueList;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationDataSourceDataSourceComponent;

import java.math.BigDecimal;
import java.util.Date;

public class ApplicationAbstractServletPrototypeComponent extends  AbstractApplicationJavaClassComponent{

    protected void addImports() {
        addImport(getGenerator().getComponentByClass(ApplicationDataSourceDataSourceComponent.class));
        addImport(StringUtils.class);
        addImport(DateUtils.class);
        addImport(Date.class);
        addImport(ObjectService.class);
        addImport(Statement.class);
        addImport(SelectValueList.class);
        addImport(JsonObject.class);
        addImport(JsonElement.class);
        addImport(JsonNull.class);
    }


    public void addGetJsonElementAsString() {
        JavaMethodElement method = addMethod("getJsonElementAsString");
        method.setType("String");
        method.addParameter("JsonObject", "jsonObject");
        method.addParameter("String",    "elementName");
        method.addText("JsonElement element = jsonObject.get(elementName);");
        method.addText("return ( element == null || element instanceof JsonNull ) ? null : element.getAsString();");
    }

    public void addStringToBigDecimal() {
        JavaMethodElement method = addMethod("stringToBigDecimal");
        method.setType(BigDecimal.class);
        method.addParameter("String", "value");
        method.addText("return StringUtils.toBigDecimal(value);");
    }

    public void addStringToBoolean() {
        JavaMethodElement method = addMethod("stringToBoolean");
        method.setType(Boolean.class);
        method.addParameter("String", "value");
        method.addText("return StringUtils.toBoolean(value);");
    }

    public void addBigDecimalToString() {
        JavaMethodElement method = addMethod("bigDecimalToString");
        method.setType("String");
        method.addParameter("BigDecimal", "value");
        method.addText("return value == null ? null : value.toString();");
    }

    public void addBooleanToString() {
        JavaMethodElement method = addMethod("booleanToString");
        method.setType("String");
        method.addParameter("Boolean", "value");
        method.addText("return value == null ? \"false\" : ( value ? \"true\" : \"false\");");
    }

    public void addDateFormats() {
        JavaMethodElement method = addMethod("getDateFormat");
        method.setType("String");
        method.addText("return DateUtils.ISO8601_DATE_FORMAT;");

        method = addMethod("getDateTimeFormat");
        method.setType("String");
        method.addText("return DateUtils.ISO8601_DATE_TIME_FORMAT;");
    }

    public void addStringToDate() {
        JavaMethodElement method = addMethod("stringToDate");
        method.setType(Date.class);
        method.addParameter("String", "value");
        method.addText("return DateUtils.toDate(value, getDateFormat());");
    }

    public void addStringToDateTime() {
        JavaMethodElement method = addMethod("stringToDateTime");
        method.setType(Date.class);
        method.addParameter("String", "value");
        method.addText("return DateUtils.toDate(value, getDateTimeFormat());");
    }

    public void addDateToString() {
        JavaMethodElement method = addMethod("dateToString");
        method.setType("String");
        method.addParameter("Date", "date");
        method.addText("return DateUtils.toString(date, getDateFormat());");
    }

    public void addDateTimeToString() {
        JavaMethodElement method = addMethod("dateTimeToString");
        method.setType("String");
        method.addParameter("Date", "date");
        method.addText("return DateUtils.toString(date, getDateTimeFormat());");
    }

    public void addNodeGuidToObjectId() {
        JavaMethodElement method = addMethod("nodeGuidToObjectId");
        method.setType("BigDecimal");
        method.addParameter("String", "guid");
        method.addText("return dataSource.nodeGuidToObjectId(guid);");
    }

    public void addObjectIdToNodeGuid() {
        JavaMethodElement method = addMethod("objectIdToNodeGuid");
        method.setType("String");
        method.addParameter("BigDecimal", "id");
        method.addText("return dataSource.objectIdToNodeGuid(id);");
    }

    public void addObjectIdToString() {
        JavaMethodElement method = addMethod("objectIdToString");
        method.setType("String");
        method.addParameter("BigDecimal", "id");
        method.addText("return objectService.idToString(id);");
    }


    protected void addSplitQueryPattern() {
        JavaMethodElement method = addMethod("splitQueryPattern");
        method.setType("String[]");
        method.addParameter("String","pattern");

        method.addText("if ( pattern == null || pattern.length() == 0) { return new String[0]; }");
        method.addText("String[] result = StringUtils.splitAndTrim(pattern, \" \");");
        method.addText("for (int i=0; i < result.length; i++) {");
        method.addText("    result[i] = (result[i].length() > 0 ? \"%\" + result[i].toUpperCase() + \"%\" : \"%\");");
        method.addText("}");
        method.addText("return result;");
    }

    @Override
    public void process() {
        super.process();

        setName(getComponentByClass(ApplicationAbstractServletComponent.class).getName() + "Prototype");

        setExtends(AbstractHttpServlet.class.getName());

        addImports();

        addVariable(getGenerator().getComponentByClass(ApplicationDataSourceDataSourceComponent.class)
                .getName(), "dataSource").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        addVariable("ObjectService", "objectService");

        addGetJsonElementAsString();

        addStringToBigDecimal();
        addStringToBoolean();

        addBigDecimalToString();
        addBooleanToString();

        addDateFormats();
        addStringToDate();
        addStringToDateTime();
        addDateToString();
        addDateTimeToString();

        addNodeGuidToObjectId();
        addObjectIdToNodeGuid();
        addObjectIdToString();

        addSplitQueryPattern();

    }

}
