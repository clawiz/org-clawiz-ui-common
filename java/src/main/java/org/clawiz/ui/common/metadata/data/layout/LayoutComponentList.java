/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.layout;

import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;

import java.util.ArrayList;


public class LayoutComponentList extends MetadataNodeList<AbstractLayoutComponent> {

    private <T extends AbstractLayoutComponent> void _makeClassesSearch(LayoutComponentList container, ArrayList<T> result, Class<T> clazz) {
        for (AbstractLayoutComponent component : container ) {
            if ( clazz.isAssignableFrom(component.getClass()) ) {
                result.add((T)component);
            }
            if ( component instanceof AbstractContainer) {
                _makeClassesSearch(((AbstractContainer) component).getComponents(), result, clazz);
            }
        }
    }


    public <T extends AbstractLayoutComponent> ArrayList<T> getComponentsByClass(Class<T> clazz) {
        ArrayList<T> result = new ArrayList<>();

        _makeClassesSearch(this, result, clazz);

        return result;
    }



}
