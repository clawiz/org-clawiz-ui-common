/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;

public abstract class AbstractUiGeneratorContext {

    String packageName;
    String destinationPath;

    AbstractApplicationGeneratorContext applicationContext;
    AbstractApplicationGenerator applicationGenerator;


    public AbstractApplicationGeneratorContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(AbstractApplicationGeneratorContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public AbstractApplicationGenerator getApplicationGenerator() {
        return applicationGenerator;
    }

    public void setApplicationGenerator(AbstractApplicationGenerator applicationGenerator) {
        this.applicationGenerator = applicationGenerator;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public void prepare(Session session) {
        if ( applicationContext == null ) {
            throw new CoreException("Generate application context must be defined for view context");
        }
    }

    abstract public void setMetadataNode(MetadataNode metadataNode);

    abstract public MetadataNode getMetadataNode();
}
