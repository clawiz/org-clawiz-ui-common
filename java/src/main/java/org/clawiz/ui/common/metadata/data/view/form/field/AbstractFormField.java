/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.view.form.field;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.property.ReadOnlyFormFieldProperty;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;

import java.math.BigDecimal;

public class AbstractFormField extends AbstractFormFieldPrototype {

    String    hint;

    AbstractSelectValue selectValueContext;

    public BigDecimal getPrecision() {
        throw new CoreException("Method ?.getPrecision not implemented", this.getClass().getName());
    }

    public BigDecimal getScale() {
        throw new CoreException("Method ?.getScale not implemented", this.getClass().getName());
    }


    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isReadOnly() {
        return getProperties().get(ReadOnlyFormFieldProperty.class) != null;
    }

    public boolean isRequired() {
        return false;
    }

    public String getToStringJavaVariableName() {
        return getJavaVariableName() + "ToString";
    }

    public String getGetMethodName() {
        return (
                (getValueType() != null && getValueType() instanceof ValueTypeBoolean)
                ? "is" : "get"
                )
                + StringUtils.toUpperFirstChar(getJavaClassName());
    }

    public String getSetMethodName() {
        return "set" + StringUtils.toUpperFirstChar(getJavaClassName());
    }

    public String getGetToStringMethodName() {
        return getGetMethodName() + "ToString";
    }

    public String getSetToStringMethodName() {
        return getSetMethodName() + "ToString";
    }

    public AbstractValueType getValueType() {
        throw new CoreException("Method ?.getValueType not implemented", this.getClass().getName());
    }


    public TypeField getTypeField() {
        return null;
    }

    public AbstractSelectValue getSelectValueContext() {
        return selectValueContext;
    }

}
