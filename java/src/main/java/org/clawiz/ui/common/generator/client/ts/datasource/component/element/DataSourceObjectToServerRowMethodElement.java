/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;

public class DataSourceObjectToServerRowMethodElement extends AbstractClientDataSourceTypeScriptMethodElemenet {

    @Override
    public void process() {
        super.prepare();

        setName("objectToServerRow");
        addParameter("object", getObjectInterfaceName());

        addText("let row : any = {};");

        int     maxLength       = 0;
        boolean objectsExists   = false;
        boolean promiseDeclared = false;

        //noinspection Duplicates
        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }
            if ( field.getJavaVariableName().length() > maxLength ) {
                maxLength = field.getJavaVariableName().length();
            }
            if ( field.getValueType() instanceof ValueTypeObject) {
                objectsExists = true;
            }
        }

        addText("");
        addText("let me     = this;");
        addText("");

        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            if ( ! (field.getValueType() instanceof ValueTypeObject ) ) {

                String getter = "object." + field.getJavaVariableName();
                if ( ! ( field.getValueType() instanceof ValueTypeBoolean) ) {
                    getter = prepareFieldFromObjectGetter(field, getter);
                }

                addText("row." + field.getJavaVariableName()
                        + StringUtils.space(maxLength - field.getJavaVariableName().length())
                        + " = " + getter + ";");
            }
        }
        addText("");

        for(TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            if ( field.getValueType() instanceof ValueTypeObject ) {
                if ( promiseDeclared ) {
                    addText("    return me.idToGuid(object." + field.getJavaVariableName() + ");");
                    addText("  })");
                } else {
                    promiseDeclared = true;
                    addText("return me.idToGuid(object." + field.getJavaVariableName() + ")");
                }
                addText("  .then((guid) => {");
                addText("    row." + field.getJavaVariableName() + " = guid;");
            }
        }

        if ( promiseDeclared ) {

            addText("    return me.idToGuid(object.id)");
            addText("             .then((guid) => { ");
            addText("                               if ( ! guid ) {");
            addText("                                 return me.clawiz.getPromiseReject('Object id ? for ? not found in cw_core_objects or guid is null', object.id, me.table.name);");
            addText("                               }");
            addText("                               row.id = guid;");
            addText("                               return Promise.resolve(row); ");
            addText("                              })");
            addText("             .catch((reason) => { ");
            addText("                               return Promise.reject(reason);");
            addText("                              });");
            addText("  })");
            addText("  .catch((reason) => {");
            addText("    return me.clawiz.getPromiseReject('Exception on prepare row from object for table ? and id ? : ?', row.table_name, object.id, reason.message, reason);");
            addText("  });");
            addText("");

        } else {
            addText("return me.idToGuid(object.id)");
            addText("    .then((id) => {");
            addText("      row.id = id;");
            addText("      return Promise.resolve(row);");
            addText("    })");
            addText("  .catch((reason) => {");
            addText("    return me.clawiz.getPromiseReject('Exception on prepare row from object for table ? and id ? : ?', row.table_name, object.id, reason.message, reason);");
            addText("  });");
            addText("");
        }




    }
}
