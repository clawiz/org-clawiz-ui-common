/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.grid.model;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.StringMap;

import java.util.ArrayList;

public class AbstractGridModel<R extends GridRowModel> implements GridModel<R> {

    private GridRowModelList<R>               rows = new GridRowModelList<R>();
    private ArrayList<StringMap<JsonElement>> data = new ArrayList<>();
    private boolean                           allRowsFetched;

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public GridRowModelList<R> getRows() {
        return rows;
    }

    @Override
    public void addRow(R row) {
        rows.add(row);
    }

    public void setRows(GridRowModelList<R> rows) {
        this.rows = rows;
    }

    public ArrayList<StringMap<JsonElement>> getData() {
        return data;
    }

    public void setData(ArrayList<StringMap<JsonElement>> data) {
        this.data = data;
    }

    public boolean isAllRowsFetched() {
        return allRowsFetched;
    }

    public void setAllRowsFetched(boolean allRowsFetched) {
        this.allRowsFetched = allRowsFetched;
    }

     int totalRowsCount;

    public int getTotalRowsCount() {
        return totalRowsCount;
    }

    public void setTotalRowsCount(int totalRowsCount) {
        this.totalRowsCount = totalRowsCount;
    }
}
