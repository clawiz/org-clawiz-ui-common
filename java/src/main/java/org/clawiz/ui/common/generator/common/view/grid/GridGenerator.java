/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.view.grid;


import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.AbstractUiGenerator;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.AbstractApplicationGenerator;

public class GridGenerator extends AbstractUiGenerator {

    GridGeneratorContext gridContext;
    GridGeneratorHelper gridHelper;

    public GridGeneratorContext getContext() {
        return gridContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        this.gridContext = (GridGeneratorContext) context;
    }

    public AbstractApplicationGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }


    public Grid getGrid() {
        return getContext().getGrid();
    }

    @Override
    protected void process() {
        super.process();

        if ( getGrid().getTopToolbar() == null ) {
            Toolbar toolbar = getGrid().createChildNode(Toolbar.class, "topToolbar");
            getGrid().setTopToolbar(toolbar);
            toolbar.setLeft(toolbar.createChildNode(AbstractContainer.class, "left"));
            toolbar.setRight(toolbar.createChildNode(AbstractContainer.class, "right"));
        }
        if ( getGrid().getBottomToolbar() == null ) {
            Toolbar toolbar = getGrid().createChildNode(Toolbar.class, "bottomToolbar");
            getGrid().setBottomToolbar(toolbar);
            toolbar.setLeft(toolbar.createChildNode(AbstractContainer.class, "left"));
            toolbar.setRight(toolbar.createChildNode(AbstractContainer.class, "right"));
        }

        if ( ! getGrid().isGeneratorHelperProcessed() ) {
            gridHelper.prepare(getGrid());
            getGrid().setGeneratorHelperProcessed(true);
        }

    }
}

