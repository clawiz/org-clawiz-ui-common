/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.StringMap;
import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValueList;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.generator.server.java.application.common.form.component.ApplicationAbstractFormApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.*;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.choosevaluerequest.FormApiServletTypeQuerySelectValuesMethodElement;
import org.clawiz.ui.common.generator.server.java.view.form.component.AbstractFormJavaClassComponent;

public class FormApiServletPrototypeComponent extends AbstractFormJavaClassComponent {

    public static final String ADD_VARIABLES_EXTENSION_NAME = "addFormApiServletPrototypeVariables";

    FormApiServletProcessDoPostMethodElement processDoPost;

    public static String getServletPath(Form form) {
        return "/api/form/" + StringUtils.toLowerFirstChar(form.getJavaClassName());
    }

    public FormApiServletProcessDoPostMethodElement getProcessDoPost() {
        return processDoPost;
    }

    protected void addImports() {
        addImport(StringUtils.class);
        addImport(JsonObject.class);
        addImport(JsonElement.class);
        addImport(JsonPrimitive.class);
        addImport(StringMap.class);
        addImport(AbstractHttpRequestContext.class);
        addImport(ApiResponseContext.class);
        addImport(Statement.class);
        addImport(Statement.class);
        addImport(Query.class);
        addImport(SelectValueList.class);

        if ( getGenerator().getRootType() != null ) {
            Type rootType = getGenerator().getRootType();
            addImport(rootType.getPackageName() + "." + rootType.getJavaClassName().toLowerCase() + "." + rootType.getJavaClassName() + "Model");
            addImport(rootType.getPackageName() + "." + rootType.getJavaClassName().toLowerCase() + "." + rootType.getJavaClassName() + "Service");
            addImport(rootType.getPackageName() + "." + rootType.getJavaClassName().toLowerCase() + "." + rootType.getJavaClassName() + "Object");
        }
    }

    protected void addVariables() {

        if ( getGenerator().getRootType() != null ) {
            Type rootType = getGenerator().getRootType();
            addVariable(rootType.getJavaClassName() + "Model", rootType.getJavaVariableName() + "Model")
                    .withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
            addVariable(rootType.getJavaClassName() + "Service", rootType.getJavaVariableName() + "Service")
                    .withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        }


        processExtensions(ADD_VARIABLES_EXTENSION_NAME);

    }

    protected void addGetPath() {
        JavaMethodElement pathMethod = addMethod("String", "getPath");
        pathMethod.addText("return \"" + getServletPath(getForm()) + "\";");
    }

    protected void addQuerySelectValuesMethods() {
        for(AbstractFormField formField : getForm().getFields() ) {
            AbstractSelectValue svc = formField.getSelectValueContext();
            if ( svc == null || svc instanceof StaticListSelectValue) {
                continue;
            }

            if ( svc instanceof TypeSelectValue) {
                addElement(FormApiServletTypeQuerySelectValuesMethodElement.class).setFormField(formField);
//            } else if ( svc instanceof JdbcValueSelectContext) {
//                addElement(FormApiServletProcessJdbcSelectChooseValueRequestMethodElement.class).setFormField(formField);
            } else {
                throwException("Wrong field '?' choose value context class '?'", new Object[]{formField.getJavaClassName(), svc.getClass().getName()});
            }

        }
    }

/*
    protected void addButtonActions() {
        for(Button button : getForm().getComponentsByClass(Button.class)) {
            FormApiServletButtonActionMethodElement buttonElement = addElement(FormApiServletButtonActionMethodElement.class);
            buttonElement.setButton(button);
        }
    }
*/

    @Override
    public void process() {
        super.process();

        setName(getFormGenerator().getComponentByClass(FormApiServletComponent.class).getName() + "Prototype");

        AbstractJavaClassComponent abstractServlet =  getFormGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractFormApiServletComponent.class);
        addImport(abstractServlet.getFullName());
        setExtends(abstractServlet.getName() + "<" + getGenerator().getRequestContextClassName() + ">");

        addImports();
        addVariables();

        addGetPath();

        addElement(FormApiServletCreateRequestMethodElement.class);

        addElement(FormApiServletPrepareQueryMethodElement.class);
        addElement(FormApiServletPrepareModelMethodElement.class);
        addElement(FormApiServletCreateMethodElement.class);
        addElement(FormApiServletLoadMethodElement.class);

        addQuerySelectValuesMethods();
        addElement(FormApiServletQuerySelectValuesMethodElement.class);

        addElement(FormApiServletProcessSaveMethodElement.class);
        addElement(FormApiServletSaveMethodElement.class);

        processDoPost = addElement(FormApiServletProcessDoPostMethodElement.class);

        addElement(FormApiServletProcessExceptionMethodElement.class);

        addElement(FormApiServletModelToStringMapMethodElement.class);
        addElement(FormApiServletPrepareResponseMethodElement.class);

//        addElement(FormApiServletDoGetMethodElement.class);
        addElement(FormApiServletDoPostMethodElement.class);

//        addChooseValueContexts();
//        addButtonActions();


    }

}
