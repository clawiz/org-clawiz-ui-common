package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container;

import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.ui.common.metadata.data.layout.container.column.Column;

public class MpsColumnPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer {
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200665078";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Column";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) Column.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        Column structure = (Column) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        Column structure = (Column) node;
        
    }
}
