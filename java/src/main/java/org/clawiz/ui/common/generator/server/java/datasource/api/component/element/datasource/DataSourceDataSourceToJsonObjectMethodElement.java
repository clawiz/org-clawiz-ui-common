/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component.element.datasource;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationAbstractDataSourceExecuteContextComponent;

public class DataSourceDataSourceToJsonObjectMethodElement extends AbstractDataSourceDataSourceJavaClassMethodElement {

    @Override
    public void process() {
        super.process();

        setName("toJsonObject");

        setType("JsonObject");

        String contextClassName = getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractDataSourceExecuteContextComponent.class).getName();
        addParameter(contextClassName, "context");
        addParameter(getRowModelClassName() + "", "row");


        addText("");
        addText("JsonObject object = new JsonObject();");
        addText("set(object, \"id\",         objectIdToNodeGuid(context, row.getId()));");
        addText("set(object, \"scn\",        row.getScn());");
        addText("set(object, \"action\",     row.getAction());");
        addText("set(object, \"table_name\", \"" + getDataSource().getType().getTableName() + "\");");
        addText("");

        int maxLength = 0;
        for ( TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getJavaVariableName().length() > maxLength ) {
                maxLength = field.getJavaVariableName().length();
            }
        }
        for ( TypeField field : getDataSource().getType().getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {  continue;  }


            String getter = "row." + field.getGetMethodName() + "()";
            if ( field.getValueType() instanceof ValueTypeObject) {
                getter = "objectIdToNodeGuid(context, " + getter + ")";
            } else if ( field.getValueType() instanceof ValueTypeEnumeration ) {
                getter = getter + " != null ? " + getter + ".toString() : null";
            }

            addText("set(object, \"" + field.getJavaVariableName()
                    + "\"," + StringUtils.space(maxLength - field.getJavaVariableName().length())
                    + " " + getter + ");");
        }
        addText("");

        addText("return object;");

    }
}
