/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form;


import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGenerator;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.*;

public class ServerFormGenerator extends FormGenerator {

    ServerFormGeneratorContext formContext;
    FormApiModelComponent modelClassComponent;
    FormApiRequestContextComponent requestContextComponent;
    FormApiResponseContextComponent responseContextComponent;

    public ServerFormGeneratorContext getContext() {
        return formContext;
    }

    public ServerJavaApplicationGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        formContext = (ServerFormGeneratorContext) context;
    }

    @Override
    public String getPackageName() {
        return formContext.getPackageName() != null ? formContext.getPackageName() : super.getPackageName();
    }

    public String getModelClassName() {
        return modelClassComponent.getName();
    }

    public String getRequestContextClassName() {
        return requestContextComponent.getName();
    }

    public String getResponseContextClassName() {
        return responseContextComponent.getName();
    }

    public <T extends FormApiServletPrototypeComponent> Class<T> getApiServletPrototypeComponentClass() {
        return (Class<T>) FormApiServletPrototypeComponent.class;
    }

    public Type getRootType() {
        return getForm().getRootType();
    }

    protected void addFieldSelectValueClasses() {

        for ( AbstractFormField formField : getFields() ) {
            if ( formField.getSelectValueContext() == null || formField.getSelectValueContext() instanceof StaticListSelectValue) {
                continue;
            }
            addComponent(FormApiFieldSelectValueModelComponent.class).setFormField(formField);
            addComponent(FormApiFieldSelectValueModelPrototypeComponent.class).setFormField(formField);
            addComponent(FormApiFieldSelectValueComponent.class).setFormField(formField);
            addComponent(FormApiFieldSelectValuePrototypeComponent.class).setFormField(formField);
        }

    }

    @Override
    protected void process() {
        logDebug("Start generate form '" + getPackageName() + "." + getForm().getJavaClassName() + "'");
        super.process();

        addFieldSelectValueClasses();

        modelClassComponent = addComponent(FormApiModelComponent.class);
        addComponent(FormApiModelPrototypeComponent.class);

        responseContextComponent = addComponent(FormApiResponseContextComponent.class);
        requestContextComponent = addComponent(FormApiRequestContextComponent.class);

        addComponent(FormApiResponseContextPrototypeComponent.class);
        addComponent(FormApiRequestContextPrototypeComponent.class);

        addComponent(FormApiServletComponent.class);
        addComponent(getApiServletPrototypeComponentClass());

    }

    @Override
    protected void done() {
        super.done();
        logDebug("Done generate form '" + getPackageName() + "." + getForm().getJavaClassName() + "'");
    }
}
