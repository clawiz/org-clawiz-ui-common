/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.install;

import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.application.Application;

public abstract class AbstractXMLScriptApplicationInstaller extends AbstractXMLScriptElementInstaller {

    Application    application;
    MetadataBase   metadataBase;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void process() {

        String packageName     = getPackageName();

        String applicationName = getAttribute("name");
        if ( applicationName == null ) {
            throwException("Parameter 'name' not defined for application install in script ?", getScriptFileName());
        }

        String tokens[] = StringUtils.splitByLastTokenOccurrence(applicationName, "\\.");

        if ( tokens.length > 1 ) {
            applicationName = tokens[1];
            packageName     += "." + tokens[0];
        }


        setApplication(metadataBase.getNode(Application.class, packageName, applicationName, true));
    }
}
