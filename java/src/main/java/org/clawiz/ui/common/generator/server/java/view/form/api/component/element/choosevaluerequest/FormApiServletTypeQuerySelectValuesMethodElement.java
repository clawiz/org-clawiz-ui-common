/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element.choosevaluerequest;


import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.portal.application.form.query.QuerySelectValuesContext;

import java.util.ArrayList;

public class FormApiServletTypeQuerySelectValuesMethodElement extends AbstractFormApiServletQuerySelectValuesMethodElement {


    @Override
    public void process() {

        super.process();

        getComponent().addImport(Statement.class);
        getComponent().addImport(QuerySelectValuesContext.class);

        String valueClassName = getFormField().getJavaClassName() + "SelectValue";
        String modelClassName = getFormField().getJavaClassName() + "SelectValueModel";

        setType("SelectValueList<" + valueClassName + ">");

        TypeSelectValue typeContext = (TypeSelectValue) formField.getSelectValueContext();

        TypeKey queryKey    = typeContext.getQueryKey();
        TypeKey toStringKey = typeContext.getToStringKey();

        ArrayList<String> columnAppends = new ArrayList<>();
        ArrayList<String> orderAppends = new ArrayList<>();
        for ( TypeKeyField keyField : toStringKey.getFields() ) {
            TypeField typeField  = keyField.getField();
            String    orderColumnName = typeField.isUpperFieldExists() ? typeField.getUpperColumnName() : typeField.getColumnName();

            columnAppends.add("queryContext.addColumn(\"" + typeField.getColumnName() + "\");");
            orderAppends.add("queryContext.addOrderBy(\"" + orderColumnName + "\");");
        }


        addText("SelectValueList<" + valueClassName + "> values = new SelectValueList<>();");
        addText("");
        addText("QuerySelectValuesContext queryContext = new QuerySelectValuesContext();");
        addText("queryContext.setPattern(pattern);");
        addText("queryContext.setPatterns(splitQueryPattern(pattern));");
        addText("queryContext.setFrom(\"" + typeContext.getType().getTableName() + "\");");
        addText("");

        addText("");
        addText("queryContext.addColumn(\"id\");");
        addText("");
        for ( String str : columnAppends ) {
            addText(str);
        }
        addText("");

        for (TypeKeyField keyField : queryKey.getFields() ) {
            TypeField typeField  = keyField.getField();

            addText("queryContext.addPatternWhere(\""
                    + ( typeField.isUpperFieldExists() ? typeField.getUpperColumnName() : typeField.getColumnName() )
                    + " like ?\");");

        }

        addText("");
        for ( String str : orderAppends ) {
            addText(str);
        }
        addText("");

        addText("");
        addText("Statement statement = executeQuery(queryContext.getSql(), queryContext.getParametersArray());");
        addText("int       iCount    = 10;");
        addText("while ( statement.next() && iCount-- > 0 ) {");
        addText("    " + valueClassName + "      value = new " + valueClassName + "();");
        addText("    " + modelClassName + " data  = new " + modelClassName + "();");
        addText("    value.setData(data);");
        addText("    values.add(value);");
        addText("");
        addText("    value.setValue(objectIdToNodeGuid(statement.getBigDecimal(1)));");
        addText("    data.setId(statement.getBigDecimal(1));");
        int index = 2;
        for ( TypeKeyField keyField : toStringKey.getFields() ) {
            TypeField typeField = keyField.getField();
            String getClassName = typeField.getValueType() instanceof ValueTypeEnumeration ? "String" : typeField.getValueType().getStatementValueTypeName();
            String getter = "statement.get" + typeField.getValueType().getStatementValueTypeName() +"(" + index + ")";
            if ( typeField.getValueType() instanceof ValueTypeBoolean) {
                getter = "StringUtils.toBoolean(" + getter + ")";
            } else if ( typeField.getValueType() instanceof ValueTypeEnumeration ) {
                getter = typeField.getType().getServicePackageName() + "." + typeField.getValueTypeJavaClassName()
                        + ".to" + typeField.getValueTypeJavaClassName() + "(" + getter + ")";
            }


            addText("    data." + typeField.getSetMethodName() + "(" + getter + ");");
        }
        addText("");
        addText("    value.prepare(getSession());");
        addText("    data.setId(null);");
        addText("");
        addText("}");
        addText("statement.close();");
        addText("");


        addText("return values;");

    }
}
