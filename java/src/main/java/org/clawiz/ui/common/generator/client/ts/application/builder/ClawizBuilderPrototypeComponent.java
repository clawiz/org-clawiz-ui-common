/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.application.builder;

import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptFieldElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.element.ClawizBuilderInitMethodElement;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigImplementationComponent;

public class ClawizBuilderPrototypeComponent extends AbstractClientApplicationTypeScriptBuilderComponent {

    ApplicationClawizComponent clawizComponent;

    protected void addClawizVariable() {

        TypeScriptFieldElement clawizField = addField(StringUtils.toLowerFirstChar(clawizComponent.getName()), clawizComponent.getName());
        clawizField.setStatic(true);

        TypeScriptMethodElement getter = addMethod("clawiz")
                .withMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
        getter.setType(clawizComponent.getName());
        getter.setStatic(true);

        getter.addText("return " + getName() + "." + clawizField.getName() + ";");

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(ClawizBuilderComponent.class).getName() + "Prototype");


        setExtends(TypeScriptClientCommonGeneratorContext.CLASS_NAME_CLAWIZ_BUILDER);


        clawizComponent = getGenerator().getComponentByClass(ApplicationClawizComponent.class);

        addImport(TypeScriptClientCommonGeneratorContext.CLASS_NAME_CLAWIZ_BUILDER);
        addImport(clawizComponent);
        addImport(getGenerator().getComponentByClass(ApplicationClawizImplementationComponent.class));
        addImport(getGenerator().getComponentByClass(ApplicationClawizConfigComponent.class));
        addImport(getGenerator().getComponentByClass(ApplicationClawizConfigImplementationComponent.class));

        addClawizVariable();

        addElement(ClawizBuilderInitMethodElement.class);

    }
}
