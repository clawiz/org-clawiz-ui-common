/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.form.response;


import com.google.gson.JsonElement;
import com.google.gson.internal.StringMap;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValueList;
import org.clawiz.ui.common.portal.application.form.model.FormModel;

public abstract class FormApiResponseContext<M extends FormModel> extends ApiResponseContext {

    M model;
    StringMap<JsonElement>            data         = new StringMap<>();
    SelectValueList                   selectValues;

    public M getModel() {
        return model;
    }

    public void setModel(M model) {
        this.model = model;
    }

    public StringMap<JsonElement> getData() {
        return data;
    }

    public void setData(StringMap<JsonElement> data) {
        this.data = data;
    }

    public SelectValueList getSelectValues() {
        return selectValues;
    }

    public void setSelectValues(SelectValueList selectValues) {
        this.selectValues = selectValues;
    }

/*


    private ResponseActionList actions = new ResponseActionList();

    public ResponseActionList getActions() {
        return actions;
    }

    public void addAction(ResponseAction action) {
        getActions().add(action);
    }

    public ChooseValueRequestContext chooseValues;
    public ChooseValueRequestContext getChooseValues() {
        return chooseValues;
    }

    public void setChooseValues(ChooseValueRequestContext chooseValues) {
        this.chooseValues = chooseValues;
    }

*/

}
