/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.AbstractSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValueModel;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.type.TypeSelectValue;
import org.clawiz.ui.common.generator.server.java.view.form.component.AbstractFormJavaClassComponent;

public class FormApiFieldSelectValueModelPrototypeComponent extends AbstractFormJavaClassComponent {


    AbstractFormField formField;

    public AbstractFormField getFormField() {
        return formField;
    }

    public void setFormField(AbstractFormField formField) {
        this.formField = formField;
    }

    public AbstractSelectValue getSelectValueContext(){
        return getFormField().getSelectValueContext();
    }

    protected void prepareTypeContextFields(TypeSelectValue context) {

        if ( context.getType().getToStringKey() == null ) {
            throwException("Type ? toStringKey not defined", context.getType().getFullName());
        }

        for (TypeKeyField keyField : context.getToStringKey().getFields() ) {
            TypeField field = keyField.getField();

            String valueTypeClassName = field.getValueTypeJavaClassName();
            if ( field.getValueType() instanceof ValueTypeEnumeration ) {
                valueTypeClassName = field.getType().getServicePackageName() + "." + valueTypeClassName;
            }
            addVariable(valueTypeClassName, field.getJavaVariableName(), null, true, true);
        }

    }

    @Override
    public void process() {
        super.process();


        boolean found = false;
        for (FormApiFieldSelectValueModelComponent selectValueModelComponent : getGenerator().getComponentListByClass(FormApiFieldSelectValueModelComponent.class) ) {
            if ( selectValueModelComponent.getFormField() == getFormField() ) {
                setName(selectValueModelComponent.getName() + "Prototype");
                found = true;
                break;
            }
        }

        if ( ! found ) {
            throwException("Internal error. Generator does not contain component FormApiFieldSelectValueModelComponent for field ?", getFormField().getName());
        }

        addImport(SelectValueModel.class);
        setExtends("SelectValueModel");

        if ( getSelectValueContext() instanceof TypeSelectValue) {
            prepareTypeContextFields((TypeSelectValue) getSelectValueContext());
        } else {
            throwException("Wrong select value context class ?", getFormField().getClass().getName());
        }


    }
}
