package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.application;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsApplicationPrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsFormReference> forms = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGridReference> grids = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsMenuReference> menus = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreenReference> screens = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSourceReference> dataSources = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsFormReference> getForms() {
        return this.forms;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGridReference> getGrids() {
        return this.grids;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsMenuReference> getMenus() {
        return this.menus;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreenReference> getScreens() {
        return this.screens;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSourceReference> getDataSources() {
        return this.dataSources;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2436863927721479196";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Application";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721479196", "org.clawiz.ui.common.language.structure.Application", ConceptPropertyType.CHILD, "111278499620156423", "forms"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721479196", "org.clawiz.ui.common.language.structure.Application", ConceptPropertyType.CHILD, "111278499620156426", "grids"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721479196", "org.clawiz.ui.common.language.structure.Application", ConceptPropertyType.CHILD, "2436863927721525827", "menus"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721479196", "org.clawiz.ui.common.language.structure.Application", ConceptPropertyType.CHILD, "111278499620156438", "screens"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2436863927721479196", "org.clawiz.ui.common.language.structure.Application", ConceptPropertyType.CHILD, "2062302247217797210", "dataSources"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getForms() ) {
            addConceptNodeChild("2436863927721479196", "forms", value);
        }
        for (AbstractMpsNode value : getGrids() ) {
            addConceptNodeChild("2436863927721479196", "grids", value);
        }
        for (AbstractMpsNode value : getMenus() ) {
            addConceptNodeChild("2436863927721479196", "menus", value);
        }
        for (AbstractMpsNode value : getScreens() ) {
            addConceptNodeChild("2436863927721479196", "screens", value);
        }
        for (AbstractMpsNode value : getDataSources() ) {
            addConceptNodeChild("2436863927721479196", "dataSources", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.application.Application.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.application.Application structure = (org.clawiz.ui.common.metadata.data.application.Application) node;
        
        structure.getForms().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsFormReference mpsNode : getForms() ) {
            if ( mpsNode.getForm() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "forms", true, mpsNode.getForm());
            } else {
                structure.getForms().add(null);
            }
        }
        
        structure.getGrids().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGridReference mpsNode : getGrids() ) {
            if ( mpsNode.getGrid() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "grids", true, mpsNode.getGrid());
            } else {
                structure.getGrids().add(null);
            }
        }
        
        structure.getMenus().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsMenuReference mpsNode : getMenus() ) {
            if ( mpsNode.getMenu() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "menus", true, mpsNode.getMenu());
            } else {
                structure.getMenus().add(null);
            }
        }
        
        structure.getScreens().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreenReference mpsNode : getScreens() ) {
            if ( mpsNode.getScreen() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "screens", true, mpsNode.getScreen());
            } else {
                structure.getScreens().add(null);
            }
        }
        
        structure.getDataSources().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSourceReference mpsNode : getDataSources() ) {
            if ( mpsNode.getDataSource() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "dataSources", true, mpsNode.getDataSource());
            } else {
                structure.getDataSources().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.application.Application structure = (org.clawiz.ui.common.metadata.data.application.Application) node;
        
        getForms().clear();
        for (org.clawiz.ui.common.metadata.data.view.form.Form metadataNode : structure.getForms() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "forms", true, metadataNode);
            } else {
                getForms().add(null);
            }
        }
        
        getGrids().clear();
        for (org.clawiz.ui.common.metadata.data.view.grid.Grid metadataNode : structure.getGrids() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "grids", true, metadataNode);
            } else {
                getGrids().add(null);
            }
        }
        
        getMenus().clear();
        for (org.clawiz.ui.common.metadata.data.menu.Menu metadataNode : structure.getMenus() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "menus", true, metadataNode);
            } else {
                getMenus().add(null);
            }
        }
        
        getScreens().clear();
        for (org.clawiz.ui.common.metadata.data.view.screen.Screen metadataNode : structure.getScreens() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "screens", true, metadataNode);
            } else {
                getScreens().add(null);
            }
        }
        
        getDataSources().clear();
        for (org.clawiz.ui.common.metadata.data.datasource.DataSource metadataNode : structure.getDataSources() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "dataSources", true, metadataNode);
            } else {
                getDataSources().add(null);
            }
        }
        
    }
}
