package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractGridColumnPropertyPrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "968894741575457380";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractGridColumnProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty) node;
        
    }
}
