package org.clawiz.ui.common.metadata.data.application;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ApplicationPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.form.FormList forms = new org.clawiz.ui.common.metadata.data.view.form.FormList();
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.grid.GridList grids = new org.clawiz.ui.common.metadata.data.view.grid.GridList();
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.menu.MenuList menus = new org.clawiz.ui.common.metadata.data.menu.MenuList();
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.screen.ScreenList screens = new org.clawiz.ui.common.metadata.data.view.screen.ScreenList();
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.datasource.DataSourceList dataSources = new org.clawiz.ui.common.metadata.data.datasource.DataSourceList();
    
    public Application withName(String value) {
        setName(value);
        return (Application) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.FormList getForms() {
        return this.forms;
    }
    
    public Application withForm(org.clawiz.ui.common.metadata.data.view.form.Form value) {
        getForms().add(value);
        return (Application) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.GridList getGrids() {
        return this.grids;
    }
    
    public Application withGrid(org.clawiz.ui.common.metadata.data.view.grid.Grid value) {
        getGrids().add(value);
        return (Application) this;
    }
    
    public org.clawiz.ui.common.metadata.data.menu.MenuList getMenus() {
        return this.menus;
    }
    
    public Application withMenu(org.clawiz.ui.common.metadata.data.menu.Menu value) {
        getMenus().add(value);
        return (Application) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.screen.ScreenList getScreens() {
        return this.screens;
    }
    
    public Application withScreen(org.clawiz.ui.common.metadata.data.view.screen.Screen value) {
        getScreens().add(value);
        return (Application) this;
    }
    
    public org.clawiz.ui.common.metadata.data.datasource.DataSourceList getDataSources() {
        return this.dataSources;
    }
    
    public Application withDataSource(org.clawiz.ui.common.metadata.data.datasource.DataSource value) {
        getDataSources().add(value);
        return (Application) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getForms()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getGrids()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getMenus()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getScreens()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getDataSources()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getForms()) {
            references.add(node);
        }
        
        for (MetadataNode node : getGrids()) {
            references.add(node);
        }
        
        for (MetadataNode node : getMenus()) {
            references.add(node);
        }
        
        for (MetadataNode node : getScreens()) {
            references.add(node);
        }
        
        for (MetadataNode node : getDataSources()) {
            references.add(node);
        }
        
    }
}
