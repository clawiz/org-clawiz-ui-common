/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.grid;

import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.TypeScriptGridComponentComponent;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.TypeScriptGridComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.TypeScriptGridRowModelComponent;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.TypeScriptGridRowModelPrototypeComponent;
import org.clawiz.ui.common.generator.common.view.grid.GridGenerator;

public class TypeScriptUiGridGenerator extends GridGenerator {

    TypeScriptUiGridGeneratorContext gridGeneratorContext;
    TypeScriptGridRowModelComponent  rowModelComponent;

    @Override
    public TypeScriptUiGridGeneratorContext getContext() {
        return gridGeneratorContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        gridGeneratorContext = (TypeScriptUiGridGeneratorContext) context;
        super.setContext(context);
    }

    protected <T extends TypeScriptGridComponentComponent> Class<T> getGridViewComponentClass() {
        return (Class<T>) TypeScriptGridComponentComponent.class;
    }

    protected <T extends TypeScriptGridComponentPrototypeComponent> Class<T> getGridViewComponentPrototypeClass() {
        return (Class<T>) TypeScriptGridComponentPrototypeComponent.class;
    }

    public String getRowModelClassName() {
        return rowModelComponent.getName();
    }

    @Override
    protected void process() {
        super.process();

        logDebug("Start generating grid " + getGrid().getName());

        rowModelComponent = addComponent(TypeScriptGridRowModelComponent.class);
        addComponent(TypeScriptGridRowModelPrototypeComponent.class);

        addComponent(getGridViewComponentClass());
        addComponent(getGridViewComponentPrototypeClass());


        logDebug("Done generating grid " + getGrid().getName());
    }
}
