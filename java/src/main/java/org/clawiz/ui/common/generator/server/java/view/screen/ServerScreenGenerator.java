/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.screen;


import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.metadata.data.view.screen.Screen;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.common.view.screen.ScreenGenerator;
import org.clawiz.ui.common.generator.server.java.view.screen.servlet.component.ScreenApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.screen.servlet.component.ScreenApiServletPrototypeComponent;

public class ServerScreenGenerator extends ScreenGenerator {

    public static final String SCREENS_PACKAGE_NAME = "screen";

    ServerScreenGeneratorContext screenContext;

    ScreenApiServletComponent          apiServlet;
    ScreenApiServletPrototypeComponent apiServletPrototype;

    public ServerScreenGeneratorContext getContext() {
        return screenContext;
    }

    public void setScreenContext(ServerScreenGeneratorContext screenContext) {
        this.screenContext = screenContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        setScreenContext((ServerScreenGeneratorContext) context);
    }

    public String getScreenPackageName(Screen screen) {
        return getPackageName() + "." + SCREENS_PACKAGE_NAME + "." + GeneratorUtils.toJavaClassName(screen.getName()).toLowerCase();
    }

    public ScreenApiServletComponent getApiServlet() {
        return apiServlet;
    }

    public void setApiServlet(ScreenApiServletComponent apiServlet) {
        this.apiServlet = apiServlet;
    }

    public ScreenApiServletPrototypeComponent getApiServletPrototype() {
        return apiServletPrototype;
    }

    public void setApiServletPrototype(ScreenApiServletPrototypeComponent apiServletPrototype) {
        this.apiServletPrototype = apiServletPrototype;
    }

    protected void addApiServletComponents() {
        setApiServlet(addComponent(ScreenApiServletComponent.class));
        setApiServletPrototype(addComponent(ScreenApiServletPrototypeComponent.class));
    }


    @Override
    protected void process() {

        super.process();
        logDebug("Start generate screen '" + getPackageName() + "." + getScreen().getName() + "'");

        addApiServletComponents();

    }

    @Override
    protected void done() {
        logDebug("Done generate screen '" + getPackageName() + "." + getScreen().getName() + "'");
        super.done();
    }
}
