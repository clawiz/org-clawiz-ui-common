/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form.component;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.client.ts.AbstractTypeScriptClientClassComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;


public class AbstractTypeScriptClassFormComponent extends AbstractTypeScriptClientClassComponent {

    TypeScriptUiFormGenerator formGenerator;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        formGenerator = (TypeScriptUiFormGenerator) generator;
    }

    @Override
    public TypeScriptUiFormGenerator getGenerator() {
        return formGenerator;
    }

    @Override
    protected TypeScriptClientCommonGenerator getApplicationGenerator() {
        return getGenerator().getContext().getApplicationGenerator();
    }

    public Form getForm() {
        return getGenerator().getForm();
    }

    public FormFieldList getFields() {
        return getGenerator().getFields();
    }

    public FormFieldList getUnitedWithRootTypeFormFields() {
        return getGenerator().getUnitedWithRootTypeFormFields();
    }

    @SuppressWarnings("Duplicates")
    public String toTypeScriptType(AbstractFormField formField) {

        if ( formField.getTypeField() == null ) {
            return toTypeScriptType(formField.getValueType());
        }

        if ( ! (formField.getValueType() instanceof ValueTypeEnumeration) ) {
            return toTypeScriptType(formField.getValueType());
        }

        DataSource ds       = getApplicationGenerator().getContext().getTypeDataSource(formField.getTypeField().getType());
        String     typeName = ds.getJavaClassName() + formField.getTypeField().getValueTypeJavaClassName();

        addImport(
                formField.getTypeField().getValueTypeJavaClassName()
                , typeName
                , getApplicationGenerator().getDataSourceImportPath(ds));


        return typeName;
    }

    public String toTypeScriptType(Grid grid) {

        String typeName = grid.getJavaClassName() + "Component";

        addImport(
                typeName
                , getApplicationGenerator().getApplicationDestinationPath()
                        + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), grid.getPackageName())
                        + "/" + grid.getJavaClassName().toLowerCase()
                        + "/" + nameToDottedFileName(typeName));


        return typeName;
    }

    @Override
    public String getDestinationPath() {
        return getApplicationGenerator().getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), getForm().getPackageName())
                + "/" + getForm().getJavaClassName().toLowerCase();
    }
}
