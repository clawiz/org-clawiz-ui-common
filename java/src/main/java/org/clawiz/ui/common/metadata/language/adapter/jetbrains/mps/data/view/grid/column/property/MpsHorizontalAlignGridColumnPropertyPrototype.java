package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsHorizontalAlignGridColumnPropertyPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsAbstractGridColumnProperty {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign align;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign getAlign() {
        return this.align;
    }
    
    public void setAlign(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.align = null;
        }
        this.align = org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign.toMpsHorizontalAlign(value);
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6422760559407755898";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.HorizontalAlignGridColumnProperty";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6422760559407755898", "org.clawiz.ui.common.language.structure.HorizontalAlignGridColumnProperty", ConceptPropertyType.PROPERTY, "6422760559407755899", "align"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("6422760559407755898", "align", org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.align.MpsHorizontalAlign.toConceptNodePropertyString(getAlign()));
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty) node;
        
        structure.setAlign(( getAlign() != null ? org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign.valueOf(getAlign().toString()) : null ));
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty structure = (org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty) node;
        
        setAlign(structure.getAlign() != null ? structure.getAlign().toString() : null);
        
    }
}
