package org.clawiz.ui.common.metadata.data.view.grid.property;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class RowsPerPageGridPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty {
    
    @ExchangeAttribute
    private BigDecimal count;
    
    public RowsPerPageGridProperty withName(String value) {
        setName(value);
        return (RowsPerPageGridProperty) this;
    }
    
    public BigDecimal getCount() {
        return this.count;
    }
    
    public void setCount(BigDecimal value) {
        this.count = value;
    }
    
    public RowsPerPageGridProperty withCount(BigDecimal value) {
        setCount(value);
        return (RowsPerPageGridProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
