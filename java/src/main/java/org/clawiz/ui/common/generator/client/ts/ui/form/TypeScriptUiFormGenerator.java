/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form;

import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormModelComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormModelPrototypeComponent;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;

public class TypeScriptUiFormGenerator extends FormGenerator {

    TypeScriptUiFormGeneratorContext formGeneratorContext;

    TypeScriptFormModelComponent     modelComponent;

    @Override
    public TypeScriptUiFormGeneratorContext getContext() {
        return formGeneratorContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        formGeneratorContext = (TypeScriptUiFormGeneratorContext) context;
        super.setContext(context);
    }

    protected <T extends TypeScriptFormModelComponent> Class<T> getFormModelComponentClass() {
        return (Class<T>) TypeScriptFormModelComponent.class;
    }

    protected <T extends TypeScriptFormModelPrototypeComponent> Class<T> getFormModelComponentPrototypeClass() {
        return (Class<T>) TypeScriptFormModelPrototypeComponent.class;
    }

    protected <T extends TypeScriptFormComponentComponent> Class<T> getFormViewComponentClass() {
        return (Class<T>) TypeScriptFormComponentComponent.class;
    }

    protected <T extends TypeScriptFormComponentPrototypeComponent> Class<T> getFormViewComponentPrototypeClass() {
        return (Class<T>) TypeScriptFormComponentPrototypeComponent.class;
    }

    public String getModelClassName() {
        return modelComponent.getName();
    }

    @Override
    protected void process() {
        super.process();

        logDebug("Start generating form " + getForm().getName());

        modelComponent = addComponent(getFormModelComponentClass());
        addComponent(getFormModelComponentPrototypeClass());

        addComponent(getFormViewComponentClass());
        addComponent(getFormViewComponentPrototypeClass());


        logDebug("Done generating form " + getForm().getName());

    }
}
