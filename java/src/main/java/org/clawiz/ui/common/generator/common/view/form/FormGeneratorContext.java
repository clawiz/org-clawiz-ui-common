/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.common.view.form;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.metadata.data.view.grid.GridList;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;

import java.util.HashMap;

public class FormGeneratorContext extends AbstractUiGeneratorContext {

    Form                 form;

    @Override
    public MetadataNode getMetadataNode() {
        return form;
    }

    @Override
    public void setMetadataNode(MetadataNode metadataNode) {
        setForm((Form) metadataNode);
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public class GridParentContext {
        Grid      parentGrid;
        TypeField linkField;

        public Grid getParentGrid() {
            return parentGrid;
        }

        public TypeField getLinkField() {
            return linkField;
        }
    }

    private GridList                         grids                   = new GridList();
    private HashMap<Grid, GridList>          gridChildsCache         = new HashMap<>();
    private HashMap<Grid, GridParentContext> gridParentsContextCache = new HashMap<>();

    public GridList getGrids() {
        return grids;
    }

    public GridList getGridChilds(Grid grid) {
        return gridChildsCache.get(grid);
    }

    public GridParentContext getGridParentContext(Grid grid) {
        return gridParentsContextCache.get(grid);
    }

    private TypeField getGridLinkField(Grid grid, Type type, Session session) {
        if ( grid.getRootType() == null ) {
            return null;
        }
        TypeField linkField = null;
        for(TypeField typeField : grid.getRootType().getFields() ) {
            if ( typeField.getValueType() instanceof ValueTypeObject &&
                    ((ValueTypeObject) typeField.getValueType()).getReferencedType().getFullName().equals(type.getFullName()) ) {
                if ( linkField != null ) {
                    session.throwException("Grid ? in form ? has more than one link field to type ?", grid.getFullName(), getForm().getFullName(), type.getFullName());
                }
                linkField = typeField;
            }
        }
        return linkField;
    }


    private void addGridReference(Grid grid, Grid parentGrid, TypeField linkField, Session session) {
        GridParentContext parentContext = gridParentsContextCache.get(grid);
        if ( parentContext != null ) {
            session.throwException("Grid ? in form ? already linked to parent ? and cannot be toegether linked to ?"
                    ,grid.getFullName()
                    ,getForm().getFullName()
                    ,(parentContext.parentGrid != null ? parentContext.parentGrid.getFullName() : getForm().getFullName() )
                    ,parentGrid.getFullName());
        }

        parentContext = new GridParentContext();
        parentContext.parentGrid = parentGrid;
        parentContext.linkField  = linkField;
        gridParentsContextCache.put(grid, parentContext);

        if ( parentGrid != null ) {
            gridChildsCache.get(parentGrid).add(grid);
        }
    }

    @Override
    public void prepare(Session session) {
        super.prepare(session);

        for ( GridLayoutComponent gridLayoutComponent : getForm().getLayoutComponents().getComponentsByClass(GridLayoutComponent.class)) {
            grids.add(gridLayoutComponent.getGrid());
        }

        for ( Grid grid : grids ) {
            gridChildsCache.put(grid, new GridList());
            TypeField linkField = getGridLinkField(grid, getForm().getRootType(), session);
            if ( linkField != null ) {
                addGridReference(grid, null, linkField, session);
            }
            for (Grid check : grids ) {
                if ( check == grid ) {
                    continue;
                }
                TypeField gridLinkField = getGridLinkField(grid, check.getRootType(), session);
                if ( gridLinkField != null ) {
                    linkField = gridLinkField;
                    addGridReference(grid, check, linkField, session);
                }
            }
            if ( linkField == null ) {
                session.throwException("Cannot define parent link for grid ? of form ?", grid.getFullName(), form.getFullName());
            }
        }
    }
}
