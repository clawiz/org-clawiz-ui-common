/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid.api.component.element;


import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn;
import org.clawiz.ui.common.generator.server.java.view.grid.component.element.AbstractGridServletMethodElement;

public class GridApiProcessLoadServletMethodElement extends AbstractGridServletMethodElement {

    @Override
    public void process() {
        super.process();

        setName("processLoad");
        setType(getResponseContextClassName());
        addParameter(getRequestContextClassName(), "request");

        addText(getResponseContextClassName() + " response = new " + getResponseContextClassName() + "();");
        addText(getModelClassName() + "           model    = new " + getModelClassName() + "();");
        addText("response.setModel(model);");

        addText("prepareLoadContext(request);");
        addText("Statement statement = request.getLoadContext().getStatement();");

        addText("");

        addText("int rowIndex  = request.getLoadContext().getLastRowIndex() + 1;");
        addText("while ( rowIndex < request.getStartRow() && statement.next() ) {");
        addText("    rowIndex++;");
        addText("}");
        addText("");

        addText("while (  rowIndex <= request.getEndRow()  && statement.next() ) {");
        addText("");
        addText("    rowIndex++;");
        addText("");
        addText("    " + getRowModelClassName() + " row = new " + getRowModelClassName() + "();");
        addText("    model.addRow(row);");
        addText("");
        addText("    row.setId(statement.getBigDecimal(1));");

        int index = 2;
        for (AbstractGridColumn column : getGrid().getColumns()) {

            if ( column.getValueType() instanceof ValueTypeList) {
                continue;
            }

            if ( column instanceof TypeFieldGridColumn) {

                if ( column.getValueType() instanceof ValueTypeEnumeration) {
                    if ( column.getTypeField() != null ) {
                        addText("    row." + column.getSetMethodName() + "("
                                + getComponent().getValueTypeJavaClassName(column)
                                + ".to" + column.getTypeField().getValueTypeJavaClassName() + "("
                                + "(statement.getString(" + (index++) + "))));");
                    } else {
                        throwException("Value type ENUM of not type column '?' not supported", column.getName());
                    }

                } else {
                    String getter = "statement.get" + column.getValueType().getStatementValueTypeName() +"(" + (index++) + ")";
                    if ( column.getValueType() instanceof ValueTypeBoolean ) {
                        getter = "StringUtils.toBoolean(" + getter + ")";
                    }
                    addText("    row." + column.getSetMethodName() + "(" + getter + ");");
                }
            }

        }

        addText("");
        addText("}");
        addText("");
        addText("model.setAllRowsFetched(rowIndex <= request.getEndRow());");
        addText("");
        addText("request.getLoadContext().setLastRowIndex(rowIndex-1);");
        addText("afterProcessLoad(request);");
        addText("");


        addText("return response;");

    }
}
