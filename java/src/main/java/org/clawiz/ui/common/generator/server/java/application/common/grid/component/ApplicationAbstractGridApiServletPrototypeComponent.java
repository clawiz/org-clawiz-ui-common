/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.grid.component;

import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.query.QueryBuilder;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.ui.common.generator.server.java.application.component.ApplicationAbstractServletComponent;
import org.clawiz.ui.common.portal.application.grid.helper.GridApiHelper;
import org.clawiz.ui.common.portal.application.grid.GridQueryCacheMode;
import org.clawiz.ui.common.portal.application.grid.request.GridApiLoadContext;

import java.util.concurrent.ConcurrentHashMap;

public class ApplicationAbstractGridApiServletPrototypeComponent extends AbstractApplicationGridJavaClassComponent {

    public void addImports() {
        addImport(Query.class);
        addImport(QueryBuilder.class);
        addImport(GridQueryCacheMode.class);
        addImport(ConcurrentHashMap.class);
        addImport(GridApiHelper.class);
        addImport(TypeModel.class);
        addImport(Statement.class);
        addImport(ConcurrentHashMap.class);
        addImport(GridApiLoadContext.class);

    }

    public void addVariables() {
        addVariable("ConcurrentHashMap<String, Query>", "staticQueryCache")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE).withStatic(true)
                .setInitStatement("new ConcurrentHashMap<>()");
        addVariable("ConcurrentHashMap<String, Query>", "sessionQueryCache")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                .setInitStatement("new ConcurrentHashMap<>()");
        addVariable("QueryBuilder", "queryBuilder").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        addVariable("GridApiHelper", "gridApiHelper").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);

        addVariable("ConcurrentHashMap<String, GridApiLoadContext>", "_loadContexts", "new ConcurrentHashMap<>()");

        JavaVariableElement autoCloseVariable = addVariable("boolean", "autoCloseLoadStatement", "true").withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
        autoCloseVariable.addGetter();
        autoCloseVariable.addSetter();
    }

    public void addGetTypeModel() {
        JavaMethodElement method = addMethod("TypeModel", "getTypeModel").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addText("return null;");
    }

    public void addPrepareQuery() {
        JavaMethodElement method = addMethod(null, "prepareQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addParameter("Query", "query");
    }

    @SuppressWarnings("Duplicates")
    public void addCreateQuery() {
        JavaMethodElement method = addMethod("Query", "createQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addText("Query query = queryBuilder.create();");
        method.addText("prepareQuery(request, query);");
        method.addText("gridApiHelper.processQueryFilter(request, query, getTypeModel());");
        method.addText("return query;");
    }

    public void addGetQuery() {
        JavaMethodElement method = addMethod("Query", "getQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");

        method.addText("Query query = null;");
        method.addText("String hashString = getClass().getName() + \":\" + request.toHashString();");
        method.addText("switch (getQueryCacheMode()) {");
        method.addText("    case STATIC:  query = staticQueryCache.get(hashString);  break;");
        method.addText("    case SESSION: query = sessionQueryCache.get(hashString); break;");
        method.addText("}");
        method.addText("if ( query != null ) {");
        method.addText("    return query;");
        method.addText("}");
        method.addText("query = createQuery(request);");
        method.addText("switch (getQueryCacheMode()) {");
        method.addText("    case STATIC:  staticQueryCache.put(hashString,query); break;");
        method.addText("    case SESSION: sessionQueryCache.put(hashString, query); break;");
        method.addText("}");
        method.addText("");
        method.addText("return query;");

    }

    public void addGetQueryCacheMode() {
        JavaMethodElement method = addMethod("GridQueryCacheMode", "getQueryCacheMode").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addText("return GridQueryCacheMode.STATIC;");
    }

    public void addExecuteLoadQuery() {
        JavaMethodElement method = addMethod("Statement", "executeLoadQuery").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");
        method.addText("return getQuery(request).execute(gridApiHelper.getRequestFilterValues(request, dataSource));");
    }


    public void addPrepareLoadContext() {

        JavaMethodElement method = addMethod(null, "prepareLoadContext").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");

        method.addText("String             loadToken   = request.getHttpRequest().getParameterValue(\"loadToken\");");
        method.addText("request.setLoadToken(loadToken);");
        method.addText("");
        method.addText("GridApiLoadContext loadContext = null;");
        method.addText("if ( loadToken != null && ! isAutoCloseLoadStatement() ) {");
        method.addText("    loadContext = _loadContexts.get(loadToken);");
        method.addText("    if ( loadContext == null ) {");
        method.addText("        loadContext = new GridApiLoadContext();");
        method.addText("        _loadContexts.put(loadToken, loadContext);");
        method.addText("    }");
        method.addText("} else {");
        method.addText("    loadContext = new GridApiLoadContext();");
        method.addText("}");
        method.addText("");
        method.addText("if ( request.getStartRow() <= loadContext.getLastRowIndex() ) {");
        method.addText("    if ( loadContext.getStatement() != null ) {");
        method.addText("        loadContext.getStatement().close();");
        method.addText("        loadContext.setStatement(null);");
        method.addText("    }");
        method.addText("}");
        method.addText("");
        method.addText("if ( loadContext.getStatement() == null ) {");
        method.addText("    loadContext.setStatement(executeLoadQuery(request));");
        method.addText("}");
        method.addText("");
        method.addText("request.setLoadContext(loadContext);");

    }

    public void addAfterProcessLoad() {

        JavaMethodElement method = addMethod(null, "afterProcessLoad").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");

        method.addText("if ( request.getLoadContext() == null ) {");
        method.addText("            return;");
        method.addText("        }");
        method.addText("");
        method.addText("if ( request.getLoadToken() == null || isAutoCloseLoadStatement() ) {");
        method.addText("    request.getLoadContext().getStatement().close();");
        method.addText("    if ( request.getLoadToken() != null ) {");
        method.addText("        _loadContexts.remove(request.getLoadToken());");
        method.addText("    }");
        method.addText("}");

    }

    public void addCloseLoadContext() {

        JavaMethodElement method = addMethod(null, "closeLoadContext").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addParameter("R", "request");

        method.addText("String             loadToken   = request.getHttpRequest().getParameterValue(\"loadToken\");");
        method.addText("if ( loadToken != null ) {");
        method.addText("    GridApiLoadContext loadContext = _loadContexts.get(loadToken);");
        method.addText("    if ( loadContext != null ) {");
        method.addText("        loadContext.getStatement().close();");
        method.addText("    }");
        method.addText("    _loadContexts.remove(loadToken);");
        method.addText("}");

    }

    @Override
    public void process() {
        super.process();

        setName(getComponentByClass(ApplicationAbstractGridApiServletComponent.class).getName() + "Prototype");

        setTypeParameters("R extends " + getComponentByClass(ApplicationAbstractGridApiRequestContextComponent.class).getName());

        setExtends(getComponentByClass(ApplicationAbstractServletComponent.class).getFullName());

        addImports();

        addVariables();

        addGetQueryCacheMode();
        addGetTypeModel();
        addPrepareQuery();
        addCreateQuery();
        addGetQuery();
        addExecuteLoadQuery();


        addPrepareLoadContext();
        addAfterProcessLoad();
        addCloseLoadContext();

    }

}
