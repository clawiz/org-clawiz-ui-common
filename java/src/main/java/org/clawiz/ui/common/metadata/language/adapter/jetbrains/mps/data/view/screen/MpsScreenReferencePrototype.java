package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsScreenReferencePrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen screen;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen getScreen() {
        return this.screen;
    }
    
    public void setScreen(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen value) {
        this.screen = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499620156401";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.ScreenReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499620156401", "org.clawiz.ui.common.language.structure.ScreenReference", ConceptPropertyType.REFERENCE, "111278499620156402", "screen"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("111278499620156401", "screen", getScreen());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getScreen());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
