package org.clawiz.ui.common.metadata.data.view.grid;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class GridPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String title;
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.form.Form editForm;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource dataSource;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.grid.column.GridColumnList columns = new org.clawiz.ui.common.metadata.data.view.grid.column.GridColumnList();
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumnList orderBy = new org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumnList();
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.grid.property.GridPropertyList properties = new org.clawiz.ui.common.metadata.data.view.grid.property.GridPropertyList();
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar topToolbar;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar bottomToolbar;
    
    public Grid withName(String value) {
        setName(value);
        return (Grid) this;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String value) {
        this.title = value;
    }
    
    public Grid withTitle(String value) {
        setTitle(value);
        return (Grid) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.form.Form getEditForm() {
        return this.editForm;
    }
    
    public void setEditForm(org.clawiz.ui.common.metadata.data.view.form.Form value) {
        this.editForm = value;
    }
    
    public Grid withEditForm(org.clawiz.ui.common.metadata.data.view.form.Form value) {
        setEditForm(value);
        return (Grid) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource getDataSource() {
        return this.dataSource;
    }
    
    public void setDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value) {
        this.dataSource = value;
    }
    
    public Grid withDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value) {
        setDataSource(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource> T createDataSource(Class<T> nodeClass) {
        if ( getDataSource() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "dataSource", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource value = createChildNode(nodeClass, "dataSource");
        setDataSource(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource createDataSource() {
        return createDataSource(org.clawiz.ui.common.metadata.data.view.datasource.AbstractViewDataSource.class);
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.column.GridColumnList getColumns() {
        return this.columns;
    }
    
    public Grid withColumn(org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn value) {
        getColumns().add(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn> T createColumn(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn value = createChildNode(nodeClass, "columns");
        getColumns().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn createColumn() {
        return createColumn(org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn.class);
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumnList getOrderBy() {
        return this.orderBy;
    }
    
    public Grid withOrderBy(org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn value) {
        getOrderBy().add(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn> T createOrderBy(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn value = createChildNode(nodeClass, "orderBy");
        getOrderBy().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn createOrderBy() {
        return createOrderBy(org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn.class);
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.property.GridPropertyList getProperties() {
        return this.properties;
    }
    
    public Grid withPropertie(org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty value) {
        getProperties().add(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty createPropertie() {
        return createPropertie(org.clawiz.ui.common.metadata.data.view.grid.property.AbstractGridProperty.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar getTopToolbar() {
        return this.topToolbar;
    }
    
    public void setTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        this.topToolbar = value;
    }
    
    public Grid withTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        setTopToolbar(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar> T createTopToolbar(Class<T> nodeClass) {
        if ( getTopToolbar() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "topToolbar", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value = createChildNode(nodeClass, "topToolbar");
        setTopToolbar(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar createTopToolbar() {
        return createTopToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar.class);
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar getBottomToolbar() {
        return this.bottomToolbar;
    }
    
    public void setBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        this.bottomToolbar = value;
    }
    
    public Grid withBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value) {
        setBottomToolbar(value);
        return (Grid) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar> T createBottomToolbar(Class<T> nodeClass) {
        if ( getBottomToolbar() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "bottomToolbar", this.getFullName());
        }
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar value = createChildNode(nodeClass, "bottomToolbar");
        setBottomToolbar(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar createBottomToolbar() {
        return createBottomToolbar(org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getEditForm() != null ) { 
            getEditForm().prepare(session);
        }
        if ( getDataSource() != null ) { 
            getDataSource().prepare(session);
        }
        for (MetadataNode node : getColumns()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getOrderBy()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getTopToolbar() != null ) { 
            getTopToolbar().prepare(session);
        }
        if ( getBottomToolbar() != null ) { 
            getBottomToolbar().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getEditForm());
        
        references.add(getDataSource());
        
        for (MetadataNode node : getColumns()) {
            references.add(node);
        }
        
        for (MetadataNode node : getOrderBy()) {
            references.add(node);
        }
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
        references.add(getTopToolbar());
        
        references.add(getBottomToolbar());
        
    }
}
