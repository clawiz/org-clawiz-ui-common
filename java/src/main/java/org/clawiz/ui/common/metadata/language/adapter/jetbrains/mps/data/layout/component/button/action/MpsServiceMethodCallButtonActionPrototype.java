package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceMethodCallButtonActionPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef serviceMethod;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef value) {
        this.serviceMethod = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "9216576144992430537";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.ServiceMethodCallButtonAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "9216576144992430537", "org.clawiz.ui.common.language.structure.ServiceMethodCallButtonAction", ConceptPropertyType.CHILD, "9216576144992430583", "serviceMethod"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("9216576144992430537", "serviceMethod", getServiceMethod());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.button.action.ServiceMethodCallButtonAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.action.ServiceMethodCallButtonAction structure = (org.clawiz.ui.common.metadata.data.layout.component.button.action.ServiceMethodCallButtonAction) node;
        
        if ( getServiceMethod().getMethod() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "serviceMethod", false, getServiceMethod().getMethod());
        } else {
            structure.setServiceMethod(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.button.action.ServiceMethodCallButtonAction structure = (org.clawiz.ui.common.metadata.data.layout.component.button.action.ServiceMethodCallButtonAction) node;
        
        if ( structure.getServiceMethod() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "serviceMethod", false, structure.getServiceMethod());
        } else {
            setServiceMethod(null);
        }
        
    }
}
