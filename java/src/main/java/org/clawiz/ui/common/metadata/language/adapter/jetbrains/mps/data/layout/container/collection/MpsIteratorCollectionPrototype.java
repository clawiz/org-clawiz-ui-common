package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsIteratorCollectionPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.MpsAbstractCollection {
    
    public String iteratorName;
    
    public String variableName;
    
    public String getIteratorName() {
        return this.iteratorName;
    }
    
    public void setIteratorName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.iteratorName = null;
        }
        this.iteratorName = value;
    }
    
    public String getVariableName() {
        return this.variableName;
    }
    
    public void setVariableName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.variableName = null;
        }
        this.variableName = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234832515";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.IteratorCollection";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832515", "org.clawiz.ui.common.language.structure.IteratorCollection", ConceptPropertyType.PROPERTY, "7111202990234873188", "iteratorName"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832515", "org.clawiz.ui.common.language.structure.IteratorCollection", ConceptPropertyType.PROPERTY, "7111202990234873190", "variableName"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("7111202990234832515", "iteratorName", getIteratorName());
        addConceptNodeProperty("7111202990234832515", "variableName", getVariableName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection structure = (org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection) node;
        
        structure.setIteratorName(getIteratorName());
        
        structure.setVariableName(getVariableName());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection structure = (org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection) node;
        
        setIteratorName(structure.getIteratorName());
        
        setVariableName(structure.getVariableName());
        
    }
}
