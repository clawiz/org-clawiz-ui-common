package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeFieldGridColumnPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField typeField;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getTypeField() {
        return this.typeField;
    }
    
    public void setTypeField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.typeField = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4089903107538073256";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.TypeFieldGridColumn";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "4089903107538073256", "org.clawiz.ui.common.language.structure.TypeFieldGridColumn", ConceptPropertyType.REFERENCE, "4089903107538073384", "typeField"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("4089903107538073256", "typeField", getTypeField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn) node;
        
        if ( getTypeField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "typeField", false, getTypeField());
        } else {
            structure.setTypeField(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn) node;
        
        if ( structure.getTypeField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "typeField", false, structure.getTypeField());
        } else {
            setTypeField(null);
        }
        
    }
}
