/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form.component.element;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;

public class UiFormComponentPrepareSavedDataMethodElement extends AbstractUiFormComponentMethodElement {

    @Override
    public void process() {
        super.process();

        setName("prepareSavedData");
        setType("Promise<any>");

        addParameter("savedData", getModelClassName());

        addText("");
        addText("return super.prepareSavedData(savedData)");
        addText("  .then(() => {");

        int maxLength=0;
        String anySuffix = "";
        for (AbstractFormField field : getForm().getFields()) {
            if ( field.getName().length() > maxLength ) { maxLength = field.getName().length(); }
            if ( field.getValueType() instanceof ValueTypeDate || field.getValueType() instanceof ValueTypeDateTime) {
                anySuffix = "       ";
            }
        }

        for (AbstractFormField field : getForm().getFields() ) {


            String space  = StringUtils.space(maxLength - field.getName().length());

            if ( field.getValueType() instanceof ValueTypeDate) {
                addText("    (<any>savedData)." + field.getJavaVariableName() + space + " = this.dateUtils.dateToString(this.data." + field.getJavaVariableName() + ", this.dateUtils.ISO8601_DATE_FORMAT);");
            } else if ( field.getValueType() instanceof ValueTypeDateTime) {
                addText("    (<any>savedData)." + field.getJavaVariableName() + space + " = this.dateUtils.dateTimeToString(this.data." + field.getJavaVariableName() + ", this.dateUtils.ISO8601_DATE_TIME_FORMAT);");
            } else {
                addText("    savedData" + anySuffix + "." + field.getJavaVariableName() + space + " = this.data." + field.getJavaVariableName() + ";");
            }
/*
            if ( field.getValueType() instanceof ValueTypeDate
                    || field.getValueType() instanceof ValueTypeDateTime
                    || field.getValueType() instanceof ValueTypeEnumeration
                    || field.getValueType() instanceof ValueTypeObject) {
                addText("    delete savedData['" + field.getToStringJavaVariableName() + "'];");
            }

            if ( field.getSelectValueContext() instanceof StaticValuesSelectValueContext ) {
                addText("    delete savedData['" + field.getJavaVariableName() + "SelectOptions'];");
            }
*/

        }

        addText("    return Promise.resolve();");
        addText("  });");
        addText("");


    }
}
