package org.clawiz.ui.common.metadata.data.view.grid.column;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractGridColumnPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String caption;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.view.grid.column.property.GridColumnPropertyList properties = new org.clawiz.ui.common.metadata.data.view.grid.column.property.GridColumnPropertyList();
    
    public AbstractGridColumn withName(String value) {
        setName(value);
        return (AbstractGridColumn) this;
    }
    
    public String getCaption() {
        return this.caption;
    }
    
    public void setCaption(String value) {
        this.caption = value;
    }
    
    public AbstractGridColumn withCaption(String value) {
        setCaption(value);
        return (AbstractGridColumn) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.column.property.GridColumnPropertyList getProperties() {
        return this.properties;
    }
    
    public AbstractGridColumn withPropertie(org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty value) {
        getProperties().add(value);
        return (AbstractGridColumn) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty createPropertie() {
        return createPropertie(org.clawiz.ui.common.metadata.data.view.grid.column.property.AbstractGridColumnProperty.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
    }
}
