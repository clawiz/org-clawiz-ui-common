/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.datasource.component;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element.ApplicationDataSourceDataSourceApiDoPostMethodElement;

import java.math.BigDecimal;

public class ApplicationDataSourceApiServletPrototypeComponent extends AbstractApplicationDataSourceJavaClassComponent {

    public static String getServletPath(AbstractApplicationGeneratorContext applicationContext) {
        return "/api/datasource/" + StringUtils.toLowerFirstChar(applicationContext.getApplicationJavaClassName());
    }

    protected void addGetPath() {
        JavaMethodElement method = addMethod("String", "getPath");
        method.addText("return \"" + getServletPath(getApplicationContext()) + "\";");
    }

    protected void addImports() {

        addImport(BigDecimal.class);
        addImport(AbstractHttpRequestContext.class);
        addImport(Gson.class);
        addImport(JsonObject.class);
        addImport(JsonArray.class);
        addImport(JsonElement.class);
        addImport(StringUtils.class);
    }

    @Override
    public void process() {
        super.process();
        setOverwriteMode(OverwriteMode.OVERWRITE);

        setName(getComponentNameByClass(ApplicationDataSourceApiServletComponent.class) + "Prototype");

        setExtends(getComponentByClass(ApplicationAbstractDataSourceServletComponent.class).getName());

        addImports();

        addVariable(getComponentByClass(ApplicationDataSourceDataSourceComponent.class).getName(), "dataSource");

        addGetPath();


        addElement(ApplicationDataSourceDataSourceApiDoPostMethodElement.class);




    }
}
