package org.clawiz.ui.common.metadata.data.view.grid.column;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeFieldGridColumnPrototype extends org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.field.TypeField typeField;
    
    public TypeFieldGridColumn withName(String value) {
        setName(value);
        return (TypeFieldGridColumn) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField getTypeField() {
        return this.typeField;
    }
    
    public void setTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        this.typeField = value;
    }
    
    public TypeFieldGridColumn withTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        setTypeField(value);
        return (TypeFieldGridColumn) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getTypeField() != null ) { 
            getTypeField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getTypeField());
        
    }
}
