/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.common.datasource.component.element;


import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationAbstractDataSourceExecuteContextComponent;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.ApplicationAbstractDataSourceRowModelComponent;

public class ApplicationDataSourceDataSourceApiDoPostMethodElement extends AbstractApplicationDataSourceServletMethodElement {

    @Override
    public void process() {
        super.process();

        setName("doPost");

        addParameter("AbstractHttpRequestContext", "httpRequest");


        addText("");
        addText("BigDecimal lastReceivedScn = dataSource.getLastReceivedScn();");
        addText("if ( lastReceivedScn == null ) {");
        addText("    lastReceivedScn = new BigDecimal(0);");
        addText("}");
        addText("");
        addText("BigDecimal firstServerScn  = StringUtils.toBigDecimal(httpRequest.getParameterValue(\"firstServerScn\"));");
        addText("String     objectGuid      = httpRequest.getParameterValue(\"objectId\");");
        addText("if ( firstServerScn == null && objectGuid == null ) {");
        addText("    throwException(\"One of 'firstServerScn' or 'objectId' parameters mst be defined\");");
        addText("}");
        addText("if ( firstServerScn == null ) {");
        addText("    firstServerScn = new BigDecimal(0);");
        addText("}");
        addText("");
        addText("String data                = httpRequest.getParameterValue(\"data\");");
        addText("");
        addText("if ( ! StringUtils.isEmpty(data) ) {");
        addText("    ");
        addText("    JsonElement objects  = (new Gson()).fromJson(data, JsonElement.class);");
        addText("    if ( ! ( objects instanceof JsonArray) ) {");
        addText("        throwException(\"Data string must be json array : ?\", data);");
        addText("    }");
        addText("    ");
        addText("    for( JsonElement element : ((JsonArray)objects) ) {");
        addText("    ");
        addText("        if (! (element instanceof JsonObject) ) {");
        addText("            throwException(\"Array element is not json object : ?\", element.toString());");
        addText("        }");
        addText("    ");
        addText("        JsonObject object = (JsonObject) element;");
        addText("        BigDecimal rowScn = dataSource.getAsBigDecimal(object, \"scn\");");
        addText("        if ( rowScn == null ) {");
        addText("            throwException(\"Scn not defined in row data\");");
        addText("        }");
        addText("    ");
        addText("        if ( rowScn.compareTo(lastReceivedScn) > 0 ) {");
        addText("            dataSource.saveChange(dataSource.fromJsonObject(object));");
        addText("            lastReceivedScn = rowScn;");
        addText("        }");
        addText("    }");
        addText("    ");
        addText("    dataSource.saveLastReceivedScn(lastReceivedScn);");
        addText("    commit();");
        addText("");
        addText("}");
        addText("");
        String contextClassName = getGenerator().getComponentByClass(ApplicationAbstractDataSourceExecuteContextComponent.class).getName();
        addText(contextClassName + " context = new " + contextClassName + "();");
        addText("context.setDataSource(dataSource);");
        addText("context.setFirstScn(objectGuid == null ? firstServerScn : new BigDecimal(0));");
        addText("context.setLastScn(new BigDecimal(getSession().getTransactionManager().getCurrentScn(getSession())));");
        addText("context.setObjectGuid(objectGuid);");
        addText("");
        addText("context.loadChanges();");
        addText("");
        addText("JsonArray objects = new JsonArray();");
        addText("for (Object object : context.getRows() ) {");
        addText("    objects.add(dataSource.toJsonObject(context, ("
                + getGenerator().getComponentByClass(ApplicationAbstractDataSourceRowModelComponent.class).getName()
                + ") object));");
        addText("}");
        addText("");
        addText("JsonObject response = new JsonObject();");
        addText("response.addProperty(\"status\", \"OK\");");
        addText("response.add(\"rows\", objects);");
        addText("");
        addText("write(httpRequest, response.toString());");
        addText("");


    }
}
