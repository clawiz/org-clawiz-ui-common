package org.clawiz.ui.common.metadata.data.view.screen;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ScreenPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.ui.common.metadata.data.view.screen.Screen extendsScreen;
    
    @ExchangeElement
    private org.clawiz.ui.common.metadata.data.layout.LayoutComponentList components = new org.clawiz.ui.common.metadata.data.layout.LayoutComponentList();
    
    public Screen withName(String value) {
        setName(value);
        return (Screen) this;
    }
    
    public org.clawiz.ui.common.metadata.data.view.screen.Screen getExtendsScreen() {
        return this.extendsScreen;
    }
    
    public void setExtendsScreen(org.clawiz.ui.common.metadata.data.view.screen.Screen value) {
        this.extendsScreen = value;
    }
    
    public Screen withExtendsScreen(org.clawiz.ui.common.metadata.data.view.screen.Screen value) {
        setExtendsScreen(value);
        return (Screen) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.LayoutComponentList getComponents() {
        return this.components;
    }
    
    public Screen withComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value) {
        getComponents().add(value);
        return (Screen) this;
    }
    
    public <T extends org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent> T createComponent(Class<T> nodeClass) {
        org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent value = createChildNode(nodeClass, "components");
        getComponents().add(value);
        return (T) value;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent createComponent() {
        return createComponent(org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getExtendsScreen() != null ) { 
            getExtendsScreen().prepare(session);
        }
        for (MetadataNode node : getComponents()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getExtendsScreen());
        
        for (MetadataNode node : getComponents()) {
            references.add(node);
        }
        
    }
}
