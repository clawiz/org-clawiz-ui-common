/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.server.java.AbstractServerJavaClassComponent;
import org.clawiz.ui.common.generator.server.java.datasource.ServerDataSourceGenerator;

public class
AbstractDataSourceApiJavaClassComponent extends AbstractServerJavaClassComponent {

    ServerDataSourceGenerator dataSourceGenerator;
    DataSource api;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        dataSourceGenerator = (ServerDataSourceGenerator) generator;
        api = dataSourceGenerator.getDataSource();
    }

    public DataSource getDataSource() {
        return api;
    }

    @Override
    public ServerDataSourceGenerator getGenerator() {
        return dataSourceGenerator;
    }

    @Override
    public void process() {
        super.process();
        setPackageName(getGenerator().getPackageName() + "." + GeneratorUtils.toJavaClassName(getDataSource().getJavaClassName()).toLowerCase());

    }

    public String getRowModelClassName() {
        return getGenerator().getRowModelComponent().getName();
    }

    protected void addFieldValueTypeEnumerationImports() {
        for (TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeEnumeration) {
                addImport(getDataSource().getType().getPackageName()//.toLowerCase()
                        + "." + getDataSource().getType().getJavaClassName().toLowerCase()
                        + "." + field.getValueTypeJavaClassName()
                );
            }


        }
    }

}
