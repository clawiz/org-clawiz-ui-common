package org.clawiz.ui.common.metadata.data.layout.container.collection;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class IteratorCollectionPrototype extends org.clawiz.ui.common.metadata.data.layout.container.collection.AbstractCollection {
    
    @ExchangeAttribute
    private String iteratorName;
    
    @ExchangeAttribute
    private String variableName;
    
    public IteratorCollection withName(String value) {
        setName(value);
        return (IteratorCollection) this;
    }
    
    public String getIteratorName() {
        return this.iteratorName;
    }
    
    public void setIteratorName(String value) {
        this.iteratorName = value;
    }
    
    public IteratorCollection withIteratorName(String value) {
        setIteratorName(value);
        return (IteratorCollection) this;
    }
    
    public String getVariableName() {
        return this.variableName;
    }
    
    public void setVariableName(String value) {
        this.variableName = value;
    }
    
    public IteratorCollection withVariableName(String value) {
        setVariableName(value);
        return (IteratorCollection) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
