/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.AbstractApplicationGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.builder.ClawizBuilderIndexComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigImplementationPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizImplementationPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.router.ApplicationRouterServiceComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.router.ApplicationRouterServicePrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.storage.*;
import org.clawiz.ui.common.generator.client.ts.application.ui.*;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormComponentComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormModelComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormModelPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridComponentComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridRowModelComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridRowModelPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGenerator;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.application.builder.ClawizBuilderComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.ClawizBuilderPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizConfigPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizComponent;
import org.clawiz.ui.common.generator.client.ts.application.builder.clawiz.ApplicationClawizPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.service.ApplicationServiceComponent;
import org.clawiz.ui.common.generator.client.ts.application.service.ApplicationServicePrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.ClientTypeScriptDataSourceGenerator;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;
import org.clawiz.ui.common.generator.common.view.grid.GridGeneratorContext;

import java.util.ArrayList;

public class TypeScriptClientCommonGenerator extends AbstractApplicationGenerator {

    ApplicationObjectImplementationComponent       objectComponent;
    TypeScriptClientCommonGeneratorContext         context;

    ArrayList<ClientTypeScriptDataSourceGenerator> dataSourceGenerators = new ArrayList<>();

    @Override
    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>)TypeScriptClientCommonGeneratorContext.class;
    }

    @Override
    public TypeScriptClientCommonGeneratorContext getContext() {
        return this.context;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        this.context = (TypeScriptClientCommonGeneratorContext) context;
    }

    public String getApplicationDestinationPath() {
        return getContext().getApplicationDestinationPath();
    }

    public String getDataSourceImportPath(DataSource dataSource) {
        return getContext().getDestinationPath() + "/" + getContext().getApplicationJavaClassName().toLowerCase() + "/common"
                + GeneratorUtils.getPackageRelativePath(getPackageName(), dataSource.getPackageName())
                + "/" + dataSource.getJavaClassName().toLowerCase();
    }

    public ArrayList<ClientTypeScriptDataSourceGenerator> getDataSourceGenerators() {
        return dataSourceGenerators;
    }

    public ApplicationObjectImplementationComponent getObjectComponent() {
        return objectComponent;
    }

    protected void addStorageComponents() {
        objectComponent = addComponent(ApplicationObjectImplementationComponent.class);
        addComponent(ApplicationObjectImplementationPrototypeComponent.class);

        addComponent(ApplicationStorageServiceComponent.class);
        addComponent(ApplicationStorageServicePrototypeComponent.class);

        addComponent(ApplicationDataSourceComponent.class);
        addComponent(ApplicationDataSourcePrototypeComponent.class);

        addComponent(CommonDataSourceInterfacesComponent.class);

        addComponent(ApplicationStorageIndexComponent.class);

    }

    protected void addServiceComponents() {

        addComponent(ApplicationServiceComponent.class);
        addComponent(ApplicationServicePrototypeComponent.class);

    }

    protected void addRouterComponents() {
        addComponent(ApplicationRouterServiceComponent.class);
        addComponent(ApplicationRouterServicePrototypeComponent.class);
    }

    public String getBuilderClassNamePrefix() {
        return "";
    }

    protected void addBuilderComponents() {
        addComponent(ApplicationClawizComponent.class);

        addServiceComponents();

        addComponent(ApplicationClawizConfigComponent.class);
        addComponent(ApplicationClawizConfigPrototypeComponent.class);

        addComponent(ApplicationClawizPrototypeComponent.class);

        addComponent(ApplicationClawizConfigImplementationComponent.class);
        addComponent(ApplicationClawizConfigImplementationPrototypeComponent.class);

        addComponent(ApplicationClawizImplementationComponent.class);
        addComponent(ApplicationClawizImplementationPrototypeComponent.class);

        addComponent(ClawizBuilderComponent.class);
        addComponent(ClawizBuilderPrototypeComponent.class);

        addComponent(ClawizBuilderIndexComponent.class);

    }

    protected void addApplicationUiComponents() {

        addComponent(ApplicationAbstractUiComponentComponent.class);
        addComponent(ApplicationAbstractUiComponentPrototypeComponent.class);

        addComponent(ApplicationAbstractFormModelComponent.class);
        addComponent(ApplicationAbstractFormModelPrototypeComponent.class);

        addComponent(ApplicationAbstractFormComponentComponent.class);
        addComponent(ApplicationAbstractFormComponentPrototypeComponent.class);

        addComponent(ApplicationAbstractGridRowModelComponent.class);
        addComponent(ApplicationAbstractGridRowModelPrototypeComponent.class);

        addComponent(ApplicationAbstractGridComponentComponent.class);
        addComponent(ApplicationAbstractGridComponentPrototypeComponent.class);

    }

    @Override
    protected void process() {
        super.process();

        logDebug("Start generate web typescript application " + getContext().getApplicationJavaClassName());

        addStorageComponents();


        addRouterComponents();

        addBuilderComponents();

        addApplicationUiComponents();

    }

    protected void addDataSourceGenerators() {

        for ( DataSourceGeneratorContext dataSourceContext : getContext().getDataSourceContexts()) {
            dataSourceContext.setApplicationGenerator(this);

            ClientTypeScriptDataSourceGenerator dataSourceGenerator = getService(ClientTypeScriptDataSourceGenerator.class, true);
            dataSourceGenerator.setContext(dataSourceContext);
            dataSourceGenerator.setIncrementalMode(isIncrementalMode());

            dataSourceGenerator.run();

            dataSourceGenerators.add(dataSourceGenerator);
        }

        ApplicationStorageServicePrototypeComponent storageServicePrototypeComponent = getComponentByClass(ApplicationStorageServicePrototypeComponent.class);
        storageServicePrototypeComponent.getPrepareSourcesMethod().addText("");
        storageServicePrototypeComponent.getPrepareSourcesMethod().addText("return Promise.all(promises);");

    }

    public <T extends TypeScriptUiFormGenerator> Class<T> getFormGeneratorClass() {
        return null;
    }

    protected TypeScriptUiFormGenerator addFormGenerator(FormGeneratorContext formContext) {

        if ( getFormGeneratorClass() == null ) {
            return null;
        }

        formContext.setApplicationGenerator(this);

        TypeScriptUiFormGenerator formGenerator = getService(getFormGeneratorClass(), true);
        formGenerator.setContext(formContext);
        formGenerator.setIncrementalMode(isIncrementalMode());

        formGenerator.run();

        return formGenerator;
    }

    public <T extends TypeScriptUiGridGenerator> Class<T> getGridGeneratorClass() {
        return null;
    }


    protected TypeScriptUiGridGenerator addGridGenerator(GridGeneratorContext gridContext) {

        if ( getGridGeneratorClass() == null ) {
            return null;
        }

        gridContext.setApplicationGenerator(this);

        TypeScriptUiGridGenerator gridGenerator = getService(getGridGeneratorClass(), true);
        gridGenerator.setContext(gridContext);
        gridGenerator.setIncrementalMode(isIncrementalMode());

        gridGenerator.run();

        return gridGenerator;
    }

    @Override
    protected void done() {
        super.done();

        ApplicationServicePrototypeComponent servicePrototypeComponent = getComponentByClass(ApplicationServicePrototypeComponent.class);
        if ( servicePrototypeComponent != null ) {
            servicePrototypeComponent.addPrepareDataSourceFieldsMethod();
        }

        for ( FormGeneratorContext formContext : getContext().getFormContexts()) {
            addFormGenerator(formContext);
        }

        for ( GridGeneratorContext gridContext : getContext().getGridContexts()) {
            addGridGenerator(gridContext);
        }

        addDataSourceGenerators();

        logDebug("Done generate web typescript application " + getContext().getApplicationJavaClassName());
    }
}
