package org.clawiz.ui.common.metadata.data.view.form.field.property;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class HintFormFieldPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty {
    
    @ExchangeAttribute
    private String hint;
    
    public HintFormFieldProperty withName(String value) {
        setName(value);
        return (HintFormFieldProperty) this;
    }
    
    public String getHint() {
        return this.hint;
    }
    
    public void setHint(String value) {
        this.hint = value;
    }
    
    public HintFormFieldProperty withHint(String value) {
        setHint(value);
        return (HintFormFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
