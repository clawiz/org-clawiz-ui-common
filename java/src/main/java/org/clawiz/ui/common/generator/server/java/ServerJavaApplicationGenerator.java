/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java;


import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.AbstractApplicationGenerator;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;
import org.clawiz.ui.common.generator.common.view.grid.GridGeneratorContext;
import org.clawiz.ui.common.generator.common.view.screen.ScreenGeneratorContext;
import org.clawiz.ui.common.generator.server.java.application.common.datasource.component.*;
import org.clawiz.ui.common.generator.server.java.datasource.ServerDataSourceGenerator;
import org.clawiz.ui.common.generator.server.java.datasource.api.component.DataSourceApiServletComponent;
import org.clawiz.ui.common.generator.server.java.application.common.form.component.*;
import org.clawiz.ui.common.generator.server.java.application.common.grid.component.*;
import org.clawiz.ui.common.generator.server.java.application.component.*;
import org.clawiz.ui.common.generator.server.java.view.form.ServerFormGenerator;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.grid.ServerGridGenerator;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.GridApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.screen.ServerScreenGenerator;
import org.clawiz.ui.common.generator.server.java.view.screen.servlet.component.ScreenApiServletComponent;

import java.util.ArrayList;

public class ServerJavaApplicationGenerator extends AbstractApplicationGenerator {

    ServerJavaApplicationGeneratorContext serverApplicationGeneratorContext;

    ArrayList<ServerFormGenerator>        formGenerators   = new ArrayList<>();
    ArrayList<ServerGridGenerator>        gridGenerators   = new ArrayList<>();
    ArrayList<ServerScreenGenerator>      screenGenerators = new ArrayList<>();
    ArrayList<ServerDataSourceGenerator>  dataSourceGenerators = new ArrayList<>();

    @Override
    public ServerJavaApplicationGeneratorContext getContext() {
        return serverApplicationGeneratorContext;
    }

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        serverApplicationGeneratorContext = (ServerJavaApplicationGeneratorContext) context;
    }

    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>) ServerJavaApplicationGeneratorContext.class;
    }

    public <T extends ServerFormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) ServerFormGenerator.class;
    }

    public <T extends ServerGridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) ServerGridGenerator.class;
    }

    public <T extends ServerScreenGenerator> Class<T> getScreenGeneratorClass() {
        return (Class<T>) ServerScreenGenerator.class;
    }

    public <T extends ServerDataSourceGenerator> Class<T> getDataSourceGeneratorClass() {
        return (Class<T>) ServerDataSourceGenerator.class;
    }

    public ArrayList<ServerFormGenerator> getFormGenerators() {
        return formGenerators;
    }

    public ArrayList<ServerGridGenerator> getGridGenerators() {
        return gridGenerators;
    }

    public ArrayList<ServerScreenGenerator> getScreenGenerators() {
        return screenGenerators;
    }

    public ArrayList<ServerDataSourceGenerator> getDataSourceGenerators() {
        return dataSourceGenerators;
    }

    protected <T extends ApplicationAbstractServletComponent> Class<T> getApplicationAbstractServletComponentClass() {
        return (Class<T>) ApplicationAbstractServletComponent.class;
    }

    protected <T extends ApplicationAbstractServletPrototypeComponent> Class<T> getApplicationAbstractServletPrototypeComponentClass() {
        return (Class<T>) ApplicationAbstractServletPrototypeComponent.class;
    }

    protected ApplicationContextPrototypeComponent getApplicationContextPrototypeComponent() {
        return applicationContextPrototypeComponent;
    }

    private ApplicationContextPrototypeComponent applicationContextPrototypeComponent;

    @Override
    protected void process() {
        super.process();

        logDebug("Start generate server application " + getContext().getApplicationJavaClassName());

        addComponent(ApplicationContextComponent.class);
        applicationContextPrototypeComponent = addComponent(ApplicationContextPrototypeComponent.class);

        addComponent(getApplicationAbstractServletComponentClass());

        addComponent(ApplicationAbstractDataSourceRowModelComponent.class);
        addComponent(ApplicationAbstractDataSourceRowModelPrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceRequestContextComponent.class);
        addComponent(ApplicationAbstractDataSourceRequestContextPrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceResponseContextComponent.class);
        addComponent(ApplicationAbstractDataSourceResponseContextPrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceExecuteContextComponent.class);
        addComponent(ApplicationAbstractDataSourceExecuteContextPrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceDataSourceComponent.class);
        addComponent(ApplicationAbstractDataSourceDataSourcePrototypeComponent.class);

        addComponent(ApplicationDataSourceDataSourceComponent.class);
        addComponent(ApplicationDataSourceDataSourcePrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceServletComponent.class);
        addComponent(ApplicationAbstractDataSourceServletPrototypeComponent.class);

        addComponent(ApplicationAbstractDataSourceApiServletComponent.class);
        addComponent(ApplicationAbstractDataSourceApiServletPrototypeComponent.class);

        addComponent(ApplicationDataSourceApiServletComponent.class);
        addComponent(ApplicationDataSourceApiServletPrototypeComponent.class);



        addComponent(ApplicationAbstractFormApiModelComponent.class);
        addComponent(ApplicationAbstractFormApiModelPrototypeComponent.class);

        addComponent(ApplicationAbstractFormApiRequestContextComponent.class);
        addComponent(ApplicationAbstractFormApiResponseContextComponent.class);

        addComponent(ApplicationAbstractFormApiResponseContextPrototypeComponent.class);
        addComponent(ApplicationAbstractFormApiRequestContextPrototypeComponent.class);

        addComponent(ApplicationAbstractFormApiServletComponent.class);
        addComponent(ApplicationAbstractFormApiServletPrototypeComponent.class);

        addComponent(ApplicationAbstractGridApiRowModelComponent.class);
        addComponent(ApplicationAbstractGridApiRowModelPrototypeComponent.class);

        addComponent(ApplicationAbstractGridApiModelComponent.class);
        addComponent(ApplicationAbstractGridApiModelPrototypeComponent.class);

        addComponent(ApplicationAbstractGridApiParametersComponent.class);
        addComponent(ApplicationAbstractGridApiParametersPrototypeComponent.class);

        addComponent(ApplicationAbstractGridApiRequestContextComponent.class);
        addComponent(ApplicationAbstractGridApiResponseContextComponent.class);

        addComponent(ApplicationAbstractGridApiRequestContextPrototypeComponent.class);
        addComponent(ApplicationAbstractGridApiResponseContextPrototypeComponent.class);

        addComponent(ApplicationAbstractGridApiServletComponent.class);
        addComponent(ApplicationAbstractGridApiServletPrototypeComponent.class);


        addComponent(getApplicationAbstractServletPrototypeComponentClass());




    }


    protected ServerFormGenerator addFormGenerator(FormGeneratorContext formContext) {

        formContext.setApplicationGenerator(this);

        ServerFormGenerator formGenerator = getService(getFormGeneratorClass(), true);
        formGenerator.setContext(formContext);
        formGenerator.setIncrementalMode(isIncrementalMode());

        formGenerator.run();

        getApplicationContextPrototypeComponent().getPrepareMethod().addText("addServlet(" +
                formGenerator.getPackageName()
                + "." + formContext.getForm().getJavaClassName().toLowerCase() + "."
                + FormApiServletComponent.formToClassName(formContext.getForm()) + ".class);");

        getFormGenerators().add(formGenerator);

        return formGenerator;
    }

    protected ServerGridGenerator addGridGenerator(GridGeneratorContext gridContext) {

        gridContext.setApplicationGenerator(this);

        ServerGridGenerator gridGenerator = getService(getGridGeneratorClass(), true);
        gridGenerator.setContext(gridContext);
        gridGenerator.setIncrementalMode(isIncrementalMode());

        gridGenerator.run();

        getApplicationContextPrototypeComponent().getPrepareMethod().addText("addServlet(" +
                gridGenerator.getPackageName()
                + "." + gridContext.getGrid().getJavaClassName().toLowerCase() + "."
                + GridApiServletComponent.gridToClassName(gridContext.getGrid()) + ".class);");

        getGridGenerators().add(gridGenerator);

        return gridGenerator;
    }

    protected ServerScreenGenerator addScreenGenerator(ScreenGeneratorContext screenContext) {

        screenContext.setApplicationGenerator(this);

        ServerScreenGenerator screenGenerator = getService(getScreenGeneratorClass(), true);
        screenGenerator.setContext(screenContext);
        screenGenerator.setIncrementalMode(isIncrementalMode());

        screenGenerator.run();

        getApplicationContextPrototypeComponent().getPrepareMethod().addText("addServlet(" +
                screenGenerator.getPackageName()
                + "." + screenContext.getScreen().getName().toLowerCase() + "."
                + ScreenApiServletComponent.screenToApiServletClassName(screenContext.getScreen()) + ".class);");


        getScreenGenerators().add(screenGenerator);

        return screenGenerator;
    }

    protected ServerDataSourceGenerator addDataSourceGenerator(DataSourceGeneratorContext apiContext) {

        apiContext.setApplicationGenerator(this);

        ServerDataSourceGenerator apiGenerator = getService(getDataSourceGeneratorClass(), true);
        apiGenerator.setContext(apiContext);
        apiGenerator.setIncrementalMode(isIncrementalMode());

        apiGenerator.run();

        getApplicationContextPrototypeComponent().getPrepareMethod().addText("addServlet(" +
                apiContext.getPackageName()
                + "." + apiContext.getDataSource().getJavaClassName().toLowerCase() + "."
                + DataSourceApiServletComponent.dataSourceToApiClassName(apiContext.getDataSource()) + ".class);");


        getDataSourceGenerators().add(apiGenerator);

        return apiGenerator;
    }


    @Override
    protected void done() {
        super.done();

        for(FormGeneratorContext formContext : getContext().getFormContexts()) {
            addFormGenerator(formContext);
        }

        for ( GridGeneratorContext gridContext : getContext().getGridContexts()) {
            addGridGenerator(gridContext);
        }

        for ( ScreenGeneratorContext screenContext : getContext().getScreenContexts()) {
            addScreenGenerator(screenContext);
        }

        getApplicationContextPrototypeComponent().getPrepareMethod().addText("addServlet(" +
                getPackageName()
                + ".datasource."
                + getContext().getApplicationJavaClassName()
                + "DataSourceApiServlet.class);");

        for ( DataSourceGeneratorContext apiContext : getContext().getDataSourceContexts()) {
            addDataSourceGenerator(apiContext);
        }

    }

}
