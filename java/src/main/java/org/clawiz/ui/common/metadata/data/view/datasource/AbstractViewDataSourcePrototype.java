package org.clawiz.ui.common.metadata.data.view.datasource;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractViewDataSourcePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type type;
    
    public AbstractViewDataSource withName(String value) {
        setName(value);
        return (AbstractViewDataSource) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.type = value;
    }
    
    public AbstractViewDataSource withType(org.clawiz.core.common.metadata.data.type.Type value) {
        setType(value);
        return (AbstractViewDataSource) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getType() != null ) { 
            getType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getType());
        
    }
}
