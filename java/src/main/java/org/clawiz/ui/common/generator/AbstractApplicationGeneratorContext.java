/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.application.Application;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.menu.Menu;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.metadata.data.view.screen.Screen;
import org.clawiz.ui.common.generator.common.menu.MenuGeneratorContext;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;
import org.clawiz.ui.common.generator.common.view.grid.GridGeneratorContext;
import org.clawiz.ui.common.generator.common.view.screen.ScreenGeneratorContext;

import java.util.ArrayList;
import java.util.HashMap;

public class AbstractApplicationGeneratorContext extends AbstractUiGeneratorContext {

    Application application;

    ArrayList<FormGeneratorContext>       formContexts       = new ArrayList<>();
    ArrayList<GridGeneratorContext>       gridContexts       = new ArrayList<>();
    ArrayList<ScreenGeneratorContext>     screenContexts     = new ArrayList<>();
    ArrayList<MenuGeneratorContext>        menuContexts       = new ArrayList<>();
    ArrayList<DataSourceGeneratorContext> dataSourceContexts = new ArrayList<>();

    HashMap<String, FormGeneratorContext>       formContextsCache = new HashMap<>();
    HashMap<String, GridGeneratorContext>       gridContextsCache = new HashMap<>();
    HashMap<String, ScreenGeneratorContext>     screenContextsCache = new HashMap<>();
    HashMap<String, MenuGeneratorContext>       menuContextsCache = new HashMap<>();
    HashMap<String, DataSourceGeneratorContext> dataSourceContextsCache = new HashMap<>();

    @Override
    public void setMetadataNode(MetadataNode metadataNode) {
        setApplication((Application) metadataNode);
    }

    @Override
    public MetadataNode getMetadataNode() {
        return application;
    }

    public String getApplicationJavaClassName() {
        return application.getJavaClassName();
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public ArrayList<FormGeneratorContext> getFormContexts() {
        return formContexts;
    }

    public ArrayList<GridGeneratorContext> getGridContexts() {
        return gridContexts;
    }

    public ArrayList<ScreenGeneratorContext> getScreenContexts() {
        return screenContexts;
    }

    public ArrayList<MenuGeneratorContext> getMenuContexts() {
        return menuContexts;
    }

    public ArrayList<DataSourceGeneratorContext> getDataSourceContexts() {
        return dataSourceContexts;
    }

    public DataSourceGeneratorContext getDataSourceContext(Type type) {
        if ( type == null ) {
            return null;
        }
        for (DataSourceGeneratorContext dataSourceContext : getDataSourceContexts() ) {
            if ( dataSourceContext.getDataSource().getType().getFullName().equals(type.getFullName())) {
                return dataSourceContext;
            }
        }
        return null;
    }

    private void fillContextValues(AbstractUiGeneratorContext context) {
        if ( context.getPackageName() == null ) {
            context.setPackageName(context.getMetadataNode().getPackageName() != null ? context.getMetadataNode().getPackageName() : getPackageName());
        }
        context.setDestinationPath(getDestinationPath());
    }

    private HashMap<Class, Class> nodeToContextMap = new HashMap<>();

    public <N extends MetadataNode, C extends AbstractUiGeneratorContext> void addNodeToContextMap(Class<N> nodeClass, Class<C> contextClass) {
        nodeToContextMap.put(nodeClass, contextClass);
    }

    protected void prepareNodeToContextMap() {

        addNodeToContextMap(Menu.class, MenuGeneratorContext.class);

        addNodeToContextMap(Form.class, FormGeneratorContext.class);

        addNodeToContextMap(Grid.class, GridGeneratorContext.class);

        addNodeToContextMap(Screen.class, ScreenGeneratorContext.class);

        addNodeToContextMap(DataSource.class, DataSourceGeneratorContext.class);
    }

    public <C extends AbstractUiGeneratorContext> AbstractUiGeneratorContext newNodeContext(MetadataNode node, Session session) {
        Class<C> contextClass = nodeToContextMap.get(node.getClass());
        if ( contextClass == null ) {
            throw new CoreException("MetadataNode to generate io context map not contain value for node class '?'", node.getClass().getName());
        }
        AbstractUiGeneratorContext context;
        try {
            context = contextClass.newInstance();
        } catch (Exception e) {
            throw new CoreException("Exception on create ui generate context for MetadataNode '?' of class '?' : ?", node.getName(), node.getClass().getName(), e.getMessage(), e);
        }
        context.setMetadataNode(node);
        fillContextValues(context);
        context.setApplicationContext(this);
        context.prepare(session);
        return context;
    }

    private void addLayoutComponentContext(AbstractLayoutComponent component, Session session) {

        if ( component instanceof AbstractContainer) {
            for (AbstractLayoutComponent child : ((AbstractContainer) component).getComponents()) {
                addLayoutComponentContext(child, session);
            }
        }


        /*if ( component instanceof Form ) {
            addFormContext((Form) component, session);
        } else */
        if ( component instanceof GridLayoutComponent) {
            addGridContext(((GridLayoutComponent) component).getGrid(), session);
        }

    }


    protected void addFormContext(Form form, Session session) {
        if ( formContextsCache.containsKey(form.getFullName())) {
            return;
        }
        for (FormGeneratorContext context : getFormContexts() ) {
            if ( context.getForm().getName().equals(form.getName())) {
                return;
            }
        }

        FormGeneratorContext context = (FormGeneratorContext) newNodeContext(form, session);
        formContexts.add(context);
        formContextsCache.put(form.getFullName(), context);

        addTypeDataSourceContext(form.getRootType(), session);

        for(AbstractLayoutComponent layoutComponent : form.getLayoutComponents() ) {
            addLayoutComponentContext(layoutComponent, session);
        }


    }

    protected void addGridContext(Grid grid, Session session) {
        if ( gridContextsCache.containsKey(grid.getFullName())) {
            return;

        }
        GridGeneratorContext context = (GridGeneratorContext) newNodeContext(grid, session);
        gridContexts.add(context);
        gridContextsCache.put(grid.getFullName(), context);

        addTypeDataSourceContext(grid.getRootType(), session);

        if ( grid.getEditForm() != null ) {
            addFormContext(grid.getEditForm(), session);
        }

    }

    private void makeAddScreenContext(Screen screen, Session session) {
        if ( screenContextsCache.containsKey(screen.getFullName()) ) {
            return;
        }


        ScreenGeneratorContext context = (ScreenGeneratorContext) newNodeContext(screen, session);
        screenContexts.add(context);
        screenContextsCache.put(screen.getFullName(), context);
    }


    protected void addScreenContext(Screen screen, Session session) {

        throw new CoreException("Not implemented");

/*
        if ( screen.getExtendsScreen() != null ) {
            makeAddScreenContext(screen.getExtendsScreen(), session);
        }

        makeAddScreenContext(screen, session);

        if ( screen.getPanel() != null ) {
            addLayoutComponentContext(screen.getPanel(), session);
        }
*/

    }

    private void addMenuContext(Menu menu, Session session) {
        if ( menuContextsCache.containsKey(menu.getFullName()) ) {
            return;
        }

        MenuGeneratorContext context = (MenuGeneratorContext) newNodeContext(menu, session);
        menuContexts.add(context);
        menuContextsCache.put(menu.getFullName(), context);

        throw new CoreException("Menu items (forms, grids e.t.c.) process not implemented");
    }

    private void addDataSourceContext(DataSource dataSource, Session session) {
        if ( dataSourceContextsCache.containsKey(dataSource.getFullName()) ) {
            return;
        }
        DataSourceGeneratorContext context = (DataSourceGeneratorContext) newNodeContext(dataSource, session);
        dataSourceContexts.add(context);
        dataSourceContextsCache.put(dataSource.getFullName(), context);
    }

    private void addTypeDataSourceContext(Type type, Session session) {

        DataSource dataSource = new DataSource();
        String packageName = getPackageName() + ".datasource." + type.getJavaVariableName().toLowerCase();
        dataSource.setPackageName(packageName);
        dataSource.setName(type.getJavaClassName() + "DataSource");
        dataSource.setType(type);

        if ( dataSourceContextsCache.containsKey(dataSource.getFullName())) {
            return;
        }


        addDataSourceContext(dataSource,session);

        for (TypeField field : type.getFields()) {
            if ( field.getValueType() instanceof ValueTypeObject && ((ValueTypeObject) field.getValueType()).getReferencedType() != null ) {
                addTypeDataSourceContext(((ValueTypeObject) field.getValueType()).getReferencedType(), session);
            }
        }

    }


    @Override
    public void prepare(Session session) {
        prepareNodeToContextMap();

        for(DataSource dataSource : application.getDataSources()) {
            addDataSourceContext(dataSource, session);
        }

        for(Form form : application.getForms()) {
            addFormContext(form, session);
        }

        for(Grid grid : application.getGrids()) {
            addGridContext(grid, session);
        }

        for(Screen screen : application.getScreens()) {
            addScreenContext(screen, session);
        }

        for(Menu menu : application.getMenus()) {
            addMenuContext(menu, session);
        }


    }

}
