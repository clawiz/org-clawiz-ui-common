/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.view.form;


import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.install.EmptyMetadataNodeInstaller;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.LayoutComponentList;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.view.datasource.TypeViewDataSource;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;

public class Form extends FormPrototype {

    public Type getRootType() {
        return getDataSource() instanceof TypeViewDataSource
                ? ((TypeViewDataSource) getDataSource()).getType() : null;
    }

    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) EmptyMetadataNodeInstaller.class;
    }

    private boolean _isFormFieldExistsInContainer(LayoutComponentList components, AbstractFormField field) {

        for (AbstractLayoutComponent component : components ) {

            if ( component instanceof Field && ((Field) component).getFormField() == field ) {
                return true;
            } else if ( component instanceof AbstractContainer) {
                if ( _isFormFieldExistsInContainer(((AbstractContainer) component).getComponents(), field)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isFormFieldExistsInLayout(AbstractFormField field) {
        return _isFormFieldExistsInContainer(getLayoutComponents(), field);
    }

    private boolean generatorHelperProcessed = false;

    public boolean isGeneratorHelperProcessed() {
        return generatorHelperProcessed;
    }

    public void setGeneratorHelperProcessed(boolean generatorHelperProcessed) {
        this.generatorHelperProcessed = generatorHelperProcessed;
    }



}
