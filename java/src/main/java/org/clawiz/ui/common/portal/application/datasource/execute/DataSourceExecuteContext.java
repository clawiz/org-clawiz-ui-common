/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.portal.application.datasource.execute;

import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.portal.application.datasource.AbstractDataSource;
import org.clawiz.ui.common.portal.application.datasource.model.DataSourceRowModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class DataSourceExecuteContext<R extends DataSourceRowModel> {

    DataSourceExecuteContext parentContext;
    AbstractDataSource dataSource;

    BigDecimal                              objectId;
    String                                  objectGuid;
    BigDecimal                              typeId;
    ArrayList<DataSourceExecuteFilter> filters = new ArrayList<>();
    BigDecimal                              firstScn;
    BigDecimal                              lastScn;

    ArrayList<R>                            rows                 = new ArrayList<>();
    ArrayList<BigDecimal>                   references           = new ArrayList<>();
    HashMap<BigDecimal, BigDecimal>         referencesCache      = new HashMap<>();
    HashMap<BigDecimal, BigDecimal>         loadedObjectsIdCache = new HashMap<>();
    HashMap<BigDecimal, String>             idToGuidCache        = new HashMap<>();

    Session                                 session;
    ObjectService                           objectService;
    TypeService                             typeService;

    public DataSourceExecuteContext getParentContext() {
        return parentContext;
    }

    public void setParentContext(DataSourceExecuteContext parentContext) {
        this.parentContext = parentContext;
        this.firstScn      = parentContext.getFirstScn();
        this.lastScn       = parentContext.getLastScn();
        this.objectId      = parentContext.getObjectId();
    }

    public AbstractDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(AbstractDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public BigDecimal getObjectId() {
        return objectId;
    }

    public void setObjectId(BigDecimal objectId) {
        this.objectId = objectId;
    }

    public String getObjectGuid() {
        return objectGuid;
    }

    public void setObjectGuid(String objectGuid) {
        this.objectGuid = objectGuid;
    }

    public BigDecimal getTypeId() {
        return typeId;
    }

    public void setTypeId(BigDecimal typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getFirstScn() {
        return firstScn;
    }

    public void setFirstScn(BigDecimal firstScn) {
        this.firstScn = firstScn;
    }

    public BigDecimal getLastScn() {
        return lastScn;
    }

    public void setLastScn(BigDecimal lastScn) {
        this.lastScn = lastScn;
    }

    public ArrayList<R> getRows() {
        return rows;
    }

    public boolean isObjectLoaded(BigDecimal objectId) {
        if ( loadedObjectsIdCache.containsKey(objectId)) {
            return true;
        }
        return parentContext != null && parentContext.isObjectLoaded(objectId);
    }

    public String objectIdToRemoteNodeGuid(BigDecimal id) {
        if ( id == null ) {
            return null;
        }

        BigDecimal remoteNodeId = session.getRemoteNodeId();
        if ( remoteNodeId == null ) {
            dataSource.throwException("Remote node not logged in");
        }


        String guid = session.executeQueryString("select node_guid from cw_core_remote_node_guids where remote_node_id = ? and object_id = ?"
                , remoteNodeId, id);
        if ( guid != null ) {
            idToGuidCache.put(id, guid);
            return guid;
        }

        guid = idToGuidCache.get(id);
        if ( guid != null ) {
            return guid;
        }

        guid = objectService.idToGuid(id);
        if ( guid == null ) {
            dataSource.throwException("Unknown object id ?", id);
        }
        idToGuidCache.put(id, guid);

        return guid;
    }

    public void addRow(R row) {

        rows.add(row);
        loadedObjectsIdCache.put(row.getId(), row.getId());

        if ( parentContext != null ) {
            //noinspection unchecked
            parentContext.addRow(row);
        }

    }

    public ArrayList<DataSourceExecuteFilter> getFilters() {
        return filters;
    }

    public DataSourceExecuteFilter addFilter(String whereCondition, Object... parameters) {
        DataSourceExecuteFilter filter = new DataSourceExecuteFilter(whereCondition, parameters);
        filters.add(filter);
        return filter;
    }

    public void addRowIdFilter(Object value) {
        filters.add(new DataSourceExecuteFilter("a.id"  + " = ?", value));
    }

    public void addColumnEqualFilter(String columnName, Object value) {
        filters.add(new DataSourceExecuteFilter(
                "a." + StringUtils.substring("o_" + columnName, 0, 30) + " = ?", value));
        filters.add(new DataSourceExecuteFilter(
                "a." + StringUtils.substring("n_" + columnName, 0, 30) + " = ?", value));
    }

    public ArrayList<BigDecimal> getReferences() {
        return references;
    }

    public void addRererence(BigDecimal id) {

        if ( id == null ) {
            return;
        }

        if ( ! referencesCache.containsKey(id) ) {
            references.add(id);
            referencesCache.put(id, id);
        }

        if ( parentContext != null ) {
            parentContext.addRererence(id);
        }

    }


    protected void processReferences() {

        while ( references.size() > 0 ) {

            BigDecimal objectId = references.get(0);


            if ( isObjectLoaded(objectId) ) {
                references.remove(0);
                continue;
            }

            DataSourceExecuteContext referenceContext = new DataSourceExecuteContext();
            referenceContext.setParentContext(this);
            referenceContext.setDataSource(this.dataSource);
            referenceContext.setObjectId(objectId);

            referenceContext.loadChanges();

            references.remove(0);

        }

    }


    public void prepareData() {

        // On direct objectId request we must return EXACT ONE record !
        if ( objectId == null ) {
            processReferences();
        }

        rows.sort(Comparator.comparing(DataSourceRowModel::getScn));

        ArrayList<R> resultRows = new ArrayList<R>();

        BigDecimal lastScn = null;
        for (Object object : getRows() ) {
            DataSourceRowModel row = (DataSourceRowModel) object;

            if ( row.getScn().equals(lastScn)) {
                continue;
            }
            lastScn = row.getScn();
            resultRows.add((R) row);

            if ( row.getAction().equals("DELETE") && ! idToGuidCache.containsKey(row.getId())) {
                Statement statement = dataSource.getSession().executeQuery("select object_guid from cw_core_transaction_log where object_id = ? and object_guid is not null", row.getId());
                if ( statement.next() ) {
                    idToGuidCache.put(row.getId(), statement.getString(1));
                }
                statement.close();
            }
        }

        rows = resultRows;

    }

    protected void prepareContext() {
        session       = dataSource.getSession();
        objectService = session.getService(ObjectService.class);
        typeService   = session.getService(TypeService.class);


        if ( objectGuid != null ) {
            objectId = dataSource.nodeGuidToObjectId(objectGuid);
            if ( objectId == null ) {
                Statement  statement = dataSource.executeQuery("select object_id, type_id from cw_core_transaction_log where object_guid = ?", objectGuid);
                if ( statement.next() ) {
                    objectId = statement.getBigDecimal(1);
                    typeId   = statement.getBigDecimal(2);
                }
                statement.close();
                if ( objectId == null ) {
                    dataSource.throwException("Unknown object guid ?", objectGuid);
                }
            }
        }

        if ( objectId != null && typeId == null ) {
            typeId = objectService.idToTypeId(objectId);
            if ( typeId == null ) {
                Statement statement = session.executeQuery("select type_id from cw_core_transaction_log where object_id = ?", objectId);
                if ( statement.next() ) {
                    typeId = statement.getBigDecimal(1);
                }
                statement.close();
            }
            if ( typeId == null ) {
                dataSource.throwException("Cannot resolve type for object id ?", objectId);
            }

        }

    }

    public void loadChanges() {
        prepareContext();
        dataSource.loadChanges(this);
        prepareData();
    }
}
