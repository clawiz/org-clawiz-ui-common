/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource;

import org.clawiz.core.common.storage.user.UserService;
import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.common.datasource.DataSourceGenerator;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGenerator;
import org.clawiz.ui.common.generator.server.java.datasource.api.component.*;

public class ServerDataSourceGenerator extends DataSourceGenerator {

    DataSourceRowModelComponent rowModelComponent;
    DataSourceRequestContextComponent requestContextComponent;
    DataSourceResponseContextComponent responseContextComponent;
    DataSourceExecuteContextComponent executeContextComponentt;

    DataSourceApiServletComponent apiComponent;
    DataSourceDataSourceComponent dataSourceComponent;

    UserService                              userService;

    ServerDataSourceGeneratorContext serverApiContext;

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        serverApiContext = (ServerDataSourceGeneratorContext) context;
    }

    @Override
    public ServerDataSourceGeneratorContext getContext() {
        return serverApiContext;
    }

    public DataSourceApiServletComponent getApiComponent() {
        return apiComponent;
    }

    public DataSourceRowModelComponent getRowModelComponent() {
        return rowModelComponent;
    }

    public DataSourceRequestContextComponent getRequestContextComponent() {
        return requestContextComponent;
    }

    public DataSourceResponseContextComponent getResponseContextComponent() {
        return responseContextComponent;
    }

    public DataSourceExecuteContextComponent getExecuteContextComponentt() {
        return executeContextComponentt;
    }

    public ServerDataSourceGeneratorContext getApiContext() {
        return serverApiContext;
    }

    public DataSourceDataSourceComponent getDataSourceComponent() {
        return dataSourceComponent;
    }

    public ServerJavaApplicationGenerator getApplicationGenerator() {
        return getContext().getApplicationGenerator();
    }

    protected void fillApiDefaults() {
/*
        for (TypeField field : getDataSource().getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeObject ) {
                ValueTypeObject vto = (ValueTypeObject) field.getValueType();
                if ( vto.getReferencedType() != null
                        && userService.getTypeId().equals(vto.getReferencedType().getId()) ) {
                    boolean found = false;
                    for (AbstractDataSourceFilter filter : getDataSource().getFilters() ) {
                        if ( filter instanceof UserEqualTypeFieldDataSourceFilter
                                && ((UserEqualTypeFieldDataSourceFilter) filter).getTypeField().getName().equals(field.getName())) {
                            found = true;
                            break;
                        }
                    }
                    if ( ! found ) {
                        UserEqualTypeFieldDataSourceFilter filter = new UserEqualTypeFieldDataSourceFilter();
                        filter.setTypeField(field);
                        getDataSource().getFilters().add(filter);
                    }
                }
            }
        }
*/
    }



    @Override
    protected void process() {
        logDebug("Start generate datasource '" + getPackageName() + "." + getDataSource().getJavaClassName() + "'");

        super.process();

        fillApiDefaults();

        rowModelComponent = addComponent(DataSourceRowModelComponent.class);
        addComponent(DataSourceRowModelPrototypeComponent.class);

        requestContextComponent = addComponent(DataSourceRequestContextComponent.class);
        addComponent(DataSourceRequestContextPrototypeComponent.class);

        responseContextComponent = addComponent(DataSourceResponseContextComponent.class);
        addComponent(DataSourceResponseContextPrototypeComponent.class);

        executeContextComponentt = addComponent(DataSourceExecuteContextComponent.class);
        addComponent(DataSourceExecuteContextPrototypeComponent.class);


        dataSourceComponent = addComponent(DataSourceDataSourceComponent.class);
        addComponent(DataSourceDataSourcePrototypeComponent.class);

        apiComponent = addComponent(DataSourceApiServletComponent.class);
        addComponent(DataSourceApiServletPrototypeComponent.class);



    }



    @Override
    protected void done() {
        super.done();

        logDebug("Done generate datasource '" + getPackageName() + "." + getDataSource().getJavaClassName() + "'");

    }
}
