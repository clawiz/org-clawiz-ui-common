/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.grid.component;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.common.generator.client.ts.AbstractTypeScriptClientClassComponent;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGenerator;


public class AbstractTypeScriptGridClassComponent extends AbstractTypeScriptClientClassComponent {

    TypeScriptUiGridGenerator gridGenerator;

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        gridGenerator = (TypeScriptUiGridGenerator) generator;
    }

    @Override
    public TypeScriptUiGridGenerator getGenerator() {
        return gridGenerator;
    }

    @Override
    protected TypeScriptClientCommonGenerator getApplicationGenerator() {
        return getGenerator().getContext().getApplicationGenerator();
    }

    protected Grid getGrid() {
        return getGenerator().getGrid();
    }

    @SuppressWarnings("Duplicates")
    public String toTypeScriptType(AbstractGridColumn column) {

        if ( column.getTypeField() == null ) {
            return toTypeScriptType(column.getValueType());
        }

        if ( ! (column.getValueType() instanceof ValueTypeEnumeration) ) {
            return toTypeScriptType(column.getValueType());
        }

        DataSource ds       = getApplicationGenerator().getContext().getTypeDataSource(column.getTypeField().getType());
        String     typeName = ds.getJavaClassName() + column.getTypeField().getValueTypeJavaClassName();

        addImport(
                column.getTypeField().getValueTypeJavaClassName()
                , typeName
                , getApplicationGenerator().getDataSourceImportPath(ds));


        return typeName;
    }

    @Override
    public String getDestinationPath() {
        return getApplicationGenerator().getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getApplicationGenerator().getPackageName(), getGrid().getPackageName())
                + "/" + getGrid().getJavaClassName().toLowerCase();
    }
}
