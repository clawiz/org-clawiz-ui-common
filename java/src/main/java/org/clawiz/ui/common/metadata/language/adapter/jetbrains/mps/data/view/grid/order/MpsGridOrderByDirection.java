package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order;

import org.clawiz.core.common.CoreException;

public enum MpsGridOrderByDirection {

    ASC, DESC;

    
    public static MpsGridOrderByDirection toMpsGridOrderByDirection(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsGridOrderByDirection.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsGridOrderByDirection value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsGridOrderByDirection value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case ASC : return "ASC";
            case DESC : return "DESC";
        }
         
        return null;
    }
}
