package org.clawiz.ui.common.metadata.data.layout;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractLayoutComponentPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign horizontalAlign;
    
    @ExchangeAttribute
    private org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign verticalAlign;
    
    @ExchangeAttribute
    private String text;
    
    @ExchangeAttribute
    private String onClickExpression;
    
    @ExchangeAttribute
    private String visibleExpression;
    
    @ExchangeAttribute
    private String enabledExpression;
    
    @ExchangeAttribute
    private String disabledExpression;
    
    public AbstractLayoutComponent withName(String value) {
        setName(value);
        return (AbstractLayoutComponent) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign getHorizontalAlign() {
        return this.horizontalAlign;
    }
    
    public void setHorizontalAlign(org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign value) {
        this.horizontalAlign = value;
    }
    
    public AbstractLayoutComponent withHorizontalAlign(org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign value) {
        setHorizontalAlign(value);
        return (AbstractLayoutComponent) this;
    }
    
    public org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign getVerticalAlign() {
        return this.verticalAlign;
    }
    
    public void setVerticalAlign(org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign value) {
        this.verticalAlign = value;
    }
    
    public AbstractLayoutComponent withVerticalAlign(org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign value) {
        setVerticalAlign(value);
        return (AbstractLayoutComponent) this;
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(String value) {
        this.text = value;
    }
    
    public AbstractLayoutComponent withText(String value) {
        setText(value);
        return (AbstractLayoutComponent) this;
    }
    
    public String getOnClickExpression() {
        return this.onClickExpression;
    }
    
    public void setOnClickExpression(String value) {
        this.onClickExpression = value;
    }
    
    public AbstractLayoutComponent withOnClickExpression(String value) {
        setOnClickExpression(value);
        return (AbstractLayoutComponent) this;
    }
    
    public String getVisibleExpression() {
        return this.visibleExpression;
    }
    
    public void setVisibleExpression(String value) {
        this.visibleExpression = value;
    }
    
    public AbstractLayoutComponent withVisibleExpression(String value) {
        setVisibleExpression(value);
        return (AbstractLayoutComponent) this;
    }
    
    public String getEnabledExpression() {
        return this.enabledExpression;
    }
    
    public void setEnabledExpression(String value) {
        this.enabledExpression = value;
    }
    
    public AbstractLayoutComponent withEnabledExpression(String value) {
        setEnabledExpression(value);
        return (AbstractLayoutComponent) this;
    }
    
    public String getDisabledExpression() {
        return this.disabledExpression;
    }
    
    public void setDisabledExpression(String value) {
        this.disabledExpression = value;
    }
    
    public AbstractLayoutComponent withDisabledExpression(String value) {
        setDisabledExpression(value);
        return (AbstractLayoutComponent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
