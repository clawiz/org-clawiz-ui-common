/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid.api.component;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.StringMap;

import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.query.element.order.QueryOrderDirection;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.server.java.application.common.grid.component.ApplicationAbstractGridApiServletComponent;
import org.clawiz.ui.common.generator.server.java.view.grid.api.component.element.*;
import org.clawiz.ui.common.generator.server.java.view.grid.component.AbstractGridJavaClassComponent;

import java.util.ArrayList;

public class GridApiServletPrototypeComponent extends AbstractGridJavaClassComponent {

    GridApiServletProcessDoPostMethodElement processDoPost;

    public static String getServletPath(Grid grid) {
        return "/api/grid/" + StringUtils.toLowerFirstChar(grid.getJavaClassName());
    }


    public GridApiServletProcessDoPostMethodElement getProcessDoPost() {
        return processDoPost;
    }

    protected void addImports() {
        addImport(StringUtils.class);
        addImport(AbstractHttpRequestContext.class);
        addImport(Statement.class);
        addImport(JsonElement.class);
        addImport(JsonPrimitive.class);
        addImport(StringMap.class);
        addImport(ArrayList.class);
        addImport(ApiResponseContext.class);
        addImport(Query.class);
        addImport(QueryOrderDirection.class);

        if ( getGenerator().getRootType() != null ) {
            Type rootType = getGenerator().getRootType();
            addImport(rootType.getPackageName() + "." + rootType.getJavaClassName().toLowerCase() + "." + rootType.getJavaClassName() + "Model");
            addImport(rootType.getPackageName() + "." + rootType.getJavaClassName().toLowerCase() + "." + rootType.getJavaClassName() + "Service");
        }
    }

    protected void addVariables() {
        if ( getGenerator().getRootType() != null ) {
            Type rootType = getGenerator().getRootType();
            addVariable(rootType.getJavaClassName() + "Model", StringUtils.toLowerFirstChar(rootType.getJavaClassName()) + "Model").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
            addVariable(rootType.getJavaClassName() + "Service", StringUtils.toLowerFirstChar(rootType.getJavaClassName()) + "Service").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        }
    }

    protected void addGetPath() {
        JavaMethodElement pathMethod = addMethod("String", "getPath");
        pathMethod.addText("return \"" + getServletPath(getGrid()) + "\";");
    }

    public void addGetTypeModel() {
        if ( getGrid().getRootType() == null ) {
            return;
        }
        JavaMethodElement method = addMethod(getGrid().getRootType().getJavaClassName() + "Model", "getTypeModel").withAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);
        method.addText("return " + StringUtils.toLowerFirstChar(getGrid().getRootType().getJavaClassName()) + "Model;");
    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(GridApiServletComponent.class).getName() + "Prototype");

        addImports();

        AbstractJavaClassComponent abstractServlet =  getGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractGridApiServletComponent.class);
        addImport(abstractServlet.getFullName());
        setExtends(abstractServlet.getName()+"<" + getGenerator().getRequestContextClassName() + ">");


        addVariables();

        addGetPath();
        addGetTypeModel();

        addElement(GridApiCreateRequestServletMethodElement.class);

        addElement(GridApiPrepareQueryServletMethodElement.class);
        addElement(GridApiProcessLoadServletMethodElement.class);
        addElement(GridApiProcessDeleteMethodElement.class);
        addElement(GridApiProcessCloseMethodElement.class);

        processDoPost = addElement(GridApiServletProcessDoPostMethodElement.class);

        addElement(GridApiServletProcessExceptionMethodElement.class);

        addElement(GridApiServletModelToStringMapMethodElement.class);
        addElement(GridApiServletPrepareResponseMethodElement.class);

//        addElement(GridApiServletDoGetMethodElement.class);
        addElement(GridApiServletDoPostMethodElement.class);
    }

}
