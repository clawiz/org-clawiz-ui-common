package org.clawiz.ui.common.metadata.data.layout.container.collection.direction;

import org.clawiz.core.common.CoreException;

public enum CollectionDirection {

    HORIZONTAL, VERTICAL;

    
    public static CollectionDirection toCollectionDirection(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return CollectionDirection.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong CollectionDirection value '?'", value);
        }
        
    }
    
    public static String toDescription(CollectionDirection value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case HORIZONTAL: return "horizontal";
            case VERTICAL  : return "vertical";
        }
        
        return value.toString();
    }
}
