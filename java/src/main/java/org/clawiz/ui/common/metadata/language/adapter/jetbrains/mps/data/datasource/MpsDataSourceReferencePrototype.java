package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsDataSourceReferencePrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSource dataSource;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSource getDataSource() {
        return this.dataSource;
    }
    
    public void setDataSource(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSource value) {
        this.dataSource = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2062302247217268568";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.DataSourceReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "2062302247217268568", "org.clawiz.ui.common.language.structure.DataSourceReference", ConceptPropertyType.REFERENCE, "2062302247217269536", "dataSource"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("2062302247217268568", "dataSource", getDataSource());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
        addForeignKey(getDataSource());
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
