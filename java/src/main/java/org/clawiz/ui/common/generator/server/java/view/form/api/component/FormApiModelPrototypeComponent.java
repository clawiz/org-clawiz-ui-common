/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component;


import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.generator.server.java.application.common.form.component.ApplicationAbstractFormApiModelComponent;
import org.clawiz.ui.common.generator.server.java.view.form.component.AbstractFormJavaClassComponent;

import java.math.BigDecimal;
import java.util.Date;

public class FormApiModelPrototypeComponent extends AbstractFormJavaClassComponent {

    public static final String ADD_FIELD_VARIABLES_EXTENSION_NAME = "addFormApiModelPrototypeFieldVariables";

    public void addFieldVariable(AbstractFormField field) {

        if ( field.getValueType() instanceof ValueTypeList ) {
            return;
        }

        addVariable(getValueTypeJavaClassName(field), field.getJavaVariableName(), null, true, true);
        if ( field.getValueType() instanceof ValueTypeEnumeration) {
            addVariable("String", field.getToStringJavaVariableName(), null, true, true);
        }

    }

    public void addFieldVariables() {
        for(AbstractFormField field : getUnitedWithRootTypeFormFields() ) {
            addFieldVariable(field);
        }
        processExtensions(ADD_FIELD_VARIABLES_EXTENSION_NAME);
    }

    @Override
    public void process() {
        super.process();

        setName(getFormGenerator().getComponentByClass(FormApiModelComponent.class).getName() + "Prototype");

        AbstractJavaClassComponent abstractServlet =  getFormGenerator().getApplicationGenerator().getComponentByClass(ApplicationAbstractFormApiModelComponent.class);
        addImport(abstractServlet.getFullName());

        setExtends(abstractServlet.getName());

        addImport(BigDecimal.class);
        addImport(Date.class);

        addFieldVariables();


    }


}
