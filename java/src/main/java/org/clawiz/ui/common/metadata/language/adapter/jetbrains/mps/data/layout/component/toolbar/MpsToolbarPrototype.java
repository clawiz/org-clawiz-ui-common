package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsToolbarPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer left;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer right;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer getLeft() {
        return this.left;
    }
    
    public void setLeft(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer value) {
        this.left = value;
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer getRight() {
        return this.right;
    }
    
    public void setRight(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer value) {
        this.right = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234832524";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Toolbar";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832524", "org.clawiz.ui.common.language.structure.Toolbar", ConceptPropertyType.CHILD, "7111202990234832525", "left"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "7111202990234832524", "org.clawiz.ui.common.language.structure.Toolbar", ConceptPropertyType.CHILD, "7111202990234832531", "right"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("7111202990234832524", "left", getLeft());
        addConceptNodeChild("7111202990234832524", "right", getRight());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar structure = (org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) node;
        
        if ( getLeft() != null ) {
            structure.setLeft((org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer) getLeft().toMetadataNode(structure, "left"));
        } else {
            structure.setLeft(null);
        }
        
        if ( getRight() != null ) {
            structure.setRight((org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer) getRight().toMetadataNode(structure, "right"));
        } else {
            structure.setRight(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar structure = (org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar) node;
        
        if ( structure.getLeft() != null ) {
            setLeft(loadChildMetadataNode(structure.getLeft()));
        } else {
            setLeft(null);
        }
        
        if ( structure.getRight() != null ) {
            setRight(loadChildMetadataNode(structure.getRight()));
        } else {
            setRight(null);
        }
        
    }
}
