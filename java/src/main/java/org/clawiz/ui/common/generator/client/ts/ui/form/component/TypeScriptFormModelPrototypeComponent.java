/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form.component;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormModelComponent;

import static org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext.CLASS_NAME_SELECT_VALUE;

public class TypeScriptFormModelPrototypeComponent extends AbstractTypeScriptClassFormComponent {


    public static final String AFTER_PROCESS_EXTENSION_NAME = "afterTypeScriptFormModelPrototypeProcess";

    protected void addFieldVariable(AbstractFormField field) {

        addField(field.getJavaVariableName(), toTypeScriptType(field), true);
        if ( field.getValueType() instanceof ValueTypeObject
                || field.getValueType() instanceof ValueTypeDate
                || field.getValueType() instanceof ValueTypeDateTime
                ) {
            addField(field.getToStringJavaVariableName(), "string", true);
        }

        if ( field.getSelectValueContext() instanceof StaticListSelectValue) {
            addField(field.getToStringJavaVariableName(), "string", true);
            addImport(CLASS_NAME_SELECT_VALUE);
            addField(field.getJavaVariableName() + "SelectOptions"
                    , getImportMapName(CLASS_NAME_SELECT_VALUE) + "[]"
                    , true);
        }


    }


    @Override
    public void addImport(String name, String path) {
        super.addImport(name, path);
    }

    @Override
    public void addImport(String name) {
        super.addImport(name);
    }

    @Override
    public void process() {

        super.process();

        setName(getGenerator().getComponentByClass(TypeScriptFormModelComponent.class).getName() + "Prototype");

        setExtends(getApplicationGenerator().getComponentByClass(ApplicationAbstractFormModelComponent.class));

        for(AbstractFormField field : getUnitedWithRootTypeFormFields() ) {
            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }
            addFieldVariable(field);
        }

        processExtensions(AFTER_PROCESS_EXTENSION_NAME);

    }

}
