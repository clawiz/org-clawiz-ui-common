package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsScreenPrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen extendsScreen;
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> components = new ArrayList<>();
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen getExtendsScreen() {
        return this.extendsScreen;
    }
    
    public void setExtendsScreen(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen value) {
        this.extendsScreen = value;
    }
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> getComponents() {
        return this.components;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499619515974";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.Screen";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499619515974", "org.clawiz.ui.common.language.structure.Screen", ConceptPropertyType.REFERENCE, "8930075074315814273", "extendsScreen"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499619515974", "org.clawiz.ui.common.language.structure.Screen", ConceptPropertyType.CHILD, "111278499619580445", "components"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getComponents() ) {
            addConceptNodeChild("111278499619515974", "components", value);
        }
        addConceptNodeRef("111278499619515974", "extendsScreen", getExtendsScreen());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.screen.Screen.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.screen.Screen structure = (org.clawiz.ui.common.metadata.data.view.screen.Screen) node;
        
        if ( getExtendsScreen() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "extendsScreen", false, getExtendsScreen());
        } else {
            structure.setExtendsScreen(null);
        }
        
        structure.getComponents().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent mpsNode : getComponents() ) {
            if ( mpsNode != null ) {
                structure.getComponents().add((org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent) mpsNode.toMetadataNode(structure, "components"));
            } else {
                structure.getComponents().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getExtendsScreen());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.screen.Screen structure = (org.clawiz.ui.common.metadata.data.view.screen.Screen) node;
        
        if ( structure.getExtendsScreen() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "extendsScreen", false, structure.getExtendsScreen());
        } else {
            setExtendsScreen(null);
        }
        
        getComponents().clear();
        for (org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent metadataNode : structure.getComponents() ) {
            if ( metadataNode != null ) {
                getComponents().add(loadChildMetadataNode(metadataNode));
            } else {
                getComponents().add(null);
            }
        }
        
    }
}
