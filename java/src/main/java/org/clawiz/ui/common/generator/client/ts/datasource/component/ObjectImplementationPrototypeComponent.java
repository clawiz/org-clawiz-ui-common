/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component;

import org.clawiz.core.common.system.generator.typescript.component.AbstractTypeScriptClassComponent;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.common.metadata.data.datasource.DataSource;
import org.clawiz.ui.common.generator.client.ts.application.storage.ApplicationObjectImplementationComponent;
import org.clawiz.ui.common.generator.client.ts.application.storage.CommonDataSourceInterfacesComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.AbstractTypeScriptClientDataSourceClassComponent;

public class ObjectImplementationPrototypeComponent extends AbstractTypeScriptClientDataSourceClassComponent {

    CommonDataSourceInterfacesComponent interfacesComponent;

    protected void addFields() {

        addField("id", "number");

        for( TypeField field : getDataSource().getType().getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            if ( field.getValueType() instanceof ValueTypeList) {  continue;  }

            if ( field.getValueType() instanceof ValueTypeEnumeration) {
                addImport(field.getValueTypeJavaClassName(), "./" + nameToDottedFileName(field.getValueTypeJavaClassName()));
            }

            addField(field.getJavaVariableName(), toTypeScriptType(field));

            if ( field.getValueType() instanceof ValueTypeObject ) {
                ValueTypeObject vto = (ValueTypeObject) field.getValueType();
                if ( vto.getReferencedType() != null ) {
                    DataSource referenceDataSource = getGenerator().getContext().getApplicationGenerator()
                            .getContext().getTypeDataSource(vto.getReferencedType());
                    if ( referenceDataSource != null ) {
                        String referencedClassName = referenceDataSource.getJavaClassName();
                        addImport(referencedClassName, interfacesComponent);
                        addField(getFieldDereferenceVariableName(field), referencedClassName);
                    }
                }
            }

        }

    }

    @Override
    public void process() {
        super.process();

        interfacesComponent = getApplicationGenerator().getComponentByClass(CommonDataSourceInterfacesComponent.class);


        setName(getGenerator().getComponentByClass(ObjectImplementationComponent.class).getName() + "Prototype");

        AbstractTypeScriptClassComponent applicationObjectImplementation = getGenerator().getApplicationGenerator().getComponentByClass(ApplicationObjectImplementationComponent.class);

        addImport(applicationObjectImplementation);
        setExtends(applicationObjectImplementation.getName());

        addImport(getObjectInterfaceName(), interfacesComponent);
        addImplements(getObjectInterfaceName());

        addFields();


    }

}
