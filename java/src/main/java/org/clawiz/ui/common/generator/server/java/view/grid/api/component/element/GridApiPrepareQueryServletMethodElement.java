/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.grid.api.component.element;


import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.common.metadata.data.view.grid.column.TypeFieldGridColumn;
import org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn;
import org.clawiz.ui.common.generator.server.java.view.grid.component.element.AbstractGridServletMethodElement;

public class GridApiPrepareQueryServletMethodElement extends AbstractGridServletMethodElement {

    @Override
    public void process() {
        super.process();

        setName("prepareQuery");
        setAccessLevel(AccessLevel.PROTECTED);


        addParameter(getRequestContextClassName(), "request");
        addParameter("Query", "query");

        Type rootType = getRootType();
        if ( rootType == null ) {
            throwException("Cannot create query without root type");
        }
        String modelServiceName = StringUtils.toLowerFirstChar(rootType.getJavaClassName()) + "Model";

        addText("query.select()");
        addText("        .column(" + modelServiceName + ".id())");

        for(AbstractGridColumn column : getGrid().getColumns() ) {
            if ( column.getValueType() instanceof ValueTypeList) {
                continue;
            }
            if ( column instanceof TypeFieldGridColumn) {
                TypeFieldGridColumn fieldColumn = (TypeFieldGridColumn) column;
                addText("        .column(" + modelServiceName + "." + fieldColumn.getTypeField().getJavaVariableName() + "())");
            } else {
                throwException("Wrong grid column ? class ?", column.getName(), column.getClass().getName());
            }
        }

        if ( getGrid().getOrderBy().size() > 0 ) {
            addText("        .orderBy()");
            for (GridOrderByColumn orderColumn : getGrid().getOrderBy() ) {
                AbstractGridColumn column                  = orderColumn.getColumn();
                if ( column instanceof TypeFieldGridColumn ) {
                    TypeFieldGridColumn fieldColumn = (TypeFieldGridColumn) column;
                    addText("        .column(" + modelServiceName + "." + fieldColumn.getTypeField().getJavaVariableName() + "()" +
                            ", QueryOrderDirection." + orderColumn.getDirection().toString() + ")");
                } else {
                    throwException("Wrong grid column ? class ?", column.getName(), column.getClass().getName());
                }
            }
        }

        addText("        ;");

    }
}
