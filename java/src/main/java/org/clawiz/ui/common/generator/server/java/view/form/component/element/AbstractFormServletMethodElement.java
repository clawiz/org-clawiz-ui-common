/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.component.element;


import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.FormFieldList;
import org.clawiz.ui.common.generator.server.java.view.form.ServerFormGenerator;
import org.clawiz.ui.common.generator.server.java.view.form.component.AbstractFormJavaClassComponent;

public class AbstractFormServletMethodElement extends JavaMethodElement {


    private ServerFormGenerator formGenerator;
    private AbstractFormJavaClassComponent formComponent;
    private Form form;


    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        formComponent = (AbstractFormJavaClassComponent) component;
        formGenerator = formComponent.getGenerator();
        form          = formComponent.getForm();
    }

    @Override
    public AbstractFormJavaClassComponent getComponent() {
        return formComponent;
    }

    public ServerFormGenerator getGenerator() {
        return formGenerator;
    }

    public Form getForm() {
        return form;

    }

    public FormFieldList getFields() {
        return form.getFields();
    }

    public FormFieldList getUnitedWithRootTypeFormFields() {
        return getGenerator().getUnitedWithRootTypeFormFields();
    }


    public String getModelClassName() {
        return getGenerator().getModelClassName();
    }

    public String getRequestContextClassName() {
        return getGenerator().getRequestContextClassName();
    }

    public String getResponseContextClassName() {
        return getGenerator().getResponseContextClassName();
    }

    public Type getRootType() {
        return getGenerator().getRootType();
    }

}
