/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.application.component;


import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.ui.common.generator.server.java.AbstractServerJavaClassComponent;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGenerator;
import org.clawiz.ui.common.generator.server.java.ServerJavaApplicationGeneratorContext;

public class AbstractApplicationJavaClassComponent extends AbstractServerJavaClassComponent {

    ServerJavaApplicationGeneratorContext applicationContext;
    ServerJavaApplicationGenerator applicationGenerator;


    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        this.applicationGenerator = (ServerJavaApplicationGenerator) generator;
    }

    public ServerJavaApplicationGenerator getGenerator() {
        return this.applicationGenerator;
    }

    public ServerJavaApplicationGeneratorContext getApplicationContext() {
        if ( applicationContext == null ) {
            applicationContext = getGenerator().getContext();
        }
        return applicationContext;
    }

    public String getApplicationName() {
        return getApplicationContext().getApplicationJavaClassName();
    }

    public <T extends AbstractComponent> T getComponentByClass(Class<T> clazz) {
        return getGenerator().getComponentByClass(clazz);
    }

    public <T extends AbstractComponent> String getComponentNameByClass(Class<T> clazz) {
        return getComponentByClass(clazz).getName();
    }

    @Override
    public void process() {
        super.process();
        setPackageName(getApplicationContext().getPackageName());// + ".portal");
    }
}
