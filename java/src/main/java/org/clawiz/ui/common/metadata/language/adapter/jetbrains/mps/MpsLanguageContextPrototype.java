package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps;

import java.lang.String;

public class MpsLanguageContextPrototype extends org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext {
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public void prepare() {
        
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsAbstractMenu.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsMenu.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.item.MpsAbstractMenuItem.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.row.MpsRow.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.MpsAbstractContainer.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.column.MpsColumn.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.field.MpsField.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsForm.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.MpsAbstractButton.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.MpsButton.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.application.MpsApplication.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.menu.MpsMenuReference.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsAbstractFormField.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsLocalFormField.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsAbstractFormFieldProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsHintFormFieldProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsLocalGridColumn.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsAbstractGridColumnProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByColumn.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.property.MpsReadOnlyFormFieldProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreen.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.MpsFormLayoutComponent.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.MpsGridLayoutComponent.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.MpsFormReference.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGridReference.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.screen.MpsScreenReference.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsAbstractViewDataSource.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsTypeViewDataSource.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.datasource.MpsEmptyViewDataSource.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsTypeFieldGridColumn.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsHiddenColumnProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.field.MpsTypeFieldFormField.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSource.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.MpsDataSourceReference.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.datasource.filter.MpsAbstractDataSourceFilter.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsAbstractGridProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsHorizontalAlignGridColumnProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.property.MpsVerticalAlignGridColumnProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsNoActionButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsSubmitButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsAbstractButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsRowsPerPageGridProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.property.MpsInlineEditGridProperty.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsCancelButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.MpsAbstractCollection.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.panel.MpsPanel.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container.collection.MpsIteratorCollection.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.MpsText.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.toolbar.MpsToolbar.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.button.action.MpsCreateRowButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.button.action.MpsDeleteRowButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.button.action.MpsEditRowButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.button.action.MpsSaveButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.form.button.action.MpsDiscardButtonAction.class);
        addClassMap(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component.button.action.MpsServiceMethodCallButtonAction.class);
        
    }
}
