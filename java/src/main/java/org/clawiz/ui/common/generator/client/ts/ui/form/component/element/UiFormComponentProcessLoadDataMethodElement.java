/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.ui.form.component.element;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.StaticListSelectValue;
import org.clawiz.ui.common.metadata.data.view.form.field.selectvalue.staticlist.value.SelectValue;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;

public class UiFormComponentProcessLoadDataMethodElement extends AbstractUiFormComponentMethodElement {

    protected String getEnumFieldOptionsExpression(StaticListSelectValue context) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');

        String prefix = "";
        for (SelectValue value : context.getValues() ) {
            sb.append(prefix);
            sb.append("{ value : '");
            sb.append(
                    value.getValue() != null ?  value.getValue().replaceAll("'", "\\'").toUpperCase() : ""
            );
            sb.append("', text :'");
            sb.append(
                    value.getText() != null ?  value.getText().replaceAll("\\'", "\\\\'") : ""
            );
            sb.append("' }");
            prefix = ", ";
        }

        sb.append(']');

        return sb.toString();
    }


    protected void addSetParentToTopLevelGrids() {

        for (Grid grid : getGenerator().getContext().getGrids()) {

            FormGeneratorContext.GridParentContext parentContext = getGenerator().getContext().getGridParentContext(grid);
            if ( parentContext.getParentGrid() == null ) {
                addText("");
                addText("    this." + getGenerator().getNodeVariableName(grid) + "Component.setParentLink({");
                addText("        name : '" + getGenerator().getNodeVariableName(grid)  + "'");
                addText("        ,values : [");
                addText("          {");
                addText("            columnName : '" + parentContext.getLinkField().getJavaVariableNameWithoutId() + "'");
                addText("            ,value     : loadedData.id");
                addText("          }");
                addText("        ]");
                addText("      });");
            }

            for (Grid child : getGenerator().getContext().getGridChilds(grid) ) {
                FormGeneratorContext.GridParentContext childContext = getGenerator().getContext().getGridParentContext(child);
                addText("");
                addText("    this." + getGenerator().getNodeVariableName(grid) + "Component.setChildLink({");
                addText("        grid : this." + getGenerator().getNodeVariableName(child) + "Component") ;
                addText("        ,name : '" + getGenerator().getNodeVariableName(child) +  "'");
                addText("        ,values : [");
                addText("          {");
                addText("            columnName : '" + childContext.getLinkField().getJavaVariableNameWithoutId() + "'");
                addText("          }");
                addText("        ]");
                addText("      });");
            }
        }

        addText("");

    }


    @Override
    public void process() {
        super.process();

        setName("processLoadData");
        setType("Promise<any>");

        addParameter("loadedData", getModelClassName());

        addText("");
        addText("return super.processLoadData(loadedData)");
        addText("  .then(() => {");

        for (AbstractFormField field : getForm().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeDate) {
                addText("    loadedData." + field.getJavaVariableName() + "         = this.dateUtils.stringToDate((<any>loadedData)['" + field.getJavaVariableName() + "'], this.dateUtils.ISO8601_DATE_FORMAT);");
                addText("    loadedData." + field.getToStringJavaVariableName() + " = this.dateUtils.dateToString(loadedData." + field.getJavaVariableName() + ");");


            } else if ( field.getValueType() instanceof ValueTypeDateTime) {
                addText("    loadedData." + field.getJavaVariableName() + " = this.dateUtils.stringToDateTime((<any>loadedData)['" + field.getJavaVariableName() + "'], this.dateUtils.ISO8601_DATE_TIME_FORMAT);");
                addText("    loadedData." + field.getToStringJavaVariableName() + " = this.dateUtils.dateTimeToString(loadedData." + field.getJavaVariableName() + ");");
            }
            if ( field.getSelectValueContext() instanceof StaticListSelectValue) {
                addText("    loadedData." + field.getJavaVariableName() + "SelectOptions = " + getEnumFieldOptionsExpression((StaticListSelectValue) field.getSelectValueContext()) + ";");
            }

        }

        addSetParentToTopLevelGrids();

        addText("    return Promise.resolve();");
        addText("  });");
        addText("");


    }
}
