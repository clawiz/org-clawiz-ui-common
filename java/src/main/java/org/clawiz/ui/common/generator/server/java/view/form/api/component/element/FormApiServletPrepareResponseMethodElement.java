/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.view.form.api.component.element;


import org.clawiz.core.common.system.generator.java.component.element.JavaBlockElement;
import org.clawiz.ui.common.generator.server.java.view.form.component.element.AbstractFormServletMethodElement;

public class FormApiServletPrepareResponseMethodElement extends AbstractFormServletMethodElement {


    public static final String AFTER_PREPARE_RESPONSE_EXTENSION_NAME = "afterPrepareResponse";

    JavaBlockElement actionSwitch;

    public JavaBlockElement getActionSwitch() {
        return actionSwitch;
    }

    @Override
    public void process() {
        super.process();
        setName("prepareResponse");
        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");

        addParameter(getResponseContextClassName(), "response");

        addText("if ( response.getModel() != null ) {");
        addText("    response.setData(modelToStringMap(response.getModel()));");
        addText("}");

        processExtensions(AFTER_PREPARE_RESPONSE_EXTENSION_NAME);

    }



}
