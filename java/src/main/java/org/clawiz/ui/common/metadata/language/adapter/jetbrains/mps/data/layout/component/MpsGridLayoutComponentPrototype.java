package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.component;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsGridLayoutComponentPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid grid;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid getGrid() {
        return this.grid;
    }
    
    public void setGrid(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.MpsGrid value) {
        this.grid = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "111278499619584134";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.GridLayoutComponent";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "111278499619584134", "org.clawiz.ui.common.language.structure.GridLayoutComponent", ConceptPropertyType.REFERENCE, "111278499619585100", "grid"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("111278499619584134", "grid", getGrid());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent) node;
        
        if ( getGrid() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "grid", false, getGrid());
        } else {
            structure.setGrid(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getGrid());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent structure = (org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent) node;
        
        if ( structure.getGrid() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "grid", false, structure.getGrid());
        } else {
            setGrid(null);
        }
        
    }
}
