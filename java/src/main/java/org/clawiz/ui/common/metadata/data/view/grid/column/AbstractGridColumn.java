/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.metadata.data.view.grid.column;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign;
import org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign;
import org.clawiz.ui.common.metadata.data.view.grid.column.property.HiddenColumnProperty;
import org.clawiz.ui.common.metadata.data.view.grid.column.property.HorizontalAlignGridColumnProperty;
import org.clawiz.ui.common.metadata.data.view.grid.column.property.VerticalAlignGridColumnProperty;

import java.math.BigDecimal;

public class AbstractGridColumn extends AbstractGridColumnPrototype {

    public boolean isHidden() {
        return getProperties().get(HiddenColumnProperty.class) != null;
    }

    public BigDecimal getPrecision() {
        throw new CoreException("Method ?.getPrecision not implemented", getClass().getName());
    }

    public BigDecimal getScale() {
        throw new CoreException("Method ?.getScale not implemented", getClass().getName());
    }

    public VerticalAlign getVerticalAlign() {
        VerticalAlignGridColumnProperty property = getProperties().get(VerticalAlignGridColumnProperty.class);
        return property != null  && property.getAlign() != null ? property.getAlign() : VerticalAlign.TOP;
    }

    public HorizontalAlign getHorizontalAlign() {
        HorizontalAlignGridColumnProperty property = getProperties().get(HorizontalAlignGridColumnProperty.class);
        if ( property != null && property.getAlign() != null ) {
            return property.getAlign();
        }

        if ( getValueType() instanceof ValueTypeNumber ) {
            return HorizontalAlign.RIGHT;
        } else if ( getValueType() instanceof ValueTypeDate) {
            return HorizontalAlign.CENTER;
        } else if ( getValueType() instanceof ValueTypeDateTime) {
            return HorizontalAlign.CENTER;
        } else if ( getValueType() instanceof ValueTypeBoolean) {
            return HorizontalAlign.CENTER;
        } else if ( getValueType() instanceof ValueTypeEnumeration) {
            return HorizontalAlign.CENTER;
        }

        return HorizontalAlign.LEFT;
    }

    public String getToStringJavaVariableName() {
        return getJavaClassName() + "ToString";
    }

    public String getGetMethodName() {
        return "get" + StringUtils.toUpperFirstChar(getJavaClassName());
    }

    public String getSetMethodName() {
        return "set" + StringUtils.toUpperFirstChar(getJavaClassName());
    }

    public String getGetToStringMethodName() {
        return getGetMethodName() + "ToString";
    }

    public String getSetToStringMethodName() {
        return getSetMethodName() + "ToString";
    }

    public AbstractValueType getValueType() {
        throw new CoreException("Method ?.getValueType not implemented", this.getClass().getName());
    }

    public boolean isTypeFieldColumn() {
        throw new CoreException("Method ?.isTypeFieldColumn not implemented", this.getClass().getName());
    }
    public TypeField getTypeField() {
        throw new CoreException("Method ?.getTypeField not implemented", this.getClass().getName());
    }

    public Type getType() {
        return getTypeField() != null ? getTypeField().getType() : null;
    }

    public boolean isLocalColumn() {
        throw new CoreException("Method ?.isLocalColumn not implemented", this.getClass().getName());
    }

}
