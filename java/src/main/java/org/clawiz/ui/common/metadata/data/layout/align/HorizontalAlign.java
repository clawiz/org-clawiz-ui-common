package org.clawiz.ui.common.metadata.data.layout.align;

import org.clawiz.core.common.CoreException;

public enum HorizontalAlign {

    LEFT, RIGHT, CENTER;

    
    public static HorizontalAlign toHorizontalAlign(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return HorizontalAlign.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong HorizontalAlign value '?'", value);
        }
        
    }
    
    public static String toDescription(HorizontalAlign value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case LEFT  : return "left";
            case RIGHT : return "right";
            case CENTER: return "center";
        }
        
        return value.toString();
    }
}
