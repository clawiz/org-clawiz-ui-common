/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.server.java.datasource.api.component.element.datasource;

import org.clawiz.core.common.metadata.data.type.field.TypeField;

public class DataSourceDataSourceSaveChangeMethodElement extends AbstractDataSourceDataSourceJavaClassMethodElement {

    @Override
    public void process() {
        super.process();

        setName("saveChange");

        addParameter(getRowModelClassName() + "", "row");

        addText("");
        addText("DataSourceActionType actionType = DataSourceActionType.toAction(row.getAction());");
        addText("");
//        addText("if ( ( actionType == DataSourceActionType.CHANGE || actionType == DataSourceActionType.DELETE ) && row.getId() == null ) {");
//        addText("    throwException(\"Remote node @ guid ? not found\", getSession().getRemoteNodeId(), row.getId());");
//        addText("}");
        addText("");
        addText("" + getDataSource().getType().getJavaClassName() + "Object object = null;");
        addText("//noinspection Duplicates");
        addText("if ( actionType == DataSourceActionType.NEW ) {");
        addText("    object = " + getServiceVariableName() + ".create();");
        addText("} else if ( actionType == DataSourceActionType.CHANGE ) {");
        addText("    object = " + getServiceVariableName() + ".load(row.getId());");
        addText("} else if ( actionType == DataSourceActionType.DELETE ) {");
        addText("    deleteNodeGuid(row.getRemoteNodeGuid());");
        addText("    " + getServiceVariableName() + ".delete(row.getId());");
        addText("    return;");
        addText("}");
        addText("");
        for (TypeField field : getDataSource().getType().getFields() ) {

            addText("object." + field.getSetMethodName() + "(row." + field.getGetMethodName() + "());");

        }

        addText("");
        addText("object.save();");
        addText("");
        addText("if ( row.getRemoteNodeGuid() != null ) {");
        addText("    saveNodeGuid(object.getId(), row.getRemoteNodeGuid());");
        addText("}");

    }
}
