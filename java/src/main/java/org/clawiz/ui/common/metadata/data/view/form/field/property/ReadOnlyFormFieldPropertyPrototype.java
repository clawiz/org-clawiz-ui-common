package org.clawiz.ui.common.metadata.data.view.form.field.property;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ReadOnlyFormFieldPropertyPrototype extends org.clawiz.ui.common.metadata.data.view.form.field.property.AbstractFormFieldProperty {
    
    public ReadOnlyFormFieldProperty withName(String value) {
        setName(value);
        return (ReadOnlyFormFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
