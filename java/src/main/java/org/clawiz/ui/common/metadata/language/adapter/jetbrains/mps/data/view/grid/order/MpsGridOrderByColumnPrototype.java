package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsGridOrderByColumnPrototype extends AbstractMpsNode {
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByDirection direction;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn column;
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByDirection getDirection() {
        return this.direction;
    }
    
    public void setDirection(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.direction = null;
        }
        this.direction = org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByDirection.toMpsGridOrderByDirection(value);
    }
    
    public org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn getColumn() {
        return this.column;
    }
    
    public void setColumn(org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.column.MpsAbstractGridColumn value) {
        this.column = value;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "5185462771582755039";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.GridOrderByColumn";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "5185462771582755039", "org.clawiz.ui.common.language.structure.GridOrderByColumn", ConceptPropertyType.PROPERTY, "5185462771582765632", "direction"));
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "5185462771582755039", "org.clawiz.ui.common.language.structure.GridOrderByColumn", ConceptPropertyType.REFERENCE, "5185462771582755040", "column"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("5185462771582755039", "direction", org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.view.grid.order.MpsGridOrderByDirection.toConceptNodePropertyString(getDirection()));
        addConceptNodeRef("5185462771582755039", "column", getColumn());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn) node;
        
        structure.setDirection(( getDirection() != null ? org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByDirection.valueOf(getDirection().toString()) : null ));
        
        if ( getColumn() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "column", false, getColumn());
        } else {
            structure.setColumn(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn structure = (org.clawiz.ui.common.metadata.data.view.grid.order.GridOrderByColumn) node;
        
        setDirection(structure.getDirection() != null ? structure.getDirection().toString() : null);
        
        if ( structure.getColumn() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "column", false, structure.getColumn());
        } else {
            setColumn(null);
        }
        
    }
}
