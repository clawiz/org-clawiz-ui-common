/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.common.generator.client.ts.datasource.component;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.datasource.AbstractTypeScriptClientDataSourceClassComponent;
import org.clawiz.ui.common.generator.client.ts.datasource.component.element.DataModelInitMethodElement;

public class DataModelPrototypeComponent extends AbstractTypeScriptClientDataSourceClassComponent {


    protected void addFields() {

        for (TypeField field :  getDataSource().getType().getFields()) {
            addField(field.getJavaVariableName(), getImportMapName(TypeScriptClientCommonGeneratorContext.CLASS_NAME_DATA_MODEL_FIELD));
        }
    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(DataModelComponent.class).getName() + "Prototype");


        setExtends(getImportMapName(TypeScriptClientCommonGeneratorContext.CLASS_NAME_ABSTRACT_DATA_MODEL_IMPLEMENTATION));

        addImport(TypeScriptClientCommonGeneratorContext.CLASS_NAME_DATA_MODEL_FIELD);
        addImport(TypeScriptClientCommonGeneratorContext.CLASS_NAME_ABSTRACT_DATA_MODEL_FIELD_IMPLEMENTATION);

        addFields();

        addElement(DataModelInitMethodElement.class);



    }

}
