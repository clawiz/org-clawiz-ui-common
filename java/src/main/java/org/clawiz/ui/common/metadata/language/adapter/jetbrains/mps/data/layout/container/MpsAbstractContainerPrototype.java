package org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.container;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractContainerPrototype extends org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent {
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> components = new ArrayList<>();
    
    public ArrayList<org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent> getComponents() {
        return this.components;
    }
    
    public String getLanguageId() {
        return "6f8d612f-ee4d-4251-9265-15f00fa10ead";
    }
    
    public String getLanguageName() {
        return "org.clawiz.ui.common.language";
    }
    
    public String getLanguageConceptId() {
        return "6586513253200665075";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.ui.common.language.structure.AbstractContainer";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6f8d612f-ee4d-4251-9265-15f00fa10ead", "org.clawiz.ui.common.language", "6586513253200665075", "org.clawiz.ui.common.language.structure.AbstractContainer", ConceptPropertyType.CHILD, "6586513253200665134", "components"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getComponents() ) {
            addConceptNodeChild("6586513253200665075", "components", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer structure = (org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer) node;
        
        structure.getComponents().clear();
        for (org.clawiz.ui.common.metadata.language.adapter.jetbrains.mps.data.layout.MpsAbstractLayoutComponent mpsNode : getComponents() ) {
            if ( mpsNode != null ) {
                structure.getComponents().add((org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent) mpsNode.toMetadataNode(structure, "components"));
            } else {
                structure.getComponents().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer structure = (org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer) node;
        
        getComponents().clear();
        for (org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent metadataNode : structure.getComponents() ) {
            if ( metadataNode != null ) {
                getComponents().add(loadChildMetadataNode(metadataNode));
            } else {
                getComponents().add(null);
            }
        }
        
    }
}
